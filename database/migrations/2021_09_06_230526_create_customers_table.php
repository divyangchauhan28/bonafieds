<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->string('customer_id')->unique();
            $table->string('salutation',15);
            $table->string('firstname',30);
            $table->string('lastname',30);
            $table->date('birth_date');
            $table->string('gender',15);
            $table->string('marital_status',30);
            $table->string('nationality',30);
            $table->string('street',100);
            $table->string('postal_code',15);
            $table->string('city',50);
            $table->string('country',100);
            $table->string('telephone',15);
            $table->string('mobile',15);
            $table->string('email',74);
            $table->bigInteger('parent_id')->default(0);
            $table->boolean('is_active')->default(1)->nullable();
            $table->boolean('is_deleted')->default(0)->nullable();
            $table->bigInteger('created_by')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}

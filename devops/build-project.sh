#!/bin/bash

composer install --no-interaction
npm install

ln -f -s .env.development .env

php artisan key:generate
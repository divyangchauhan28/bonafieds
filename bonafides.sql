CREATE DATABASE  IF NOT EXISTS `srcdb` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `srcdb`;
-- MySQL dump 10.13  Distrib 5.7.34, for osx10.12 (x86_64)
--
-- Host: poojacreators.cz2ew78fao4z.us-west-1.rds.amazonaws.com    Database: srcdb
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED='';

--
-- Table structure for table `chat_communication`
--

DROP TABLE IF EXISTS `chat_communication`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chat_communication` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `chat_id` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `communication` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `attachment` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `asked_by` bigint DEFAULT NULL,
  `communication_date` datetime DEFAULT NULL,
  `is_reply` tinyint(1) DEFAULT '0',
  `is_viewed` tinyint(1) DEFAULT '0',
  `replied_by` bigint DEFAULT NULL,
  `parent_id` bigint DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chat_communication`
--

LOCK TABLES `chat_communication` WRITE;
/*!40000 ALTER TABLE `chat_communication` DISABLE KEYS */;
INSERT INTO `chat_communication` VALUES (1,'3n3m4mnbnm2b4nm3bn42mn3b','Hello',NULL,1,'2021-12-01 16:24:52',0,1,2,0),(2,'7b5ab72fcab2128198ee09a75c57ed20','hello',NULL,1,'2021-12-01 17:04:11',0,1,2,0),(3,'e0949d6c16cce1890e2943b4e5d06535','How are you',NULL,1,'2021-12-01 17:05:42',0,1,2,0),(4,'aa4c02bc4d97beaa23e0c8ebff84de3a','need some questions can you please respond me',NULL,2,'2021-12-01 17:19:20',0,1,1,0),(5,'7cb496da313984f1ee7a9b5999f62828','by when i can answer',NULL,2,'2021-12-01 17:21:24',0,1,1,0),(6,'881ddad110c05bf738f581f4ecaaa394','can i get the answer',NULL,2,'2021-12-01 17:27:12',0,1,1,0),(7,'0a1f39d3ece45d8b0bf83ca60cf48002','Hallo Freddins alles klar',NULL,1,'2021-12-04 15:02:35',0,1,16,0),(8,'1ee122eafa964147b6f8a7c43e7fc7c8','ja mir geht es gut danke für die nachfrage',NULL,16,'2021-12-04 15:03:06',0,1,1,0),(9,'f3dafc5c0a39381a71f49dac98c081f3','hi Raj',NULL,16,'2021-12-04 21:19:20',0,1,1,0),(10,'f05c54c61e4b2e4f47a3b0bce4cd3a85','hello',NULL,1,'2021-12-04 21:19:26',0,1,16,0),(11,'ed6ec2a165294d88b60bd0ec57bd1419','HOll Zwetschgeee',NULL,1,'2021-12-07 21:39:17',0,1,16,0),(12,'1ee94f65116fce68bcd31d1469bd683a','Mach Fenster Zue',NULL,1,'2021-12-07 21:40:02',0,1,16,0),(13,'aefa1c9906c009b2fb69b4abc0908860','hahahahaha du bish en zwetschge',NULL,16,'2021-12-07 22:13:47',0,1,1,0),(14,'9a91a416059ce584914c8144c6863e94','Hoi Freddins Han grad Frau Mennu Ufeglade und weis ned wie ich ihri Dokumente chan hinzuefüege..\nIch bruche dini hilf will ich ez mit em CRm nüm druss chume',NULL,1,'2021-12-15 15:07:05',0,1,16,0),(15,'6e55d0f7cdb2d1f49f44f64a72791401','okei ich wird das luege',NULL,16,'2021-12-15 19:56:45',0,1,1,0),(16,'34b70a5bd6b504bcf6eab91ed543ba9c','Hallo Freddins \nBeim Hochladen der Datei springt es immer noch zurück\nund es nimmt nicht mehr als 6 Dokumente auf danach wider fehlermeldung.',NULL,1,'2021-12-16 20:32:43',0,1,16,0),(17,'3ec74506e9bca63d586f305546f9f32a','Guten Tag Herr Mordeku \n\nHaben Sie alle Zahlungen überprüft, von den Kunden für LLA.\n\nGruss \nAdmin',NULL,1,'2021-12-29 16:26:41',0,1,40,0);
/*!40000 ALTER TABLE `chat_communication` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commissions`
--

DROP TABLE IF EXISTS `commissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commissions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `commission_id` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `minimum` int DEFAULT NULL,
  `target_from` int NOT NULL,
  `target_to` int NOT NULL,
  `approach` int NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `is_deleted` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `commissions_commission_id_unique` (`commission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commissions`
--

LOCK TABLES `commissions` WRITE;
/*!40000 ALTER TABLE `commissions` DISABLE KEYS */;
INSERT INTO `commissions` VALUES (1,'ceaac810bd16040a35a0eb284d07e757','Starter',0,0,20000,25,1,0,'2021-09-21 00:13:56','2021-09-21 00:13:56'),(2,'f4413071de6bf85dae5aae818258adbb','Junior Berater',20000,0,150000,28,1,0,'2021-09-21 00:14:56','2021-09-21 00:14:56'),(3,'6001f3daa0fa40b88d5e07e59cfd2979','Senior Berater',150000,0,250000,35,1,0,'2021-09-26 11:40:19','2021-10-12 19:30:10'),(4,'cd7b377e5205c8a6003eee775a0375aa','Test',NULL,1000,5000,35,1,1,'2021-10-21 11:36:23','2021-10-21 11:36:32'),(5,'a8b34c38ffd7ac0cc0b7ee0ccd701bf7','Overhead Junior Teamleiter',10000000,0,100000000,5,1,0,'2021-11-02 14:42:09','2021-11-02 17:47:59'),(6,'dd32c90908122d054e626c1b84a01d9b','Overhead Senior Teamleiter',10000000,0,100000000,8,1,0,'2021-11-02 14:42:51','2021-11-02 17:48:05'),(7,'a0638ee4ca9efaeabcf296469bfedb24','Overhead Junior Partner',10000000,0,100000000,11,1,0,'2021-11-02 14:43:16','2021-11-02 17:48:12'),(8,'1783bfbe363f3246347f74858438e8be','Junior Teamleiter',250000,0,1000000,43,1,0,'2021-11-02 14:49:15','2021-11-02 14:49:15'),(9,'1479ea5b34feb3f17482d8eddd0f4b84','Senior Teamleiter',250000,0,1000000,48,1,0,'2021-11-02 14:49:43','2021-11-02 14:49:43'),(10,'b9adc8784db7f138b41e1feabc167660','Junior Partner',250000,0,1000000,55,1,0,'2021-11-02 14:49:59','2021-11-02 14:49:59'),(11,'9b71190d0de4a15cdae02080b2e6bd45','Experte',1000000,0,1000000,43,1,0,'2021-11-08 15:29:58','2021-12-10 14:21:10'),(12,'9877611cf457e96060d5a1c80899d4b7','Finanzberater I',1000000,0,1000000,48,1,0,'2021-11-08 15:30:38','2021-12-10 14:21:48'),(13,'bb565d9cf45a7304c475ae991765f0af','Finanzberater II',1000000,0,1000000,55,1,0,'2021-11-08 15:31:27','2021-12-10 14:21:59'),(14,'58b6f582cdb5bd215623c537d3272d46','Storno',10000000,0,100000000,10,1,0,'2021-11-16 16:52:44','2021-11-16 16:52:44');
/*!40000 ALTER TABLE `commissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `communication`
--

DROP TABLE IF EXISTS `communication`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `communication` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `com_id` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sender_id` bigint DEFAULT NULL,
  `message` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attachment` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `communication_date` datetime DEFAULT NULL,
  `is_replied` tinyint(1) DEFAULT '0',
  `parent_id` bigint DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `communication`
--

LOCK TABLES `communication` WRITE;
/*!40000 ALTER TABLE `communication` DISABLE KEYS */;
INSERT INTO `communication` VALUES (3,'4cd2b16c4ac0711945eb0aae4c03f28e',1,'testing communication','uploads/2021/11/e8a3ef0f99b4cd0455bff30f15b27712.png','2021-11-10 08:08:51',0,0,0),(4,'7d4ae51022f3bef8368e6c5677722f9d',1,'Hi Nachbearbeitung Jossamma','uploads/2021/11/a4ccf3f9dc5d6cacdb325839de49d39d.pdf','2021-11-11 14:21:56',0,0,0),(5,'f4f156220778b4f4414a250a0192a267',1,'hi you have an appointment tommorow at 3 pm','','2021-11-17 00:07:15',0,0,0),(6,'e79c44dbfa4f03680bb7f7dc3beb5285',1,'Testing Communication 2','','2021-11-18 22:28:50',0,0,0),(7,'c4a3d5322115f8a944027f23f09c5015',1,'Hallo \r\n\r\nHeute Banking Schulung','','2021-11-22 13:52:18',0,0,0),(8,'3e2383ee072a515eed041d1d1257ae8f',1,'Kein Link einf. bei Meetings\r\nwen Lik eingefügt ist (muss zb. Zoomeinladung) dann aber auch email sichtbar machen.\r\nWen link angeklickt wird das ist es nicht auffindbar.','','2021-11-23 16:17:18',0,0,0),(9,'cbe71d1ea2cae935ad36e6b421590cdc',1,'Hallo Prince Auf deiner Lohnabrechnung von November wurde nur Herr Kravchyk Konstantyn LLA abgerechnet das ist auch die einzige Provision die wir erhalten haben minus die hälfte des VBV.','','2021-11-29 15:45:36',0,0,0),(10,'1d750ff037f50f6bdb61c8e9a4115c25',1,'hi','','2021-12-04 21:29:09',0,0,0);
/*!40000 ALTER TABLE `communication` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `communication_employee`
--

DROP TABLE IF EXISTS `communication_employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `communication_employee` (
  `communication_id` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `employee_id` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_viewed` tinyint(1) DEFAULT '0',
  `viewed_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `communication_employee`
--

LOCK TABLES `communication_employee` WRITE;
/*!40000 ALTER TABLE `communication_employee` DISABLE KEYS */;
INSERT INTO `communication_employee` VALUES ('4cd2b16c4ac0711945eb0aae4c03f28e','c0d3294ac90c3b40e9cc3376c02ec023',1,'2021-11-19 00:11:23'),('7d4ae51022f3bef8368e6c5677722f9d','c0d3294ac90c3b40e9cc3376c02ec023',1,'2021-11-19 00:11:25'),('f4f156220778b4f4414a250a0192a267','c0d3294ac90c3b40e9cc3376c02ec023',1,'2021-11-19 00:11:29'),('e79c44dbfa4f03680bb7f7dc3beb5285','3f2bcdb1bc6b721efec844a6534c0d09',1,'2021-11-18 22:36:49'),('c4a3d5322115f8a944027f23f09c5015','c0d3294ac90c3b40e9cc3376c02ec023',1,'2021-11-22 13:52:42'),('3e2383ee072a515eed041d1d1257ae8f','c0d3294ac90c3b40e9cc3376c02ec023',1,'2021-11-23 18:24:19'),('cbe71d1ea2cae935ad36e6b421590cdc','040515d719fc42f9e433c15c17e72acb',0,NULL),('1d750ff037f50f6bdb61c8e9a4115c25','c0d3294ac90c3b40e9cc3376c02ec023',1,'2021-12-04 21:31:37');
/*!40000 ALTER TABLE `communication_employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_commission`
--

DROP TABLE IF EXISTS `customer_commission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_commission` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `cc_id` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `policy_id` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `commission_id` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `policy_amount` decimal(10,2) DEFAULT NULL,
  `commission_factor` int DEFAULT NULL,
  `sales_point` int DEFAULT NULL,
  `employee_id` bigint DEFAULT NULL,
  `employee_commission` decimal(10,2) DEFAULT NULL,
  `commission_status` tinyint(1) DEFAULT '2',
  `commission_type` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'Credit',
  `processed_on` datetime DEFAULT NULL,
  `updated_by` bigint DEFAULT NULL,
  `updated_on` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_commission`
--

LOCK TABLES `customer_commission` WRITE;
/*!40000 ALTER TABLE `customer_commission` DISABLE KEYS */;
INSERT INTO `customer_commission` VALUES (5,'d783b9466af5a5fc2374d548863cf13f','fb0b5f665d0ffbfd92e83d97c9bd4bdd','906fc709cff591fe1abb4cee41012d8b','ceaac810bd16040a35a0eb284d07e757',96000.00,25,3840,2,975.00,1,'Credit',NULL,8,'2021-10-15 00:14:56'),(6,'817eaa3dc46e588361fb2a944b0653a8','b9c16e6870f6b44bb69dd1022c01c520','8c9f3ea3752a7a5d22285c6a5546ef6f','ceaac810bd16040a35a0eb284d07e757',600.00,25,24,2,25.00,1,'Credit',NULL,1,'2021-10-15 00:14:56'),(7,'70062e2aa5420917a9a5f22018622b8f','afa076552d2ffb086a7f29d940d1a28a','b9b742914ff2d9043efd1cca3b77854d','ceaac810bd16040a35a0eb284d07e757',192000.00,25,7680,2,1925.00,2,'Credit',NULL,8,'2021-10-15 00:14:56'),(8,'cc2e8a9ffdc8bb3178e1ac7d17cc1abe','9939e2820ce3184fc2ffffd8e28dd035','5b4a6a2741f60665a9dafea11ec140e5','ceaac810bd16040a35a0eb284d07e757',216000.00,25,8640,2,2175.00,1,'Credit',NULL,1,'2021-10-15 00:14:56'),(9,'21de54e4cf6751c6753ce9dfd8095515','acb9ce2de214985fe376797fbfa8e4e3','839aec66e87cadc433bfd03b8a1e3593','ceaac810bd16040a35a0eb284d07e757',3303840.00,25,132154,2,33050.00,1,'Credit',NULL,8,'2021-10-15 00:14:56'),(10,'22d36b873c9ebba1b31d6386c4eb481e','9939e2820ce3184fc2ffffd8e28dd035','2dd6c07156c3ab111829f435a5f26b94','ceaac810bd16040a35a0eb284d07e757',144000.00,25,5760,2,1450.00,1,'Credit',NULL,8,'2021-10-15 00:14:56'),(11,'c85690d9c55cf08c71713acd54f838ac','733ebb1175a49aaafad730217cb840e3','3a7f0c7bb86c65bddba83d6f9b91ce33','ceaac810bd16040a35a0eb284d07e757',90000.00,25,3600,2,900.00,1,'Credit',NULL,1,'2021-10-15 00:14:56'),(12,'d03526ba0682414e46e67d1c23b24207','ad928eca1d82e603b7e9e95d81f71db6','c774de99e1519844ef7feec05fdd1be1','ceaac810bd16040a35a0eb284d07e757',1830000.00,25,73200,2,18300.00,1,'Credit','2021-10-19 15:10:13',1,'2021-10-19 15:10:13'),(13,'c9eaacc2ee63854505a9ad234d85581f','15e5f5b0ab908882a1f3396796e73cea','c8af747606d90ae3f74b3632687ac052','ceaac810bd16040a35a0eb284d07e757',1368000.00,25,54720,27,13700.00,1,'Credit','2021-10-22 15:26:23',1,'2021-10-22 15:26:23'),(14,'f902c29443c443b95f07dae75673ac04','6c85d2b1705111420aa52369a4b92e21','96b60593a2369f8d0f687b95d18fc6ca','6001f3daa0fa40b88d5e07e59cfd2979',88800.00,35,3552,16,1260.00,1,'Credit','2021-11-01 19:09:28',1,'2021-11-01 19:09:28'),(15,'1fc63adc06599e85593f81121e277702','d9ff3cc80480256abc53f5253fc1d5f3','7f344915637e93c427a3acf70638b34f',NULL,3303840.00,NULL,132154,1,0.00,1,'Credit','2021-11-02 16:41:38',1,'2021-11-02 16:41:38'),(16,'3e01b2007c4a866c3e07cbed939fcc7f','1be171bd013afc50dce0b3c2d9f9fedd','38fe5d4328a9119f46c9a9f6520ddacc','1479ea5b34feb3f17482d8eddd0f4b84',144000.00,48,5760,16,2784.00,1,'Credit','2021-11-02 17:54:59',1,'2021-11-02 17:54:59'),(17,'4ed656411e33644becb3c7431eb5881c','d9ff3cc80480256abc53f5253fc1d5f3','2f0c502f257ce342109b647af290d1fc',NULL,216000.00,NULL,8640,1,0.00,1,'Credit','2021-11-02 17:57:01',1,'2021-11-02 17:57:01'),(18,'5998dfd93a8f106d06c8c9903dac588f','0a4d19a6bf05c2b0cc111b58d1bc0391','429a18052247bdb163f120cbb587b62d',NULL,60000.00,NULL,2400,1,0.00,1,'Credit','2021-11-11 14:29:15',1,'2021-11-11 14:29:15'),(19,'0a090182e0d40b83e27b773a8aa3d6c5','76b1c17c069b35adb8fd05b8653d717f','746fe7f43c6de4ba412227f3d2913fe3','1783bfbe363f3246347f74858438e8be',69960.00,43,2798,43,1204.00,1,'Credit','2021-12-04 21:24:26',1,'2021-12-04 21:24:26'),(20,'4eaf1ba74f338b060efe551de406acfe','46c23acdc3b1a878c0c5a93f7124e329','db9d5974978ddbd5d0e5e2fc120001be','1479ea5b34feb3f17482d8eddd0f4b84',187200.00,48,7488,16,3595.00,1,'Credit','2021-12-10 14:26:14',1,'2021-12-10 14:26:14'),(21,'b3c6bf92425e76c968a71b8c51f6ed5e','e243fa0398bbc8cade97ee47705579d3','7a63b2b456626c8ae6b301c095fd8c6e',NULL,75600.00,NULL,3024,1,0.00,1,'Credit','2021-12-13 23:40:09',1,'2021-12-13 23:40:09'),(22,'ba57cca1081f1edc9d522576be1142b1','46c23acdc3b1a878c0c5a93f7124e329','739a472003a7d383db7c468437c1ecff','1479ea5b34feb3f17482d8eddd0f4b84',120000.00,48,4800,16,2304.00,1,'Credit','2021-12-13 17:57:26',1,'2021-12-13 17:57:26'),(23,'493e0900d60b79793864837c800c8669','d8fcfe00187c481ae65cd26e96406a8d','6301ebca211419aba57b52d611ad68bc','1479ea5b34feb3f17482d8eddd0f4b84',93600.00,48,3744,16,1798.00,1,'Credit','2021-12-16 20:20:16',1,'2021-12-16 20:20:16');
/*!40000 ALTER TABLE `customer_commission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_insurance`
--

DROP TABLE IF EXISTS `customer_insurance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_insurance` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `ci_id` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provider_id` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_id` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `insurance_amount` decimal(10,2) DEFAULT NULL,
  `insurance_duration` int DEFAULT NULL,
  `installment_mode` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `insurance_status` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `employee_id` bigint DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `is_dead` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  `is_approved` tinyint(1) DEFAULT '0',
  `updated_by` bigint DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_insurance`
--

LOCK TABLES `customer_insurance` WRITE;
/*!40000 ALTER TABLE `customer_insurance` DISABLE KEYS */;
INSERT INTO `customer_insurance` VALUES (1,'906fc709cff591fe1abb4cee41012d8b','fb0b5f665d0ffbfd92e83d97c9bd4bdd','abed6856143b63959da816ce8bda2b92','291f0a6f79cf308ff5a7ea16faeab662',200.00,40,'Fortnightly','Approve',NULL,NULL,2,0,1,0,1,1,'2021-10-10 14:23:50'),(2,'8c9f3ea3752a7a5d22285c6a5546ef6f','b9c16e6870f6b44bb69dd1022c01c520','abed6856143b63959da816ce8bda2b92','291f0a6f79cf308ff5a7ea16faeab662',50.00,1,'Monthly','Approve',NULL,NULL,2,0,0,0,1,1,'2021-10-10 14:27:39'),(3,'839aec66e87cadc433bfd03b8a1e3593','acb9ce2de214985fe376797fbfa8e4e3','abed6856143b63959da816ce8bda2b92','291f0a6f79cf308ff5a7ea16faeab662',6883.00,40,'Yearly','Approve',NULL,NULL,2,1,0,0,1,1,'2021-10-15 19:53:46'),(4,'f558b4cd65592ff3e7d427c1dc882a75','9939e2820ce3184fc2ffffd8e28dd035','79d154488772b77d72d433a1002c81c0','0',NULL,NULL,'Daily','In Progress',NULL,NULL,2,1,0,1,0,NULL,NULL),(5,'2dd6c07156c3ab111829f435a5f26b94','9939e2820ce3184fc2ffffd8e28dd035','79d154488772b77d72d433a1002c81c0','291f0a6f79cf308ff5a7ea16faeab662',300.00,40,'Monthly','Approve',NULL,NULL,2,0,0,0,1,1,'2021-10-15 19:54:28'),(6,'5b4a6a2741f60665a9dafea11ec140e5','9939e2820ce3184fc2ffffd8e28dd035','ad48a67d7a23d304eed1c85a1b0e91b9','291f0a6f79cf308ff5a7ea16faeab662',450.00,40,'Monthly','Approve',NULL,NULL,2,1,0,0,1,1,'2021-10-12 19:32:51'),(7,'b9b742914ff2d9043efd1cca3b77854d','afa076552d2ffb086a7f29d940d1a28a','79d154488772b77d72d433a1002c81c0','291f0a6f79cf308ff5a7ea16faeab662',400.00,40,'Monthly','Approve',NULL,NULL,2,1,1,0,1,1,'2021-10-12 19:32:44'),(8,'3a7f0c7bb86c65bddba83d6f9b91ce33','733ebb1175a49aaafad730217cb840e3','ad48a67d7a23d304eed1c85a1b0e91b9','291f0a6f79cf308ff5a7ea16faeab662',300.00,25,'Monthly','Approve',NULL,NULL,2,1,0,0,1,1,'2021-10-15 19:54:31'),(9,'c774de99e1519844ef7feec05fdd1be1','ad928eca1d82e603b7e9e95d81f71db6','ad48a67d7a23d304eed1c85a1b0e91b9','291f0a6f79cf308ff5a7ea16faeab662',6100.00,25,'Monthly','Approve',NULL,NULL,2,1,0,0,1,1,'2021-10-19 14:59:50'),(10,'c8af747606d90ae3f74b3632687ac052','15e5f5b0ab908882a1f3396796e73cea','ad48a67d7a23d304eed1c85a1b0e91b9','291f0a6f79cf308ff5a7ea16faeab662',3800.00,30,'Yearly','Approve',NULL,NULL,27,1,0,0,1,1,'2021-10-22 15:26:08'),(11,'96b60593a2369f8d0f687b95d18fc6ca','6c85d2b1705111420aa52369a4b92e21','79d154488772b77d72d433a1002c81c0','291f0a6f79cf308ff5a7ea16faeab662',200.00,37,'Monthly','Approve',NULL,NULL,16,0,1,0,1,1,'2021-11-01 19:08:40'),(12,'7f344915637e93c427a3acf70638b34f','d9ff3cc80480256abc53f5253fc1d5f3','79d154488772b77d72d433a1002c81c0','291f0a6f79cf308ff5a7ea16faeab662',6883.00,40,'Yearly','Approve',NULL,NULL,1,1,0,1,1,1,'2021-11-02 16:41:18'),(13,'38fe5d4328a9119f46c9a9f6520ddacc','1be171bd013afc50dce0b3c2d9f9fedd','79d154488772b77d72d433a1002c81c0','291f0a6f79cf308ff5a7ea16faeab662',300.00,40,'Monthly','Approve',NULL,NULL,16,1,1,0,1,1,'2021-11-02 17:54:53'),(14,'2f0c502f257ce342109b647af290d1fc','d9ff3cc80480256abc53f5253fc1d5f3','79d154488772b77d72d433a1002c81c0','291f0a6f79cf308ff5a7ea16faeab662',450.00,40,'Monthly','Approve',NULL,NULL,1,1,0,0,1,1,'2021-11-02 17:56:56'),(15,'429a18052247bdb163f120cbb587b62d','0a4d19a6bf05c2b0cc111b58d1bc0391','79d154488772b77d72d433a1002c81c0','291f0a6f79cf308ff5a7ea16faeab662',500.00,10,'Monthly','Approve',NULL,NULL,1,1,0,0,1,1,'2021-11-11 14:26:01'),(16,'7a63b2b456626c8ae6b301c095fd8c6e','e243fa0398bbc8cade97ee47705579d3','79d154488772b77d72d433a1002c81c0','291f0a6f79cf308ff5a7ea16faeab662',450.00,14,'Monthly','Approve',NULL,NULL,1,1,0,0,1,1,'2021-12-13 17:55:55'),(17,'8249c2ec11bcef17599474e5e8de11ee','15e5f5b0ab908882a1f3396796e73cea','79d154488772b77d72d433a1002c81c0','291f0a6f79cf308ff5a7ea16faeab662',300.00,20,'Monthly','In Progress',NULL,NULL,1,1,0,0,0,NULL,NULL),(18,'abf17ecd323f82fb5f0481104964ac1b','b6e340bbca7ea09dd281997dfebc31db','79d154488772b77d72d433a1002c81c0','291f0a6f79cf308ff5a7ea16faeab662',150.00,10,'Monthly','In Progress',NULL,NULL,1,1,0,0,0,NULL,NULL),(19,'fca4b3c7d5ba4566efab2f5d56b97d8a','e243fa0398bbc8cade97ee47705579d3',NULL,NULL,NULL,NULL,'Daily','In Progress',NULL,NULL,0,1,0,1,0,NULL,NULL),(20,'746fe7f43c6de4ba412227f3d2913fe3','76b1c17c069b35adb8fd05b8653d717f','79d154488772b77d72d433a1002c81c0','291f0a6f79cf308ff5a7ea16faeab662',110.00,53,'Monthly','Approve',NULL,NULL,43,1,0,0,1,1,'2021-11-29 14:23:38'),(21,'4ad62d6817180f557dfe71402d00ffae','afa076552d2ffb086a7f29d940d1a28a','79d154488772b77d72d433a1002c81c0','291f0a6f79cf308ff5a7ea16faeab662',5000.00,10,'Daily','In Progress',NULL,NULL,2,1,0,0,0,NULL,NULL),(22,'da1e855dd645bfd8ca6535aa7dced0d4','afa076552d2ffb086a7f29d940d1a28a','ad48a67d7a23d304eed1c85a1b0e91b9','291f0a6f79cf308ff5a7ea16faeab662',1000.00,1,'Daily','In Progress',NULL,NULL,2,1,0,0,0,NULL,NULL),(23,'db9d5974978ddbd5d0e5e2fc120001be','46c23acdc3b1a878c0c5a93f7124e329','79d154488772b77d72d433a1002c81c0','291f0a6f79cf308ff5a7ea16faeab662',400.00,39,'Monthly','Approve',NULL,NULL,16,1,1,0,1,1,'2021-12-10 14:25:55'),(24,'739a472003a7d383db7c468437c1ecff','46c23acdc3b1a878c0c5a93f7124e329','79d154488772b77d72d433a1002c81c0','291f0a6f79cf308ff5a7ea16faeab662',250.00,40,'Monthly','Approve',NULL,NULL,16,1,0,0,1,1,'2021-12-13 17:56:03'),(25,'6301ebca211419aba57b52d611ad68bc','d8fcfe00187c481ae65cd26e96406a8d','79d154488772b77d72d433a1002c81c0','291f0a6f79cf308ff5a7ea16faeab662',200.00,39,'Monthly','Approve',NULL,NULL,16,0,0,0,1,1,'2021-12-15 19:58:32'),(26,'56f75536e72cdf7b298ae21881cfbe4f','d8fcfe00187c481ae65cd26e96406a8d','79d154488772b77d72d433a1002c81c0','291f0a6f79cf308ff5a7ea16faeab662',200.00,39,'Monthly','In Progress',NULL,NULL,16,1,0,1,0,NULL,NULL),(27,'9b415898953169b96c3b559ce244efd2','18e02ac26eae389b043f6d4a1e1c338c','79d154488772b77d72d433a1002c81c0','291f0a6f79cf308ff5a7ea16faeab662',10000.00,6,'Yearly','In Progress',NULL,NULL,0,1,0,0,0,NULL,NULL),(28,'41b518e73067d75ac1add66b5d984e81','4e26a8818d77f5edd45568c5da3b5cea','79d154488772b77d72d433a1002c81c0','291f0a6f79cf308ff5a7ea16faeab662',250.00,35,'Monthly','In Progress',NULL,NULL,16,1,0,0,0,NULL,NULL),(29,'947aec9895ffa27d393d8fda6606d131','5d701f89774ecc3230a80cc088ea55e1','79d154488772b77d72d433a1002c81c0','291f0a6f79cf308ff5a7ea16faeab662',350.00,35,'Monthly','In Progress',NULL,NULL,0,1,0,0,0,NULL,NULL),(30,'452724a302ae14ccaef100d12ca8bb54','3d734736ec2e584e947f968637b5c541','79d154488772b77d72d433a1002c81c0','291f0a6f79cf308ff5a7ea16faeab662',NULL,NULL,'Monthly','In Progress',NULL,NULL,NULL,1,0,0,0,NULL,NULL);
/*!40000 ALTER TABLE `customer_insurance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `salutation` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `birth_date` date NOT NULL,
  `gender` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `marital_status` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nationality` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `postal_code` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(74) COLLATE utf8_unicode_ci NOT NULL,
  `category` text COLLATE utf8_unicode_ci,
  `family_member` int DEFAULT NULL,
  `parent_id` tinyint(1) DEFAULT '0',
  `employee_id` varchar(45) COLLATE utf8_unicode_ci DEFAULT '0',
  `is_active` tinyint(1) DEFAULT '1',
  `is_deleted` tinyint(1) DEFAULT '0',
  `created_by` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `customers_customer_id_unique` (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=140 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (1,'fb0b5f665d0ffbfd92e83d97c9bd4bdd','Herr','Jitendra','Prajapati','1986-06-24','Männlich','Unmarried','asd','sad','asd','asd','sads','1231231231','1231231231','jitendra@crm.com',NULL,NULL,0,'16',1,1,'1','2021-09-23 22:43:39','2021-11-12 13:51:04'),(2,'c4145d0640976403d69ce2e53f954d49','Herr','Raj','Rathod','2021-10-05','Männlich','Married','indian','abc','123123','acb','india','1231231231','1231231231','raj@poojacreators.com',NULL,NULL,0,'2',1,1,'1','2021-10-05 20:36:58','2021-11-12 13:51:07'),(3,'1be171bd013afc50dce0b3c2d9f9fedd','Herr','Rudra J','Prajapati','2011-10-20','Männlich','Unmarried','sdaf','sdaf','234234','sdaf','afsdf','1231231231','123123131','rudra@gmail.com',NULL,NULL,1,'2',1,0,'1','2021-10-06 23:20:19','2021-10-07 22:26:53'),(4,'b9c16e6870f6b44bb69dd1022c01c520','Herr','Diana','Davis','1998-03-01','Männlich','Married','Indian','Muscat','1234','Muscat','Oman','98756738746','7685749373','a@a.com',NULL,NULL,1,'2',1,0,'1','2021-10-08 20:04:54','2021-10-08 20:04:54'),(5,'9939e2820ce3184fc2ffffd8e28dd035','Herr','Freddins','Pulikoden','2011-10-20','Männlich','Verheiratet','Österreich','Lärchenstrasse 6H','8953','Dietikon','Schweiz','0764168401','0764168401','freddins_95@hotmail.com',NULL,NULL,0,'16',1,1,'1','2021-10-11 23:09:09','2021-11-12 13:51:12'),(6,'acb9ce2de214985fe376797fbfa8e4e3','Frau','Needhu','Pulikoden','1993-04-18','Weiblich','Verheiratet','-','Lärchenstrasse 6H','8953','Dietikon','Schweiz','-','-','freddins_95@hotmail.com',NULL,NULL,5,'2',1,0,'1','2021-10-11 23:10:28','2021-10-11 23:12:15'),(7,'afa076552d2ffb086a7f29d940d1a28a','Herr','Gian Carlos','Dominguez Abad','1999-06-17','Männlich','Ledig','Spanien','Gjuchstrasse','8953','Dietikon','Schweiz','0791715883','-','g.dominguez@bonafides.ch',NULL,NULL,0,'2',0,1,'1','2021-10-12 13:25:29','2021-12-07 21:24:57'),(8,'733ebb1175a49aaafad730217cb840e3','Frau','Diana','Carlos','2011-10-20','Weiblich','-','-','-','-','-','-','-','-','-',NULL,NULL,7,'2',0,0,'1','2021-10-12 13:26:18','2021-12-04 14:35:36'),(9,'ad928eca1d82e603b7e9e95d81f71db6','Herr','Raj','Rathod','1994-09-09','Männlich','Married','Indian','Muttrah','100','Muscat','Oman','7874983351','7874983351','raj.rr.rathod94@gmail.com',NULL,NULL,0,'2',1,1,'1','2021-10-19 14:55:01','2021-11-12 13:51:29'),(10,'9593d911780c6f9ac0fe7c56ea1a4307','Frau','Pooja','Rathod','1994-11-20','Weiblich','Married','indian','muttrah','100','muscat','oman','7874983351','7874983351','raj.rr.rathod94@gmail.com',NULL,NULL,9,'2',1,0,'1','2021-10-19 14:58:32','2021-10-19 14:58:32'),(11,'15e5f5b0ab908882a1f3396796e73cea','Herr','Akash','Ashar','1994-10-20','Männlich','Single','Indian','Muttrah','100','Mucat','Oman','7874983351','7874983351','akash@ashar.com',NULL,NULL,0,'2',0,1,'1','2021-10-19 15:02:56','2021-12-07 21:24:59'),(12,'18e02ac26eae389b043f6d4a1e1c338c','Herr','Edwin','Davis','1996-10-20','Männlich','Single','Indian','Darsait','100','Muscat','Oman','1234567891','1234567891','ed@de.com',NULL,NULL,0,'0',1,0,'27','2021-10-20 22:55:28','2021-10-20 22:56:56'),(13,'6c85d2b1705111420aa52369a4b92e21','Frau','Needhu','Pulikoden','1993-04-18','Weiblich','Verheiratet','Schweiz','Lärchenstrasse 6H','8953','Dietikon','Schweiz','0764168401','0764168401','nthottumkara@hotmail.com',NULL,NULL,0,'0',1,0,'16','2021-11-01 19:06:37','2021-11-01 19:06:37'),(14,'d9ff3cc80480256abc53f5253fc1d5f3','Herr','Shakira','Zihlmann','1998-02-16','Männlich','Ledig','Schweizerin','49 Bertastrasse','8003','Zürich','Schweiz','0789160094','97869786','shakira.zihlmann@hotmail.com',NULL,NULL,0,'16',1,1,'1','2021-11-02 16:38:18','2021-11-11 19:18:12'),(15,'ffbe9c1ff144e5429691a24d1698e54c','Herr','Christo','Cheruparambil','1996-11-27','Männlich','Ledig','Schweiz','Klingenbergstrasse','8976','Neftenbach','Winterthur','987654321','987654321','c.christo@hotmail.com',NULL,NULL,0,'16',1,1,'1','2021-11-09 15:58:57','2021-11-09 15:59:37'),(16,'0a4d19a6bf05c2b0cc111b58d1bc0391','Herr','Jossamma','Kalloor','1967-05-31','Männlich','Verheiratet','AT','Fliederweg 5','4612','Wangen bei Olten','Schweiz','0786442835','0786442835','jossammak@gmail.com',NULL,NULL,0,'16',0,1,'1','2021-11-09 16:01:59','2021-12-07 21:25:02'),(17,'e243fa0398bbc8cade97ee47705579d3','Herr','Johnson','Kalloor','1970-05-13','Männlich','Verheiratet','IN','Fliederweg 5','4612','Wangen bei Olten','Schweiz','0788761621','0788761621','33.kalloorjohnson@gmail.com',NULL,NULL,16,'0',1,0,'1','2021-11-09 16:04:36','2021-11-09 16:04:36'),(18,'575c8c80b31fc4439c2d542964f3d1cf','Frau','Selina','Schoch','1989-12-06','Weiblich','Ledig','CH','Alfred-Strebel-Weg 15','8047','Zürich','Schweiz','0765619069','0765619069','selina_schoch86@hotmail.com',NULL,NULL,0,'38',0,1,'1','2021-11-09 21:14:13','2021-12-07 21:25:04'),(19,'456d69848a267ecca83b8ed349167100','Frau','Melanie Doralina','Pina Moncion','1998-02-05','Weiblich','Ledig','CH','Hohlstrasse 187','8004','Zürich','Schweiz','0765695227','0765695227','laatina__x3@hotmail.com',NULL,NULL,0,'38',0,1,'1','2021-11-09 21:16:46','2021-12-07 21:25:07'),(20,'61a1a3829f815246858ccba5d30fc8a2','Herr','Raudy Manuel','Pina Moncion','1996-12-20','Männlich','Ledig','CH','Hohlstrasse 187','Zürich','Zürich','Schweiz','0764217813','0764217813','moncion1996@hotmail.com',NULL,NULL,0,'38',0,1,'1','2021-11-09 21:19:34','2021-12-07 21:25:09'),(21,'d567a3357bd72ac4bc34a3f8e4fe5217','Herr','Samir','Bajrami','1997-07-03','Männlich','Ledig','CH','Meneggplatz 18','8041','Zürich','Schweiz','0765224646','0765224646','samir.bajrami7@hotmail.com',NULL,NULL,0,'38',0,1,'1','2021-11-09 21:22:57','2021-12-07 21:25:11'),(22,'6feda7c2c0644244c2808df210c33919','Frau','Lorena','Sprascio','1995-12-02','Weiblich','Ledig','CH','Pfisternweg 7','6340','Baar','Schweiz','0795212423','0795212423','lorena.sprascio@hotmail.com',NULL,NULL,0,'38',0,1,'1','2021-11-09 21:25:33','2021-12-07 21:25:14'),(23,'ed74b913f680e208c68eee4f55a677b2','Herr','Lorenzo','Locatelli','2001-11-19','Männlich','Ledig','IT','Bankstrasse 9','Neuenhof','5432','Schweiz','0764180423','0764180423','lorenzo.locatelli7@outlook.com',NULL,NULL,0,'37',0,1,'1','2021-11-09 21:40:44','2021-12-07 21:25:17'),(24,'bb4cb9d398b7d4f56de30ab9d1d79943','Frau','Rina','Giacomo','1964-02-09','Weiblich','Ledig','IT','Zürcherstrasse 126B','Neuenhof','5432','Schweiz','0793196390','0793196390','Rina.Giacomo@dhl.com',NULL,NULL,0,'37',0,1,'1','2021-11-09 21:44:55','2021-12-07 21:24:51'),(25,'a092728a8980576f42dd65034549187d','Frau','Behina','Binandeh','1997-10-28','Weiblich','Ledig','CH','Landshutstrasse 6','Bätterkinden','3315','Schweiz','0788574265','0788574265','behina_binandeh_1997@hotmail.com',NULL,NULL,0,'37',0,1,'1','2021-11-09 21:47:56','2021-12-07 21:25:19'),(26,'380e65a2608e4360aede5285886b8fbc','Herr','Mohammad','Mehdi Izadi','1991-09-21','Männlich','Ledig','IR','Worblentalstrasse 1','Ittingen','3048','Schweiz','0787533130','0787533130','mehdi.izadi1991@gmail.com',NULL,NULL,0,'37',1,1,'1','2021-11-09 21:50:49','2021-12-07 21:25:22'),(27,'be5d141b738028cb3b9da5b64a5c9239','Herr','Mahnoor','Khawaja','2000-06-13','Männlich','Ledig','CH','Alberstrasse 8','Neuenhof','5432','Schweiz','0765323561','0765323561','mahnoor.khawaja@hotmail.com',NULL,NULL,0,'37',1,1,'1','2021-11-09 21:54:42','2021-12-07 21:25:25'),(28,'5e0b60588ca00ad26b98d8a01d885493','Frau','Inka','Hennig','2000-10-08','Weiblich','Ledig','CH','Seefeldstrasse 24','Zürich','8008','Schweiz','0764554134','0764554134','inkahennig@gmail.com',NULL,NULL,0,'36',1,1,'1','2021-11-09 21:57:12','2021-12-07 21:25:28'),(29,'76190a23e4c3c5b62f4c40f9d8237384','Herr','Faris','Baumgartner','1999-02-06','Männlich','Ledig','CH','Untere Briggerstrasse 31','8406','Winterthur','Schweiz','0764180602','0764180602','faris.baumgartner@hotmail.ch',NULL,NULL,0,'36',1,1,'1','2021-11-10 16:50:11','2021-12-07 21:25:31'),(30,'1d3a7dd7021222a99fe6bda7640f2840','Herr','Prince','Tiwary','1990-04-01','Männlich','Ledig','CH','Baslerstrasse 279','4632','Trimbach','Schweiz','0779579130','0779579130','prince.tiwary1234@gmail.com',NULL,NULL,0,'39',1,1,'1','2021-11-10 17:11:31','2021-12-07 21:25:33'),(31,'c2229048f8903dfe63fe01b559abcb38','Herr','Joemie','Saint Fleur','2001-04-22','Männlich','Ledig','CH','Zürcherstrasse 166','8952','Schlieren','Schweiz','0763280544','0763280544','joemie22@hotmail.com',NULL,NULL,0,'39',1,1,'1','2021-11-10 17:14:19','2021-12-07 21:25:36'),(32,'ba9b9b65d34993fb1b658a330f88d2c9','Herr','Kastella','Kalume','2001-09-15','Männlich','Ledig','CH','Bornfeldstrasse 26','4600','Olten','Schweiz','0763301787','0763301787','ka.stella2009@hotmail.com',NULL,NULL,0,'39',1,1,'1','2021-11-10 17:16:39','2021-12-07 21:25:39'),(33,'fb031421b29da7d1afd2c09558919144','Herr','Goutham','Harsha Kirikera','1991-05-20','Männlich','Ledig','CH','Lerchenstrasse 23','8045','Zürich','Schweiz','-','-','gouthamkgh@hotmail.com',NULL,NULL,0,'39',1,1,'1','2021-11-10 17:23:33','2021-12-07 21:25:42'),(34,'fe72b0f7b0b6a0ea9582d1ad9ac03e8e','Herr','Ernest','Bentum','1998-12-24','Männlich','Ledig','CH','Zürcherstrasse 246','8953','Dietikon','Schweiz','0798703123','0798703123','ernestbentumyaw@yahoo.com',NULL,NULL,0,'40',1,1,'1','2021-11-10 17:25:56','2021-12-07 21:25:45'),(35,'c0eb0697b59e539cdec612d367fa7b6b','Frau','Jessica','Cammarano','2001-04-03','Weiblich','Ledig','CH','Alberich Zwyssigstrasse 90','5430','Wettingen','Schweiz','0766056014','0766056014','jessica-cammarano01@gmx.ch',NULL,NULL,0,'42',1,1,'1','2021-11-11 19:29:43','2021-12-07 21:25:48'),(36,'9c4c073124df41ba8c4420572764af4e','Herr','Berthel','Valentina Laura','2002-02-28','Männlich','Ledig','CH','Landschau 34','6276','Hohenrain','Schweiz','0791085497','0791085497','laura.ramirez@gmx.net',NULL,NULL,0,'42',1,1,'1','2021-11-11 19:34:17','2021-12-07 21:25:51'),(37,'fed52120549efab93506fb741ad45aa6','Herr','Martin','Wütrich','1993-03-18','Männlich','Ledig','CH','Speiserstrasse 10','4600','Olten','Schweiz','0786664934','0786664934','martin.wu@gmx.ch',NULL,NULL,0,'37',1,1,'1','2021-11-11 19:45:40','2021-12-07 21:25:53'),(38,'b8de3f29a4b7b3c225633d9408431b4e','Herr','Burhan','Imeri','1997-05-17','Männlich','Ledig','CH','Horwerstrasse 38','6010','Kriens','Schweiz','0791959482','0791959482','burhanii1997@gmail.com',NULL,NULL,0,'37',1,1,'1','2021-11-11 19:48:03','2021-12-07 21:25:56'),(39,'108482f0fc5cc45a4759dc66556c091a','Herr','Oliver','Friedrich','1998-07-08','Männlich','Ledig','CH','Egelseestrasse 3','5454','Bellikon','Schweiz','0796151566','0796151566','o.friedrich@bluewin.ch',NULL,NULL,0,'37',1,1,'1','2021-11-11 19:50:22','2021-12-07 21:25:59'),(40,'6ca2db6c6e985edd945dd8c6568c4ee2','Herr','Dominik','Santesso','1999-06-27','Männlich','Verheiratet','CH','Feldstrasse 19','4600','Olten','Schweiz','0788482025','0788482025','santessodominik@gmail.com',NULL,NULL,0,NULL,1,1,'1','2021-11-11 19:56:17','2021-12-07 21:26:02'),(41,'8115c6c6bedd4e078371c700a506a4d6','Herr','Othmar','Hürlimann','1993-05-20','Männlich','Led','CH','Waldmatt 30','6024','Hildisrieden','Schweiz','0787043832','0787043832','othi1993123@gmail.com',NULL,NULL,0,'37',1,1,'1','2021-11-11 20:01:15','2021-12-07 21:26:05'),(42,'44214cc18e961caf1c6ac7b25886d956','Frau','Anae','Rüdeberg','2001-06-07','Männlich','Ledig','CH','Püntenstrasse 43','8143','Stallikon','Schweiz','0765825207','0765825207','anae.ruedeberg@outlook.com',NULL,NULL,0,'16',1,1,'1','2021-11-11 20:07:58','2021-12-07 21:26:08'),(43,'17faa267aaeea09dd7ce7c8ec6f1ca8c','Frau','Alexandrina','Batista','2003-03-10','Weiblich','Ledig','CH','Letzigraben 154','8047','Zürich','Schweiz','0783513922','0783513922','alexandrinapatricia790@gmail.com',NULL,NULL,0,'16',1,1,'1','2021-11-11 20:10:45','2021-12-07 21:26:10'),(44,'1b4b75e216872b06cb1a79ee023231f2','Herr','Namgyal','Lhawang','1969-12-10','Männlich','Ledig','CH','Leimgrübelstrasse 15','8052','Zürich','Schweiz','0764416996','0764416996','adanala@ymail.com',NULL,NULL,0,'16',1,1,'1','2021-11-11 20:14:34','2021-12-07 21:26:13'),(45,'795edb9f2a52704012b4a7c37a306235','Herr','Johnson','Kalloor','1970-05-13','Männlich','Verheiratet','IN','Fliederweg 5','4612','Wangen bei Olten','Schweiz','0788761621','0788761621','33.kalloorjohnson@gmail.com',NULL,NULL,0,'16',1,1,'1','2021-11-12 14:13:47','2021-12-07 21:26:16'),(46,'cae0c69be53c3858194aeca0ccb007ec','Frau','Nsimba Maria','Pecosse','1988-09-22','Weiblich','Verheiratet','CH','Uetlibergstrasse 82','8045','Zürich','Schweiz','0765483703','0765483703','arletete36@gmail.com',NULL,NULL,0,'43',1,1,'1','2021-11-12 14:16:13','2021-12-07 21:26:19'),(47,'3d6b344cc1c9ed42c6a3a6ced312d6c3','Herr','Yanis Matumona','Pecosse','2012-04-25','Männlich','Ledig','CH','Uetlibergstrasse 82','8045','Zürich','Schweiz','0765483703','0765483703','arlete-patricia@hotmail.com',NULL,NULL,0,'43',1,1,'1','2021-11-12 14:18:03','2021-12-07 21:26:22'),(48,'9ba83adede0eba0d28fc3c974a5109c2','Herr','Eliezer','Pecosse','2015-03-23','Männlich','Ledig','CH','Uetlibergstrasse 82','8045','Zürich','Schweiz','0765483703','0765483703','arlete-patricia@hotmail.com',NULL,NULL,0,'43',1,1,'1','2021-11-12 14:19:58','2021-12-07 21:26:24'),(49,'76b1c17c069b35adb8fd05b8653d717f','Herr','Luca','Pecosse','2008-02-25','Männlich','Ledig','CH','Uetlibergstrasse 82','8045','Zürich','Schweiz','0765483703','0765483703','arlete-patricia@hotmail.com',NULL,NULL,0,'43',1,1,'1','2021-11-12 14:21:22','2021-12-07 21:26:27'),(50,'d0383d38de7947b9c246ede276a5cb7d','Herr','Isidro','Rojas Torres','1982-05-16','Männlich','Verheiratet','UY','Badnerstrasse 54','8952','Schlieren','Paraguay','0779216681','0779216681','Rojasisidro523@gmail.com',NULL,NULL,0,'43',1,1,'1','2021-11-18 19:56:08','2021-12-07 21:26:30'),(51,'82347edc7f384c0b23ad648fc5343fa3','Frau','Mahnoor','Khawaja','2000-06-13','Männlich','Ledig','CH','ALberstrasse 8','5432','Neuenhof','Schweiz','076 532 35 61','076 532 35 61','mahnoor.khawaja@hotmail.com',NULL,NULL,0,'37',1,1,'1','2021-11-18 20:01:03','2021-12-07 21:26:33'),(52,'af5b46a60d2f5d8c6a2f4adedba35069','Herr','Danish','Khawaja','2002-01-21','Männlich','Ledig','CH','Albertstrasse 8','5432','Neuenhof','Schweiz','0765190874','0765190874','khawaja.d@hotmail.com',NULL,NULL,0,'37',1,1,'1','2021-11-18 20:17:39','2021-12-07 21:26:36'),(53,'79a17694052e19219f5b5c3f4ca15325','Herr','Juri','Läubli','1999-10-17','Männlich','Ledig','CH','Maurstrasse 34','8117','Fällanden','Schweiz','0766037654','0766037654','juri@laubli.ch',NULL,NULL,0,'36',1,1,'1','2021-11-18 20:20:13','2021-12-07 21:26:38'),(54,'9b8e0626f36ad085bbc05e094430d5ae','Herr','Kostiantyn','Kravchyk','1990-05-18','Männlich','Ledig','UA','Köshenrütistrasse  145','8052','Zürich','UA','076 496 79 63','076 496 79 63','kravchyk@inorg.chem.ethz.ch',NULL,NULL,0,'39',1,1,'1','2021-11-18 20:30:17','2021-12-07 21:26:41'),(55,'90becabe1c5b679a688f4d398a423320','Herr','Aleksandar','Naumov','2002-03-28','Männlich','Ledig','CH','Schützenmattweg 7','5610','Wohlen','Schweiz','076 581 32 08','076 581 32 08','ace.naumov1@hotmail.com',NULL,NULL,0,'41',1,1,'1','2021-11-18 20:35:39','2021-12-07 21:26:44'),(56,'14f65e4266607c54680ed8356536f246','Frau','Leonie','Mordeku','2000-07-02','Weiblich','Ledig','GH','Waldeggstrasse 4','9500','Wil','Schweiz','0764280706','0764280706','leonie.mordeku@gmail.com',NULL,NULL,0,'40',1,1,'1','2021-11-18 20:44:52','2021-12-07 21:26:47'),(57,'b9644fa4f83325074586cfc43e89fe46','Herr','Blessing','Ougishwa','2000-06-17','Männlich','Ledig','CG','Frohwiesstrasse 8','8630','Rüti','Kongo','079 594 53 50','079 594 53 50','ougishwablessing@gmail.com',NULL,NULL,0,'40',1,1,'1','2021-11-18 20:53:02','2021-12-07 21:26:50'),(58,'66379c5d702338cc09c9578f04fafd5c','Herr','Gabriel','Johnson','1986-03-03','Männlich','Ledig','LR','Guschastrasse 9','9475','Sevelen','LR','077 960 57 27','077 960 57 27','gabrieljhb86@gmail.com',NULL,NULL,0,'40',1,1,'1','2021-11-19 14:09:46','2021-12-07 21:26:52'),(59,'78acc841424cede04c055e72c4af0b97','Frau','Eva Johanna','Galliker','2002-05-31','Weiblich','Ledig','CH','Hausmattenstrasse 22A','5735','Pfeffikon','Schweiz','0796261051','0796261051','evagalliker16@gmail.com',NULL,NULL,0,'42',1,1,'1','2021-11-19 14:14:45','2021-12-07 21:26:55'),(60,'92c35197ee906eed6d335b53bb8a3f72','Frau','Lorena','Vargas','2002-11-05','Weiblich','Ledig','CH','Kapuzinerweg 33','6006','Luzern','Schweiz','0791229001','0791229001','lorena.vargas@bluewin.ch',NULL,NULL,0,'42',1,1,'1','2021-11-19 14:17:34','2021-12-07 21:26:58'),(61,'dce29718bac3c4cf255e1074d93eba6d','Herr','Farai','Magaya','1980-03-03','Männlich','Geschieden','CH','Hafenstrasse 28','8590','Romanshorn','ZW','077 998 48 83','077 998 48 83','gayasberg@gmx.ch',NULL,NULL,0,'40',1,1,'1','2021-11-19 14:23:31','2021-12-07 21:27:01'),(62,'b6e340bbca7ea09dd281997dfebc31db','Herr','Mohit','Ashar','1994-09-09','Männlich','Single','Indian','Muttrah','100','Muscat','Oman','7874983351','7874983351','raj@poojacreators.com',NULL,NULL,0,'16',0,1,'1','2021-11-22 20:46:55','2021-12-07 21:27:04'),(63,'46c23acdc3b1a878c0c5a93f7124e329','Herr','Raj','Sharma','1995-01-01','Männlich','Ledig','IN','Ruediweg','8957','Zürich','Schweiz','0878920','98765421','r.sharma@gmail.com',NULL,NULL,0,'16',1,0,'1','2021-12-10 14:24:19','2021-12-10 14:24:19'),(64,'38ef4386b7fdb794bba5ffe114ad1f04','Herr','Sheila','Varma','2000-01-02','Männlich','Verheiratet','CH','Rautistrasse','8047','Zürich','Schweiz','9812739','91837485247','s.varma@gmail.com',NULL,NULL,0,'16',1,1,'1','2021-12-10 14:28:15','2021-12-10 14:28:18'),(65,'d8fcfe00187c481ae65cd26e96406a8d','Frau','Meenu','Thottumkara','1996-11-27','Weiblich','Ledig','CH','Schneckenackerstrasse 31','8200','Schaffhausen','CH','0764057147','0764057147','m-thottumkara@hotmail.com',NULL,NULL,0,'16',1,0,'1','2021-12-15 15:02:41','2021-12-16 20:19:46'),(66,'734b143fb179ec375d1f185cbfd68fbd','Mr','James','Rathod','1986-06-24','Male',NULL,NULL,'sdaf','21312','ds',NULL,NULL,'8989878788','abcd@1234.com',NULL,5,0,'0',0,0,'2','2022-01-05 21:47:30','2022-01-05 21:47:30'),(67,'4e26a8818d77f5edd45568c5da3b5cea','Herr','Hans','Peter','1991-01-18','Männlich','Ledig','Österreich','rautistrasse 1','8055','zürich','CH','64565592','96565656','hans.p@hotmail.com',NULL,NULL,0,'16',1,0,'1','2022-01-18 15:11:23','2022-01-18 15:11:23'),(68,'0646d9efa582bcd569675407124faaf0','Mr','James','Rathod','1986-06-24','Male',NULL,NULL,'sdaf','21312','ds',NULL,NULL,'8989878788','abcd@1234.com',NULL,5,0,'0',0,0,'2','2022-01-19 13:05:03','2022-01-19 13:05:03'),(69,'31500334643552ea0c51f9a1d1debe26','Mr','James','Rathod','1986-06-24','Male',NULL,NULL,'sdaf','21312','ds',NULL,NULL,'8989878788','abcd@1234.com',NULL,5,0,'0',0,0,'2','2022-01-20 17:33:26','2022-01-20 17:33:26'),(70,'2b54b8637b9cf7a7f05c9a4b34f89a30','Mr','Divyang','Chauhan','1990-11-28','Male',NULL,NULL,'sdaf','21312','ds',NULL,NULL,'8989878788','abcd@1234.com',NULL,5,0,'0',0,1,'1','2022-01-20 18:02:44','2022-02-01 14:38:19'),(71,'5d701f89774ecc3230a80cc088ea55e1','Mr','Divyang','Chauhan','1990-11-28','Male',NULL,NULL,'sdaf','21312','ds',NULL,NULL,'8989878788','abcd@1234.com',NULL,5,0,'0',0,1,'1','2022-01-20 18:11:25','2022-02-03 14:30:20'),(72,'91bdb2206b1b64b198b71e963387a76f','Mr','Kiran','TEst','1990-11-28','Male',NULL,NULL,'sdaf','21312','ds',NULL,NULL,'8989878788','abcd@1234.com',NULL,5,0,'0',0,1,'1','2022-01-20 18:14:37','2022-02-03 14:30:06'),(73,'f4a5d9d6ffd88658dc963f30f368d3ff','Mr','firstname','lastname','2022-01-12','Männlich',NULL,NULL,'test street','456123','test location',NULL,NULL,'7894561230','test@h.com',NULL,10,0,'0',0,0,'1','2022-01-20 19:01:04','2022-01-20 19:01:04'),(74,'fc3c0de2b5891b2398d4391ad5f9ab22','Mr','firstname','lastname','2022-01-12','Weiblich',NULL,NULL,'test street','456123','test location',NULL,NULL,'7894561230','test@h.com',NULL,10,0,'0',0,1,'1','2022-01-20 19:01:37','2022-02-03 14:30:35'),(75,'411452bf0e3d44fd5c44f344fdb61a33','Divy','R','Chauhan','2022-01-20','Männlich',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'8888888888','divy@gmail.com',NULL,3,0,'0',0,0,'1','2022-01-20 20:16:50','2022-01-20 20:16:50'),(76,'b7c949be5a1f17bbf09656328ec1188d','Divy','R','Chauhan','2022-01-20','Männlich',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'8888888888','divyangchauhan2811@gmail.com',NULL,3,0,'0',0,0,'1','2022-01-20 20:17:25','2022-01-20 20:17:25'),(77,'cd2b02668a3537732f2f6fc37ef14180','Divy','R','Chauhan','1989-01-26','Männlich',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'8989898989','divyangchauhan2811@gmail.com',NULL,4,0,'0',0,0,'1','2022-01-20 20:18:28','2022-01-20 20:18:28'),(78,'f4173b735c021994d1bbc18f03793c01','Mr','Divyang','Chauhan','1990-11-28','Male',NULL,NULL,'sdaf','21312','ds',NULL,NULL,'8989878788','abcd@1234.com',NULL,5,0,'0',0,0,'1','2022-01-21 11:00:43','2022-01-21 11:00:43'),(79,'b8a6503898761b8c6b76da6815956365','Demo','user','username','1991-01-28','Männlich',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'9898989898','divy123@gmail.com',NULL,2,0,'0',0,0,'1','2022-01-21 11:03:06','2022-01-21 11:03:06'),(80,'f6b60142b73702e877439b716cd863b1','Demo','user','username','2022-01-03','Männlich',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'98989898','demo@gmail.com',NULL,4,0,'0',0,0,'1','2022-01-21 11:07:08','2022-01-21 11:07:08'),(81,'b4451cbeb245923d03e1ae0ea1126874','Demo test','demo','demo','2022-01-02','Männlich',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'9898989898','demotest@gmail.com',NULL,5,0,'0',0,0,'1','2022-01-21 11:15:39','2022-01-21 11:15:39'),(82,'7f97629825e46f6fa2e14444b9cc056a','Mr.','Akash','Ashar','2002-01-04','Männlich',NULL,NULL,'Muscat','100','Oman',NULL,NULL,'9867456532','akash@ashar.com',NULL,2,0,'0',0,0,'1','2022-01-21 11:24:16','2022-01-21 11:24:16'),(83,'65b1104c9af2740ca41772c60a496458','David Villforth','Chris','Alvarez','2022-01-19','Männlich',NULL,NULL,'Rautistrasse 33','6400','Zuerich',NULL,NULL,'787664591','info@bonafides.ch',NULL,5,0,'0',0,0,'1','2022-01-21 20:03:55','2022-01-21 20:03:55'),(84,'122805d0d5073308a1cbfd521478390c','Mr','Divyang','Chauhan','1990-11-28','Male',NULL,NULL,'sdaf','21312','ds',NULL,NULL,'8989878788','abcd@1234.com','Apple Watch Serie 7 - Dein Gewinnspiel',5,0,'0',0,0,'1','2022-01-27 18:07:28','2022-01-27 18:07:28'),(85,'b25b9a8efd60e3733c76552b2acd2479','Mr','Divyang','Chauhan','1990-12-28','Male',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'8306270820','divyangchauhan.cmpica123@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',2,0,'0',0,0,'1','2022-01-27 18:23:42','2022-01-27 18:23:42'),(86,'4f99b33eb849a5d0c3a7980794b94015','Mr','Divyang','Chauhan','1990-12-28','Male',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'8306270820','divyangchauhan.cmpica123@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',2,0,'0',0,0,'1','2022-01-27 18:24:04','2022-01-27 18:24:04'),(87,'77a28846103bf2722b575ba25a3d374e','Mr','Divyang','Chauhan','1990-12-28','Male',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'8306270820','divyangchauhan.cmpica123@gmail.com',NULL,2,0,'0',0,0,'1','2022-01-27 18:27:04','2022-01-27 18:27:04'),(88,'10cf55fcdc5aa9867716b8720cca7e98','Mr','Divyang','Chauhan','1990-12-28','Male',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'8306270820','divyangchauhan.cmpica123@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',2,0,'0',0,0,'1','2022-01-27 18:32:12','2022-01-27 18:32:12'),(89,'3520d352d2067b9ca1db5787d436feea','Mr','Divyang','Chauhan','1990-12-28','Male',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'8306270820','divyangchauhan.cmpica123@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',2,0,'0',0,0,'1','2022-01-27 18:32:54','2022-01-27 18:32:54'),(90,'6f7a37d77f558a6a246f3293a5192ce2','Mr','Divyang','Chauhan','1990-11-28','Male',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'9898989898','divyangchauhan.cmpica123@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',2,0,'0',0,0,'1','2022-01-27 18:33:54','2022-01-27 18:33:54'),(91,'85a89b2a02eb43c0556e2ee7679538a6','Mr','Divyang','Chauhan','1990-11-28','Male',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'9898989898','divyangchauhan.cmpica123@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',2,0,'0',0,0,'1','2022-01-27 18:45:38','2022-01-27 18:45:38'),(92,'be2ec421f23f13285164053899615e0e','Mr','Divyang','Chauhan','1990-12-28','Männlich',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'8306270820','divyangchauhan.cmpica123@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',2,0,'0',0,0,'1','2022-01-27 18:52:03','2022-01-27 18:52:03'),(93,'54f639d5e034a6b61313f4a543f9df0d','Mr','Divyang','Chauhan','1990-11-28','Männlich',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'9898989898','divyangchauhan.cmpica123@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',2,0,'0',0,0,'1','2022-01-27 18:52:34','2022-01-27 18:52:34'),(94,'4b6d1ad1a591d7a9503b2a7db2ebcfc0','Divyang','R','Chauhan','1990-01-04','Männlich',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'8306270820','divyangchauhan.cmpica123@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',3,0,'0',0,0,'1','2022-01-31 11:49:32','2022-01-31 11:49:32'),(95,'7ff0d0604c420f2a9c76db5fdd338aae','Divyang','R','Chauhan','1990-01-04','Männlich',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'8306270820','divyangchauhan.cmpica123@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',3,0,'0',0,0,'1','2022-01-31 11:53:07','2022-01-31 11:53:07'),(96,'be4c99bb06e3666790e402b73eb0bdb3','Divyang','R','Chauhan','2022-01-01','Männlich',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'8306270820','divyangchauhan.cmpica123@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',3,0,'0',0,0,'1','2022-01-31 11:54:52','2022-01-31 11:54:52'),(97,'4179a5680324538d48565f91aaf5cf77','Divyang','R','Chauhan','2009-01-08','Männlich',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'8306270820','divyangchauhan.cmpica123@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',5,0,'0',0,0,'1','2022-01-31 11:59:08','2022-01-31 11:59:08'),(98,'9f843ec15bd80df3e5d239cea575f98a','Mr','Edwin','Davis','2001-01-04','Männlich',NULL,NULL,'Ahmedabad','380015','Ahmedabad',NULL,NULL,'1234123412','raj@poojacreators.com','Luxusspa Wochenende im Grand Hotel Dolder - Dein Gewinnspiel',4,0,'0',0,0,'1','2022-01-31 12:02:11','2022-01-31 12:02:11'),(99,'d98620d6735c1bd73fc5b78efda52906','Divyang','R','Chauhan','1990-11-28','Männlich',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'8306270820','divyangchauhan.cmpica@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',8,0,'0',0,0,'1','2022-01-31 12:05:55','2022-01-31 12:05:55'),(100,'265d15ea01692593a075846aa3930081','Mr','Divyang','Chauhan','1990-11-28','Männlich',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'8306270820','divyangchauhan.cmpica@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',5,0,'0',0,0,'1','2022-01-31 12:07:46','2022-01-31 12:07:46'),(101,'ba07ee9381aa7994d94c57eaf2ae8143','Mr','Divyang','Chauhan','2009-01-28','Männlich',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'8306270820','divyangchauhan.cmpica@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',5,0,'0',0,0,'1','2022-01-31 12:16:19','2022-01-31 12:16:19'),(102,'3ffe1db82414f34e01b56fb9cfffd5a6','Mr','Raj','Rathod','1994-09-09','Männlich',NULL,NULL,'Ahmedabad','380015','Ahmedabad',NULL,NULL,'7874983351','raj.rr.rathod94@gmail.com','Luxusspa Wochenende im Grand Hotel Dolder - Dein Gewinnspiel',3,0,'0',0,0,'1','2022-01-31 12:16:40','2022-01-31 12:16:40'),(103,'579835cb4c3d177fee58fd6c22a9b735','Mr','Divyang','Chauhan','2022-01-18','Männlich',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'8306270820','divyangchauhan.cmpica@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',3,0,'0',0,0,'1','2022-01-31 21:53:23','2022-01-31 21:53:23'),(104,'2607677a27fc1459dc12df981790a70e','Mr','Divyang','Chauhan','2022-01-06','Männlich',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'8306270820','divyangchauhan.cmpica@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',5,0,'0',0,0,'1','2022-01-31 22:03:45','2022-01-31 22:03:45'),(105,'c5426f2c2a5792f1fc88938aadf132cf','Mrs.','Pooja','Rathod','1994-01-25','Männlich',NULL,NULL,'Ahmedabad','380015','Ahmedabad',NULL,NULL,'1234123411','raj.rr.rathod94@gmail.com','Luxusspa Wochenende im Grand Hotel Dolder - Dein Gewinnspiel',3,0,'0',0,0,'1','2022-01-31 23:38:13','2022-01-31 23:38:13'),(106,'cfe2830163dbe0883726c30bc10c32c2','Herr','Gregor','Fritsche','1985-10-31','Männlich',NULL,'CH','Bodenluegeten 6','8840','Einsiedeln','CH',NULL,'795178425','fritscheg@hotmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',4,0,'16',0,0,'1','2022-02-01 03:30:13','2022-02-01 18:55:25'),(107,'0617653b7546d28092d20236b33aa49d','Mr','Divyang','Chauhan','2022-02-10','Männlich',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'8306270820','divyangchauhan.cmpica@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',4,0,'0',0,0,'1','2022-02-01 11:49:37','2022-02-01 11:49:37'),(108,'a9e0eb87b528db2d94f0fab2bc89b25f','Mr','Divyang','Chauhan','2022-02-02','Männlich',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'8306270820','divyangchauhan.cmpica@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',4,0,'0',0,0,'1','2022-02-01 11:52:32','2022-02-01 11:52:32'),(109,'4d1394809d0eccb84e4bf088904d58fb','Mr','Divyang','Chauhan','2022-02-02','Männlich',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'8306270820','divyangchauhan.cmpica@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',5,0,'0',0,0,'1','2022-02-01 11:54:32','2022-02-01 11:54:32'),(110,'da885801a76308503e557f90423add84','Mr','Divyang','Chauhan','2021-12-01','Männlich',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'8333333333','divyangchauhan.cmpica@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',6,0,'0',0,0,'1','2022-02-01 11:56:17','2022-02-01 11:56:17'),(111,'4c75c4a2fe9b0c3c3107b68366436bd7','Mr','Divyang','Chauhan','2021-12-01','Männlich',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'840232423','divyangchauhan.cmpica@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',4,0,'0',0,0,'1','2022-02-01 11:57:52','2022-02-01 11:57:52'),(112,'fba0ec504d8dcd125970e208dc2c7bdf','Mr','Divyang','Chauhan','2021-12-02','Männlich',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'23213213123','divyangchauhan.cmpica@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',3,0,'0',0,0,'1','2022-02-01 11:59:35','2022-02-01 11:59:35'),(113,'cf7af85497d3db007bcc5b808b5339db','Mr','Divyang','Chauhan','2022-02-02','Männlich',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'12321312312','divyangchauhan.cmpica@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',6,0,'0',0,0,'1','2022-02-01 12:01:39','2022-02-01 12:01:39'),(114,'3de49010839f84cc038e6f0fbbb59bc2','Mr','Divyang','Chauhan','2022-02-02','Männlich',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'9898989898','divyangchauhan.cmpica@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',3,0,'0',0,0,'1','2022-02-01 12:02:24','2022-02-01 12:02:24'),(115,'ed57fef446902e858dbaf5c730be3871','Mr','Divyang','Chauhan','2022-02-02','Männlich',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'8306270820','divyangchauhan.cmpica@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',5,0,'0',0,0,'1','2022-02-01 12:16:24','2022-02-01 12:16:24'),(116,'5379abda4891846c1286c001565ed682','Mr','Divyang','Chauhan','2022-02-03','Männlich',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'21321321321','divyangchauhan.cmpica@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',4,0,'0',0,0,'1','2022-02-01 12:17:13','2022-02-01 12:17:13'),(117,'83200948e4bdd9fb12ddfb5749b0d7c3','Mr','Divyang','Chauhan','1990-12-28','Männlich',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'3243243432','divyangchauhan.cmpica@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',4,0,'0',0,0,'1','2022-02-01 12:18:19','2022-02-01 12:18:19'),(118,'f123754b8428147948921bc192f34540','Mr','Divyang','Chauhan','2022-02-02','Männlich',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'2323213123','divyangchauhan.cmpica@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',3,0,'0',0,0,'1','2022-02-01 12:20:08','2022-02-01 12:20:08'),(119,'7da1ec404fa435f28ff3f6c99390a8d6','Mr','Divyang','Chauhan','1990-11-28','Männlich',NULL,NULL,'Ah','SAs','Ahmedabad',NULL,NULL,'1231231234','divyangchauhan.cmpica@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',3,0,'0',0,0,'1','2022-02-01 12:21:42','2022-02-01 12:21:42'),(120,'2645a02247cf446dc865b51d664bdb64','Mr','Divyang','Chauhan','1990-11-28','Männlich',NULL,NULL,'Ah','SAs','Ahmedabad',NULL,NULL,'21312321321','divyangchauhan.cmpica@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',3,0,'0',0,0,'1','2022-02-01 12:22:50','2022-02-01 12:22:50'),(121,'391ce832ae662c57d1dcdae5178934c1','Herr','David','Villforth','1985-02-01','Männlich',NULL,NULL,'10','8600','Dübendorf',NULL,NULL,'787664591','info@swimmo-invest.ch','Apple Watch Serie 7 - Dein Gewinnspiel',2,0,'0',0,0,'1','2022-02-01 12:35:27','2022-02-01 12:35:27'),(122,'074c21cd4d6443b6d1d97ad0213ebfcf','Frau','Alexandra','Schaller','1975-07-12','Männlich',NULL,NULL,'In den Schorenmatten 65','4058','Basel',NULL,NULL,'41763360236','alexandra_schaller2001@yahoo.de','Apple Watch Serie 7 - Dein Gewinnspiel',4,0,'0',0,0,'1','2022-02-02 10:04:25','2022-02-02 10:04:25'),(123,'8be570d2f40bf4e499e6c7be50f8a7b1','Herr','Chr','Zemp','1978-03-27','Männlich',NULL,NULL,'Kirchstrasse 1','6252','Dagmersellen',NULL,NULL,'793447306','z.ett@bluewin.ch','Apple Watch Serie 7 - Dein Gewinnspiel',4,0,'0',0,0,'1','2022-02-02 12:50:53','2022-02-02 12:50:53'),(124,'3ccd93172b57216f69b50bd461d2ead4','Frau','Heeyoung','Kim','2086-04-22','Männlich',NULL,NULL,'Gustav Zeiler-Ring 3','5600','Lenzburg',NULL,NULL,'797794545','heeyoung@kim.to','Apple Watch Serie 7 - Dein Gewinnspiel',3,0,'0',0,0,'1','2022-02-03 00:16:19','2022-02-03 00:16:19'),(125,'9b8d2780cb3c98572d6f291f42ce6fee','Frau','esther','emini','1970-01-01','Männlich',NULL,NULL,'wehntalerstr.245','8046','zürich',NULL,NULL,'764881624','esther.emini1941@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',1,0,'0',0,0,'1','2022-02-03 03:13:59','2022-02-03 03:13:59'),(126,'3d734736ec2e584e947f968637b5c541','Frau','Nsimba Maria','Pecosse','1988-09-22','Weiblich','Verheiratet','CH','Uetlibergstrasse 82','8045','Zürich','Schweiz','0765483703','0765483703','arltete36@gamil.com',NULL,NULL,0,NULL,1,0,'1','2022-02-03 21:37:46','2022-02-03 21:37:46'),(127,'a73c7a56851dce650049b7fe16757c58','Herr','Markus','Sax','1974-02-26','Männlich',NULL,NULL,'Leimenstr 23','2540','Grenchen',NULL,NULL,'41798209742','roces2002@bluewin.ch','Apple Watch Serie 7 - Dein Gewinnspiel',3,0,'0',0,0,'1','2022-02-04 03:30:10','2022-02-04 03:30:10'),(128,'f168d0c012f02374acca62af3e433255','Mr','Divyang','Chauhan','2021-12-08','Männlich',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'8306270820','divyangchauhan.cmpica@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',3,0,'0',0,0,'1','2022-02-04 09:36:39','2022-02-04 09:36:39'),(129,'748f5653b6d7d654bb18c61edf4d5cb3','Mr','Divyang','Chauhan','2021-12-08','Männlich',NULL,NULL,'Ahmedabad','380058','Ahmedabad',NULL,NULL,'8306270820','divyangchauhan.cmpica@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',3,0,'0',0,0,'1','2022-02-04 09:37:03','2022-02-04 09:37:03'),(130,'43c1fa4b4b788ccf9d379f5c66010fe5','Frau','Karin','Bill','1987-07-18','Männlich',NULL,NULL,'Quellenstrasse 39a','4310','Rheinfelden',NULL,NULL,'793457555','karin-bill@bluewin.ch','Apple Watch Serie 7 - Dein Gewinnspiel',5,0,'0',0,0,'1','2022-02-04 12:06:28','2022-02-04 12:06:28'),(131,'0b7e522edb9a81b02f5c93c69c49577b','Herr','Gugliotta','Tommado','1991-04-14','Männlich',NULL,NULL,'Grubenstrasse 11','6014','luzern',NULL,NULL,'764273119','electriko_69@hotmail.con','Apple Watch Serie 7 - Dein Gewinnspiel',3,0,'0',0,0,'1','2022-02-06 02:12:09','2022-02-06 02:12:09'),(132,'af38713cc548f7f5809a3114e9b2814d','Herr','Gugliotta','Tommado','1991-04-14','Männlich',NULL,NULL,'Grubenstrasse 11','6014','luzern',NULL,NULL,'764273119','electriko_69@hotmail.con','Apple Watch Serie 7 - Dein Gewinnspiel',3,0,'0',0,0,'1','2022-02-06 02:12:30','2022-02-06 02:12:30'),(133,'871aeef257cdb3c40339ea565bb5d5b7','Mr','Ndrichim','Shabani','1996-07-16','Männlich',NULL,NULL,'Flüestrasse','5313','Klingnau',NULL,NULL,'764132228','ndricim50@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',2,0,'0',0,0,'1','2022-02-07 02:10:45','2022-02-07 02:10:45'),(134,'f8b94c66f55cdedd7e9ab0d44b381f4b','Mr','Ndrichim','Shabani','1996-07-16','Männlich',NULL,NULL,'Flüestrasse','5313','Klingnau',NULL,NULL,'764132228','ndricim50@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',2,0,'0',0,0,'1','2022-02-07 02:10:57','2022-02-07 02:10:57'),(135,'47511952c786ba29ad15fe62321d76e9','Mr','Ndrichim','Shabani','1996-07-16','Männlich',NULL,NULL,'Flüestrasse','5313','Klingnau',NULL,NULL,'764132228','ndricim50@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',2,0,'0',0,0,'1','2022-02-07 02:11:36','2022-02-07 02:11:36'),(136,'2d0476966537e60224e94b75ed6bf267','Mr','Ndrichim','Shabani','1996-07-16','Männlich',NULL,NULL,'Flüestrasse','5313','Klingnau',NULL,NULL,'764132228','ndricim50@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',2,0,'0',0,0,'1','2022-02-07 02:11:45','2022-02-07 02:11:45'),(137,'ca743962f0a3582b7de8ea5ac7ebf1be','Herr','Thomas','Vogel','1974-09-20','Männlich',NULL,NULL,'Weidpark 1','6280','Hochdorf',NULL,NULL,'41796607746','thomasvogel74@yahoo.com','Apple Watch Serie 7 - Dein Gewinnspiel',4,0,'0',0,0,'1','2022-02-07 14:28:10','2022-02-07 14:28:10'),(138,'88b39d836171b3fb87dfdbcd40450f48','Frau','Nicole','Ryf','2022-01-22','Männlich',NULL,NULL,'Lochmattstrasse 14','5012','Schönenwerd',NULL,NULL,'787337263','nicoleryf4@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',2,0,'0',0,0,'1','2022-02-07 20:29:22','2022-02-07 20:29:22'),(139,'ab413b61c5c9ae7122fe23eecee13cb6','Frau','Nicole','Ryf','2022-01-22','Männlich',NULL,NULL,'Lochmattstrasse 14','5012','Schönenwerd',NULL,NULL,'787337263','nicoleryf4@gmail.com','Apple Watch Serie 7 - Dein Gewinnspiel',2,0,'0',0,0,'1','2022-02-07 20:29:32','2022-02-07 20:29:32');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documents`
--

DROP TABLE IF EXISTS `documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `document_id` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `label_en` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `label_de` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `label_fr` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `label_it` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `is_deleted` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `documents_document_id_unique` (`document_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documents`
--

LOCK TABLES `documents` WRITE;
/*!40000 ALTER TABLE `documents` DISABLE KEYS */;
INSERT INTO `documents` VALUES (1,'ce9cb562816adc78507dccdd9ac279b4','Driving Licenses','Driving License',NULL,NULL,0,1,'2021-09-19 21:46:25','2021-11-12 13:49:38'),(2,'232ebb05e7c8dfc0f66169bf8498f7d8','Resident Card','Resident Card','Resident Card','Resident Card',0,1,'2021-09-26 11:38:35','2021-11-12 13:49:37'),(3,'1e7a90267eba001f72f729a8f7efb232','Medical Card','Medical Card','Medical Card','Medical Card',0,1,'2021-10-22 15:37:26','2021-11-12 13:49:28'),(4,'0cb3a21486d7f959e761c48c2eaffffa','ID','ID',NULL,NULL,1,0,'2021-11-12 13:49:01','2021-11-12 13:49:01'),(5,'e531c6859262346d8b4dface6e6cc352','Krankenkassenkarte','Krankenkassenkarte',NULL,NULL,1,0,'2021-11-12 13:49:20','2021-11-12 13:49:20'),(6,'42b45022f4b45f292edafa229f151acc','Führerschein','Führerschein',NULL,NULL,1,0,'2021-11-12 13:50:02','2021-11-12 13:50:02'),(7,'248c2bd1bfea65b487a04454ca1b2e11','Ausländerausweis','Ausländerausweis',NULL,NULL,1,0,'2021-11-12 13:50:40','2021-11-12 13:50:40'),(8,'5119050f4514fce90d114575090ffde9','Neuantrag','Neuantrag',NULL,NULL,1,0,'2021-11-12 14:22:44','2021-11-12 14:22:44'),(9,'4c2bd5a46ee626e11d4265b64d46a2d3','Antragsformular','Antragsformular',NULL,NULL,1,0,'2021-11-12 14:25:57','2021-11-12 14:25:57'),(10,'73841240c70c48b411dd96f7e3edb09b','AVB`s zum Antrag','AVB`s zum Antrag',NULL,NULL,1,0,'2021-11-12 14:26:15','2021-11-12 14:26:15'),(11,'b6e0144690141c3327529b1d7c67cc7d','Beilage','Beilage',NULL,NULL,1,0,'2021-11-12 14:26:26','2021-11-12 14:26:26'),(12,'66b9060c9164555d36f6476096d5ccfd','Brief Kunde','Brief Kunde',NULL,NULL,1,0,'2021-11-12 14:26:39','2021-11-12 14:26:39'),(13,'251db070f92ada4447f2cc5eb500f25b','Elektronisch unterzeichnete Antragsunterlagen','Elektronisch unterzeichnete Antragsunterlagen',NULL,NULL,1,0,'2021-11-12 14:27:08','2021-11-12 14:27:08'),(14,'a3955ed04cf341a548e87c641528c3ad','Ihre elektronisch unterzeichnete Antragsunterlagen','Ihre elektronisch unterzeichnete Antragsunterlagen',NULL,NULL,1,0,'2021-11-12 14:27:43','2021-11-12 14:27:43'),(15,'fd0afdf6f717598f32f3f1c626e3a6a8','Offerte','Offerte',NULL,NULL,1,0,'2021-11-12 14:27:59','2021-11-12 14:27:59'),(16,'f86ee59ddd1ddaf36a14c5210c8d4d0f','Police','Police',NULL,NULL,1,0,'2021-11-12 14:28:10','2021-11-12 14:28:10'),(17,'bba7df0d8928c81521c04bb7fae3ca25','Policierung Vertrag','Policierung Vertrag',NULL,NULL,1,0,'2021-11-12 14:28:27','2021-11-12 14:28:27'),(18,'4c8f4f2ff98fdf1e48e6e645c069913d','Treuefonds Zertifikat','Treuefonds Zertifikat',NULL,NULL,1,0,'2021-11-12 14:28:44','2021-11-12 14:28:44'),(19,'9b6e994a4bb38e3e7d8902bcd32d568d','Zahlungsformulare','Zahlungsformulare',NULL,NULL,1,0,'2021-11-12 14:28:57','2021-11-12 14:28:57'),(20,'b1adbc207fb3c728aa2f11ca402055f7','ID 2','ID 2',NULL,NULL,1,0,'2021-11-29 14:07:52','2021-11-29 14:07:52'),(21,'f27b91086799982ce3c884945528d10c','ID Kind','ID Kind',NULL,NULL,1,0,'2021-11-29 14:08:09','2021-11-29 14:08:09'),(22,'4bcdc77f0b8894423f3d6d0a54994083','ID Kind 2','ID Kind 2',NULL,NULL,1,0,'2021-11-29 14:08:23','2021-11-29 14:08:23'),(23,'64bda9a73eb218594555a856649e96f3','Abholung Police im digitalen Kundenportal, Vertrag 3A-6799500, Thottumkara Meenu','Abholung Police im digitalen Kundenportal, Vertrag 3A-6799500, Thottumkara Meenu',NULL,NULL,1,0,'2022-01-18 14:56:10','2022-01-18 14:56:10'),(24,'059bd0f7d05ee9eb977cae2a50d720e9','Zahlungsformulare','Zahlungsformulare 2',NULL,NULL,1,0,'2022-01-18 14:56:30','2022-01-18 14:56:30');
/*!40000 ALTER TABLE `documents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee_payslip`
--

DROP TABLE IF EXISTS `employee_payslip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee_payslip` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `payslip_id` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `employee_id` bigint DEFAULT NULL,
  `month` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `year` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payslip_file` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payslip_amount` decimal(10,2) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  `uploaded_by` bigint DEFAULT NULL,
  `uploaded_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee_payslip`
--

LOCK TABLES `employee_payslip` WRITE;
/*!40000 ALTER TABLE `employee_payslip` DISABLE KEYS */;
INSERT INTO `employee_payslip` VALUES (1,'1597e53654a64760975339272a31efd8',2,'10','2021','uploads/2021/10/b4c9dfe84fc3e0e5acdcf5b289c1ab81.png',2500.00,1,1,'2021-10-21 01:30:39'),(2,'81bb20d01dd1656c941b8c6d534c8caf',2,'11','2021','uploads/2021/10/19164f6ec12631f81713042cb013c4ed.xlsx',2300.00,1,1,'2021-10-21 01:32:11'),(3,'3198c92b509df0ec9fd2a8bd1df51b97',27,'11','2021','uploads/2021/10/b4540a44fd1990fb381cf4df0c121793.pdf',3000.00,1,1,'2021-10-21 08:04:29'),(4,'f6a2f043b1fe59aeef702a4e9400f245',2,'10','2021','uploads/2021/10/1124a86d4cc1ed13aca28b60365ea5cb.pdf',100.00,1,1,'2021-10-22 15:48:34'),(7,'1b9e9f008a1e9664149c2dda6da609ec',16,'11','2021','uploads/2021/11/81e44fde2b5eb47aaa10183d2684ff8c.pdf',12000.00,1,1,'2021-11-17 21:44:01'),(8,'2f6832ecb134e2c689b0553eeeb893bc',36,'10','2021','uploads/2021/11/eae7fc8c2c72e22da952b181f6fd4ee8.pdf',0.00,0,1,'2021-11-18 15:30:49'),(9,'3ac01a103d6b5233c7dc7a0eb97480d3',37,'10','2021','uploads/2021/11/4c169e8bfc016ac9f88503a9e3ce5c2a.pdf',901.95,0,1,'2021-11-18 15:31:26'),(10,'f3dd7f2844429d946c61b60bdd721d75',37,'10','2021','uploads/2021/11/50313bf60664dfdda4fc2077da4453ae.pdf',1900.00,0,1,'2021-11-18 15:31:52'),(11,'4345beef0471e1ad6b0f827fc468ed66',39,'10','2021','uploads/2021/11/2a75e6f8793acd8c5a09c3110d716d6b.pdf',6036.45,0,1,'2021-11-18 15:32:32'),(12,'39aba7a995c5fddfc93f53f940e11c07',40,'10','2021','uploads/2021/11/0cba962a7e7805597267a0750e6af524.pdf',388.80,0,1,'2021-11-18 15:34:20'),(13,'80c20576159256d07d64ad6dbc21bfe3',42,'10','2021','uploads/2021/11/ad1e5b225cdb041e2174bac2f08e6857.pdf',1936.65,0,1,'2021-11-18 15:34:50'),(14,'6aa3b42f25d07e2c6050c5563b7b93c0',43,'10','2021','uploads/2021/11/35340c9d422a1fd5085ebed58165e36c.pdf',4566.00,0,1,'2021-11-18 15:35:22'),(15,'6639cf06d2997ca681dedb4ee986bb5b',42,'11','2021','uploads/2021/11/a445d421da83c07f26deca782c1270ec.pdf',3386.90,0,1,'2021-11-29 15:38:07'),(16,'015224d69d56d1ed03cb65e6778ebb91',36,'11','2021','uploads/2021/11/510c641399ab6f5d58ccf0721ebd306e.pdf',1102.65,0,1,'2021-11-29 15:38:38'),(17,'6814ac71cd320a21301938515df23659',37,'11','2021','uploads/2021/11/663048184d263dbd89e27d6d8e5a97e9.pdf',1724.90,0,1,'2021-11-29 15:39:38'),(18,'d69a3b72db35cf84c9de6063b1ff5e3b',43,'11','2021','uploads/2021/11/1c8bc1d16752ae5365e353497a0f636d.pdf',3766.00,0,1,'2021-11-29 15:40:22'),(19,'c634a9357ac8a8a2868ebd12fb327772',40,'11','2021','uploads/2021/11/f72082345c3998f78af0dca1757d9374.pdf',2131.85,0,1,'2021-11-29 15:40:52'),(20,'119d6fa1b211b335a969da106bafb060',41,'11','2021','uploads/2021/11/0d31b88ee2141e8169e044e4d44a2ffb.pdf',789.00,0,1,'2021-11-29 15:41:17'),(21,'db544ee4c559cad5601c049326446917',39,'11','2021','uploads/2021/11/aec32c6e71ac9c59ebe120330d9fe0ba.pdf',793.45,0,1,'2021-11-29 15:41:45'),(22,'6b3753e1e1b90675b6f4fdaa51a27c20',38,'11','2021','uploads/2021/11/8d996f5790f36454842682f149fbb86e.pdf',0.00,0,1,'2021-11-29 15:43:02');
/*!40000 ALTER TABLE `employee_payslip` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `insurance_providers`
--

DROP TABLE IF EXISTS `insurance_providers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `insurance_providers` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `provider_id` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `label_en` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `label_de` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `label_fr` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `label_it` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `is_deleted` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `insurance_providers_provider_id_unique` (`provider_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `insurance_providers`
--

LOCK TABLES `insurance_providers` WRITE;
/*!40000 ALTER TABLE `insurance_providers` DISABLE KEYS */;
INSERT INTO `insurance_providers` VALUES (1,'abed6856143b63959da816ce8bda2b92','Test 1','Test 1','Test 1','Test 1',0,1,'2021-09-19 20:40:38','2021-11-12 13:48:36'),(2,'2aa77eb50fa5fa7a06e803e0707f0537','Test 2','Test 2','Test 2','Test 2',0,1,'2021-09-19 20:46:41','2021-11-12 13:48:37'),(4,'dd1d66384badf3d31e219e32451e4232','Test 4','Test 4','Test 4','Test 4',0,1,'2021-09-26 11:24:51','2021-11-12 13:48:38'),(5,'333be058de3b9cf1075b527bd2699508','Shakira Zihlmann','Shakira Zihlmann','Shakira Zihlmann','Shakira Zihlmann',0,1,'2021-10-11 15:02:11','2021-11-12 13:48:41'),(6,'99f4dda2aa196681f843fcdb410f0512','test','tesz','test','test',0,1,'2021-10-11 18:34:08','2021-11-12 13:48:45'),(7,'7bf304083293e10c8ecd16a39298fd3c','Kevin','Kevin','Kevin','Kevin',0,1,'2021-10-11 23:06:55','2021-11-12 13:48:42'),(8,'79d154488772b77d72d433a1002c81c0','Lichtensteinlife','Lichtensteinlife','Lichtensteinlife','Lichtensteinlife',1,0,'2021-10-11 23:13:17','2021-12-05 12:29:24'),(9,'ad48a67d7a23d304eed1c85a1b0e91b9','AXA','AXA','AXA','AXA',1,0,'2021-10-12 12:49:07','2021-10-12 12:49:07');
/*!40000 ALTER TABLE `insurance_providers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `insurance_types`
--

DROP TABLE IF EXISTS `insurance_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `insurance_types` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `type_id` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `label_en` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `label_de` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `label_fr` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `label_it` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `min_amount` decimal(10,2) DEFAULT NULL,
  `max_amount` decimal(10,2) DEFAULT NULL,
  `min_duration` int DEFAULT NULL,
  `max_duration` int DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `is_deleted` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `insurance_types_type_id_unique` (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `insurance_types`
--

LOCK TABLES `insurance_types` WRITE;
/*!40000 ALTER TABLE `insurance_types` DISABLE KEYS */;
INSERT INTO `insurance_types` VALUES (1,'291f0a6f79cf308ff5a7ea16faeab662','Vorsorge','Life insurance',NULL,NULL,10.00,573.00,10,53,1,0,'2021-09-19 21:03:15','2021-12-03 22:52:04'),(2,'1946ffcc0b0c64ff1673fdb7063f31cd','Krankenkasse','Health Insurance','Health Insurance','Health Insurance',20.00,500.00,1,10,1,0,'2021-09-20 23:06:18','2021-10-11 23:05:29'),(3,'6fad65e0a6afd858c8cbdfc8de348979','Autoversicherung','Autoversicherung','Assurance voiture','Assurance voiture',600.00,10000.00,1,15,1,0,'2021-09-26 11:27:46','2021-10-11 23:05:49'),(4,'d7e3d5e3e904ac9865fb6d017eddc8d5','Hausrat-privathaftpflicht','Home Insurance','Home Insurance','Home Insurance',200.00,10000.00,1,10,1,0,'2021-09-26 11:34:49','2021-10-11 23:06:04'),(5,'a38fff414bc39acdb2ea0755397eea3a','Rechtschutz','Rechtschutzversicherung','Assurance protection juridique','Assicurazione spese legali',100.00,500.00,1,10,1,0,'2021-10-11 23:04:05','2021-10-11 23:06:11');
/*!40000 ALTER TABLE `insurance_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lead_employee`
--

DROP TABLE IF EXISTS `lead_employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lead_employee` (
  `lead_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `employee_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lead_employee`
--

LOCK TABLES `lead_employee` WRITE;
/*!40000 ALTER TABLE `lead_employee` DISABLE KEYS */;
INSERT INTO `lead_employee` VALUES ('0b55f12760da4ce318f1440d2c966b33','3f2bcdb1bc6b721efec844a6534c0d09'),('0b55f12760da4ce318f1440d2c966b33','640f287f2868e7352e3acc98609e1abd'),('0b55f12760da4ce318f1440d2c966b33','001c12f33b055853e5dd169ea1971e73'),('0b55f12760da4ce318f1440d2c966b33','1eb4427366ba583535b3a58403415ab7'),('49f456693463bacd318f78ac3a2d7d16','c0d3294ac90c3b40e9cc3376c02ec023');
/*!40000 ALTER TABLE `lead_employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leads`
--

DROP TABLE IF EXISTS `leads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leads` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `lead_id` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `file_name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_url` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `is_deleted` tinyint(1) DEFAULT '0',
  `created_by` bigint NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `leads_lead_id_unique` (`lead_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leads`
--

LOCK TABLES `leads` WRITE;
/*!40000 ALTER TABLE `leads` DISABLE KEYS */;
INSERT INTO `leads` VALUES (3,'0b55f12760da4ce318f1440d2c966b33','Initial Leads','Initial Leads','Liste TTV AB - Freddins.xlsx','uploads/2021/10/4beedce830cd5cf33ea3f3376f4e7f75.xlsx',1,0,1,'2021-10-02 23:20:54','2021-10-19 14:34:38'),(4,'ba75cf2e72450a3e12fa19827cbe4a59','Another Lead','Another Lead','Liste TTV AB - Freddins.xlsx','uploads/2021/10/a60755d88e53b91b13ee00193bf46ee6.xlsx',1,0,1,'2021-10-02 23:42:45','2021-10-02 23:42:45'),(5,'d4056889d2aa2a2be769b3e5e286ae2f','100er Liste','100er Liste','Liste TTV AB - Freddins.xlsx','uploads/2021/11/6b04d4e4ec72da1ac66341c576356497.xlsx',1,0,1,'2021-11-01 18:58:37','2021-11-01 18:58:37'),(6,'49f456693463bacd318f78ac3a2d7d16','100er Liste','100er Liste','100er Liste.xlsx','uploads/2021/11/409d2b9336767174fad0d8f49ea01aea.xlsx',1,0,1,'2021-11-01 19:19:47','2021-11-01 19:19:47');
/*!40000 ALTER TABLE `leads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meeting_employee`
--

DROP TABLE IF EXISTS `meeting_employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meeting_employee` (
  `meeting_id` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `employee_id` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meeting_employee`
--

LOCK TABLES `meeting_employee` WRITE;
/*!40000 ALTER TABLE `meeting_employee` DISABLE KEYS */;
INSERT INTO `meeting_employee` VALUES ('0dd4ae6e1b06a5e82a74219902e4e8ad','3f2bcdb1bc6b721efec844a6534c0d09'),('0dd4ae6e1b06a5e82a74219902e4e8ad','640f287f2868e7352e3acc98609e1abd'),('0dd4ae6e1b06a5e82a74219902e4e8ad','001c12f33b055853e5dd169ea1971e73'),('6d352c5aadbe8d5395b869d4b7ce1929','3f2bcdb1bc6b721efec844a6534c0d09'),('6d352c5aadbe8d5395b869d4b7ce1929','c34f73c76db286fa8a4663646214b045'),('745b2d6f6c3a596f742e0a1dee076a00','c0d3294ac90c3b40e9cc3376c02ec023'),('c487486c3becd30f7293e8f4a1c1b918','c0d3294ac90c3b40e9cc3376c02ec023'),('c487486c3becd30f7293e8f4a1c1b918','a7da561c7d3b1ce66078a587e6e7fe8f'),('c487486c3becd30f7293e8f4a1c1b918','bf34cd178ba8826c6562e600c3098eb2'),('c487486c3becd30f7293e8f4a1c1b918','56c2a57f5c98d3782c083d2e9a9e64c9'),('c487486c3becd30f7293e8f4a1c1b918','040515d719fc42f9e433c15c17e72acb'),('c487486c3becd30f7293e8f4a1c1b918','0b7238054bcd686437692c61a96d20a8'),('c487486c3becd30f7293e8f4a1c1b918','1b6f71e8d9521fc0eb64070971707ad4'),('c487486c3becd30f7293e8f4a1c1b918','c28e80691a52447ae1fd8795ac38f2a0'),('6b415babf6e7a6ea582f2547d8d75840','3f2bcdb1bc6b721efec844a6534c0d09'),('50666cd224c5ec877930303b2fb57d8e','3f2bcdb1bc6b721efec844a6534c0d09'),('ab0e4c3426a2178990818668e380644a','c0d3294ac90c3b40e9cc3376c02ec023'),('4f67fded81adb3fd1d17da3c0d693092','c0d3294ac90c3b40e9cc3376c02ec023'),('b8b1cd361e2858db45bb630e98822bc0','3f2bcdb1bc6b721efec844a6534c0d09'),('4e31d64e93b6ea5ec8495a78866bb69b','c0d3294ac90c3b40e9cc3376c02ec023'),('d886eee1b66ae4f9567dbf0fe50e47ad','c0d3294ac90c3b40e9cc3376c02ec023'),('c89a630a7e77f03ec5453938b96d6d7b','c0d3294ac90c3b40e9cc3376c02ec023'),('c89a630a7e77f03ec5453938b96d6d7b','bf34cd178ba8826c6562e600c3098eb2'),('c89a630a7e77f03ec5453938b96d6d7b','68804cab9470b5965ce9f116f033541e'),('1122cd6219641701a1f899c7ee43ac58','0b7238054bcd686437692c61a96d20a8');
/*!40000 ALTER TABLE `meeting_employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meetings`
--

DROP TABLE IF EXISTS `meetings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meetings` (
  `id` int NOT NULL AUTO_INCREMENT,
  `meeting_id` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meeting_detail` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meeting_on` date DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `attachment_link` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `agenda_doc` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `agenda_title` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mom_doc` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mom_title` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `is_deleted` tinyint(1) DEFAULT '0',
  `scheduled_by` bigint DEFAULT NULL,
  `schedule_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meetings`
--

LOCK TABLES `meetings` WRITE;
/*!40000 ALTER TABLE `meetings` DISABLE KEYS */;
INSERT INTO `meetings` VALUES (1,'6b415babf6e7a6ea582f2547d8d75840','asfda','2021-10-22','11:11:00','11:11:00',NULL,'','','','',1,1,1,'2021-10-22 11:23:24'),(2,'50666cd224c5ec877930303b2fb57d8e','asfda','2021-10-22','11:11:00','11:11:00',NULL,'','','','',1,1,1,'2021-10-22 11:24:54'),(3,'ab2a66463d81a715d151a0980d92b267','asfda','2021-10-22','10:45:00','12:00:00',NULL,'uploads/2021/10/8e3355072a5a4c5df4e33854ff265113.xlsx','Liste TTV AB - Freddins.xlsx','uploads/2021/10/7eef5057aa5d07c5d0e28cbb114b8daf.xlsx','Liste TTV AB - Freddins.xlsx',1,1,1,'2021-10-22 11:26:41'),(4,'473dc4336673a1c105e0010d2c16c4dd','asfda','2021-10-22','10:45:00','12:00:00',NULL,'uploads/2021/10/894c482502157a9d3fef73f291c1e553.xlsx','Liste TTV AB - Freddins.xlsx','uploads/2021/10/972b1881b6d36628a5ed51a022e808ed.xlsx','Liste TTV AB - Freddins.xlsx',1,1,1,'2021-10-22 11:27:05'),(5,'3b90c21581867a67d7110d10bf8793c9','asfda','2021-10-22','10:45:00','12:00:00',NULL,'uploads/2021/10/d66ddec1992684ad8d9d6396556afe59.xlsx','Liste TTV AB - Freddins.xlsx','uploads/2021/10/705b3901d7806c74a2ac5736186d22fa.xlsx','Liste TTV AB - Freddins.xlsx',1,1,1,'2021-10-22 11:29:25'),(6,'0dd4ae6e1b06a5e82a74219902e4e8ad','asfda','2021-10-22','10:45:00','12:00:00',NULL,'uploads/2021/10/4f2418d1e377b80dd32927747a0da1c8.xlsx','Liste TTV AB - Freddins.xlsx','uploads/2021/10/a023e433692b8a5fe3ce5b6a09747bf0.xlsx','Liste TTV AB - Freddins.xlsx',1,1,1,'2021-10-22 11:30:43'),(7,'6d352c5aadbe8d5395b869d4b7ce1929','Daily kick off sales meeting','2021-10-22','22:10:00','22:10:00',NULL,'uploads/2021/10/4e80a619af7a01f0deb158461682e1dc.pdf','RAJ KANTILAL RATHOD CV.pdf','uploads/2021/10/6fb7be5cc74a9d82b20c237e30c4de50.pdf','RAJ KANTILAL RATHOD CV.pdf',1,1,1,'2021-10-22 15:29:55'),(8,'745b2d6f6c3a596f742e0a1dee076a00','Welcome Board','2021-11-01','01:11:00','01:11:00',NULL,'uploads/2021/11/f82c8e140b5bd2bd919dfc06f0625096.xlsx','Liste TTV AB - Freddins.xlsx','uploads/2021/11/d283559bd8c5add881b48b181e343d9c.xlsx','Liste TTV AB - Freddins.xlsx',1,1,1,'2021-11-01 02:37:16'),(9,'c487486c3becd30f7293e8f4a1c1b918','Schulung Asimo','2021-11-15','09:30:00','13:00:00',NULL,'','','','',1,1,1,'2021-11-11 14:31:05'),(10,'ab0e4c3426a2178990818668e380644a','AT-Bogen Schulung','2021-12-01','09:30:00','11:00:00','https://outlook.office.com/calendar/view/week','','','','',1,1,1,'2021-11-23 16:10:26'),(11,'4f67fded81adb3fd1d17da3c0d693092','Test','2021-11-23','11:42:00','11:45:00','https://mega.nz/fm/0kkCzBTL','','','','',1,1,1,'2021-11-23 16:11:47'),(12,'b8b1cd361e2858db45bb630e98822bc0','dfd','2021-12-04','02:10:00','14:00:00',NULL,'','','','',1,1,1,'2021-12-04 02:15:30'),(13,'4e31d64e93b6ea5ec8495a78866bb69b','Welcome Board','2021-12-06','12:00:00','12:30:00','www.bonafides.ch','','','','',1,1,1,'2021-12-04 21:34:45'),(14,'d886eee1b66ae4f9567dbf0fe50e47ad','Swimmo Infoabend','2021-12-10','10:12:00','10:12:00','https://www.swimmo-invest.ch/','','','','',1,1,1,'2021-12-10 20:52:48'),(15,'c89a630a7e77f03ec5453938b96d6d7b','Jahres Programm 2022','2021-12-17','08:00:00','09:30:00',NULL,'uploads/2021/12/2adc9ff90b2b6d5982621874ecf8f4b3.docx','14.12.2021 Meeting.docx','','',1,0,1,'2021-12-15 15:10:19'),(16,'1122cd6219641701a1f899c7ee43ac58','AT Bogen Prüfung','2021-12-29','15:00:00','18:00:00',NULL,'','','','',1,0,1,'2021-12-29 16:27:50');
/*!40000 ALTER TABLE `meetings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `message_id` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `message_type` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message_subject` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `action_id` bigint DEFAULT '0',
  `is_active` tinyint(1) DEFAULT '1',
  `is_deleted` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `messages_message_id_unique` (`message_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES (1,'3c83131f22f0581fc19f8c50a327c8e2','Messages','Allgemeine Nachricht',0,1,0,'2021-10-02 00:56:23','2021-10-02 00:57:45'),(2,'4ff789944bc1bf6bb96d5560345f8fbe','Messages','Materialbestellung',0,1,0,'2021-10-02 00:58:00','2021-10-02 00:58:00'),(3,'392e0893ab07905187661e0568c5b7d2','Messages','Benutzerverwaltung',0,1,0,'2021-10-02 00:58:14','2021-10-02 00:58:14'),(4,'9fb52001debb56e4fc89df43a528fc8f','Messages','Hilfe',0,1,0,'2021-10-02 00:58:30','2021-10-02 00:58:30'),(5,'4c7f11c440065559592c846203705741','Messages','Verträge',0,1,0,'2021-10-02 00:58:45','2021-10-02 00:58:45'),(6,'a49674b0b89d8d0fe10c1ff7e6eea22b','Applications','Police',0,1,0,'2021-10-02 00:59:01','2021-10-02 00:59:01'),(7,'331809f27e81e6052a2ce999d439dfb6','Applications','Mandat',0,1,0,'2021-10-02 00:59:18','2021-10-02 00:59:18'),(8,'44bb4f389b78bb23f926b24914616b49','Applications','Termin / Lead',0,1,0,'2021-10-02 00:59:34','2021-10-02 00:59:34'),(9,'24df1bd31f1ccd6e2d6a93ee933f4399','Applications','Geburtsmeldung',0,1,0,'2021-10-02 01:04:05','2021-10-02 01:04:05'),(10,'21b2eb208793355e0795675d35af51d9','Applications','Geburtsmeldung',9,1,0,'2021-10-02 01:04:35','2021-10-02 01:04:35'),(11,'3e85e509caefa8312a6ccf30e942ba84','Messages','Nachricht an Maklerbetreuer',1,1,0,'2021-10-02 01:20:02','2021-10-02 01:20:02'),(12,'69fb693a730e10a96086c137ecf29a0f','Messages','Mandat einreichen',1,1,0,'2021-10-02 01:23:18','2021-10-02 01:23:18'),(13,'c5a89305aa68e2d7d97103a25e0d3616','Messages','Meine Adresse ändern',1,1,0,'2021-10-02 01:23:38','2021-10-02 01:25:14');
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1),(4,'2019_12_14_000001_create_personal_access_tokens_table',1),(5,'2021_09_06_220431_create_modules_table',2),(6,'2021_09_06_220710_create_permissions_table',3),(7,'2021_09_06_230526_create_customers_table',4),(8,'2021_09_06_234302_create_insurance_types_table',5),(9,'2021_09_06_234314_create_insurance_providers_table',5),(10,'2021_09_19_212241_create_documents_table',6),(11,'2021_09_20_232110_create_commissions_table',7),(12,'2021_09_29_010756_create_verify_users_table',8),(13,'2021_09_30_135535_create_messages_table',9),(14,'2021_10_02_205434_create_leads_table',10),(15,'2016_06_01_000001_create_oauth_auth_codes_table',11),(16,'2016_06_01_000002_create_oauth_access_tokens_table',11),(17,'2016_06_01_000003_create_oauth_refresh_tokens_table',11),(18,'2016_06_01_000004_create_oauth_clients_table',11),(19,'2016_06_01_000005_create_oauth_personal_access_clients_table',11);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modules` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `module_id` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `module_title` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `module_identifier` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `is_default` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `modules_module_id_unique` (`module_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modules`
--

LOCK TABLES `modules` WRITE;
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules` VALUES (1,'94942cd4661f289c277d20dcc6ae53b7','Users','USERS',NULL,'2021-09-28 23:43:21','2021-10-11 23:18:33'),(2,'4942c3c5f85243ce423c61544ea1527b','Insurance Providers','INSURANCE_PROVIDERS',0,'2021-09-28 23:44:36','2021-09-28 23:48:25'),(3,'21bb702e1a005d773c3029d800829834','Insurance Types','INSURANCE_TYPES',0,'2021-09-28 23:45:00','2021-09-28 23:45:00'),(4,'be092f6178cabfb5f723a9f8d3a64b82','Docuements','DOCUEMENTS',0,'2021-09-28 23:45:17','2021-09-28 23:45:17'),(5,'7beb9e746b1c2b09f0f5f982a0339019','Commission','COMMISSION',0,'2021-09-28 23:46:28','2021-09-28 23:46:28'),(6,'f0809d8551427c0dd0691edaa0d9a9b9','Modules','MODULES',0,'2021-09-28 23:46:31','2021-09-28 23:46:47'),(7,'3c02c18ba35703a9ea021e48489593d0','User Permissions','USER_PERMISSIONS',0,'2021-09-28 23:48:05','2021-09-28 23:48:05');
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `news_id` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `news_detail` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_docs` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `is_deleted` tinyint(1) DEFAULT '0',
  `created_by` bigint DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (1,'1b8ac75a977f5a561b1da2eac7b25980','New version is uploaded on 24-Oct-2021.','uploads/2021/11/5fee6551c557b1ffe3b8b1832d42a4ec.png',1,0,1,'2021-10-22 12:15:43'),(2,'369d86e69e26028da3b7a535636b9a93','Please welcome Raj Rathod as a new employee who has joined our sales team today',NULL,1,0,1,'2021-10-22 15:30:47'),(3,'abb5c838f390e1b209172f86e0168cde','Herzlich Willkommen Herr Pulikoden',NULL,1,0,1,'2021-11-01 02:40:55'),(4,'b71d81ee14868be57596b8b3e06d36db','Herzlich Willkommen Shakira Zihlmann beginnt als Senior Beraterin',NULL,1,0,1,'2021-11-01 17:09:07'),(5,'c01ae3847d3cee37a47106a8bee87a0a','fd',NULL,1,0,1,'2021-12-01 20:38:24'),(6,'82dde89d1875520c9402822cf5b7f64e',NULL,'',1,0,1,'2021-12-15 15:18:31');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` bigint unsigned DEFAULT NULL,
  `client_id` bigint unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` VALUES ('4f9cc501116751f86131a309389d6508713a2b2e113deb47428c1ace18d9ba697be8d92e7b8a578b',2,1,'MyApp','[]',0,'2022-01-19 13:01:49','2022-01-19 13:01:49','2023-01-19 13:01:49'),('b4b37d6255acf4b040c0440356c9df66b8f15414e7b875d65afb7a229d9f64861aaba6bef4e4f679',2,1,'MyApp','[]',0,'2022-01-05 21:11:58','2022-01-05 21:11:58','2023-01-05 21:11:58'),('eaeab2ff95ea7a3a75fc0e8d2b4448163464b5f9a9da68f661535f0320b5415a85f6d52ef9ff1e19',2,1,'MyApp','[]',0,'2022-01-05 21:47:09','2022-01-05 21:47:09','2023-01-05 21:47:09');
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` bigint unsigned NOT NULL,
  `client_id` bigint unsigned NOT NULL,
  `scopes` text COLLATE utf8_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_auth_codes`
--

LOCK TABLES `oauth_auth_codes` WRITE;
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_clients` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned DEFAULT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provider` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` VALUES (1,NULL,'Customer Resource Management Personal Access Client','7ASpbeXjVEr7XzzkPtOgcn13xYmGUomk6IZjcKZW',NULL,'http://localhost',1,0,0,'2022-01-05 21:09:28','2022-01-05 21:09:28'),(2,NULL,'Customer Resource Management Password Grant Client','HtLx7rOCoGj9ArqHqojRpmCA3uQuZieGR2prUurO','users','http://localhost',0,1,0,'2022-01-05 21:09:28','2022-01-05 21:09:28');
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `client_id` bigint unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_personal_access_clients`
--

LOCK TABLES `oauth_personal_access_clients` WRITE;
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
INSERT INTO `oauth_personal_access_clients` VALUES (1,1,'2022-01-05 21:09:28','2022-01-05 21:09:28');
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
INSERT INTO `password_resets` VALUES ('mittalshah1312@gmail.com','$2y$10$83sAv5oBgsCTA8DWUYC.I.AmEAyWm6eqbT5ygabKZ5cZGNwo0QRHm','2021-12-07 09:07:10');
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint NOT NULL,
  `module_id` bigint NOT NULL,
  `is_active` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personal_access_tokens` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint unsigned NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `policy_documents`
--

DROP TABLE IF EXISTS `policy_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policy_documents` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `pd_id` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `policy_id` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `document_id` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `document_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `document_path` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_pending` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  `is_approved` tinyint(1) DEFAULT '2',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `policy_documents`
--

LOCK TABLES `policy_documents` WRITE;
/*!40000 ALTER TABLE `policy_documents` DISABLE KEYS */;
INSERT INTO `policy_documents` VALUES (1,'e29e6eb2c46fbc52de185eb7fca91f08','906fc709cff591fe1abb4cee41012d8b','ce9cb562816adc78507dccdd9ac279b4','home-bg.png','uploads/2021/10/12b6473ca42a1459f87c523973472b07.png',0,0,2),(3,'1104f8a354e0d0c4b24dcfddd672f198','c774de99e1519844ef7feec05fdd1be1','ce9cb562816adc78507dccdd9ac279b4','F35A6607-FF97-40EF-9977-C3296FEB4B4B.jpeg','uploads/2021/10/1bdb4e2fa9af4497b8ce496d6b40aed0.jpeg',0,0,1),(4,'b8030cb08772f190e3909b32fb81009a','c8af747606d90ae3f74b3632687ac052','ce9cb562816adc78507dccdd9ac279b4','F35A6607-FF97-40EF-9977-C3296FEB4B4B.jpeg','uploads/2021/10/81c7c201552c2afda1f35314f50bf5f3.jpeg',0,0,1),(5,'54f7900371207db9aed6af4a18564445','906fc709cff591fe1abb4cee41012d8b','232ebb05e7c8dfc0f66169bf8498f7d8','DB6D7E0C-19E1-40DC-872A-0406E9095C68.jpeg','uploads/2021/10/4cce7d5ce1b6a677337407d3dd963387.jpeg',0,0,2),(6,'8da48ec6a424e15e8900d236c59fd4e4','906fc709cff591fe1abb4cee41012d8b','1e7a90267eba001f72f729a8f7efb232','','',1,0,2),(7,'b306d69811d090ded7bb956fb45fc77b','429a18052247bdb163f120cbb587b62d','5119050f4514fce90d114575090ffde9','3A-6723516, Kalloor Jossamma (1).pdf','uploads/2021/11/b2f5edb0631aadd744fbbaedc01d1bc3.pdf',0,0,2),(8,'ec59f81bdb2f2455c3f3531aa55362f7','429a18052247bdb163f120cbb587b62d','4c2bd5a46ee626e11d4265b64d46a2d3','Antragsformular.pdf','uploads/2021/11/561a66f0864f7d1b8e7e91536a34995b.pdf',0,0,2),(9,'de70e78c55d2566adb7efbcf6b8fb9b2','429a18052247bdb163f120cbb587b62d','73841240c70c48b411dd96f7e3edb09b','AVB\'s zum Antrag.pdf','uploads/2021/11/91fb4afd12a7d03cce34a738aba63c8b.pdf',0,0,2),(10,'804d61adde4395ec6d04a042fb705049','429a18052247bdb163f120cbb587b62d','b6e0144690141c3327529b1d7c67cc7d','Beilage.pdf','uploads/2021/11/cff111502c07e34027a9ecab4884b9d5.pdf',0,0,2),(11,'9e7ddb2e7eb0adc386b512025484960b','429a18052247bdb163f120cbb587b62d','66b9060c9164555d36f6476096d5ccfd','Brief Kunde.pdf','uploads/2021/11/860e8202f75b8271528ebdd55ee84105.pdf',0,0,2),(12,'1547a042abb124c52dd0219692e84925','429a18052247bdb163f120cbb587b62d','251db070f92ada4447f2cc5eb500f25b','Elektronisch unterzeichnete Antragsunterlagen.pdf','uploads/2021/11/5543edb38a9ade6b273622cc06bb2914.pdf',0,0,2),(13,'fd628926ddb7bde962b665901baca010','429a18052247bdb163f120cbb587b62d','0cb3a21486d7f959e761c48c2eaffffa','ID Copy.pdf','uploads/2021/11/4bc2548b7a3f049418338521ac8974ff.pdf',0,0,2),(14,'d697dcb9029fbdd77e99a25e92237272','429a18052247bdb163f120cbb587b62d','e531c6859262346d8b4dface6e6cc352','WhatsApp Image 2021-11-11 at 22.16.41.jpeg','uploads/2021/11/707d84f7848ac068431d62ca9bb359d6.jpeg',0,0,0),(15,'c67ebbb0f4e7474786dd26d040263d5a','429a18052247bdb163f120cbb587b62d','fd0afdf6f717598f32f3f1c626e3a6a8','Offerte.pdf','uploads/2021/11/7eb412ac64eb28daed11f89d8201b211.pdf',0,0,2),(16,'b1533d3876ec69cd8f8b102755e399f1','429a18052247bdb163f120cbb587b62d','f86ee59ddd1ddaf36a14c5210c8d4d0f','Police.pdf','uploads/2021/11/0d241e8ab70cea1d4e40be5476d1ceb5.pdf',0,0,2),(17,'f22336f4256562071b745b3196618a84','429a18052247bdb163f120cbb587b62d','bba7df0d8928c81521c04bb7fae3ca25','','',1,0,2),(18,'666446a560fd8926d6b58ebf60f97d18','429a18052247bdb163f120cbb587b62d','4c8f4f2ff98fdf1e48e6e645c069913d','Treuefonds Zertifikat.pdf','uploads/2021/11/83ced22f3cb1a69904657f2b87850253.pdf',0,0,2),(19,'da92b7495a7d256adfa8ba317c48faf9','429a18052247bdb163f120cbb587b62d','9b6e994a4bb38e3e7d8902bcd32d568d','Zahlungsformulare.pdf','uploads/2021/11/067018ca2374d299d356bd22fb8eccec.pdf',0,0,2),(20,'45c78b93f085d96731e1756e24eecf81','7a63b2b456626c8ae6b301c095fd8c6e','0cb3a21486d7f959e761c48c2eaffffa','ID Copy.pdf','uploads/2021/11/102113497f03f0f8907ae55aa610d171.pdf',0,0,2),(21,'e256cba2234b947ca0f983684f70c3f6','7a63b2b456626c8ae6b301c095fd8c6e','4c2bd5a46ee626e11d4265b64d46a2d3','Antragsformular.pdf','uploads/2021/11/f9e1fa0b4a1ed6219e82fa3509f7232a.pdf',0,0,2),(22,'83a6153b8f14e03031cfb8e8a5b5e1ee','7a63b2b456626c8ae6b301c095fd8c6e','73841240c70c48b411dd96f7e3edb09b','AVB\'s zum Antrag.pdf','uploads/2021/11/3d75946f40e8cb0f5779acc0c0424aeb.pdf',0,0,2),(23,'9aabd0c94f333603c2a76fe83e4248c9','7a63b2b456626c8ae6b301c095fd8c6e','b6e0144690141c3327529b1d7c67cc7d','Beilage.pdf','uploads/2021/11/dc919dff5c9d775181adcf49d7b961de.pdf',0,0,2),(24,'f746abb132adc0af3111f7a528f67a5b','7a63b2b456626c8ae6b301c095fd8c6e','66b9060c9164555d36f6476096d5ccfd','Brief Kunde (1).pdf','uploads/2021/11/5a8dcbb534c7e40dfcce7d61c447f696.pdf',0,0,2),(25,'5977624172b1be3ad058bb82db9cd4e7','7a63b2b456626c8ae6b301c095fd8c6e','251db070f92ada4447f2cc5eb500f25b','Elektronisch unterzeichnete Antragsunterlagen.pdf','uploads/2021/11/45a938de7cbdefad0e4b9835c2c8faa8.pdf',0,0,2),(26,'c48015c348e980fbf30260de8a2474d0','7a63b2b456626c8ae6b301c095fd8c6e','fd0afdf6f717598f32f3f1c626e3a6a8','Offerte.pdf','uploads/2021/11/9754e2cfef893783377a4df686590a7b.pdf',0,0,2),(27,'0b2a6ffd6179a54ef456879c25e83d66','7a63b2b456626c8ae6b301c095fd8c6e','f86ee59ddd1ddaf36a14c5210c8d4d0f','Police.pdf','uploads/2021/11/c5e2852df5e2849f68b45d2fe64ccb89.pdf',0,0,2),(28,'d9a4287e6cc1b3e0502d57ca9cfd6042','7a63b2b456626c8ae6b301c095fd8c6e','4c8f4f2ff98fdf1e48e6e645c069913d','Treuefonds Zertifikat.pdf','uploads/2021/11/4111fd43f14a93544f13ab048b01f76d.pdf',0,0,2),(29,'9c9c64f8f0da3d561f1a010d9bac0a53','7a63b2b456626c8ae6b301c095fd8c6e','bba7df0d8928c81521c04bb7fae3ca25','','',1,0,0),(30,'bed56e341cca40fee707bb363b40a2ab','7a63b2b456626c8ae6b301c095fd8c6e','9b6e994a4bb38e3e7d8902bcd32d568d','Zahlungsformulare (1).pdf','uploads/2021/11/828f1d6c4f9101e29a6563d6383ba421.pdf',0,0,2),(31,'56cb537ab668021a1bb91bf37a5e1617','c8af747606d90ae3f74b3632687ac052','f86ee59ddd1ddaf36a14c5210c8d4d0f','Screenshot 2021-11-18 at 7.56.49 PM.png','uploads/2021/11/b8fef7e914aa88899b7130782b901034.png',0,0,2),(32,'9006bcb3c59702fba74523e711c9a964','746fe7f43c6de4ba412227f3d2913fe3','0cb3a21486d7f959e761c48c2eaffffa','ID Copy (3).pdf','uploads/2021/11/aaabf3353a465d1fb8f4a2153e211b22.pdf',0,0,2),(33,'5a831c5d8624d913fb54f8849a97f4c5','746fe7f43c6de4ba412227f3d2913fe3','b1adbc207fb3c728aa2f11ca402055f7','ID Copy.pdf','uploads/2021/11/369a1c0d0dae5b5cc274282bc3181dc9.pdf',0,0,2),(34,'28201a4da5e5391de5a1454d2caf0742','746fe7f43c6de4ba412227f3d2913fe3','f27b91086799982ce3c884945528d10c','ID Copy (2).pdf','uploads/2021/11/3fe263ac71d34e19fccabc0c10c505e5.pdf',0,0,2),(35,'13aad04faff95ddd4d062f5d3351753e','746fe7f43c6de4ba412227f3d2913fe3','4bcdc77f0b8894423f3d6d0a54994083','ID Copy (1).pdf','uploads/2021/11/0d81d8310a858abe61f2feadeb9d668e.pdf',0,0,2),(36,'906433bcd858b8c5f897eb1b81e6cab3','746fe7f43c6de4ba412227f3d2913fe3','4c2bd5a46ee626e11d4265b64d46a2d3','Antragsformular.pdf','uploads/2021/11/5cd9ff90cb67b88c1cb51422500c562f.pdf',0,0,2),(37,'604c456fae9291c56041fcb21bfcbb58','746fe7f43c6de4ba412227f3d2913fe3','73841240c70c48b411dd96f7e3edb09b','AVB\'s zum Antrag.pdf','uploads/2021/11/49bee9c0a781f1b9a537916bbbed25f1.pdf',0,0,2),(38,'e1630da146398706cd0eaff1f2cd0533','746fe7f43c6de4ba412227f3d2913fe3','66b9060c9164555d36f6476096d5ccfd','Brief Kunde.pdf','uploads/2021/11/2c68faf1a0e2aa9f3e95c6350ede7906.pdf',0,0,2),(39,'0b4e5b66083b80c6c244c15117d039a7','746fe7f43c6de4ba412227f3d2913fe3','251db070f92ada4447f2cc5eb500f25b','Elektronisch unterzeichnete Antragsunterlagen.pdf','uploads/2021/11/d9b7b3b7c616460e50821a09a67c51d7.pdf',0,0,2),(40,'c27fb1d8608eed7d16e0caa712131569','8c9f3ea3752a7a5d22285c6a5546ef6f','0cb3a21486d7f959e761c48c2eaffffa','New Text Document.txt','uploads/2021/12/b940804b3828f12652cc5e85645d3733.txt',0,0,2),(41,'8e2a47bc9bf2075179bb05c44c0e06b7','c8af747606d90ae3f74b3632687ac052','0cb3a21486d7f959e761c48c2eaffffa','Screenshot 2021-12-07 at 9.34.24 AM.png','uploads/2021/12/c95d92d15936c788cdbfdda2fd1a9735.png',0,0,2),(42,'b798e5dda7e4cd430566b42b06567de0','c8af747606d90ae3f74b3632687ac052','e531c6859262346d8b4dface6e6cc352','Screenshot 2021-12-07 at 9.34.24 AM.png','uploads/2021/12/fbb136507ca84895c81937d5b0954284.png',0,0,2),(43,'1db29841ffe7721c31e2b8b3a1219c31','c8af747606d90ae3f74b3632687ac052','42b45022f4b45f292edafa229f151acc','Screenshot 2021-12-07 at 9.34.24 AM.png','uploads/2021/12/5ed578c54dd883a6426b43e0e6cdde53.png',0,0,2),(44,'faffc5a4ee98016182d0793672323ff6','c8af747606d90ae3f74b3632687ac052','248c2bd1bfea65b487a04454ca1b2e11','Screenshot 2021-12-07 at 9.34.24 AM.png','uploads/2021/12/62c2a83b21623e0c12d5c6d4ddaff325.png',0,0,2),(45,'3b7a704c70935ce9ba631676f3805b33','c8af747606d90ae3f74b3632687ac052','5119050f4514fce90d114575090ffde9','Screenshot 2021-12-07 at 9.34.24 AM.png','uploads/2021/12/6c5655c376adeaab4c1f86f3086748d3.png',0,0,2),(46,'f97ec036883527192a6387aeebbfb736','c8af747606d90ae3f74b3632687ac052','4c2bd5a46ee626e11d4265b64d46a2d3','Screenshot 2021-12-07 at 9.34.24 AM.png','uploads/2021/12/75b9b9e3a7933e8e3ae5b48edbc75cc5.png',0,0,2),(47,'887319b79379b8cd5eafb9f6e2fa1b58','c8af747606d90ae3f74b3632687ac052','73841240c70c48b411dd96f7e3edb09b','Screenshot 2021-12-07 at 9.34.24 AM.png','uploads/2021/12/9b571a7948adfaf85d2f5b2b76c92c1a.png',0,0,2),(48,'b6cd22296c604df285cb77109c2047b1','c8af747606d90ae3f74b3632687ac052','b6e0144690141c3327529b1d7c67cc7d','Screenshot 2021-12-07 at 9.34.24 AM.png','uploads/2021/12/d832f8df3a98f42b5151e2948997ed6d.png',0,0,2),(49,'4e0ee6ce0a73f294c06a13c82962ad3d','c8af747606d90ae3f74b3632687ac052','66b9060c9164555d36f6476096d5ccfd','Screenshot 2021-12-07 at 9.34.24 AM.png','uploads/2021/12/f8cdf057d8ecb09da7175f2c04a0a60f.png',0,0,2),(50,'50660325c59785631d41609c4297b71c','c8af747606d90ae3f74b3632687ac052','251db070f92ada4447f2cc5eb500f25b','Screenshot 2021-12-07 at 9.34.24 AM.png','uploads/2021/12/6d578f400972d1b920788e3bff45c93d.png',0,0,2),(51,'9154c64627035212b5bb6f14ca822635','c8af747606d90ae3f74b3632687ac052','a3955ed04cf341a548e87c641528c3ad','Screenshot 2021-12-07 at 9.34.24 AM.png','uploads/2021/12/c3d14a0476c0ec1288bf788cd724fdbc.png',0,0,2),(52,'85629fbc6bd5913033af57b92167b636','c8af747606d90ae3f74b3632687ac052','fd0afdf6f717598f32f3f1c626e3a6a8','Screenshot 2021-12-07 at 9.34.24 AM.png','uploads/2021/12/47a2188ed7fe089140f2ab8b0afca13c.png',0,0,2),(53,'0df372be43c434a99f6785a30b8afddd','c8af747606d90ae3f74b3632687ac052','bba7df0d8928c81521c04bb7fae3ca25','Screenshot 2021-12-07 at 9.34.24 AM.png','uploads/2021/12/179d80fdcd5441f4e1566432c164a30a.png',0,0,2),(54,'04e9b6f44c77b0cd47a65c3ff745d98c','c8af747606d90ae3f74b3632687ac052','4c8f4f2ff98fdf1e48e6e645c069913d','Screenshot 2021-12-07 at 9.34.24 AM.png','uploads/2021/12/5b24e9dfc36c8550b242568827e594d3.png',0,0,2),(55,'c72378eea6db1cece85ae28d4a6f2cc1','c8af747606d90ae3f74b3632687ac052','9b6e994a4bb38e3e7d8902bcd32d568d','Screenshot 2021-12-07 at 9.34.24 AM.png','uploads/2021/12/761e4ebd92a9d287a03799a4d8eafe97.png',0,0,2),(56,'526e98de92822cbf693553db06f3304c','c8af747606d90ae3f74b3632687ac052','b1adbc207fb3c728aa2f11ca402055f7','Screenshot 2021-12-07 at 9.34.24 AM.png','uploads/2021/12/a93c760feba1e41331025e8de88938b8.png',0,0,2),(57,'ae74727d67a5ec144c177cb740d0f656','c8af747606d90ae3f74b3632687ac052','f27b91086799982ce3c884945528d10c','Screenshot 2021-12-07 at 9.34.24 AM.png','uploads/2021/12/0b66b9a99b8cca34ac8d5a5e2b1f0147.png',0,0,2),(58,'330c6c44bf67a5ab70aca61be68a2cbc','c8af747606d90ae3f74b3632687ac052','4bcdc77f0b8894423f3d6d0a54994083','Screenshot 2021-12-07 at 9.34.24 AM.png','uploads/2021/12/55b0a581003550846505f2ae11367d7a.png',0,0,2),(59,'d7005c4fdec8ee6e7526a78487a2f142','6301ebca211419aba57b52d611ad68bc','0cb3a21486d7f959e761c48c2eaffffa','ID Copy.pdf','uploads/2021/12/d3763b612ea4d9c80d4d7516b682b4cf.pdf',0,0,2),(60,'f605433c2f6d8ed710e5eec6c4de6c37','6301ebca211419aba57b52d611ad68bc','b1adbc207fb3c728aa2f11ca402055f7','ID Copy (1).pdf','uploads/2021/12/1c92b97e6fe7fea6e27474ebe489de26.pdf',0,0,2),(61,'3e9155143d6f0b6bae0f37131f755b4d','6301ebca211419aba57b52d611ad68bc','4c2bd5a46ee626e11d4265b64d46a2d3','Antragsformular.pdf','uploads/2021/12/1084606626cccec96acd4e6a3739b5ac.pdf',0,0,2),(62,'dd1952dfaebc4d49246530fa2f7a9f11','6301ebca211419aba57b52d611ad68bc','73841240c70c48b411dd96f7e3edb09b','AVB\'s zum Antrag.pdf','uploads/2021/12/58f0981436ca575a15942f76a64064f2.pdf',0,0,2),(63,'6582bdcd230d6c7fd021b322d377a470','6301ebca211419aba57b52d611ad68bc','66b9060c9164555d36f6476096d5ccfd','Brief Kunde.pdf','uploads/2021/12/0b82b5fd324b30e37c74dabe3e36d944.pdf',0,0,2),(64,'fa84d1bbaa7b602b3aa1f18b2e12e006','6301ebca211419aba57b52d611ad68bc','251db070f92ada4447f2cc5eb500f25b','Elektronisch unterzeichnete Antragsunterlagen.pdf','uploads/2021/12/31deb2c37408dfdc54789d49ddfac329.pdf',0,0,2),(65,'b1a161650a30c4a201e7c7ba137b9f06','6301ebca211419aba57b52d611ad68bc','e531c6859262346d8b4dface6e6cc352','','',1,0,1),(66,'23f624dd9cf8bb87479e67162a816e2a','6301ebca211419aba57b52d611ad68bc','42b45022f4b45f292edafa229f151acc','','',1,1,2),(67,'349e5998ca24f16e784750c6295aa3d3','6301ebca211419aba57b52d611ad68bc','f86ee59ddd1ddaf36a14c5210c8d4d0f','Police.pdf','uploads/2022/01/e694f57f7180eba7f92fb63d94a64ff0.pdf',0,1,2),(68,'6136e05cec0dd5a8a41052ee6af9af4c','6301ebca211419aba57b52d611ad68bc','9b6e994a4bb38e3e7d8902bcd32d568d','Sven.Godel_bonafides_arbeitsvertrag .docx','uploads/2022/01/6cc53c8173e89f76b05d5d94378e21e6.docx',0,1,2),(69,'eb7fb72b47e508ac6777cf534a31176e','6301ebca211419aba57b52d611ad68bc','fd0afdf6f717598f32f3f1c626e3a6a8','Offerte.pdf','uploads/2022/01/ed5a36f29b18cb3726b5d657f69fbc45.pdf',0,0,2),(70,'dbfd7e1492fde139f247aa921bb8d551','6301ebca211419aba57b52d611ad68bc','bba7df0d8928c81521c04bb7fae3ca25','','',1,1,2),(71,'3d29db5e0e663fac120c05e15bca31f7','9b415898953169b96c3b559ce244efd2','0cb3a21486d7f959e761c48c2eaffffa','Screenshot 2022-01-16 at 4.25.19 PM.png','uploads/2022/01/cc075ccd1873dafdb256db375a55f31c.png',0,0,2),(72,'df700c7d7ad0398fc77b7e2d48ffea35','9b415898953169b96c3b559ce244efd2','e531c6859262346d8b4dface6e6cc352','Screenshot 2022-01-16 at 4.25.02 PM.png','uploads/2022/01/2c720c07c995554a0f145468b8ce1019.png',0,0,2),(73,'74a1915f4bc93f89a672961c9b8e4661','9b415898953169b96c3b559ce244efd2','42b45022f4b45f292edafa229f151acc','Screenshot 2022-01-16 at 4.25.02 PM.png','uploads/2022/01/3f77a993d06fa37f30bc6ce2744a9836.png',0,0,2),(74,'413c46327adc37104f8ff4f56f8a4dc4','9b415898953169b96c3b559ce244efd2','248c2bd1bfea65b487a04454ca1b2e11','Screenshot 2022-01-16 at 4.25.02 PM.png','uploads/2022/01/94791f4321eda518f8bba8a9763e1d2a.png',0,0,2),(75,'0abebd92c66a21eff453c3cd8ae6126a','9b415898953169b96c3b559ce244efd2','5119050f4514fce90d114575090ffde9','Screenshot 2022-01-16 at 4.25.02 PM.png','uploads/2022/01/c69e95e621b324d13c6a48e255bd7911.png',0,0,2),(76,'f125051d77232d38252b2e6dee42399f','9b415898953169b96c3b559ce244efd2','4c2bd5a46ee626e11d4265b64d46a2d3','Screenshot 2022-01-16 at 4.25.02 PM.png','uploads/2022/01/e9fa11377cf950b6da09fa9025787ac7.png',0,0,2),(77,'93bb144616f63454733906c65db14570','9b415898953169b96c3b559ce244efd2','73841240c70c48b411dd96f7e3edb09b','Screenshot 2022-01-16 at 4.25.02 PM.png','uploads/2022/01/fe982bb033c08f734b7f0168fbf89aa5.png',0,0,2),(78,'5d4385907aee82a4b5348cd1b6ebf770','9b415898953169b96c3b559ce244efd2','b6e0144690141c3327529b1d7c67cc7d','Screenshot 2022-01-16 at 4.25.02 PM.png','uploads/2022/01/f9bcf42c29cbecf691b45fdb4ab0b143.png',0,0,2),(79,'21c91f8cfd54ee4350d1fac5d839c81d','9b415898953169b96c3b559ce244efd2','66b9060c9164555d36f6476096d5ccfd','Screenshot 2022-01-16 at 4.25.02 PM.png','uploads/2022/01/23a0a67925eb12a2c302ca909ebf8fe8.png',0,0,2),(80,'b493ad9129a2dfa1745540f732319c6c','9b415898953169b96c3b559ce244efd2','251db070f92ada4447f2cc5eb500f25b','Screenshot 2022-01-16 at 4.25.02 PM.png','uploads/2022/01/ae7babefb34acf04ba3c89dac9ec7f6a.png',0,0,2),(81,'e8b7ea74bd480b088c48ae162137b773','9b415898953169b96c3b559ce244efd2','a3955ed04cf341a548e87c641528c3ad','Screenshot 2022-01-16 at 4.25.02 PM.png','uploads/2022/01/73c26a3b4ad3da3ab46eaacce980b4e0.png',0,0,2),(82,'d0a30a98938f4e164a56fb7f377637ba','9b415898953169b96c3b559ce244efd2','fd0afdf6f717598f32f3f1c626e3a6a8','Screenshot 2022-01-16 at 4.25.02 PM.png','uploads/2022/01/a067bafc51a3755496a331c6e310263a.png',0,0,2),(83,'a9ee898ed9af144ecd73a7a0bce2aa42','9b415898953169b96c3b559ce244efd2','f86ee59ddd1ddaf36a14c5210c8d4d0f','Screenshot 2022-01-16 at 4.25.02 PM.png','uploads/2022/01/a30f4424a3aa4add7a2fb1cfb07a7833.png',0,0,2),(84,'5377c25436ed17e48c2ca134e4d8cd5f','9b415898953169b96c3b559ce244efd2','bba7df0d8928c81521c04bb7fae3ca25','Screenshot 2022-01-16 at 4.25.02 PM.png','uploads/2022/01/cc79a78125d851e4a4fb26373a4073dc.png',0,0,2),(85,'46e96cd0d2c99fa2c2a941000730894b','9b415898953169b96c3b559ce244efd2','4c8f4f2ff98fdf1e48e6e645c069913d','Screenshot 2022-01-16 at 4.25.02 PM.png','uploads/2022/01/6b7ce3453db19ea256c26343f157fe49.png',0,0,2),(86,'a9544d02c5b1396a25ec6fe1c8aa6b40','9b415898953169b96c3b559ce244efd2','9b6e994a4bb38e3e7d8902bcd32d568d','Screenshot 2022-01-16 at 4.25.02 PM.png','uploads/2022/01/f2a72477e36f2baaacd729c179dd0d46.png',0,0,2),(87,'832520d0ca08a964c4d1c791ce774b24','9b415898953169b96c3b559ce244efd2','b1adbc207fb3c728aa2f11ca402055f7','Screenshot 2022-01-16 at 4.25.02 PM.png','uploads/2022/01/81c1cac8ac8288d4816304b0a75d2d67.png',0,0,2),(88,'2b20d0d015967788673cbd9feeb918f5','9b415898953169b96c3b559ce244efd2','f27b91086799982ce3c884945528d10c','Screenshot 2022-01-16 at 4.25.02 PM.png','uploads/2022/01/c3a90130e42492e1dd43a705f84057dc.png',0,0,2),(89,'6e817276e668707ac11ab0bd4de75c62','9b415898953169b96c3b559ce244efd2','4bcdc77f0b8894423f3d6d0a54994083','Screenshot 2022-01-16 at 4.25.02 PM.png','uploads/2022/01/50b80f0a3aee20681c71fcdfbe9d8df5.png',0,0,2),(90,'f6326e28632a4c092c002b066df505eb','6301ebca211419aba57b52d611ad68bc','f86ee59ddd1ddaf36a14c5210c8d4d0f','Police.pdf','uploads/2022/01/062c4ada8c9320a4d42420984d902c5c.pdf',0,0,2),(91,'3de42e1f4d5ce5576c57e1746a34025d','6301ebca211419aba57b52d611ad68bc','4c8f4f2ff98fdf1e48e6e645c069913d','Treuefonds Zertifikat.pdf','uploads/2022/01/0b0f4d5c294f59fb0dfcc9c80c745332.pdf',0,0,2),(92,'3f87ffa98e0e9261186752d00c0b3be4','6301ebca211419aba57b52d611ad68bc','9b6e994a4bb38e3e7d8902bcd32d568d','Zahlungsformulare (1).pdf','uploads/2022/01/5143de8ea0d8eb9ec84d1a41455a96cf.pdf',0,0,2),(93,'a4d2e207fc316b68298d25e3f0a544ef','6301ebca211419aba57b52d611ad68bc','059bd0f7d05ee9eb977cae2a50d720e9','Zahlungsformulare.pdf','uploads/2022/01/64ad575c5e9ec7ce46eab8fc44512d5c.pdf',0,0,2),(94,'e6b9bb1f86ef9a68c0e28e7dad1813b1','41b518e73067d75ac1add66b5d984e81','4c2bd5a46ee626e11d4265b64d46a2d3','Antragsformular.pdf','uploads/2022/01/435ce35a89dcdd688a5c6d815e96ee47.pdf',0,0,2),(95,'4be44c7684e8cc054f6e95cff495fb9f','41b518e73067d75ac1add66b5d984e81','73841240c70c48b411dd96f7e3edb09b','AVB\'s zum Antrag.pdf','uploads/2022/01/39a4fe09b1f8d6d73f1b78c00ada9dee.pdf',0,0,2),(96,'8b938288e36bd95a4d968161af607b6d','41b518e73067d75ac1add66b5d984e81','66b9060c9164555d36f6476096d5ccfd','Brief Kunde.pdf','uploads/2022/01/9b03c5fa6df25beddaa317210ddd0eb2.pdf',0,0,2),(97,'ac872ac41db6d1867dc39659adeff88a','41b518e73067d75ac1add66b5d984e81','251db070f92ada4447f2cc5eb500f25b','Elektronisch unterzeichnete Antragsunterlagen.pdf','uploads/2022/01/7800fcbc196bf225c921c42ed7bb67f0.pdf',0,0,2),(98,'d52361ab5f46ef12897c84446e5eb21f','41b518e73067d75ac1add66b5d984e81','fd0afdf6f717598f32f3f1c626e3a6a8','Offerte.pdf','uploads/2022/01/42925852cb2755380af69ce514087f43.pdf',0,0,2),(99,'cf40cbf89a3e0974beeb5913c4763a85','41b518e73067d75ac1add66b5d984e81','0cb3a21486d7f959e761c48c2eaffffa','Angebot+D1048817918.pdf','uploads/2022/01/6267ec508f94b818b511b413d82ce32c.pdf',0,0,2),(100,'364653efd5a226cb014d00c003953b48','41b518e73067d75ac1add66b5d984e81','b1adbc207fb3c728aa2f11ca402055f7','ID Copy (1).pdf','uploads/2022/01/11b5fce7d1a2e7589ff867e553fa21da.pdf',0,0,2),(101,'55bbe5009aa5310aaf7e45c9a5af3d2a','41b518e73067d75ac1add66b5d984e81','f86ee59ddd1ddaf36a14c5210c8d4d0f','Police.pdf','uploads/2022/01/f68e4749bbee062ac5143134eb95cddc.pdf',0,0,2),(102,'bf9f3b72eb101053ccb4af794c78b6d9','41b518e73067d75ac1add66b5d984e81','bba7df0d8928c81521c04bb7fae3ca25','','',1,0,2),(103,'5719ff6884abcf3fe431ed176fd528de','41b518e73067d75ac1add66b5d984e81','a3955ed04cf341a548e87c641528c3ad','Angebot+D1048817918.pdf','uploads/2022/01/9c94e035d295d89838ef9330a22338ed.pdf',0,0,2),(104,'f4662219bd2dda0200ded2b8484dff9b','41b518e73067d75ac1add66b5d984e81','e531c6859262346d8b4dface6e6cc352','','',1,0,2),(105,'2215e9bb47d935240a0fb3993519869a','739a472003a7d383db7c468437c1ecff','251db070f92ada4447f2cc5eb500f25b','','',1,0,2),(106,'c961bcab87ca4a36aa5274643a4a9833','739a472003a7d383db7c468437c1ecff','e531c6859262346d8b4dface6e6cc352','','',1,0,2),(107,'028aac6c2e478e3e26b418aa56c8e8d8','947aec9895ffa27d393d8fda6606d131','0cb3a21486d7f959e761c48c2eaffffa','','',1,0,2),(108,'099d825c4c580aa3c98b5abf8eb9a4e1','947aec9895ffa27d393d8fda6606d131','4c2bd5a46ee626e11d4265b64d46a2d3','','',1,0,2),(109,'86c4c18b5e566a297d48da227dc8f3f0','452724a302ae14ccaef100d12ca8bb54','4c2bd5a46ee626e11d4265b64d46a2d3','Antragsformular.pdf','uploads/2022/02/33c2ab5ddec92fe9b9583db456cf6cb0.pdf',0,0,2),(110,'58fdf484bbbf2e2722414233d475199e','452724a302ae14ccaef100d12ca8bb54','73841240c70c48b411dd96f7e3edb09b','AVB\'s zum Antrag.pdf','uploads/2022/02/dc6b957d86f0f9f0ed1fd547eaf094f9.pdf',0,0,2),(111,'9324b4b2393167b87db05a43c888c073','452724a302ae14ccaef100d12ca8bb54','66b9060c9164555d36f6476096d5ccfd','Brief Kunde.pdf','uploads/2022/02/d1de7fe171a1295bb65540d0c9182ca6.pdf',0,0,2),(112,'23a7f7e7111419ff585b6aa9a9fefe95','452724a302ae14ccaef100d12ca8bb54','251db070f92ada4447f2cc5eb500f25b','Elektronisch unterzeichnete Antragsunterlagen.pdf','uploads/2022/02/f4137c14773655ab47a48a96499d0496.pdf',0,0,2),(113,'a6637b64fb54ac6cb48ea7ac783a1fb5','452724a302ae14ccaef100d12ca8bb54','0cb3a21486d7f959e761c48c2eaffffa','ID Copy (1).pdf','uploads/2022/02/5c7dfc4bcb492f451c27749b3023c396.pdf',0,0,2),(114,'19e2b7cebcc98c16a68dfccf02511f1f','452724a302ae14ccaef100d12ca8bb54','b1adbc207fb3c728aa2f11ca402055f7','ID Copy.pdf','uploads/2022/02/8dc91b4fe2841fbc4df9cf341daa49cc.pdf',0,0,2),(115,'059587597eaf666681e7225b009d2f53','452724a302ae14ccaef100d12ca8bb54','fd0afdf6f717598f32f3f1c626e3a6a8','Offerte.pdf','uploads/2022/02/47593bce439a581044e274b7ad5fdfe8.pdf',0,0,2),(116,'8f7a92aef82103e6c8ac69baee388adb','452724a302ae14ccaef100d12ca8bb54','f86ee59ddd1ddaf36a14c5210c8d4d0f','Police.pdf','uploads/2022/02/31d509f8de1c9dc2b8c331b405cffbe1.pdf',0,0,2),(117,'1f10d437914651fe2756d28ecb8bf6c8','452724a302ae14ccaef100d12ca8bb54','bba7df0d8928c81521c04bb7fae3ca25','','',1,0,2),(118,'2319b8c7562976eb444fbd91ed8f3108','452724a302ae14ccaef100d12ca8bb54','4c8f4f2ff98fdf1e48e6e645c069913d','Treuefonds Zertifikat.pdf','uploads/2022/02/c7d41b648faec55be346d97d86049ae9.pdf',0,0,2),(119,'ba9697e663b23d4894881d0336baa4ee','452724a302ae14ccaef100d12ca8bb54','9b6e994a4bb38e3e7d8902bcd32d568d','Zahlungsformulare (1).pdf','uploads/2022/02/5357d607db52591f214f4f1bf42af3f6.pdf',0,0,2),(120,'e8e4e47205747369ac5d868ad0cdc846','452724a302ae14ccaef100d12ca8bb54','059bd0f7d05ee9eb977cae2a50d720e9','Zahlungsformulare (2).pdf','uploads/2022/02/ab1b9b16f6556ff070dcddf945e35729.pdf',0,0,2);
/*!40000 ALTER TABLE `policy_documents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provider_types`
--

DROP TABLE IF EXISTS `provider_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_types` (
  `provider_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `type_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provider_types`
--

LOCK TABLES `provider_types` WRITE;
/*!40000 ALTER TABLE `provider_types` DISABLE KEYS */;
INSERT INTO `provider_types` VALUES ('abed6856143b63959da816ce8bda2b92','291f0a6f79cf308ff5a7ea16faeab662'),('abed6856143b63959da816ce8bda2b92','1946ffcc0b0c64ff1673fdb7063f31cd'),('abed6856143b63959da816ce8bda2b92','6fad65e0a6afd858c8cbdfc8de348979'),('2aa77eb50fa5fa7a06e803e0707f0537','291f0a6f79cf308ff5a7ea16faeab662'),('2aa77eb50fa5fa7a06e803e0707f0537','1946ffcc0b0c64ff1673fdb7063f31cd'),('dd1d66384badf3d31e219e32451e4232','291f0a6f79cf308ff5a7ea16faeab662'),('333be058de3b9cf1075b527bd2699508','291f0a6f79cf308ff5a7ea16faeab662'),('99f4dda2aa196681f843fcdb410f0512','291f0a6f79cf308ff5a7ea16faeab662'),('99f4dda2aa196681f843fcdb410f0512','1946ffcc0b0c64ff1673fdb7063f31cd'),('99f4dda2aa196681f843fcdb410f0512','6fad65e0a6afd858c8cbdfc8de348979'),('7bf304083293e10c8ecd16a39298fd3c','291f0a6f79cf308ff5a7ea16faeab662'),('7bf304083293e10c8ecd16a39298fd3c','1946ffcc0b0c64ff1673fdb7063f31cd'),('7bf304083293e10c8ecd16a39298fd3c','d7e3d5e3e904ac9865fb6d017eddc8d5'),('79d154488772b77d72d433a1002c81c0','291f0a6f79cf308ff5a7ea16faeab662'),('ad48a67d7a23d304eed1c85a1b0e91b9','291f0a6f79cf308ff5a7ea16faeab662');
/*!40000 ALTER TABLE `provider_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `verified` tinyint(1) DEFAULT '0',
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_number` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_image` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `language` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT 'de',
  `is_employee` tinyint(1) DEFAULT '0',
  `is_admin` tinyint(1) DEFAULT '0',
  `commission_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `is_blocked` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_user_id_unique` (`user_id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'27f95bdf760942950b0d6c9d6796b445','CRM Admin','admin@crm.com',NULL,1,'$2y$10$1/DN6h309O/Y94nh6Rb7kOoD16gkijaqCtS7JHaYi85.RFTemd5wy','8799988765','uploads/2021/12/6662a4e7dd1a0730163798629332fe88.png','en',0,1,NULL,1,0,0,'tGrMQw7qDrVP2Thw8EkhgGWotRksF5iNRh1zQdZ9UblZQBN3rgfhlhL1TerH','2021-08-25 15:57:38','2022-01-26 16:04:51'),(2,'3f2bcdb1bc6b721efec844a6534c0d09','CRM Employee','employee@crm.com',NULL,1,'$2y$10$oPJcN/rbC1mSNxpdbxOQQeyu9QvUrus.XnlVbIeFcIUnYbYa9y0IC','12313213','uploads/2021/12/efd893a7323c9d556abf1d27f51615ac.txt','en',1,0,'ceaac810bd16040a35a0eb284d07e757',1,0,0,'MHcgcZ1rNoXMd34jyVKx9DSzgpqEl5XaNatZWLQKnyBCJELJBKEPgk1E7GhT','2021-08-25 15:58:30','2022-02-02 20:43:52'),(3,'28742471297e06c0a38673f55e1a589c','CRM Users','user@crm.com',NULL,1,'$2y$10$iXcU0Cpd2RzcppgP8TPPvOBwLPW3HHHnaJLLanvzJ5fpsyxFC3Dx.','9377839587','nutan.png','de',0,0,'ceaac810bd16040a35a0eb284d07e757',1,1,1,NULL,'2021-08-25 16:01:36','2021-11-01 19:01:55'),(5,'425e4371585358f68e4a8f98d58a33d5','Jitendra Prajapati','jitendra@crm','2021-10-22 15:08:47',1,'$2y$10$bHkW1IDKhRDLSd1dJ2/w3uAzVh6Vd0pSBNW6mGDP6xrt5FebVFgp6',NULL,NULL,'de',0,0,NULL,1,1,1,NULL,'2021-09-19 12:10:44','2021-11-01 19:01:57'),(6,'640f287f2868e7352e3acc98609e1abd','James','james@crm.com',NULL,0,'$2y$10$TWxkLuu95t5JZ0AIr6EgQeWBzQhP7EGu1GVS4jOqNosmaWLXF3zI2','7405091187',NULL,'de',1,0,NULL,1,1,1,NULL,'2021-09-29 01:34:06','2021-11-01 19:02:06'),(8,'f3cf34e4cfbbb8a0303b437b5c4480ca','James','james1@crm.com',NULL,0,'$2y$10$bgRFqdQfGKlFYJfdy4KWyu.2wDDzfX/ESC2shGdt5OiBBjdNv13aS','7405091187',NULL,'de',0,1,NULL,1,1,1,NULL,'2021-09-29 01:37:21','2021-11-01 19:01:59'),(9,'001c12f33b055853e5dd169ea1971e73','jitendra','me.jitendra1986q@gmail.com',NULL,0,'$2y$10$mTycpJbPLP2UMbkBd02XQ.R50.OBFzZwEiglVAoZ412tFlcjhtG0G','7405091187',NULL,'de',1,0,NULL,1,1,1,NULL,'2021-09-29 01:40:32','2021-11-01 19:02:09'),(10,'aaf9f6919e4f6b6f68af65d58507ae51','Jitendra Prajapati','me.jitendra1986sss@gmail.com',NULL,1,'$2y$10$EfIYmIRz26YoSBkgLJWzAe6G6GfRmEg7/poyvydF9YGeuNUyGxWWe','7405091187',NULL,'de',1,0,NULL,1,1,1,NULL,'2021-09-29 01:56:19','2021-11-02 17:50:24'),(11,'01f0357a502b9e99603fb5460b87a2d0','Jitendra CRM','jitendra2@crm',NULL,0,'$2y$10$MlNEzbH1CuuRSWipR1tqM.8QyzYVmZWJOZ954FR/vvBwLwpHx36SG','214213123',NULL,'de',1,0,'ceaac810bd16040a35a0eb284d07e757',1,0,1,NULL,'2021-09-29 02:25:09','2021-11-03 22:01:19'),(12,'c7179eda339388af70a03af238e14225','Jitendra CRM','jitendra23@crm',NULL,0,'$2y$10$wGn02rlVUM9sk.DkXQdIgO8Jm7ukzKyN99uOz4il6ta8BDkIBFBAK',NULL,NULL,'de',0,0,NULL,1,0,1,NULL,'2021-09-29 02:27:05','2021-11-03 22:01:15'),(13,'bf6d2e1910cd2cea4e9b4b7a2f68bce8','Jitendra CRM','jitendrawewe@crm',NULL,0,'$2y$10$Wd/t7SUXQMNZp88VoMnMeu294VtR0SOFekS7aJqA9X.yrxIVENwI6',NULL,NULL,'de',0,0,NULL,1,1,1,NULL,'2021-09-29 02:28:24','2021-11-01 19:02:41'),(14,'9fc1fb408d6f9597c4cc1c9fbe90b926','Jitendra CRM','jitendraewewe@crm',NULL,0,'$2y$10$yyphRJUBnhtMwSQk.xUgpOQnUBDQhDO3PW09VXAV6UE.Q0oYcs8t2',NULL,NULL,'de',0,0,NULL,1,1,1,NULL,'2021-09-29 02:29:48','2021-11-01 19:02:50'),(15,'0cf87ec14c9017cd3646607805c58290','Jitendra CRM','jitendradsa32@crm',NULL,0,'$2y$10$g2p9eeq7tdt4ZGy8Yd6V/.fP8UQS17rb1gH/mUkZbyK5kDl//r.tK',NULL,NULL,'de',0,0,NULL,1,1,1,NULL,'2021-09-29 02:31:37','2021-11-02 17:50:33'),(16,'c0d3294ac90c3b40e9cc3376c02ec023','Freddins Pulikoden','f.pulikoden@bonafides.ch',NULL,0,'$2y$10$adgWcxjVDd/4SEZyukuomuTVZXdrlS5TU5kwkyTS7zVItT/ECcBLC','0764168401','uploads/2021/11/a3b4401b9e3f248551e9416e9db9f78c.jpeg','en',1,0,'1479ea5b34feb3f17482d8eddd0f4b84',1,0,0,'nbGEioj9LxodzpsRq8NVnznTtUFWiNNOKyrDrOmVBLRMwvxbkWSNtzJpjTnZ','2021-10-11 15:00:34','2022-02-02 03:56:44'),(17,'bb19ea2bd059f868fdf91bc6eaa51f29','James Rathod','jitendrasafdfasdsafd@crm',NULL,0,'$2y$10$Cx7yrR62gw3NPTF7aaG24eOiOV1hEKI18JiDRKkDZT2zb.NGlEqJG',NULL,NULL,'de',0,0,NULL,1,0,1,NULL,'2021-10-18 11:29:18','2021-11-01 19:02:30'),(18,'372a1776a9ee92cab4c08068cf9f757a','James Rathod','jitendraasssafdfasdsafd@crm',NULL,0,'$2y$10$sm979qNHDzlw4JkeOs.p9uIA2IowIe313624.SF8wtpaYWRKvsSX.',NULL,NULL,'de',0,0,NULL,1,0,1,NULL,'2021-10-18 11:41:58','2021-11-01 19:01:03'),(19,'feb733d7348e629592a266e9e9886003','James Rathod','jitesdfdndraasssafdfasdsafd@crm',NULL,0,'$2y$10$xMBC9tBpruJfvQioQsBwTeyE1drvLPm4UE2JTqhFbxLJaYybdvzC6',NULL,NULL,'de',0,0,NULL,1,0,1,NULL,'2021-10-18 11:44:20','2021-11-01 19:01:08'),(20,'880ea1f5a3efa3cb98b0bc4faad5534d','Jit Prajapati','jitendradsasa32@crm',NULL,1,'$2y$10$KGZLOB/t5EN2axm8vVx8TeqXHs7q7NldifQlbCTlFY7FJkNfb45I2',NULL,NULL,'de',0,0,NULL,1,0,1,NULL,'2021-10-18 12:08:03','2021-11-01 19:01:12'),(21,'cf30059aec3b3a85832e31b1a36b23e8','Jitendra CRM','sdfsd@crm',NULL,0,'$2y$10$als6sHBeHonRqOZdt8gKW.7AI4hz0qkB8ISrLlY2lCYgYmNzEDspK',NULL,NULL,'de',0,0,NULL,1,0,1,NULL,'2021-10-18 12:16:17','2021-11-01 19:01:15'),(22,'2798c22855e679dbf7b342fb85657f99','James Rathod','ssadsddfsd@crm',NULL,0,'$2y$10$pjO3tPNu0QRGzJmV9JYbhO3E3jk5Ye5/abLXwnDG8cmZoaL7VHjvy',NULL,NULL,'de',0,0,NULL,1,0,1,NULL,'2021-10-18 12:18:31','2021-11-01 19:01:21'),(23,'98899e2a7ba1986a5f926479e1fe5651','safsd','abdc@1234.com',NULL,0,'$2y$10$BwBpJH/lWBKlHv9duKtHx.jDmuPoQKsR.LJFrgT1tT9W9C2upYHjS',NULL,NULL,'de',0,0,NULL,1,0,1,NULL,'2021-10-18 12:19:19','2021-11-01 19:00:14'),(24,'1a805699d780cdb05ec15a880e612797','Jitendra Prajapati','jitedndra@octopod.co.in',NULL,0,'$2y$10$A2mM8dClgwzjEhK9xthr4etanFN8Jf1k5XnWpuyPQBP5KFtjKZVw2',NULL,NULL,'de',0,0,NULL,1,0,1,NULL,'2021-10-18 12:22:25','2021-11-01 19:00:18'),(25,'60d8f6ebe7d23f585b1af105fbbf88bd','Jitendra CRM','abdc@1dsf234.com',NULL,1,'$2y$10$4wKQDIjzRVn/4cOfAP91f.T71XeCXAvC61jXDhrSGVHzR9ru.2BrS',NULL,NULL,'de',0,0,NULL,1,0,1,NULL,'2021-10-18 12:24:47','2021-11-01 19:00:32'),(26,'713106b7207634c208a2cb7ef01c53dc','Jitendra Prajapati','jitendra@octopod.co.in','2021-10-22 15:08:47',0,'$2y$10$zEP.U47aQ/LHSXZXYy08Fu8nl5JVnejMlu6NtCofe59VmqjdNLfdm',NULL,NULL,'de',0,0,NULL,1,0,1,NULL,'2021-10-18 12:55:17','2021-11-01 19:00:37'),(27,'c34f73c76db286fa8a4663646214b045','Raj Rathod','raj.rr.rathod94@gmail.com','2021-10-22 15:08:47',0,'$2y$10$ialbhuLNjhGqZ4aMGhdujuA1iVmawxVndjYSOrmKVWoN9SkDuqLJ6','7874983351',NULL,'de',1,0,'ceaac810bd16040a35a0eb284d07e757',1,0,1,'bv9WDtSR4w0ZE8OqLwfMNYs3eI0Zw7aYK9zVPgdRyaQ9gH3rf3soSiLvSOPG','2021-10-20 20:35:27','2021-11-12 13:47:44'),(28,'8feb06dddf9fc0e781ec0d9a84c724c9','Pooja Sodha','poojasodha1@gmail.com',NULL,0,'$2y$10$iCKCa9NqocBIxRKKFj8DBuQ3TMiEHyxgsX2dJPG5lsYKFdwUAMYGW','7874983351',NULL,'de',1,0,'ceaac810bd16040a35a0eb284d07e757',1,0,1,NULL,'2021-10-20 22:18:39','2021-11-01 19:00:02'),(30,'925257d90393ff2e99307bd24eb13556','Test User','testingforanything@crm.com',NULL,0,'$2y$10$FTKl/Z2tbaXSKHKP1CiPoeNhSZdgb7twtK9AxIidkLlwNHOOX1cBK','234234234',NULL,'de',0,0,'ceaac810bd16040a35a0eb284d07e757',1,0,1,NULL,'2021-10-21 11:38:22','2021-11-01 18:59:56'),(31,'440be997124b4537ebe58716b86862f3','Harsh Sodha','raj@poojacreators.com','2021-10-22 15:08:47',0,'$2y$10$LRgrJYWEG6kp7teiKVbAIes4Tx/BnFxNvYxaSA4qlF6o9htqfQsma','7874983351',NULL,'de',0,0,NULL,1,0,1,NULL,'2021-10-22 15:08:06','2021-11-01 18:59:53'),(32,'ceb31e9e399ec3381eeeb19157e87369','Robin Fernandes','pooja@poojacreators.com',NULL,0,'$2y$10$JlIcHq.OUhIRq/dit2rXmOC4v06sy.DaURe46s/GHnOwqnSNp0E8W','94242710',NULL,'de',0,0,NULL,1,0,1,NULL,'2021-10-22 16:32:39','2021-11-01 18:59:47'),(33,'8ecfa48043ba9cf2a3b6148ac2d736d3','Raj Rathod','ceopoojacreators@gmail.com','2021-10-25 09:09:23',0,'$2y$10$sHsrndUYyaGAef70U6gF.uFsHLyTsChdLaSbjFJgzWeiGCko0plFO',NULL,NULL,'de',0,0,NULL,1,0,1,NULL,'2021-10-25 09:08:48','2021-11-01 18:55:58'),(34,'1eb4427366ba583535b3a58403415ab7','Freddins Pulikoden','freddins_95@hotmail.com',NULL,0,'$2y$10$jUqueGDUF4qG9tsGXmyWnuagCZOrYFAK16wDKNYjHo3A27VPWIK7m','0764168401',NULL,'de',1,0,'1479ea5b34feb3f17482d8eddd0f4b84',0,1,1,NULL,'2021-11-01 02:33:34','2021-11-10 20:41:51'),(35,'3d06c69c9e8f74cf2d07e9741fbcf50a','Shakira Zihlmann','info@bonafides.ch','2021-11-01 17:06:48',0,'$2y$10$KByK5HqWmG4ou.Mi5e9J5.3cFu4cNT6z39ahT8diJOeiIUp.p.n6i','123456784',NULL,'de',1,0,'ceaac810bd16040a35a0eb284d07e757',1,0,1,NULL,'2021-11-01 17:06:23','2021-11-04 14:24:15'),(36,'a7da561c7d3b1ce66078a587e6e7fe8f','Christ Vunabo','c.vunabo@bonafides.ch','2021-11-03 19:54:26',0,'$2y$10$msZvNCIExmOmA4WfOCbS.ODAAxMx.wmcSbOMQGAhzKp/TUHNGmGgK','0764628897','Christ.jpg','de',1,0,'f4413071de6bf85dae5aae818258adbb',1,0,0,'TNNU2RKrpi2JN9DSc1DQR1eww7EJzwvXVMFYmhUYzZsa3Q5qg8HgeMRXlLdl','2021-11-03 19:53:57','2021-11-04 14:17:57'),(37,'bf34cd178ba8826c6562e600c3098eb2','Dario Giacomo','d.giacomo@bonafides.ch','2021-11-03 20:04:45',0,'$2y$10$hNx8dlQ88fU75NlCz1hEhOHmb3VRtz5EKaEBKg/q/.O.TNWkKJ7ey','079 944 54 79','Bild Dario.jpg','de',1,0,'1783bfbe363f3246347f74858438e8be',1,0,0,NULL,'2021-11-03 20:04:05','2021-11-03 20:11:05'),(38,'56c2a57f5c98d3782c083d2e9a9e64c9','Starlin Javier Santana Moncion','j.santana@bonafides.ch',NULL,0,'$2y$10$8vG.RxX/4nHLKUnkthxck.58VVEv0q9EgDaoG7tpEZMJUnAEBlSIK','+41763402225','Javier.jpg','de',1,0,'9877611cf457e96060d5a1c80899d4b7',1,0,0,NULL,'2021-11-03 21:06:21','2021-11-08 15:48:31'),(39,'040515d719fc42f9e433c15c17e72acb','Prince Clever','p.clever@bonafides.ch','2021-11-03 21:22:22',0,'$2y$10$9LQyD.wx34FHyZ0lGp9/luUPlhE4Az6ZI8u9lxGICo.YnIrr/R54q','0779663753',NULL,'de',1,0,'9b71190d0de4a15cdae02080b2e6bd45',1,0,0,'ACnp3FqfCdjbmEoRK4KL1m2cs1w1c36rsHdIkhaGStxj3lkoJBdVC35Xg3kR','2021-11-03 21:19:47','2021-11-08 15:48:45'),(40,'0b7238054bcd686437692c61a96d20a8','Gody Mordeku','g.mordeku@bonafides.ch',NULL,0,'$2y$10$40viG5ZV7ZPr9fqy/HxsEuKbY0ZIjn3ROSBRu7iRYAf7876HJTVX6','0763196779',NULL,'en',1,0,'6001f3daa0fa40b88d5e07e59cfd2979',1,0,0,NULL,'2021-11-09 02:28:58','2021-12-29 16:28:28'),(41,'1b6f71e8d9521fc0eb64070971707ad4','Luigi Barberio','l.barberio@bonafides.ch',NULL,0,'$2y$10$oIDfVqxkJdd8S0NAjghin./yajWpaEykTMBFybsAlPoFXgl2O1YA.','0762812000',NULL,'de',1,0,'ceaac810bd16040a35a0eb284d07e757',1,0,0,NULL,'2021-11-09 14:48:05','2021-11-10 18:01:25'),(42,'c28e80691a52447ae1fd8795ac38f2a0','Cammarano Aurelia','a.cammarano@bonafides.ch',NULL,0,'$2y$10$CTEjCJ.3yp73dmIaViIhne8NHbV2a5iolrNZaNrolh0Ix0AJ6SVam','0798502532',NULL,'de',1,0,'f4413071de6bf85dae5aae818258adbb',1,0,0,NULL,'2021-11-10 17:30:27','2021-11-10 18:00:13'),(43,'68804cab9470b5965ce9f116f033541e','Giancarlos Dominguez','G.dominguez@bonafides.ch',NULL,0,'$2y$10$Ud/UMauzwf9SPJTjXhxFWuNwbZqF8SLt3w4hMaHOllMFw.Za5.Xeu','0791715883',NULL,'de',1,0,'1783bfbe363f3246347f74858438e8be',1,0,0,NULL,'2021-11-11 17:42:57','2021-11-12 13:48:20'),(44,'9cda09cdd2a119a328fa83f326aa8f02','Mac','mittalshah1312@gmail.com',NULL,0,'$2y$10$4gYwYyJcrE933McEfiAcweHajMYygsdl2NRyYzAWvPhr4jtPwg4sq',NULL,NULL,'en',0,0,NULL,1,0,0,'bCn2CBEuIVOMgHfBlY73HT8LTWlffdVEyPb4wlhBW3Wj8hkQJlfA0XONILdT','2021-12-07 08:44:59','2021-12-07 09:05:48'),(45,'149e5bfabae6331ccc537494fe35280c','Elena Castillo','e.castillo@bonafides.ch',NULL,0,'$2y$10$cRy9fxuAlCLwnRuce2d3oem7W8W3Ckb./rjiq6GaEM7XnRTQ902YS','0764162736','uploads/2022/01/244c54d790255be59c957dfa94990339.jpeg','de',1,0,'6001f3daa0fa40b88d5e07e59cfd2979',1,0,0,NULL,'2022-01-03 14:50:23','2022-01-03 14:53:26'),(46,'2fb0ad9a07e841faebcd6f8a2b7eb679','Divyang','divyangchauhan.cmpica@gmail.com',NULL,0,'$2y$10$x3mQmo7QmmwQCFQehPb6e.BavzjF/UN1WzMI.PkQ16F/8xWvuGFJi',NULL,NULL,'de',0,0,NULL,1,0,0,'1GVK1rkqZaolgJ4xcO3KStb2sBE0vnLe2VkHg87oq34gEp2FNGVRvZQ00Cip','2022-01-19 12:29:12','2022-01-25 18:00:07'),(47,'78eff3ec809f305f79af4d169dddec79','Katiuschka Müller','k.mueller@bonafides.ch',NULL,0,'$2y$10$4NKXqu1MgKvBS/OeF5zWfOTsupXtWKk7Fl69LlTA6U0ZqjZNlzLCe','0764144800',NULL,'de',1,0,'ceaac810bd16040a35a0eb284d07e757',1,0,1,NULL,'2022-01-21 17:58:44','2022-01-21 18:03:31'),(48,'92fb46b64be871780bd6f26f3f6e9473','Katiuschka Müller','k.muller@bonafides.ch',NULL,0,'$2y$10$eC1qW722q/ApvC8Prw00wewV.CSiauG7Zo8Uw3FF12aUm3UK1/GNe','0764144800',NULL,'de',1,0,'ceaac810bd16040a35a0eb284d07e757',1,0,0,NULL,'2022-01-21 18:04:03','2022-01-21 18:04:38');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `verify_users`
--

DROP TABLE IF EXISTS `verify_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `verify_users` (
  `user_id` int NOT NULL,
  `token` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `verify_users`
--

LOCK TABLES `verify_users` WRITE;
/*!40000 ALTER TABLE `verify_users` DISABLE KEYS */;
INSERT INTO `verify_users` VALUES (6,'0e990d1de1c9a4de43412bc5e0ee07701858f18b','2021-09-29 01:34:07','2021-09-29 01:34:07'),(8,'43d035c693d6bf6a2b324e548dd036858b9b6a8d','2021-09-29 01:37:21','2021-09-29 01:37:21'),(9,'201470749466db72dbe2efc476c47b81a66966d1','2021-09-29 01:40:33','2021-09-29 01:40:33'),(10,'8b8908b223a5ba8db9654a0ba8f54f3871661997','2021-09-29 01:56:19','2021-09-29 01:56:19'),(12,'f29cfa580201a61706ab31974d847c9ceaccbd51','2021-09-29 02:27:05','2021-09-29 02:27:05'),(13,'2d07670632127d90e93c97661f9e280b317fee0d','2021-09-29 02:28:25','2021-09-29 02:28:25'),(14,'78e206e85fc70b38a86b11d8dff1af050fcba417','2021-09-29 02:29:48','2021-09-29 02:29:48'),(15,'7a20756fb53de398e693f9c9adb5e5246f627f75','2021-09-29 02:31:38','2021-09-29 02:31:38'),(16,'f8102ce0ab4a17116269e678d565f87890099816','2021-10-11 15:00:34','2021-10-11 15:00:34'),(17,'62b9cc428531af6b638dfdae471d1847e471326d','2021-10-18 11:29:19','2021-10-18 11:29:19'),(18,'088106cb4fc7f11ebc90105d9776d44d65e5343a','2021-10-18 11:41:59','2021-10-18 11:41:59'),(19,'e90a55ac172b6f22d22ebdfb9ef503d809e3f648','2021-10-18 11:44:20','2021-10-18 11:44:20'),(20,'28cbc15f0c59fd6c607f9ed2e09feefcc61fc334','2021-10-18 12:08:03','2021-10-18 12:08:03'),(21,'37e9c2b66177bd5207c7007af8aa3161d6ebfef7','2021-10-18 12:16:17','2021-10-18 12:16:17'),(22,'271dac5e3581178b85cdb7066c8fec99c8ac06a0','2021-10-18 12:18:31','2021-10-18 12:18:31'),(23,'800303e410d21b4609bc17066978500ce46f0fca','2021-10-18 12:19:19','2021-10-18 12:19:19'),(24,'f45411753965c101dc8e2c4e33da8821a4ff7e43','2021-10-18 12:22:25','2021-10-18 12:22:25'),(25,'22471e65bedaff454d0d22ba3130e42e682ffbaa','2021-10-18 12:24:47','2021-10-18 12:24:47'),(26,'a83902a8bdfeea7009a281dcd384b58035ff46ff','2021-10-18 12:55:17','2021-10-18 12:55:17'),(27,'a54f98797facd223e5ee4592f56333b7b8bf3440','2021-10-20 20:35:27','2021-10-20 20:35:27'),(28,'decfe6f58c7a793f431cb9dc9da07554935292d6','2021-10-20 22:18:39','2021-10-20 22:18:39'),(30,'966b1a5e307a0bf63fca59af86348d5a19d15448','2021-10-21 11:38:23','2021-10-21 11:38:23'),(31,'1df5b0af0ba9f675aeb76541333d44c66612097d','2021-10-22 15:08:06','2021-10-22 15:08:06'),(32,'85fefb3865910dacfa0ab37697bdb6d64207632e','2021-10-22 16:32:39','2021-10-22 16:32:39'),(33,'d25a8916a8d1be45bfc6526333ab9470e90f67e2','2021-10-25 09:08:48','2021-10-25 09:08:48'),(34,'1cdec9295af93e7b14986e0923b50d656da72c8b','2021-11-01 02:33:34','2021-11-01 02:33:34'),(35,'7dffbf66bf6a266def6321d200c0abb0a0736581','2021-11-01 17:06:23','2021-11-01 17:06:23'),(36,'ba27183148c32065b1fed26ad7fcef55ceb7ed12','2021-11-03 19:53:57','2021-11-03 19:53:57'),(37,'d71e43dac59f267ad824a54a9dde27b0d0403849','2021-11-03 20:04:05','2021-11-03 20:04:05'),(38,'4530be2fe27e448d386b1d126557b08a470d3aec','2021-11-03 21:06:21','2021-11-03 21:06:21'),(39,'fd42dcc2ae23fa9abf6b5689d72b52a37f5a2a32','2021-11-03 21:19:47','2021-11-03 21:19:47'),(40,'b037039c28b8fbb4f9673b56a79f383f0d650fbb','2021-11-09 02:28:58','2021-11-09 02:28:58'),(41,'2d6b8ae08995fe5a3d9224dce10a7324c6179055','2021-11-09 14:48:05','2021-11-09 14:48:05'),(42,'9367c6473ad224b0273f2706f38b9f462da0785a','2021-11-10 17:30:27','2021-11-10 17:30:27'),(43,'ceff2ef9adb53025323332e24fc8eaafc6331555','2021-11-11 17:42:57','2021-11-11 17:42:57'),(44,'089a53063adb4482a5800f2a713f6f47831e3f69','2021-12-07 08:44:59','2021-12-07 08:44:59'),(45,'1f175a32efbb852a1caab0c9aa1f6833f3349a35','2022-01-03 14:50:23','2022-01-03 14:50:23'),(46,'c2f9c5961f8e0fdcadf514337b5ebf7d5e9a9c73','2022-01-19 12:29:12','2022-01-19 12:29:12'),(47,'f8fac67f2ac7ad4d9cfe1898de943878e11d13a3','2022-01-21 17:58:44','2022-01-21 17:58:44'),(48,'546e929b8f581ac7da603970b06ab7884dbb0be3','2022-01-21 18:04:03','2022-01-21 18:04:03');
/*!40000 ALTER TABLE `verify_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'srcdb'
--

--
-- Dumping routines for database 'srcdb'
--
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-02-07 23:17:12

<?php

namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->isLocal()) {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) {
            if (Auth::check()) {
                $communicationCounts = DB::table('communication_employee')
                    ->leftJoin('communication', 'communication.com_id', '=', 'communication_employee.communication_id')
                    ->where(['employee_id' => Auth::user()->user_id, 'is_viewed' => 0, 'communication.is_deleted' => 0])
                    ->count();
                $view->with('communicationCount', $communicationCounts);
                $chatCount = DB::table('chat_communication')
                    ->where(['replied_by' => Auth::id(), 'is_viewed' => 0])
                    ->count();
                $view->with('chatCount', $chatCount);
                if(Auth::user()->is_admin) {
                    $reworkCount = DB::table('customer_insurance')
                        ->select('customer_insurance.*', 'customers.*', 'employee.name as employeeName', 'approver.name as approverName', 'insurance_providers.label_en as insProvider', 'insurance_types.label_en as insType')
                        ->leftJoin('customers', 'customers.customer_id', '=', 'customer_insurance.customer_id')
                        ->leftJoin('insurance_providers', 'insurance_providers.provider_id', '=', 'customer_insurance.provider_id')
                        ->leftJoin('insurance_types', 'insurance_types.type_id', '=', 'customer_insurance.type_id')
                        ->leftJoin('users as employee', 'employee.id', '=', 'customer_insurance.employee_id')
                        ->leftJoin('users as approver', 'approver.id', '=', 'customer_insurance.updated_by')
                        ->where(['customer_insurance.is_dead' => 0, 'customer_insurance.is_deleted' => 0, 'customer_insurance.insurance_status' => 'In Progress'])
                        ->count();
                }else if(Auth::user()->is_employee){
                    $reworkCount = DB::table('customer_insurance')
                        ->select('customer_insurance.*', 'customers.*', 'employee.name as employeeName', 'approver.name as approverName', 'insurance_providers.label_en as insProvider', 'insurance_types.label_en as insType')
                        ->leftJoin('customers', 'customers.customer_id', '=', 'customer_insurance.customer_id')
                        ->leftJoin('insurance_providers', 'insurance_providers.provider_id', '=', 'customer_insurance.provider_id')
                        ->leftJoin('insurance_types', 'insurance_types.type_id', '=', 'customer_insurance.type_id')
                        ->leftJoin('users as employee', 'employee.id', '=', 'customer_insurance.employee_id')
                        ->leftJoin('users as approver', 'approver.id', '=', 'customer_insurance.updated_by')
                        ->where(['customer_insurance.employee_id' => Auth::id(), 'customer_insurance.is_dead' => 0, 'customer_insurance.is_deleted' => 0, 'customer_insurance.insurance_status' => 'In Progress'])
                        ->count();
                }
                $view->with('reworkCount', $reworkCount);
            }
        });
        Schema::defaultStringLength(191);
    }
}

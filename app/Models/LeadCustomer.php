<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeadCustomer extends Model
{
    use HasFactory;

    protected $table = "lead_customer";
    protected $fillable = ['lead_id'];

}

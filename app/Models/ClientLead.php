<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientLead extends Model
{
    use HasFactory;

    protected $table = 'client';

    protected $casts = [
        'step1' => 'array',
        'step2' => 'array',
        'step3' => 'array',
        'step4' => 'array',
        'step5' => 'array',
        'step6' => 'array',
        'step7' => 'array',
        'step8' => 'array',
        'step9' => 'array'
    ];
}

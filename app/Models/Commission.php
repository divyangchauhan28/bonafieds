<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Commission extends Model
{
    use HasFactory;

    public static function calculateCommission($policyId)
    {
        $policy = DB::table('customer_insurance')
            ->select('customer_insurance.*', 'commissions.commission_id', 'commissions.approach')
            ->leftJoin('users', 'users.id', '=', 'customer_insurance.employee_id')
            ->leftJoin('commissions', 'commissions.commission_id', '=', 'users.commission_id')
            ->where('ci_id', $policyId)
            ->first();
        $monthy = $policy->insurance_amount;
        $year = 12;
        $duration = $policy->insurance_duration;
        $approch = $policy->approach;
        $fixThousund = 1000;
        $fixTwo = 2;
        $fixTwenty = 20;

        switch($policy->type_id){
            case '291f0a6f79cf308ff5a7ea16faeab662': //life
                $policyAmount = ($monthy * $year) * $duration; // 200 * 12 * 40(year) = 96000   
                $newCal = $policyAmount / $fixThousund; //96000 / 1000 = 96 
                $salesPoint = $newCal * $fixTwo; //96 x 2 = 192 
                $commission = $salesPoint * $approch; //192 x franc level (example 3) = 576 
                break;
            case '1946ffcc0b0c64ff1673fdb7063f31cd': //health
                $policyAmount = $monthy * $year; //50 x 12 = 600 
                $salesPoint = $policyAmount / $fixTwenty; //600 / 20 = 30  
                $commission = $salesPoint * $approch; //30 x franc level (example ) = 90 
                break;
            /* case '': //swimmo
                $policyAmount = $monthy; //450000
                $salesPoint = $monthy / $fixThousund; //450000 / 1000 = 450
                $commission = $salesPoint * $approch; // 450 x frac level
                break; */
            /* case '6fad65e0a6afd858c8cbdfc8de348979': //car
                
                break;
            case 'd7e3d5e3e904ac9865fb6d017eddc8d5': //home
                
                break;
            case 'a38fff414bc39acdb2ea0755397eea3a': //legal
                
                break; */
            default:
                $policyAmount = ($monthy * $year) * $duration; // 200 * 12 * 40(year) = 96000
                $salesPoint = round($policyAmount * 0.04); // 3840
                $commission = ceil(($salesPoint * $approch) / 100); // 960
                break;
        }
        
        DB::table('customer_commission')->insert([
            'cc_id' => md5(microtime()),
            'customer_id' => $policy->customer_id,
            'policy_id' => $policy->ci_id,
            'commission_id' => $policy->commission_id,
            'policy_amount' => $policyAmount,
            'commission_factor' => $policy->approach,
            'sales_point' => $salesPoint,
            'employee_commission' => $commission,
            'employee_id' => $policy->employee_id,
        ]);
    }
}

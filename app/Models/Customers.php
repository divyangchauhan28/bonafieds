<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id',
        'salutation',
        'firstname',
        'lastname',
        'birth_date',
        'street',
        'postal_code',
        'city',
        'mobile',
        'email',
        'family_member',
        'parent_id',
        'category'
    ];
}

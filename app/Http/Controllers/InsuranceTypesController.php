<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\InsuranceTypes;

class InsuranceTypesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }


    public function index()
    {
        $types = InsuranceTypes::where('is_deleted', 0)->get();
        return view('admin.types-list', compact('types'))->with('no', 1);
    }

    public function createType(Request $request, $typeId = null)
    {
        if ($typeId) {
            $type = InsuranceTypes::where('type_id', $typeId)->first();
            session()->flash('level', 'success');
            session()->flash('message', 'Insurance Type Updated Successfully');
        } else {
            $type = new InsuranceTypes();
            $type->type_id = md5(microtime());
            session()->flash('level', 'success');
            session()->flash('message', 'Insurance Type Created Successfully');
        }
        $type->label_en = $request->label_en;
        $type->label_de = $request->label_de;
        $type->label_fr = $request->label_fr ?? null;
        $type->label_it = $request->label_it ?? null;
        $type->min_amount = $request->min_amount;
        $type->max_amount = $request->max_amount;
        $type->min_duration = $request->min_duration;
        $type->max_duration = $request->max_duration;
        $type->save();
        return redirect()->back();
    }
}

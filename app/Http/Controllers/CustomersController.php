<?php

namespace App\Http\Controllers;

use App\Models\Customers;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CustomersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (Auth::user()->is_employee) {
            $customers = Customers::where(['is_deleted' => 0, 'parent_id' => 0, 'employee_id' => Auth::id()])->orderBy('created_at','desc')->get();
        } else {
            $customers = Customers::where(['is_deleted' => 0, 'parent_id' => 0])->orderBy('created_at','desc')->get();
        }
        $start = '';
        $end = '';
        $customerData = Customers::select(DB::raw('DATE(created_at) as date'), DB::raw('count(*) as total'))
            ->whereBetween('created_at',[$start,$end])
            ->groupBy('date')
            ->orderBy('date', 'asc')
            ->get()->toArray();

        $dateArray = array();
        $countArray = array();

        foreach($customerData as $key => $value){
            $dateArray[] = $value['date'];
            $countArray[] = $value['total'];
        }

        $date_data = '"' . implode ( '", "', $dateArray ) . '"';
        $count_data = implode ( ',', $countArray );
        return view('customer.customer-list', compact('customers','start','end','date_data','count_data','dateArray'))->with('no', 1);
        // return view('customer.customer-list', compact('customers','start','end'))->with('no', 1);
    }

    public function createCustomer(Request $request, $customerId = null)
    {
        if ($customerId) {
            $customer = Customers::where('customer_id', $customerId)->first();
            $customer->salutation = $request->salutation;
            $customer->firstname = $request->firstname;
            $customer->lastname = $request->lastname;
            $customer->birth_date = Carbon::createFromFormat('d/m/Y', $request->birth_date)->format('Y-m-d');
            $customer->gender = $request->gender;
            $customer->marital_status = $request->marital_status;
            $customer->nationality = $request->nationality;
            $customer->street = $request->street;
            $customer->postal_code = $request->postal_code;
            $customer->city = $request->city;
            $customer->country = $request->country;
            $customer->telephone = $request->telephone;
            $customer->mobile = $request->mobile;
            $customer->email = $request->email;
            $customer->employee_id = $request->employee_id;
            $customer->save();
            session()->flash('level', 'success');
            session()->flash('message', 'Customer Detail Updated Successfully');
        } else {
            $customer = new Customers();
            $customer->customer_id = md5(microtime());
            $customer->salutation = $request->salutation;
            $customer->firstname = $request->firstname;
            $customer->lastname = $request->lastname;
            $customer->birth_date = Carbon::createFromFormat('d/m/Y', $request->birth_date)->format('Y-m-d');
            $customer->gender = $request->gender;
            $customer->marital_status = $request->marital_status;
            $customer->nationality = $request->nationality;
            $customer->street = $request->street;
            $customer->postal_code = $request->postal_code;
            $customer->city = $request->city;
            $customer->country = $request->country;
            $customer->telephone = $request->telephone;
            $customer->mobile = $request->mobile;
            $customer->email = $request->email;
            $customer->parent_id = 0;
            $customer->employee_id = $request->employee_id;
            $customer->created_by = Auth::id();
            $customer->save();
            session()->flash('level', 'success');
            session()->flash('message', 'Customer Created Successfully');
        }
        return redirect()->back();
    }

    public function customerMembers(Request $request, $customerId = null)
    {
        $customer = Customers::where(['customer_id' => $customerId])->first();
        $members = Customers::where(['parent_id' => $customer->id, 'is_deleted' => 0])->get();
        return view('customer.member-list', compact('customer', 'members'))->with('no', 1);
    }

    public function createMember(Request $request, $memberId = null)
    {
        if ($memberId) {
            $member = Customers::where('customer_id', $memberId)->first();
            $member->salutation = $request->salutation;
            $member->firstname = $request->firstname;
            $member->lastname = $request->lastname;
            $member->birth_date = Carbon::createFromFormat('d/m/Y', $request->birth_date)->format('Y-m-d');
            $member->gender = $request->gender;
            $member->marital_status = $request->marital_status;
            $member->nationality = $request->nationality;
            $member->street = $request->street;
            $member->postal_code = $request->postal_code;
            $member->city = $request->city;
            $member->country = $request->country;
            $member->telephone = $request->telephone;
            $member->mobile = $request->mobile;
            $member->email = $request->email;
            $member->save();
            session()->flash('level', 'success');
            session()->flash('message', 'Member Detail Updated Successfully');
        } else {
            $customer = Customers::where(['customer_id' => $request->parent_id])->first();
            $member = new Customers();
            $member->customer_id = md5(microtime());
            $member->salutation = $request->salutation;
            $member->firstname = $request->firstname;
            $member->lastname = $request->lastname;
            $member->birth_date = Carbon::createFromFormat('d/m/Y', $request->birth_date)->format('Y-m-d');
            $member->gender = $request->gender;
            $member->marital_status = $request->marital_status;
            $member->nationality = $request->nationality;
            $member->street = $request->street;
            $member->postal_code = $request->postal_code;
            $member->city = $request->city;
            $member->country = $request->country;
            $member->telephone = $request->telephone;
            $member->mobile = $request->mobile;
            $member->email = $request->email;
            $customer->employee_id = $customer->employee_id;
            $member->parent_id = $request->parent_id;
            $member->created_by = Auth::id();
            $member->save();
            session()->flash('level', 'success');
            session()->flash('message', 'Member Detail Created Successfully');
        }
        return redirect()->back();
    }

    public function createPolicy(Request $request, $policyId = null)
    {
        if (isset($request->employee_id)) {
            DB::table('customer')->where('customer_id', $request->customer_id)->update(['employee_id' => $request->employee_id]);
            DB::table('customer')->where('parent_id', $request->customer_id)->update(['employee_id' => $request->employee_id]);
        }
        if ($policyId) {
            DB::table('customer_insurance')
                ->where('ci_id', $policyId)
                ->update([
                    'provider_id' => $request->provider_id,
                    'type_id' => $request->type_id,
                    'insurance_amount' => $request->insuranceAmount,
                    'insurance_duration' => $request->insuranceDuration,
                    'installment_mode' => $request->installment_mode,
                    'insurance_status' => "In Progress"
                ]);
            session()->flash('level', 'success');
            session()->flash('message', 'Policy Updated Successfully');
        } else {
            $customer = Customers::where('customer_id', $request->customer_id)->first();
            DB::table('customer_insurance')->insert([
                'ci_id' => md5(microtime()),
                'customer_id' => $request->customer_id,
                'provider_id' => $request->provider_id,
                'type_id' => $request->type_id,
                'insurance_amount' => $request->insuranceAmount,
                'insurance_duration' => $request->insuranceDuration,
                'installment_mode' => $request->installment_mode,
                'insurance_status' => "In Progress",
                'employee_id' => $customer->employee_id
            ]);
            session()->flash('level', 'success');
            session()->flash('message', 'Policy Created Successfully');
        }
        return redirect()->back();
    }

    public function policyStatus(Request $request)
    {
        $customerId = $employeeId = 0;
        if (Auth::user()->is_admin) {
            if ($request->isMethod('post')) {
                $customerId = $request->customerId ?? 0;
                $employeeId = $request->employeeId ?? 0;
                $policies = DB::table('customer_insurance')
                    ->select('customer_insurance.*', 'customers.*', 'employee.name as employeeName', 'approver.name as approverName', 'insurance_providers.label_en as insProvider', 'insurance_types.label_en as insType')
                    ->leftJoin('customers', 'customers.customer_id', '=', 'customer_insurance.customer_id')
                    ->leftJoin('insurance_providers', 'insurance_providers.provider_id', '=', 'customer_insurance.provider_id')
                    ->leftJoin('insurance_types', 'insurance_types.type_id', '=', 'customer_insurance.type_id')
                    ->leftJoin('users as employee', 'employee.id', '=', 'customer_insurance.employee_id')
                    ->leftJoin('users as approver', 'approver.id', '=', 'customer_insurance.updated_by')
                    ->where(['customer_insurance.is_dead' => 0, 'customer_insurance.is_deleted' => 0]);
                if ($request->customerId) {
                    $policies = $policies->where(['customer_insurance.customer_id' => $request->customerId]);
                }
                if ($request->employeeId) {
                    $policies = $policies->where(['customer_insurance.employee_id' => $request->employeeId]);
                }
                $policies = $policies->get();
            } else {
                $policies = DB::table('customer_insurance')
                    ->select('customer_insurance.*', 'customers.*', 'employee.name as employeeName', 'approver.name as approverName', 'insurance_providers.label_en as insProvider', 'insurance_types.label_en as insType')
                    ->leftJoin('customers', 'customers.customer_id', '=', 'customer_insurance.customer_id')
                    ->leftJoin('insurance_providers', 'insurance_providers.provider_id', '=', 'customer_insurance.provider_id')
                    ->leftJoin('insurance_types', 'insurance_types.type_id', '=', 'customer_insurance.type_id')
                    ->leftJoin('users as employee', 'employee.id', '=', 'customer_insurance.employee_id')
                    ->leftJoin('users as approver', 'approver.id', '=', 'customer_insurance.updated_by')
                    ->where(['customer_insurance.is_dead' => 0, 'customer_insurance.is_deleted' => 0])
                    ->get();
            }
            $customers = DB::table('customer_insurance')
                ->select('customers.customer_id as customerId')
                ->addSelect(DB::raw("CONCAT(salutation,' ',firstname,' ',lastname) as customerName"))
                ->leftJoin('customers', 'customers.customer_id', '=', 'customer_insurance.customer_id')
                ->where(['customer_insurance.is_dead' => 0, 'customer_insurance.is_deleted' => 0])
                ->pluck('customerName', 'customerId')->toArray();
            $employees = User::select('id', 'name')->where(['is_employee' => 1, 'is_active' => 1, 'is_blocked' => 0, 'is_deleted' => 0])->pluck('name', 'id')->toArray();
        } elseif (Auth::user()->is_employee) {
            if ($request->isMethod('post')) {
                $customerId = $request->customerId ?? 0;
                $employeeId = $request->employeeId ?? 0;
                $policies = DB::table('customer_insurance')
                    ->select('customer_insurance.*', 'customers.*', 'employee.name as employeeName', 'approver.name as approverName', 'insurance_providers.label_en as insProvider', 'insurance_types.label_en as insType')
                    ->leftJoin('customers', 'customers.customer_id', '=', 'customer_insurance.customer_id')
                    ->leftJoin('insurance_providers', 'insurance_providers.provider_id', '=', 'customer_insurance.provider_id')
                    ->leftJoin('insurance_types', 'insurance_types.type_id', '=', 'customer_insurance.type_id')
                    ->leftJoin('users as employee', 'employee.id', '=', 'customer_insurance.employee_id')
                    ->leftJoin('users as approver', 'approver.id', '=', 'customer_insurance.updated_by')
                    ->where(['customer_insurance.employee_id' => Auth::id(), 'customer_insurance.is_dead' => 0, 'customer_insurance.is_deleted' => 0]);
                if ($request->customerId) {
                    $policies = $policies->where(['customer_insurance.customer_id' => $request->customerId]);
                }
                $policies = $policies->get();
            } else {
                $policies = DB::table('customer_insurance')
                    ->select('customer_insurance.*', 'customers.*', 'employee.name as employeeName', 'approver.name as approverName', 'insurance_providers.label_en as insProvider', 'insurance_types.label_en as insType')
                    ->leftJoin('customers', 'customers.customer_id', '=', 'customer_insurance.customer_id')
                    ->leftJoin('insurance_providers', 'insurance_providers.provider_id', '=', 'customer_insurance.provider_id')
                    ->leftJoin('insurance_types', 'insurance_types.type_id', '=', 'customer_insurance.type_id')
                    ->leftJoin('users as employee', 'employee.id', '=', 'customer_insurance.employee_id')
                    ->leftJoin('users as approver', 'approver.id', '=', 'customer_insurance.updated_by')
                    ->where(['customer_insurance.employee_id' => Auth::id(), 'customer_insurance.is_dead' => 0, 'customer_insurance.is_deleted' => 0])
                    ->get();
            }
            $customers = DB::table('customer_insurance')
                ->select('customers.customer_id as customerId')
                ->addSelect(DB::raw("CONCAT(salutation,' ',firstname,' ',lastname) as customerName"))
                ->leftJoin('customers', 'customers.customer_id', '=', 'customer_insurance.customer_id')
                ->where(['customer_insurance.employee_id' => Auth::id(), 'customer_insurance.is_dead' => 0, 'customer_insurance.is_deleted' => 0])
                ->pluck('customerName', 'customerId')->toArray();
            $employees = array();
        } else {
        }
        return view('admin.policy-status', compact('customerId', 'employeeId', 'customers', 'employees', 'policies'))->with('no', 1);
    }

    public function policyRework(Request $request)
    {
        $customerId = $employeeId = 0;
        if (Auth::user()->is_admin) {
            if ($request->isMethod('post')) {
                $customerId = $request->customerId ?? 0;
                $employeeId = $request->employeeId ?? 0;
                $policies = DB::table('customer_insurance')
                    ->select('customer_insurance.*', 'customers.*', 'employee.name as employeeName', 'approver.name as approverName', 'insurance_providers.label_en as insProvider', 'insurance_types.label_en as insType')
                    ->leftJoin('customers', 'customers.customer_id', '=', 'customer_insurance.customer_id')
                    ->leftJoin('insurance_providers', 'insurance_providers.provider_id', '=', 'customer_insurance.provider_id')
                    ->leftJoin('insurance_types', 'insurance_types.type_id', '=', 'customer_insurance.type_id')
                    ->leftJoin('users as employee', 'employee.id', '=', 'customer_insurance.employee_id')
                    ->leftJoin('users as approver', 'approver.id', '=', 'customer_insurance.updated_by')
                    ->where(['customer_insurance.is_dead' => 0, 'customer_insurance.is_deleted' => 0, 'customer_insurance.insurance_status' => 'In Progress']);
                if ($request->customerId) {
                    $policies = $policies->where(['customer_insurance.customer_id' => $request->customerId]);
                }
                if ($request->employeeId) {
                    $policies = $policies->where(['customer_insurance.employee_id' => $request->employeeId]);
                }
                $policies = $policies->get();
            } else {
                $policies = DB::table('customer_insurance')
                    ->select('customer_insurance.*', 'customers.*', 'employee.name as employeeName', 'approver.name as approverName', 'insurance_providers.label_en as insProvider', 'insurance_types.label_en as insType')
                    ->leftJoin('customers', 'customers.customer_id', '=', 'customer_insurance.customer_id')
                    ->leftJoin('insurance_providers', 'insurance_providers.provider_id', '=', 'customer_insurance.provider_id')
                    ->leftJoin('insurance_types', 'insurance_types.type_id', '=', 'customer_insurance.type_id')
                    ->leftJoin('users as employee', 'employee.id', '=', 'customer_insurance.employee_id')
                    ->leftJoin('users as approver', 'approver.id', '=', 'customer_insurance.updated_by')
                    ->where(['customer_insurance.is_dead' => 0, 'customer_insurance.is_deleted' => 0, 'customer_insurance.insurance_status' => 'In Progress'])
                    ->get();
            }
            $customers = DB::table('customer_insurance')
                ->select('customers.customer_id as customerId')
                ->addSelect(DB::raw("CONCAT(salutation,' ',firstname,' ',lastname) as customerName"))
                ->leftJoin('customers', 'customers.customer_id', '=', 'customer_insurance.customer_id')
                ->where(['customer_insurance.is_dead' => 0, 'customer_insurance.is_deleted' => 0])
                ->pluck('customerName', 'customerId')->toArray();
            $employees = User::select('id', 'name')->where(['is_employee' => 1, 'is_active' => 1, 'is_blocked' => 0, 'is_deleted' => 0])->pluck('name', 'id')->toArray();
        } elseif (Auth::user()->is_employee) {
            if ($request->isMethod('post')) {
                $customerId = $request->customerId ?? 0;
                $employeeId = $request->employeeId ?? 0;
                $policies = DB::table('customer_insurance')
                    ->select('customer_insurance.*', 'customers.*', 'employee.name as employeeName', 'approver.name as approverName', 'insurance_providers.label_en as insProvider', 'insurance_types.label_en as insType')
                    ->leftJoin('customers', 'customers.customer_id', '=', 'customer_insurance.customer_id')
                    ->leftJoin('insurance_providers', 'insurance_providers.provider_id', '=', 'customer_insurance.provider_id')
                    ->leftJoin('insurance_types', 'insurance_types.type_id', '=', 'customer_insurance.type_id')
                    ->leftJoin('users as employee', 'employee.id', '=', 'customer_insurance.employee_id')
                    ->leftJoin('users as approver', 'approver.id', '=', 'customer_insurance.updated_by')
                    ->where(['customer_insurance.employee_id' => Auth::id(), 'customer_insurance.is_dead' => 0, 'customer_insurance.is_deleted' => 0, 'customer_insurance.insurance_status' => 'In Progress']);
                if ($request->customerId) {
                    $policies = $policies->where(['customer_insurance.customer_id' => $request->customerId]);
                }
                $policies = $policies->get();
            } else {
                $policies = DB::table('customer_insurance')
                    ->select('customer_insurance.*', 'customers.*', 'employee.name as employeeName', 'approver.name as approverName', 'insurance_providers.label_en as insProvider', 'insurance_types.label_en as insType')
                    ->leftJoin('customers', 'customers.customer_id', '=', 'customer_insurance.customer_id')
                    ->leftJoin('insurance_providers', 'insurance_providers.provider_id', '=', 'customer_insurance.provider_id')
                    ->leftJoin('insurance_types', 'insurance_types.type_id', '=', 'customer_insurance.type_id')
                    ->leftJoin('users as employee', 'employee.id', '=', 'customer_insurance.employee_id')
                    ->leftJoin('users as approver', 'approver.id', '=', 'customer_insurance.updated_by')
                    ->where(['customer_insurance.employee_id' => Auth::id(), 'customer_insurance.is_dead' => 0, 'customer_insurance.is_deleted' => 0, 'customer_insurance.insurance_status' => 'In Progress'])
                    ->get();
            }
            $customers = DB::table('customer_insurance')
                ->select('customers.customer_id as customerId')
                ->addSelect(DB::raw("CONCAT(salutation,' ',firstname,' ',lastname) as customerName"))
                ->leftJoin('customers', 'customers.customer_id', '=', 'customer_insurance.customer_id')
                ->where(['customer_insurance.employee_id' => Auth::id(), 'customer_insurance.is_dead' => 0, 'customer_insurance.is_deleted' => 0])
                ->pluck('customerName', 'customerId')->toArray();
            $employees = array();
        } else {
        }
        return view('admin.policy-rework', compact('customerId', 'employeeId', 'customers', 'employees', 'policies'))->with('no', 1);
    }

    public function pendingDocument()
    {
        $policies = DB::table('customer_insurance')
            ->select('customer_insurance.*', 'customers.*', 'employee.name as employeeName', 'approver.name as approverName', 'insurance_providers.label_en as insProvider', 'insurance_types.label_en as insType')
            ->addSelect(DB::raw("(SELECT COUNT(1) as DocCount FROM policy_documents WHERE policy_id = customer_insurance.ci_id AND is_deleted = 0) as totalDocs"))
            ->addSelect(DB::raw("(SELECT COUNT(1) as DocCount FROM policy_documents WHERE policy_id = customer_insurance.ci_id AND is_deleted = 0 AND is_pending = 1) as pendingDocs"))
            ->addSelect(DB::raw("(SELECT COUNT(1) as DocCount FROM policy_documents WHERE policy_id = customer_insurance.ci_id AND is_deleted = 0 AND is_pending = 0) as completeDocs"))
            ->leftJoin('customers', 'customers.customer_id', '=', 'customer_insurance.customer_id')
            ->leftJoin('insurance_providers', 'insurance_providers.provider_id', '=', 'customer_insurance.provider_id')
            ->leftJoin('insurance_types', 'insurance_types.type_id', '=', 'customer_insurance.type_id')
            ->leftJoin('users as employee', 'employee.id', '=', 'customer_insurance.employee_id')
            ->leftJoin('users as approver', 'approver.id', '=', 'customer_insurance.updated_by')
            ->where(['customer_insurance.employee_id' => Auth::id(), 'customer_insurance.is_dead' => 0, 'customer_insurance.is_deleted' => 0])
            ->having('pendingDocs', '>', 0)
            ->get();
        return view('admin.pending-document', compact('policies'))->with('no', 1);
    }

    public function policyDocument(Request $request)
    {        
        if ($request->hasfile('policy_document')) {
            $leadFile = $request->file('policy_document');
            $fileName = md5(microtime()) . '.' . $leadFile->getClientOriginalExtension();
            $docFileName = $leadFile->getClientOriginalName();
            $docFilePath = $request->file('policy_document')->storeAs('uploads/' . date('Y') . '/' . date('m'), $fileName, 'public');
            $isPending = 0;
        } else {
            $docFileName = $docFilePath = '';
            $isPending = 1;
        }
        $policyDocument = DB::table('policy_documents')->where(['policy_id' => $request->policy_id, 'document_id' => $request->document_id, 'is_pending' => 1])->first();
        if ($policyDocument) {
            DB::table('policy_documents')->where('pd_id', $policyDocument->pd_id)->update([
                'document_name' => $docFileName,
                'document_path' => $docFilePath,
                'is_pending' => $isPending
            ]);
        } else {
            DB::table('policy_documents')->insert([
                'pd_id' => md5(microtime()),
                'policy_id' => $request->policy_id,
                'document_id' => $request->document_id,
                'document_name' => $docFileName,
                'document_path' => $docFilePath,
                'is_pending' => $isPending
            ]);
        }
        session()->flash('level', 'success');
        session()->flash('message', 'Policy Document Uploaded Successfully');

        return redirect()->back();
    }

    public function searchCustomer(Request $request)
    {
        if($request->from_date==''){
            $start = Carbon::now(); 
        }
        else{ 
            $start = $request->from_date;
        }

        if($request->to_date==''){
            $end = Carbon::now(); 
        }
        else{ 
            $end = $request->to_date;
        }

        if (Auth::user()->is_employee) {
            $customers = Customers::whereBetween('created_at',[$start,$end])->where(['is_deleted' => 0, 'parent_id' => 0, 'employee_id' => Auth::id(), 'is_active' => $request->is_active])->orderBy('created_at','desc')->get();
        } else {
            $customers = Customers::whereBetween('created_at',[$start,$end])->where(['is_deleted' => 0, 'parent_id' => 0, 'is_active' => $request->is_active])->orderBy('created_at','desc')->get();
        }

        $customerData = Customers::select(DB::raw('DATE(created_at) as date'), DB::raw('count(*) as total'))
            ->whereBetween('created_at',[$start,$end])
            ->where(['is_active' => $request->is_active])
            ->groupBy('date')
            ->orderBy('date', 'asc')
            ->get()->toArray();

        $status = $request->is_active;
        $dateArray = array();
        $countArray = array();

        foreach($customerData as $key => $value){
            $dateArray[] = $value['date'];
            $countArray[] = $value['total'];
        }

        $date_data = '"' . implode ( '", "', $dateArray ) . '"';
        $count_data = implode ( ',', $countArray );
        return view('customer.customer-list', compact('customers','start','end','date_data','count_data','status'))->with('no', 1);
    }

}

<?php

namespace App\Http\Controllers;

use App\Mail\MeetingNotification;
use App\Models\Customers;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Mail;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function index()
    {
        $dashboardData = $birthdays = $ranking = array();
        $userID = Auth::id();
        if (Auth::user()->is_admin) {
            $dashboardData['customerCount'] = Customers::select(DB::raw("COUNT(1) as customerCount"))->where(['parent_id' => 0, 'is_active' => 1, 'is_deleted' => 0])->first()->customerCount;
            $dashboardData['memberCount'] = Customers::select(DB::raw("COUNT(1) as customerCount"))->where('parent_id', '<>', 0)->where(['is_active' => 1, 'is_deleted' => 0])->first()->customerCount;
            $dashboardData['activePolicyCount'] = DB::table('customer_insurance')->select(DB::raw("COUNT(1) as policyCount"))->where(['is_approved' => 1, 'is_dead' => 0, 'is_deleted' => 0])->first()->policyCount;
            $dashboardData['pendingPolicyCount'] = DB::table('customer_insurance')->select(DB::raw("COUNT(1) as policyCount"))->where(['is_approved' => 0, 'is_dead' => 0, 'is_deleted' => 0])->first()->policyCount;
            $dashboardData['activeSalesPoint'] = DB::table('customer_commission')->select(DB::raw("COALESCE(SUM(sales_point),0) as salesPointCount"))->where(['commission_status' => 1])->first()->salesPointCount;
            $dashboardData['pendingSalesPoint'] = DB::table('customer_commission')->select(DB::raw("COALESCE(SUM(sales_point),0) as salesPointCount"))->where(['commission_status' => 2])->first()->salesPointCount;
            $dashboardData['activeCommission'] = DB::table('customer_commission')->select(DB::raw("COALESCE(SUM(employee_commission),0) as salesCommission"))->where(['commission_status' => 1])->first()->salesCommission;
            $dashboardData['pendingCommission'] = DB::table('customer_commission')->select(DB::raw("COALESCE(SUM(employee_commission),0) as salesCommission"))->where(['commission_status' => 2])->first()->salesCommission;
            $birthdays = Customers::select('salutation', 'firstname', 'lastname', 'gender')
                ->addSelect(DB::raw('DATE_FORMAT(birth_date,"%d-%b-%Y") as dateOfBirth'))
                ->whereMonth('birth_date', Carbon::now()->month)
                ->whereDay('birth_date', Carbon::now()->day)
                ->where(['is_active' => 1, 'is_deleted' => 0])
                ->get();
            $upcommingMeetings = DB::table('meetings')
                ->select('meetings.*', 'users.name as scheduledBy')
                ->addSelect(DB::raw('DATE_FORMAT(meeting_on,"%d-%b-%Y") as meetingOn'))
                ->addSelect(DB::raw('CONCAT(TIME_FORMAT(start_time,"%h:%i %p"),"-",TIME_FORMAT(end_time,"%h:%i %p")) as meetingTime'))
                ->addSelect(DB::raw('DATE_FORMAT(schedule_on,"%d-%b-%Y %h:%i %p") as scheduleOn'))
                ->leftJoin('users', 'users.id', '=', 'meetings.scheduled_by')
                ->where('meeting_on', '>=', Carbon::now()->toDateString())
                ->orderBy('meetingOn', 'ASC')->orderBy('start_time', 'ASC')
                ->get();
            $news = DB::table('news')
                ->select('news.*', 'users.name as createdBy')
                ->addSelect(DB::raw('DATE_FORMAT(created_on,"%d-%b-%Y %h:%i %p") as createdOn'))
                ->leftJoin('users', 'users.id', '=', 'news.created_by')
                ->orderBy('createdOn', 'ASC')
                ->limit(10)
                ->get();
            $pendingDoc = 0;
            $employees = User::select('id', 'user_id', 'name', 'email', 'profile_image')
                ->addSelect(DB::raw("(SELECT COUNT(1) FROM chat_communication WHERE chat_communication.asked_by = users.id and chat_communication.replied_by = $userID AND chat_communication.is_viewed = 0) as unreadCount"))
                ->where(['is_employee' => 1, 'is_active' => 1, 'is_deleted' => 0])
                ->get();
        } elseif (Auth::user()->is_employee) {
            $dashboardData['customerCount'] = Customers::select(DB::raw("COUNT(1) as customerCount"))->where(['created_by' => Auth::id(), 'parent_id' => 0, 'is_active' => 1, 'is_deleted' => 0])->first()->customerCount;
            $dashboardData['memberCount'] = Customers::select(DB::raw("COUNT(1) as customerCount"))->where('parent_id', '<>', 0)->where(['created_by' => Auth::id(), 'is_active' => 1, 'is_deleted' => 0])->first()->customerCount;
            $dashboardData['activePolicyCount'] = DB::table('customer_insurance')->select(DB::raw("COUNT(1) as policyCount"))->where(['employee_id' => Auth::id(), 'is_approved' => 1, 'is_dead' => 0, 'is_deleted' => 0])->first()->policyCount;
            $dashboardData['pendingPolicyCount'] = DB::table('customer_insurance')->select(DB::raw("COUNT(1) as policyCount"))->where(['employee_id' => Auth::id(), 'is_approved' => 0, 'is_dead' => 0, 'is_deleted' => 0])->first()->policyCount;
            $dashboardData['activeSalesPoint'] = DB::table('customer_commission')->select(DB::raw("COALESCE(SUM(sales_point),0) as salesPointCount"))->where(['employee_id' => Auth::id(), 'commission_status' => 1])->first()->salesPointCount;
            $dashboardData['pendingSalesPoint'] = DB::table('customer_commission')->select(DB::raw("COALESCE(SUM(sales_point),0) as salesPointCount"))->where(['employee_id' => Auth::id(), 'commission_status' => 2])->first()->salesPointCount;
            $dashboardData['activeCommission'] = DB::table('customer_commission')->select(DB::raw("COALESCE(SUM(employee_commission),0) as salesCommission"))->where(['employee_id' => Auth::id(), 'commission_status' => 1])->first()->salesCommission;
            $dashboardData['pendingCommission'] = DB::table('customer_commission')->select(DB::raw("COALESCE(SUM(employee_commission),0) as salesCommission"))->where(['employee_id' => Auth::id(), 'commission_status' => 2])->first()->salesCommission;
            $birthdays = Customers::select('salutation', 'firstname', 'lastname', 'gender')
                ->addSelect(DB::raw('DATE_FORMAT(birth_date,"%d-%b-%Y") as dateOfBirth'))
                ->whereMonth('birth_date', Carbon::now()->month)
                ->whereDay('birth_date', Carbon::now()->day)
                ->where(['is_active' => 1, 'is_deleted' => 0])
                ->where('employee_id', Auth::id())
                ->get();
            $upcommingMeetings = DB::table('meetings')
                ->select('meetings.*', 'users.name as scheduledBy')
                ->addSelect(DB::raw('DATE_FORMAT(meeting_on,"%d-%b-%Y") as meetingOn'))
                ->addSelect(DB::raw('CONCAT(TIME_FORMAT(start_time,"%h:%i %p")," to ",TIME_FORMAT(end_time,"%h:%i %p")) as meetingTime'))
                ->addSelect(DB::raw('DATE_FORMAT(schedule_on,"%d-%b-%Y %h:%i %p") as scheduleOn'))
                ->leftJoin('users', 'users.id', '=', 'meetings.scheduled_by')
                ->whereIn('meeting_id', function ($empMeetingd) {
                    $empMeetingd->select('meeting_id')->from('meeting_employee')->where('employee_id', Auth::user()->user_id);
                })->where(['meetings.is_active' => 1, 'meetings.is_deleted' => 0])
                ->where('meeting_on', '>=', Carbon::now()->toDateString())
                ->orderBy('meetingOn', 'ASC')->orderBy('start_time', 'ASC')
                ->get();
            $news = DB::table('news')
                ->select('news.*', 'users.name as createdBy')
                ->addSelect(DB::raw('DATE_FORMAT(created_on,"%d-%b-%Y %h:%i %p") as createdOn'))
                ->leftJoin('users', 'users.id', '=', 'news.created_by')
                ->orderBy('createdOn', 'ASC')
                ->limit(10)
                ->get();
            $pendingDoc = DB::table('policy_documents')
                ->select(DB::raw("COUNT(1) as pendingDocCount"))
                ->whereIn('policy_id', function ($policies) {
                    $policies->distinct()->select('ci_id')->from('customer_insurance')->where(['employee_id' => Auth::id(), 'is_active' => 1, 'is_dead' => 0, 'is_deleted' => 0]);
                })->where(['is_pending' => 1, 'is_deleted' => 0])
                ->first()->pendingDocCount;
            $ranking = DB::table('customer_commission')
                        ->select(DB::raw("users.name, SUM(customer_commission.sales_point) as salesPointCount"))
                        ->rightJoin('users', 'users.id', '=', 'customer_commission.employee_id')
                        ->where(['customer_commission.commission_status' => 1])
                        ->groupBy('customer_commission.employee_id')
                        ->orderBy(DB::raw("SUM(customer_commission.sales_point)"), 'DESC')
                        ->take(5)
                        ->get();
            $employees = User::select('id', 'user_id', 'name', 'email', 'profile_image')
                    ->addSelect(DB::raw("(SELECT COUNT(1) FROM chat_communication WHERE chat_communication.asked_by = users.id and chat_communication.replied_by = $userID AND chat_communication.is_viewed = 0) as unreadCount"))
                    ->where(['is_active' => 1, 'is_deleted' => 0])
                    ->whereIn('id', function ($users) {
                        $users->distinct()->select('replied_by')->from('chat_communication')->where(['asked_by' => Auth::id()]);
                    })->orWhereIn('id', function ($users) {
                        $users->distinct()->select('asked_by')->from('chat_communication')->where(['replied_by' => Auth::id()]);
                    })->get();
        }
        $user = User::where('user_id', Auth::user()->user_id)->first();
        $no = 1;
        // dd($user, $dashboardData, $birthdays, $upcommingMeetings, $pendingDoc, $news, $no);
        return view('admin.index', compact('user', 'dashboardData', 'birthdays', 'upcommingMeetings', 'pendingDoc', 'news', 'no', 'ranking','employees'));
    }

    public function profile()
    {
        return view('admin.profile', [
            'user' => User::where('user_id', Auth::user()->user_id)->first()
        ]);
    }

    public function meetings()
    {
        if (Auth::user()->is_admin) {
            $meetings = DB::table('meetings')
                ->select('meetings.*', 'users.name as scheduledBy')
                ->addSelect(DB::raw('DATE_FORMAT(meeting_on,"%d-%b-%Y") as meetingOn'))
                ->addSelect(DB::raw('CONCAT(TIME_FORMAT(start_time,"%h:%i %p"),"-",TIME_FORMAT(end_time,"%h:%i %p")) as meetingTime'))
                ->addSelect(DB::raw('DATE_FORMAT(schedule_on,"%d-%b-%Y %h:%i %p") as scheduleOn'))
                ->leftJoin('users', 'users.id', '=', 'meetings.scheduled_by')
                ->where('meetings.is_deleted', 0)
                ->orderBy('meetingOn', 'desc')
                ->get();
        } else {
            $meetings = DB::table('meetings')
                ->select('meetings.*', 'users.name as scheduledBy')
                ->addSelect(DB::raw('DATE_FORMAT(meeting_on,"%d-%b-%Y") as meetingOn'))
                ->addSelect(DB::raw('CONCAT(TIME_FORMAT(start_time,"%h:%i %p")," to ",TIME_FORMAT(end_time,"%h:%i %p")) as meetingTime'))
                ->addSelect(DB::raw('DATE_FORMAT(schedule_on,"%d-%b-%Y %h:%i %p") as scheduleOn'))
                ->leftJoin('users', 'users.id', '=', 'meetings.scheduled_by')
                ->whereIn('meeting_id', function ($empMeetingd) {
                    $empMeetingd->select('meeting_id')->from('meeting_employee')->where('employee_id', Auth::user()->user_id);
                })->where(['meetings.is_active' => 1, 'meetings.is_deleted' => 0])
                ->orderBy('meetingOn', 'desc')
                ->get();
        }
        return view('admin.meetings-list', compact('meetings'))->with('no', 1);
    }

    public function manageMeeting(Request $request, $meetingId = null)
    {
        if ($request->hasfile('agenda_doc')) {
            $AgendaFile = $request->file('agenda_doc');
            $fileName = md5(microtime()) . '.' . $AgendaFile->getClientOriginalExtension();
            $AgendaFileName = $AgendaFile->getClientOriginalName();
            $AgendaFilePath = $request->file('agenda_doc')->storeAs('uploads/' . date('Y') . '/' . date('m'), $fileName, 'public');
        } else {
            $AgendaFileName = $AgendaFilePath = '';
        }
        if ($request->hasfile('mom_doc')) {
            $MomFile = $request->file('mom_doc');
            $fileName = md5(microtime()) . '.' . $MomFile->getClientOriginalExtension();
            $MomFileName = $MomFile->getClientOriginalName();
            $MomFilePath = $request->file('mom_doc')->storeAs('uploads/' . date('Y') . '/' . date('m'), $fileName, 'public');
        } else {
            $MomFileName = $MomFilePath = '';
        }
        if ($meetingId) {
            $meeting = DB::table('meetings')->where('meeting_id', $meetingId)->first();
            DB::table('meetings')->where('meeting_id', $meetingId)->update([
                'meeting_detail' => $request->meeting_detail,
                'meeting_on' => Carbon::createFromFormat('d/m/Y', $request->meeting_on)->format('Y-m-d'),
                'start_time' => Carbon::createFromFormat('h:i A', $request->start_time)->format('H:i:s'),
                'end_time' => Carbon::createFromFormat('h:i A', $request->end_time)->format('H:i:s'),
                'attachment_link' => $request->attachment_link ?? null,
                'agenda_doc' => $AgendaFilePath ?? $meeting->agenda_doc,
                'agenda_title' => $AgendaFileName ?? $meeting->agenda_title,
                'mom_doc' => $MomFilePath ?? $meeting->mom_doc,
                'mom_title' => $MomFileName ?? $meeting->mom_title,
            ]);
            DB::table('meeting_employee')->where('meeting_id', $meetingId)->delete();
            session()->flash('level', 'success');
            session()->flash('message', 'Meeting Detail Updated Successfully');
        } else {
            $meetingId = md5(microtime());
            $data = [
                'meeting_id' => $meetingId,
                'meeting_detail' => $request->meeting_detail,
                'meeting_on' => Carbon::createFromFormat('d/m/Y', $request->meeting_on)->format('Y-m-d'),
                'start_time' => Carbon::createFromFormat('h:i A', $request->start_time)->format('H:i:s'),
                'end_time' => Carbon::createFromFormat('h:i A', $request->end_time)->format('H:i:s'),
                'attachment_link' => $request->attachment_link ?? null,
                'agenda_doc' => $AgendaFilePath,
                'agenda_title' => $AgendaFileName,
                'mom_doc' => $MomFilePath,
                'mom_title' => $MomFileName,
                'scheduled_by' => Auth::id(),
                'schedule_on' => Carbon::now(),
            ];
            DB::table('meetings')->insert($data);
            session()->flash('level', 'success');
            session()->flash('message', 'Meeting Detail Saved Successfully');

        }
        if (count($request->meetingEmployees) > 0) {
            $meetingEmployees = array();
            foreach ($request->meetingEmployees as $key => $employeeId) {
                $meetingEmployees[] = array('meeting_id' => $meetingId, 'employee_id' => $employeeId);
            }
            DB::table('meeting_employee')->insert($meetingEmployees);
            $emails = DB::table('users')->select('email')->whereIn('user_id', $request->meetingEmployees)->pluck('email')->toArray();
            $data = array('emails' => $emails, 'meeting_detail' => $request->meeting_detail, 'meeting_on' => $request->meeting_on, 'start_time' => $request->start_time, 'end_time' => $request->end_time);
            Mail::send(['html' => 'emails.meetingNotification'], $data, function ($message) use ($data) {
                $message->replyTo(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
                $message->subject('Bonafides CRM');
                $message->to($data['emails']);
            });
        }
        return redirect()->back();
    }

    public function news()
    {
        $news = DB::table('news')
            ->select('news.*', 'users.name as createdBy')
            ->addSelect(DB::raw('DATE_FORMAT(created_on,"%d-%b-%Y %h:%i %p") as createdOn'))
            ->leftJoin('users', 'users.id', '=', 'news.created_by')
            ->where('news.is_deleted', 0)
            ->get();
        return view('admin.news-list', compact('news'))->with('no', 1);
    }

    public function createNews(Request $request, $newsId = null)
    {
        if ($request->hasfile('image_docs')) {
            $newsFile = $request->file('image_docs');
            $fileName = md5(microtime()) . '.' . $newsFile->getClientOriginalExtension();
            $newsFilePath = $request->file('image_docs')->storeAs('uploads/' . date('Y') . '/' . date('m'), $fileName, 'public');
        } else {
            $newsFilePath = '';
        }
        if ($newsId) {
            $news = DB::table('news')->where('news_id', $newsId)->first();
            DB::table('news')->where('news_id', $newsId)->update([
                'news_detail' => $request->news_detail,
                'image_docs' => $newsFilePath ?? $news->image_docs
            ]);
            session()->flash('level', 'success');
            session()->flash('message', 'News Detail Updated Successfully');
        } else {
            DB::table('news')->insert([
                'news_id' => md5(microtime()),
                'news_detail' => $request->news_detail,
                'image_docs' => $newsFilePath,
                'created_by' => Auth::id(),
                'created_on' => Carbon::now()
            ]);
            session()->flash('level', 'success');
            session()->flash('message', 'News Detail Saved Successfully');
        }
        return redirect()->back();
    }

    public function youtubes()
    {
        $youtubes = DB::table('youtubes')
            ->select('youtubes.*', 'users.name as createdBy')
            ->addSelect(DB::raw('DATE_FORMAT(created_on,"%d-%b-%Y %h:%i %p") as createdOn'))
            ->leftJoin('users', 'users.id', '=', 'youtubes.created_by')
            ->where('youtubes.is_deleted', 0)
            ->get();
        return view('admin.youtubes-list', compact('youtubes'))->with('no', 1);
    }

    public function createYoutubes(Request $request, $youtubesId = null)
    {
        if ($youtubesId) {
            $youtubes = DB::table('youtubes')->where('youtubes_id', $youtubesId)->first();
            DB::table('youtubes')->where('youtubes_id', $youtubesId)->update([
                'youtubes_detail' => $request->youtubes_detail
            ]);
            session()->flash('level', 'success');
            session()->flash('message', 'Youtubes Detail Updated Successfully');
        } else {
            DB::table('youtubes')->insert([
                'youtubes_id' => md5(microtime()),
                'youtubes_detail' => $request->youtubes_detail,
                'created_by' => Auth::id(),
                'created_on' => Carbon::now()
            ]);
            session()->flash('level', 'success');
            session()->flash('message', 'Youtubes Detail Saved Successfully');
        }
        return redirect()->back();
    }

    public function companys()
    {
        $companys = DB::table('companys')
            ->select('companys.*', 'users.name as createdBy')
            ->addSelect(DB::raw('DATE_FORMAT(created_on,"%d-%b-%Y %h:%i %p") as createdOn'))
            ->leftJoin('users', 'users.id', '=', 'companys.created_by')
            ->where('companys.is_deleted', 0)
            ->get();
        return view('admin.companys-list', compact('companys'))->with('no', 1);
    }

    public function createCompanys(Request $request, $companysId = null)
    {
        if ($request->hasfile('image_docs')) {
            $companysFile = $request->file('image_docs');
            $fileName = md5(microtime()) . '.' . $companysFile->getClientOriginalExtension();
            $companysFilePath = $request->file('image_docs')->storeAs('uploads/' . date('Y') . '/' . date('m') .'/companys', $fileName, 'public');
        } else {
            $companysFilePath = '';
        }
        if ($companysId) {
            $companys = DB::table('companys')->where('companys_id', $companysId)->first();
            DB::table('companys')->where('companys_id', $companysId)->update([
                'companys_detail' => $request->companys_detail,
                'companys_link' => $request->companys_link,
                'companys_description' => $request->companys_description,
                'image_docs' => $companysFilePath ?? $companys->image_docs
            ]);
            session()->flash('level', 'success');
            session()->flash('message', 'Companys Detail Updated Successfully');
        } else {
            DB::table('companys')->insert([
                'companys_id' => md5(microtime()),
                'companys_detail' => $request->companys_detail,
                'companys_link' => $request->companys_link,
                'companys_description' => $request->companys_description,
                'image_docs' => $companysFilePath,
                'created_by' => Auth::id(),
                'created_on' => Carbon::now()
            ]);
            session()->flash('level', 'success');
            session()->flash('message', 'Companys Detail Saved Successfully');
        }
        return redirect()->back();
    }

    public function communication()
    {
        if (Auth::user()->is_admin) {
            $communications = DB::table('communication')
                ->select('communication.*', 'users.name as createdBy')
                ->addSelect(DB::raw('DATE_FORMAT(communication.communication_date,"%d-%b-%Y %h:%i %p") as createdOn'))
                ->leftJoin('users', 'users.id', '=', 'communication.sender_id')
                ->where('communication.is_deleted', 0)
                ->get();
        } else {
            $communications = DB::table('communication')
                ->select('communication.*', 'users.name as createdBy', 'communication_employee.is_viewed')
                ->addSelect(DB::raw('DATE_FORMAT(communication.communication_date,"%d-%b-%Y %h:%i %p") as createdOn'))
                ->addSelect(DB::raw('DATE_FORMAT(communication_employee.viewed_on,"%d-%b-%Y %h:%i %p") as viewedOn'))
                ->leftJoin('users', 'users.id', '=', 'communication.sender_id')
                ->leftJoin('communication_employee', 'communication_employee.communication_id', '=', 'communication.com_id')
                ->where(['communication.is_deleted' => 0, 'communication_employee.employee_id' => Auth::user()->user_id])
                ->get();
        }
        return view('admin.communication-list', compact('communications'))->with('no', 1);
    }

    public function createCommunication(Request $request, $comId = null)
    {
        if ($request->hasfile('attachment')) {
            $comFile = $request->file('attachment');
            $fileName = md5(microtime()) . '.' . $comFile->getClientOriginalExtension();
            $comFilePath = $request->file('attachment')->storeAs('uploads/' . date('Y') . '/' . date('m'), $fileName, 'public');
        } else {
            $comFilePath = '';
        }
        if ($comId) {
            $communication = DB::table('communication')->where('com_id', $comId)->first();
            DB::table('communication')->where('com_id', $comId)->update([
                'message' => $request->message,
                'attachment' => $comFilePath ?? $communication->attachment
            ]);
            DB::table('communication_employee')->where('communication_id', $comId)->delete();
            session()->flash('level', 'success');
            session()->flash('message', 'Communication Detail Updated Successfully');
        } else {
            $comId = md5(microtime());
            DB::table('communication')->insert([
                'com_id' => $comId,
                'message' => $request->message,
                'attachment' => $comFilePath,
                'sender_id' => Auth::id(),
                'communication_date' => Carbon::now()
            ]);
            session()->flash('level', 'success');
            session()->flash('message', 'Communication Detail Saved Successfully');
        }
        if (count($request->communicationEmployees) > 0) {
            $comEmployees = array();
            foreach ($request->communicationEmployees as $key => $employeeId) {
                $comEmployees[] = array('communication_id' => $comId, 'employee_id' => $employeeId);
            }
            DB::table('communication_employee')->insert($comEmployees);
        }
        return redirect()->back();
    }

    public function chat(Request $request)
    {
        $userID = Auth::id();
        if (Auth::user()->is_admin) {
            $employees = User::select('id', 'user_id', 'name', 'email', 'profile_image')
                ->addSelect(DB::raw("(SELECT COUNT(1) FROM chat_communication WHERE chat_communication.asked_by = users.id and chat_communication.replied_by = $userID AND chat_communication.is_viewed = 0) as unreadCount"))
                ->where(['is_employee' => 1, 'is_active' => 1, 'is_deleted' => 0])
                ->get();
        } else {
            $employees = User::select('id', 'user_id', 'name', 'email', 'profile_image')
                ->addSelect(DB::raw("(SELECT COUNT(1) FROM chat_communication WHERE chat_communication.asked_by = users.id and chat_communication.replied_by = $userID AND chat_communication.is_viewed = 0) as unreadCount"))
                ->where(['is_active' => 1, 'is_deleted' => 0])
                ->whereIn('id', function ($users) {
                    $users->distinct()->select('replied_by')->from('chat_communication')->where(['asked_by' => Auth::id()]);
                })->orWhereIn('id', function ($users) {
                    $users->distinct()->select('asked_by')->from('chat_communication')->where(['replied_by' => Auth::id()]);
                })->get();
        }
        return view('admin.chat-list', compact('employees'))->with('no', 1);
    }
}

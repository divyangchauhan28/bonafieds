<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class EmployeeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('employee.index', [
            'user' => User::where('user_id', Auth::user()->user_id)->first()
        ]);
    }

    public function profile()
    {
        return view('employee.profile', [
            'user' => User::where('user_id', Auth::user()->user_id)->first()
        ]);
    }

    public function youtubes()
    {
        $youtubes = DB::table('youtubes')
            ->select('youtubes.*', 'users.name as createdBy')
            ->addSelect(DB::raw('DATE_FORMAT(created_on,"%d-%b-%Y %h:%i %p") as createdOn'))
            ->leftJoin('users', 'users.id', '=', 'youtubes.created_by')
            ->where('youtubes.is_deleted', 0)
            ->get();
        return view('admin.youtubes-grid', compact('youtubes'))->with('no', 1);
    }

    public function companys()
    {
        $companys = DB::table('companys')
            ->select('companys.*', 'users.name as createdBy')
            ->addSelect(DB::raw('DATE_FORMAT(created_on,"%d-%b-%Y %h:%i %p") as createdOn'))
            ->leftJoin('users', 'users.id', '=', 'companys.created_by')
            ->where('companys.is_deleted', 0)
            ->get();
        return view('admin.companys-grid', compact('companys'))->with('no', 1);
    }

    public function ranking()
    {
        $ranking = DB::table('customer_commission')
                ->select(DB::raw("users.name, SUM(customer_commission.sales_point) as salesPointCount"))
                ->rightJoin('users', 'users.id', '=', 'customer_commission.employee_id')
                ->where(['customer_commission.commission_status' => 1])
                ->groupBy('customer_commission.employee_id')
                ->orderBy(DB::raw("SUM(customer_commission.sales_point)"), 'DESC')
                ->get();
        return view('admin.ranking-list', compact('ranking'))->with('no', 1);
    }

}

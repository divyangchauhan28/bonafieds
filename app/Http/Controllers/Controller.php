<?php

namespace App\Http\Controllers;

use Config;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use function Symfony\Component\String\s;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function deductPolicyCommission($customerPolicyId)
    {
        $customerPolicy = DB::table('customer_insurance')->where('ci_id', $customerPolicyId)->first();
        if ($customerPolicy) {
            $commission = DB::table('customer_commission')->where(['customer_id' => $customerPolicy->customer_id, 'policy_id' => $customerPolicy->ci_id])->first();
            if ($commission) {
                $deductionFactor = Config::get('global.commissionDeduction'); //10
                $commissionDeduction = ceil(($commission->employee_commission * $deductionFactor) / 100); //2175 * 10 / 100
                DB::table('customer_commission')->insert([
                    'cc_id' => md5(microtime()),
                    'customer_id' => $commission->customer_id,
                    'policy_id' => $commission->policy_id,
                    'commission_id' => $commission->commission_id,
                    'policy_amount' => $commission->policy_amount,
                    'commission_factor' => $deductionFactor,
                    'sales_point' => $commission->sales_point,
                    'employee_commission' => $commissionDeduction,
                    'employee_id' => $commission->employee_id,
                    'commission_status' => 1,
                    'commission_type' => 'Debit',
                    'processed_on' => Carbon::now(),
                    'updated_by' => \Auth::id(),
                    'updated_on' => Carbon::now()
                ]);
            }
        }
    }
}

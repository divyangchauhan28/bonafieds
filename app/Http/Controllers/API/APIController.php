<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Customers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class APIController extends BaseController
{
    public function customerLead(Request $request)
    {
        $customer = new Customers();
        $customer->customer_id = md5(microtime());
        $customer->salutation = $request->salutation;
        $customer->firstname = $request->firstname;
        $customer->lastname = $request->lastname;
        $customer->gender = $request->gender;
        $customer->birth_date = Carbon::createFromFormat('d/m/Y', $request->birth_date)->format('Y-m-d');
        $customer->street = $request->street;
        $customer->postal_code = $request->postal_code;
        $customer->city = $request->city;
        $customer->mobile = $request->mobile;
        $customer->email = $request->email;
        $customer->category = $request->category;
        $customer->family_member = $request->member_count;
        $customer->parent_id = 0;
        $customer->is_active = 0;
        $customer->created_by = 1;
        $customer->save();
        return $this->sendResponse($customer, 'Customer created Successfully.');
    }
}

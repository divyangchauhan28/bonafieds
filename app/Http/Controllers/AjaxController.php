<?php

namespace App\Http\Controllers;

use App\Models\Commission;
use App\Models\Customers;
use App\Models\Documents;
use App\Models\InsuranceTypes;
use App\Models\Lead;
use App\Models\Message;
use App\Models\Modules;
use App\Models\LeadCustomer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\InsuranceProviders;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PhpParser\Comment\Doc;
use Illuminate\Support\Arr;

class AjaxController extends Controller
{

    public function ChangeUniversalEnum(Request $request)
    {
        if ($request->ajax()) {
            switch ($request->actionType) {
                case 'UserActive';
                    User::where('user_id', $request->relationId)->update(['is_active' => 1]);
                    break;

                case 'UserInactive';
                    User::where('user_id', $request->relationId)->update(['is_active' => 0]);
                    break;

                case 'UserBlock';
                    User::where('user_id', $request->relationId)->update(['is_blocked' => 1]);
                    break;

                case 'UserUnblock';
                    User::where('user_id', $request->relationId)->update(['is_blocked' => 0]);
                    break;

                case 'UserDelete';
                    User::where('user_id', $request->relationId)->update(['is_deleted' => 1]);
                    break;

                case 'UserUndelete';
                    User::where('user_id', $request->relationId)->update(['is_deleted' => 0]);
                    break;

                case 'ProviderActive';
                    InsuranceProviders::where('provider_id', $request->relationId)->update(['is_active' => 1]);
                    break;

                case 'ProviderInactive';
                    InsuranceProviders::where('provider_id', $request->relationId)->update(['is_active' => 0]);
                    break;

                case 'ProviderDelete';
                    InsuranceProviders::where('provider_id', $request->relationId)->update(['is_deleted' => 1]);
                    break;

                case 'ProviderUndelete';
                    InsuranceProviders::where('provider_id', $request->relationId)->update(['is_deleted' => 0]);
                    break;

                case 'TypeActive';
                    InsuranceTypes::where('type_id', $request->relationId)->update(['is_active' => 1]);
                    break;

                case 'TypeInactive';
                    InsuranceTypes::where('type_id', $request->relationId)->update(['is_active' => 0]);
                    break;

                case 'TypeDelete';
                    InsuranceTypes::where('type_id', $request->relationId)->update(['is_deleted' => 1]);
                    break;

                case 'TypeUndelete';
                    InsuranceTypes::where('type_id', $request->relationId)->update(['is_deleted' => 0]);
                    break;

                case 'DocumentActive';
                    Documents::where('document_id', $request->relationId)->update(['is_active' => 1]);
                    break;

                case 'DocumentInactive';
                    Documents::where('document_id', $request->relationId)->update(['is_active' => 0]);
                    break;

                case 'DocumentDelete';
                    Documents::where('document_id', $request->relationId)->update(['is_deleted' => 1]);
                    break;

                case 'DocumentUndelete';
                    Documents::where('document_id', $request->relationId)->update(['is_deleted' => 0]);
                    break;

                case 'CommissionActive';
                    Commission::where('commission_id', $request->relationId)->update(['is_active' => 1]);
                    break;

                case 'CommissionInactive';
                    Commission::where('commission_id', $request->relationId)->update(['is_active' => 0]);
                    break;

                case 'CommissionApprove';
                    DB::table('customer_commission')->where('cc_id', $request->relationId)->update(['commission_status' => 1, 'processed_on' => Carbon::now(), 'updated_by' => Auth::id(), 'updated_on' => Carbon::now()]);
                    break;

                case 'CommissionReject';
                    DB::table('customer_commission')->where('cc_id', $request->relationId)->update(['commission_status' => 0, 'updated_by' => Auth::id(), 'updated_on' => Carbon::now()]);
                    break;

                case 'CommissionDelete';
                    Commission::where('commission_id', $request->relationId)->update(['is_deleted' => 1]);
                    break;

                case 'CommissionUndelete';
                    Commission::where('commission_id', $request->relationId)->update(['is_deleted' => 0]);
                    break;

                case 'MessageActive';
                    Message::where('message_id', $request->relationId)->update(['is_active' => 1]);
                    break;

                case 'MessageInactive';
                    Message::where('message_id', $request->relationId)->update(['is_active' => 0]);
                    break;

                case 'MessageDelete';
                    Message::where('message_id', $request->relationId)->update(['is_deleted' => 1]);
                    break;

                case 'MessageUndelete';
                    Message::where('message_id', $request->relationId)->update(['is_deleted' => 0]);
                    break;

                case 'ModuleDefault';
                    Modules::where('module_id', $request->relationId)->update(['is_default' => 1]);
                    break;

                case 'ModuleOptional';
                    Modules::where('module_id', $request->relationId)->update(['is_default' => 0]);
                    break;

                case 'CustomerActive';
                    Customers::where('customer_id', $request->relationId)->update(['is_active' => 1]);
                    break;

                case 'CustomerInactive';
                    Customers::where('customer_id', $request->relationId)->update(['is_active' => 0]);
                    break;

                case 'CustomerDelete';
                    Customers::where('customer_id', $request->relationId)->update(['is_deleted' => 1]);
                    break;

                case 'CustomerUndelete';
                    Customers::where('customer_id', $request->relationId)->update(['is_deleted' => 0]);
                    break;

                case 'PayslipDelete';
                    DB::table('employee_payslip')->where('payslip_id', $request->relationId)->update(['is_deleted' => 1]);
                    break;

                case 'PayslipUndelete';
                    DB::table('employee_payslip')->where('payslip_id', $request->relationId)->update(['is_deleted' => 0]);
                    break;

                case 'MeetingActive';
                    DB::table('meetings')->where('meeting_id', $request->relationId)->update(['is_active' => 1]);
                    break;

                case 'MeetingInactive';
                    DB::table('meetings')->where('meeting_id', $request->relationId)->update(['is_active' => 0]);
                    break;

                case 'MeetingDelete';
                    DB::table('meetings')->where('meeting_id', $request->relationId)->update(['is_deleted' => 1]);
                    break;

                case 'MeetingUndelete';
                    DB::table('meetings')->where('meeting_id', $request->relationId)->update(['is_deleted' => 0]);
                    break;

                case 'CommunicationViewed';
                    DB::table('communication_employee')->where(['communication_id' => $request->relationId, 'employee_id' => Auth::user()->user_id])->update(['is_viewed' => 1, 'viewed_on' => Carbon::now()]);
                    break;

                case 'CommunicationDelete';
                    DB::table('communication')->where('com_id', $request->relationId)->update(['is_deleted' => 1]);
                    break;

                case 'CommunicationUndelete';
                    DB::table('communication')->where('com_id', $request->relationId)->update(['is_deleted' => 0]);
                    break;

                case 'NewsActive';
                    DB::table('news')->where('news_id', $request->relationId)->update(['is_active' => 1]);
                    break;

                case 'NewsInactive';
                    DB::table('news')->where('news_id', $request->relationId)->update(['is_active' => 0]);
                    break;

                case 'NewsDelete';
                    DB::table('news')->where('news_id', $request->relationId)->update(['is_deleted' => 1]);
                    break;

                case 'NewsUndelete';
                    DB::table('news')->where('news_id', $request->relationId)->update(['is_deleted' => 0]);
                    break;

                case 'YoutubesActive';
                    DB::table('youtubes')->where('youtubes_id', $request->relationId)->update(['is_active' => 1]);
                    break;

                case 'YoutubesInactive';
                    DB::table('youtubes')->where('youtubes_id', $request->relationId)->update(['is_active' => 0]);
                    break;

                case 'YoutubesDelete';
                    DB::table('youtubes')->where('youtubes_id', $request->relationId)->update(['is_deleted' => 1]);
                    break;

                case 'YoutubesUndelete';
                    DB::table('youtubes')->where('youtubes_id', $request->relationId)->update(['is_deleted' => 0]);
                    break;
                
                case 'CompanysActive';
                    DB::table('companys')->where('companys_id', $request->relationId)->update(['is_active' => 1]);
                    break;

                case 'CompanysInactive';
                    DB::table('companys')->where('companys_id', $request->relationId)->update(['is_active' => 0]);
                    break;

                case 'CompanysDelete';
                    DB::table('companys')->where('companys_id', $request->relationId)->update(['is_deleted' => 1]);
                    break;

                case 'CompanysUndelete';
                    DB::table('companys')->where('companys_id', $request->relationId)->update(['is_deleted' => 0]);
                    break;

                case 'PolicyActive';
                    DB::table('customer_insurance')->where('ci_id', $request->relationId)->update(['is_active' => 1]);
                    break;

                case 'PolicyInactive';
                    DB::table('customer_insurance')->where('ci_id', $request->relationId)->update(['is_active' => 0]);
                    break;

                case 'PolicyApprove';
                    DB::table('customer_insurance')->where('ci_id', $request->relationId)->update(['insurance_status' => 'Approve', 'is_approved' => 1, 'updated_by' => Auth::id(), 'updated_on' => Carbon::now()]);
                    Commission::calculateCommission($request->relationId);
                    break;

                case 'PolicyReject';
                    DB::table('customer_insurance')->where('ci_id', $request->relationId)->update(['insurance_status' => 'Reject', 'updated_by' => Auth::id(), 'updated_on' => Carbon::now()]);
                    break;

                case 'PolicyDead';
                    DB::table('customer_insurance')->where('ci_id', $request->relationId)->update(['is_dead' => 1]);
                    break;

                case 'PolicyUndead';
                    DB::table('customer_insurance')->where('ci_id', $request->relationId)->update(['is_dead' => 0]);
                    break;

                case 'PolicyDelete';
                    DB::table('customer_insurance')->where('ci_id', $request->relationId)->update(['is_deleted' => 1]);
                    $this->deductPolicyCommission($request->relationId);
                    break;

                case 'PolicyUndelete';
                    DB::table('customer_insurance')->where('ci_id', $request->relationId)->update(['is_deleted' => 0]);
                    break;

                case 'DocumentApprove';
                    DB::table('policy_documents')->where('pd_id', $request->relationId)->update(['is_approved' => 1]);
                    break;

                case 'DocumentReject';
                    $policy = DB::table('policy_documents')->select('policy_id')->where('pd_id', $request->relationId)->first();
                    if($policy){
                        DB::table('customer_insurance')->where('ci_id', $policy->policy_id)->update(['insurance_status' => 'In Progress']);
                        DB::table('policy_documents')->where('pd_id', $request->relationId)->update(['is_approved' => 0]);
                        DB::table('policy_documents_comments')->insert([
                            'policy_document_id' => $request->relationId,
                            'comment' => $request->comment,
                            'comment_by' => Auth::id(),
                        ]);
                    }
                    break;
                    
                case 'PolicyDocumentDelete';
                    DB::table('policy_documents')->where('pd_id', $request->relationId)->update(['is_deleted' => 1]);
                    break;

                case 'LeadDelete';
                    Lead::where('lead_id', $request->relationId)->update(['is_deleted' => 1]);
                    break;
            }
            return 1;
        }
    }

    public function manageUser(Request $request)
    {
        $user = array();
        if ($request->uId) {
            $user = User::select('users.*')
                ->addSelect(DB::raw("IF(is_admin = 1,'Admin',IF(is_employee = 1,'Employee','Customer')) as role"))
                ->where('user_id', $request->uId)->first();
        }
        $slabs = Commission::where(['is_active' => 1, 'is_deleted' => 0])->pluck('title', 'commission_id')->toArray();
        return response()->json([
            'body' => view('partials.partial-user', compact('user', 'slabs'))->render(),
        ]);
    }

    public function setPassword(Request $request)
    {
        $user = array();
        if ($request->uId) {
            $user = User::where('user_id', $request->uId)->first();
            return response()->json([
                'body' => view('partials.change-password', compact('user'))->render(),
            ]);
        }
    }

    public function manageProvider(Request $request)
    {
        $provider = array();
        if ($request->pId) {
            $provider = InsuranceProviders::where('provider_id', $request->pId)->first();
        }
        return response()->json([
            'body' => view('partials.partial-provider', compact('provider'))->render(),
        ]);
    }

    public function providerMapping(Request $request)
    {
        $provider = InsuranceProviders::where('provider_id', $request->pId)->first();
        $types = InsuranceTypes::select('type_id', 'label_en as typeTitle')->where(['is_active' => 1, 'is_deleted' => 0])->get();
        $mappedProviders = DB::table('provider_types')->where('provider_id', $request->pId)->pluck('type_id')->toArray();
        return response()->json([
            'body' => view('partials.partial-provider-mapping', compact('provider', 'types', 'mappedProviders'))->render(),
        ]);
    }

    public function manageType(Request $request)
    {
        $type = array();
        if ($request->tId) {
            $type = InsuranceTypes::where('type_id', $request->tId)->first();
        }
        return response()->json([
            'body' => view('partials.partial-type', compact('type'))->render(),
        ]);
    }

    public function manageDocument(Request $request)
    {
        $document = array();
        if ($request->dId) {
            $document = Documents::where('document_id', $request->dId)->first();
        }
        return response()->json([
            'body' => view('partials.partial-document', compact('document'))->render(),
        ]);
    }

    public function manageCommission(Request $request)
    {
        $commission = array();
        if ($request->cId) {
            $commission = Commission::where('commission_id', $request->cId)->first();
        }
        return response()->json([
            'body' => view('partials.partial-commission', compact('commission'))->render(),
        ]);
    }

    public function monthlyCommission(Request $request)
    {
        $employees = User::select('user_id', 'name as employeeName', 'commissions.minimum', 'commissions.target_to')
            ->addSelect(DB::raw("(SELECT COALESCE(SUM(policy_amount),0) FROM customer_commission WHERE customer_commission.employee_id = users.id AND customer_commission.commission_type = 'Credit' AND MONTH(customer_commission.processed_on) = MONTH(CURRENT_DATE()) AND YEAR(customer_commission.processed_on) = YEAR(CURRENT_DATE())) as actualSales"))
            ->leftJoin('commissions', 'commissions.commission_id', '=', 'users.commission_id')
            ->where(['users.is_employee' => 1, 'users.is_active' => 1, 'users.is_deleted' => 0])
            ->get();
        if (count($employees) > 0) {
            $output = array();
            foreach ($employees as $emp) {
                $output[] = array(
                    'employeeName' => $emp->employeeName,
                    'minimum' => (int)$emp->minimum,
                    'maximum' => (int)$emp->target_to,
                    'actual' => (int)$emp->actualSales
                );
            }
            return json_encode($output);
        }
    }

    public function monthlySales(Request $request)
    {
        $prePerioud = 6;
        $ranking = DB::table('customer_commission')
            ->select(DB::raw("MONTH(updated_on) as month, YEAR(updated_on) as year, SUM(sales_point) as salesPointCount"))
            ->where(['commission_status' => 1, 'employee_id' => auth()->id()])
            ->whereBetween('updated_on', 
                [Carbon::today()->startOfMonth()->subMonth($prePerioud), Carbon::now()]
            )
            ->groupBy('month')
            ->groupBy('year')
            ->orderBy('updated_on', 'desc')
            ->get()->toArray();
        
        $months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        $monthWiseData = [];
        for ($a = 0; $a <= $prePerioud; $a++) {
            $i = date('n', strtotime(-$a . 'month'));
            $j = date('Y', strtotime(-$a . 'month'));
            if ($ranking && in_array($i, array_column($ranking, 'month')) && in_array($j, array_column($ranking, 'year'))) {
                $monthWiseData[$a]['sale'] = (int)Arr::first($ranking, function($v, $k) use ($i, $j) {
                    return $v->month == $i && $v->year == $j;
                })->salesPointCount;
            } else {
                $monthWiseData[$a]['sale'] = 0;
            }
            $monthWiseData[$a]['month'] = $months[$i - 1]; //.'-'.$j;
        }
        return json_encode(array_reverse($monthWiseData));
    }

    public function manageMessage(Request $request)
    {
        $message = array();
        if ($request->mId) {
            $message = Message::where('message_id', $request->mId)->first();
        }
        $types = array('Messages' => 'Messages', 'Applications' => 'Applications', 'My Customers' => 'My Customers', 'Commission' => 'Commission', 'Customer Detail' => 'Customer Detail');
        $actions = Message::select('id', 'message_subject as actionTitle')->where(['action_id' => 0])->pluck('actionTitle', 'id')->toArray();
        return response()->json([
            'body' => view('partials.partial-message', compact('message', 'types', 'actions'))->render(),
        ]);
    }

    public function manageLead(Request $request)
    {
        $lead = array();
        if ($request->lId) {
            $lead = Lead::where('lead_id', $request->lId)->first();
        }
        $view = $request->has('isDelete')? 'partials.partial-lead-delete' : 'partials.partial-lead';
        return response()->json([
            'body' => view($view, compact('lead'))->render(),
        ]);
    }

    public function leadMapping(Request $request)
    {
        $lead = Lead::where('lead_id', $request->lId)->first();
        $employees = User::select('user_id', 'name as employeeName')->where(['is_employee' => 1, 'is_active' => 1, 'is_deleted' => 0])->get();
        $mappedEmployees = DB::table('lead_employee')->where('lead_id', $request->lId)->pluck('employee_id')->toArray();
        return response()->json([
            'body' => view('partials.partial-lead-mapping', compact('lead', 'employees', 'mappedEmployees'))->render(),
        ]);
    }

    public function leadChart(Request $request)
    {
        $leadChartData = Lead::select(
            DB::raw("(COUNT(*)) as count"),
            DB::raw("MONTHNAME(created_at) as month_name"),
            DB::raw("YEAR(created_at) as year")
            )
            ->where('leads.is_deleted', 0)         
            ->groupBy('month_name')
            ->get()->toArray();
        $months = array_column($leadChartData,'month_name');
        $year = array_column($leadChartData,'year');
        $count_data = array_column($leadChartData,'count');
        return response()->json([
            'data' => $count_data,
            'month' => $months,
            'year' => $year
        ]);
    }

    public function leadCall(Request $request)
    {
        $lead = LeadCustomer::find($request->lId);
        return response()->json([
            'body' => view('partials.partial-lead-call', compact('lead'))->render(),
        ]);
    }

    public function manageModule(Request $request)
    {
        $module = array();
        if ($request->mId) {
            $module = Modules::where('module_id', $request->mId)->first();
        }
        return response()->json([
            'body' => view('partials.partial-module', compact('module'))->render(),
        ]);
    }

    public function manageCustomer(Request $request)
    {
        $employee = User::select('id', 'name')->where(['is_employee' => 1, 'is_active' => 1, 'is_blocked' => 0, 'is_deleted' => 0])->pluck('name', 'id')->toArray();
        $customer = array();
        if ($request->cId) {
            $customer = Customers::where('customer_id', $request->cId)->first();
        }
        return response()->json([
            'body' => view('partials.partial-customer', compact('customer', 'employee'))->render(),
        ]);
    }

    public function manageMember(Request $request)
    {
        $customer = $member = array();
        if ($request->cId) {
            $customer = Customers::where('customer_id', $request->cId)->first();
        }
        if ($request->mId) {
            $member = Customers::where('customer_id', $request->mId)->first();
        }
        return response()->json([
            'body' => view('partials.partial-member', compact('customer', 'member'))->render(),
        ]);
    }

    public function managePolicy(Request $request)
    {
        $policy = $employee = array();
        if ($request->cId) {
            $policy = DB::table('customer_insurance')->where(['ci_id' => $request->pId])->first();
        }
        $customer = Customers::where('customer_id', $request->cId)->first();
        if (is_null($customer->employee_id) || $customer->employee_id == 0) {
            $employee = User::select('id', 'name')->where(['is_employee' => 1, 'is_active' => 1, 'is_blocked' => 0, 'is_deleted' => 0])->pluck('name', 'id')->toArray();
        }
        $providers = InsuranceProviders::where(['is_active' => 1, 'is_deleted' => 0])->pluck('label_en', 'provider_id')->toArray();
        $types = array();
        return response()->json([
            'body' => view('partials.partial-policy', compact('policy', 'customer', 'providers', 'types', 'employee'))->render(),
        ]);
    }

    public function customerPolicies(Request $request)
    {
        $customer = Customers::where('customer_id', $request->cId)->first();
        $policies = DB::table('customer_insurance')
            ->select('customer_insurance.*', 'employee.name as employeeName', 'approver.name as approverName', 'insurance_providers.label_en as insProvider', 'insurance_types.label_en as insType')
            ->leftJoin('customers', 'customers.customer_id', '=', 'customer_insurance.customer_id')
            ->leftJoin('insurance_providers', 'insurance_providers.provider_id', '=', 'customer_insurance.provider_id')
            ->leftJoin('insurance_types', 'insurance_types.type_id', '=', 'customer_insurance.type_id')
            ->leftJoin('users as employee', 'employee.id', '=', 'customer_insurance.employee_id')
            ->leftJoin('users as approver', 'approver.id', '=', 'customer_insurance.updated_by')
            ->where(['customer_insurance.customer_id' => $request->cId, 'customer_insurance.is_dead' => 0, 'customer_insurance.is_deleted' => 0])
            ->get();
        $no = 1;
        return response()->json([
            'body' => view('partials.partial-customer-policies', compact('customer', 'policies', 'no'))->render(),
        ]);
    }

    public function policyDocuments(Request $request)
    {
        $policy = DB::table('customer_insurance')->where(['ci_id' => $request->pId])->first();
        $policyDocuments = DB::table('policy_documents')
            ->select('documents.label_en as docTitle', 'policy_documents.*', DB::raw('COUNT(policy_documents_comments.id) as comment_count'))
            ->leftJoin('documents', 'documents.document_id', '=', 'policy_documents.document_id')
            ->leftJoin('policy_documents_comments', 'policy_documents_comments.policy_document_id', '=', 'policy_documents.pd_id')
            ->where(['policy_documents.policy_id' => $policy->ci_id, 'policy_documents.is_deleted' => 0])
            ->groupBy('policy_documents.pd_id')
            ->get();
        if (Auth::user()->is_admin) {
            $documents = Documents::where(['is_active' => 1, 'is_deleted' => 0])
                ->whereNotIn('document_id', function ($docs) use ($policy) {
                    $docs->select('document_id')->from('policy_documents')
                        ->where([
                            'policy_id' => $policy->ci_id, 
                            'is_pending' => 0, 
                            'is_deleted' => 0
                        ]);
                })->pluck('label_en', 'document_id')->toArray();
        }else if(Auth::user()->is_employee){
            $documents = Documents::where(['is_active' => 1, 'is_deleted' => 0])
                ->whereNotIn('document_id', function ($docs) use ($policy) {
                    $docs->select('document_id')->from('policy_documents')
                        ->where([
                            'policy_id' => $policy->ci_id, 
                            'is_pending' => 0, 
                            'is_deleted' => 0,
                        ])
                        ->whereIn('is_approved', [1, 2]);
                })->pluck('label_en', 'document_id')->toArray();
        }
        $no = 1;
        return response()->json([
            'body' => view('partials.partial-policy-documents', compact('policy', 'documents', 'policyDocuments', 'no'))->render(),
        ]);
    }

    public function providerTypes(Request $request)
    {
        $types = InsuranceTypes::select('insurance_types.type_id as typeId', 'label_en as typeTitle')
            ->leftJoin('provider_types', 'provider_types.type_id', '=', 'insurance_types.type_id')
            ->where(['provider_types.provider_id' => $request->providerId, 'insurance_types.is_active' => 1, 'insurance_types.is_deleted' => 0])
            ->get();
        return json_encode($types);
    }

    public function typeDetails(Request $request)
    {
        $type = InsuranceTypes::select('min_duration', 'max_duration')
            ->addSelect(DB::raw("CAST(min_amount AS DECIMAL(0)) as min_amount"))
            ->addSelect(DB::raw("CAST(max_amount AS DECIMAL(0)) as max_amount"))
            ->where('type_id', $request->typeId)->first();
        return response()->json([
            'body' => view('partials.partial-type-slider', compact('type'))->render(),
        ]);
    }

    public function managePayslip(Request $request)
    {
        $paySlip = array();
        if ($request->pId) {
            $paySlip = DB::table('employee_payslip')->where(['payslip_id' => $request->pId])->first();
        }
        $employee = User::select('id', 'name')->where(['is_employee' => 1, 'is_active' => 1, 'is_blocked' => 0, 'is_deleted' => 0])->pluck('name', 'id')->toArray();
        return response()->json([
            'body' => view('partials.partial-payslip', compact('employee', 'paySlip'))->render(),
        ]);
    }

    public function manageMeeting(Request $request)
    {
        $meeting = array();
        if ($request->mId) {
            $meeting = DB::table('meetings')->where(['meeting_id' => $request->mId])->first();
        }
        $employees = User::select('user_id as employee_id', 'name as employeeName')->where(['is_employee' => 1, 'is_active' => 1, 'is_deleted' => 0])->get();
        $mappedEmployees = DB::table('meeting_employee')->where('meeting_id', $request->mId)->pluck('employee_id')->toArray();
        return response()->json([
            'body' => view('partials.partial-meeting', compact('meeting', 'employees', 'mappedEmployees'))->render(),
        ]);
    }

    public function manageCommunication(Request $request)
    {
        $communication = $comEmployees = array();
        if ($request->cId) {
            $communication = DB::table('communication')->where(['com_id' => $request->cId])->first();
            $comEmployees = DB::table('communication_employee')->where('communication_id', $request->cId)->pluck('employee_id')->toArray();
        }
        $employees = User::select('user_id as employee_id', 'name as employeeName')->where(['is_employee' => 1, 'is_active' => 1, 'is_deleted' => 0])->get();
        return response()->json([
            'body' => view('partials.partial-communication', compact('communication', 'employees', 'comEmployees'))->render(),
        ]);
    }

    public function manageConversation(Request $request)
    {
        $empData = DB::table('users')->where('user_id', $request->uId)->first();
        $conversation = DB::table('chat_communication')
            ->select('chat_communication.id', 'chat_id', 'parent_id', 'chat_communication.asked_by as askedBy', 'chat_communication.communication', 'chat_communication.communication_date as communicationDate', 'chat_communication.attachment', 'is_reply as isReply', 'self.name as selfName', 'self.profile_image as selfPicture', 'chat_communication.replied_by as replyBy', 'crmemp.name as replyName', 'crmemp.profile_image as replyPicture')
            ->leftJoin('users as self', 'self.id', '=', 'chat_communication.asked_by')
            ->leftJoin('users as crmemp', 'crmemp.id', '=', 'chat_communication.replied_by')
            ->whereRaw("(replied_by = $empData->id AND asked_by = " . Auth::id() . ") OR (asked_by = $empData->id AND replied_by = " . Auth::id() . ")")
            ->orderBy('communication_date', 'ASC')
            ->get();
        DB::table('chat_communication')->where(['replied_by' => Auth::id(), 'asked_by' => $empData->id])->update(['is_viewed' => 1]);
        return response()->json([
            'body' => view('partials.partial-conversation', compact('empData', 'conversation'))->render()
        ]);
    }
    public function manageChatConversation(Request $request)
    {
        $empData = DB::table('users')->where('user_id', $request->uId)->first();
        $conversation = DB::table('chat_communication')
            ->select('chat_communication.id', 'chat_id', 'parent_id', 'chat_communication.asked_by as askedBy', 'chat_communication.communication', 'chat_communication.communication_date as communicationDate', 'chat_communication.attachment', 'is_reply as isReply', 'self.name as selfName', 'self.profile_image as selfPicture', 'chat_communication.replied_by as replyBy', 'crmemp.name as replyName', 'crmemp.profile_image as replyPicture')
            ->leftJoin('users as self', 'self.id', '=', 'chat_communication.asked_by')
            ->leftJoin('users as crmemp', 'crmemp.id', '=', 'chat_communication.replied_by')
            ->whereRaw("(replied_by = $empData->id AND asked_by = " . Auth::id() . ") OR (asked_by = $empData->id AND replied_by = " . Auth::id() . ")")
            ->orderBy('communication_date', 'ASC')
            ->get();
        DB::table('chat_communication')->where(['replied_by' => Auth::id(), 'asked_by' => $empData->id])->update(['is_viewed' => 1]);
        return response()->json([
            'body' => view('partials.partial-chat-conversation', compact('empData', 'conversation'))->render()
        ]);
    }
    public function manageDashboardChatConversation(Request $request)
    {
        $empData = DB::table('users')->where('user_id', $request->uId)->first();
        $conversation = DB::table('chat_communication')
            ->select('chat_communication.id', 'chat_id', 'parent_id', 'chat_communication.asked_by as askedBy', 'chat_communication.communication', 'chat_communication.communication_date as communicationDate', 'chat_communication.attachment', 'is_reply as isReply', 'self.name as selfName', 'self.profile_image as selfPicture', 'chat_communication.replied_by as replyBy', 'crmemp.name as replyName', 'crmemp.profile_image as replyPicture')
            ->leftJoin('users as self', 'self.id', '=', 'chat_communication.asked_by')
            ->leftJoin('users as crmemp', 'crmemp.id', '=', 'chat_communication.replied_by')
            ->whereRaw("(replied_by = $empData->id AND asked_by = " . Auth::id() . ") OR (asked_by = $empData->id AND replied_by = " . Auth::id() . ")")
            ->orderBy('communication_date', 'ASC')
            ->get();
        DB::table('chat_communication')->where(['replied_by' => Auth::id(), 'asked_by' => $empData->id])->update(['is_viewed' => 1]);
        return response()->json([
            'body' => view('partials.partial-dashboard-conversion', compact('empData', 'conversation'))->render()
        ]);
    }

    public function chatReply(Request $request)
    {
        $empData = DB::table('users')->where('user_id', $request->userId)->first();
        DB::table('chat_communication')->insert([
            'chat_id' => md5(microtime()),
            'replied_by' => $empData->id,
            'communication' => $request->communication,
            'is_reply' => $request->parentId ? 1 : 0,
            'asked_by' => Auth::id(),
            'communication_date' => Carbon::now(),
            'parent_id' => $request->parentId ?? 0
        ]);
        return 1;
    }

    public function manageNews(Request $request)
    {
        $news = array();
        if ($request->nId) {
            $news = DB::table('news')->where(['news_id' => $request->nId])->first();
        }
        return response()->json([
            'body' => view('partials.partial-news', compact('news'))->render(),
        ]);
    }

    public function manageYoutubes(Request $request)
    {
        $youtubes = array();
        if ($request->yId) {
            $youtubes = DB::table('youtubes')->where(['youtubes_id' => $request->yId])->first();
        }
        return response()->json([
            'body' => view('partials.partial-youtubes', compact('youtubes'))->render(),
        ]);
    }

    public function manageCompanys(Request $request)
    {
        $companys = array();
        if ($request->cId) {
            $companys = DB::table('companys')->where(['companys_id' => $request->cId])->first();
        }
        return response()->json([
            'body' => view('partials.partial-companys', compact('companys'))->render(),
        ]);
    }

    public function loadContacts(Request $request)
    {
        $userID = Auth::id();
        if (Auth::user()->is_admin) {
            $employees = User::select('id', 'user_id', 'name', 'email', 'profile_image')
                ->addSelect(DB::raw("(SELECT COUNT(1) FROM chat_communication WHERE chat_communication.asked_by = users.id and chat_communication.replied_by = $userID AND chat_communication.is_viewed = 0) as unreadCount"))
                ->where(['is_employee' => 1, 'is_active' => 1, 'is_deleted' => 0])
                ->get();
        } else {
            $employees = User::select('id', 'user_id', 'name', 'email', 'profile_image')
                ->addSelect(DB::raw("(SELECT COUNT(1) FROM chat_communication WHERE chat_communication.asked_by = users.id and chat_communication.replied_by = $userID AND chat_communication.is_viewed = 0) as unreadCount"))
                ->where(['is_active' => 1, 'is_deleted' => 0])
                ->whereIn('id', function ($users) {
                    $users->distinct()->select('replied_by')->from('chat_communication')->where(['asked_by' => Auth::id()]);
                })->orWhereIn('id', function ($users) {
                    $users->distinct()->select('asked_by')->from('chat_communication')->where(['replied_by' => Auth::id()]);
                })->get();
        }

        return response()->json([
            'body' => view('partials.partial-contacts-body', compact('employees'))->with('no', 1)->render()
        ]);
    }

    public function loadDashboardContacts(Request $request)
    {
        $userID = Auth::id();
        if (Auth::user()->is_admin) {
            $employees = User::select('id', 'user_id', 'name', 'email', 'profile_image')
                ->addSelect(DB::raw("(SELECT COUNT(1) FROM chat_communication WHERE chat_communication.asked_by = users.id and chat_communication.replied_by = $userID AND chat_communication.is_viewed = 0) as unreadCount"))
                ->where(['is_employee' => 1, 'is_active' => 1, 'is_deleted' => 0])
                ->get();
        } else {
            $employees = User::select('id', 'user_id', 'name', 'email', 'profile_image')
                ->addSelect(DB::raw("(SELECT COUNT(1) FROM chat_communication WHERE chat_communication.asked_by = users.id and chat_communication.replied_by = $userID AND chat_communication.is_viewed = 0) as unreadCount"))
                ->where(['is_active' => 1, 'is_deleted' => 0])
                ->whereIn('id', function ($users) {
                    $users->distinct()->select('replied_by')->from('chat_communication')->where(['asked_by' => Auth::id()]);
                })->orWhereIn('id', function ($users) {
                    $users->distinct()->select('asked_by')->from('chat_communication')->where(['replied_by' => Auth::id()]);
                })->get();
        }

        return response()->json([
            'body' => view('partials.partial-contacts-body', compact('employees'))->with('no', 1)->render()
        ]);
    }

    public function chatCount(){
        if (Auth::check()) {
            // $communicationCounts = DB::table('communication_employee')
            //     ->leftJoin('communication', 'communication.com_id', '=', 'communication_employee.communication_id')
            //     ->where(['employee_id' => Auth::user()->user_id, 'is_viewed' => 0, 'communication.is_deleted' => 0])
            //     ->count();
            // $view->with('communicationCount', $communicationCounts);
            $chatCount = DB::table('chat_communication')
                ->where(['replied_by' => Auth::id(), 'is_viewed' => 0])
                ->count();
            return json_encode($chatCount);
            // return response()->json([
            //     'body' => view('partials.partial-contacts-body', compact('employees'))->with('no', 1)->render()
            // ]);
                // $view->with('chatCount', $chatCount);

        }
    }

    public function policyDocument(Request $request)
    {      
        if ($request->hasfile('policy_document')) {
            $leadFile = $request->file('policy_document');
            $fileName = md5(microtime()) . '.' . $leadFile->getClientOriginalExtension();
            $docFileName = $leadFile->getClientOriginalName();
            $docFilePath = $request->file('policy_document')->storeAs('uploads/' . date('Y') . '/' . date('m'), $fileName, 'public');
            $isPending = 0;
        } else {
            $docFileName = $docFilePath = '';
            $isPending = 1;
        }
        $policyDocument = DB::table('policy_documents')->where(['policy_id' => $request->policy_id, 'document_id' => $request->document_id])->whereIn('is_pending', [0,1])->first();
        if ($policyDocument) {
            $data = [
                'document_name' => $docFileName,
                'document_path' => $docFilePath,
                'is_pending' => $isPending
            ];
            if($policyDocument->is_approved===0){
                $data['is_approved']=2;
            }
            DB::table('policy_documents')->where('pd_id', $policyDocument->pd_id)->update($data);
        } else {
            DB::table('policy_documents')->insert([
                'pd_id' => md5(microtime()),
                'policy_id' => $request->policy_id,
                'document_id' => $request->document_id,
                'document_name' => $docFileName,
                'document_path' => $docFilePath,
                'is_pending' => $isPending
            ]);
        }
        session()->flash('level', 'success');
        session()->flash('message', 'Policy Document Uploaded Successfully');
        return json_encode('done');
        // return redirect()->back();
    }

    public function policyDocumentAddComment(Request $request)
    {      
        $policy_document_id = $request->policy_document_id;
        $comment = $request->comment;
        $user_id = Auth::id();

        DB::table('policy_documents_comments')->insert([
            'policy_document_id' => $policy_document_id,
            'comment' => $comment,
            'comment_by' => $user_id,            
        ]);

        session()->flash('level', 'success');
        session()->flash('message', 'Comment added successfully');
        return json_encode('done');
    }
    public function policyDocumentShowComment(Request $request)
    {
        // dd($request->all());
        $policyComments = DB::table('policy_documents')
            ->select('policy_documents_comments.comment','policy_documents_comments.comment_by','users.name', 'policy_documents.*','policy_documents_comments.created_at')
            ->leftJoin('policy_documents_comments', 'policy_documents_comments.policy_document_id', '=', 'policy_documents.pd_id')
            ->leftJoin('users', 'users.id', '=', 'policy_documents_comments.comment_by')
            ->where(['policy_documents.pd_id' => $request->pId, 'policy_documents.is_deleted' => 0])
            ->get();
        $no = 1;
        return response()->json([
            'body' => view('partials.partial-policy-show-documents', compact('policyComments','no'))->render(),
        ]);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Lead;
use App\Models\LeadCustomer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\LeadExport;

class LeadController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function index()
    {
        $date_data = '';
        $count_data = '';
        if (Auth::user()->is_admin) {
            $leads = Lead::select('leads.*', 'users.name as createdBy')
                ->addSelect(DB::raw('DATE_FORMAT(leads.created_at,"%d-%b-%Y %h:%i %p") as createdOn'))
                ->leftJoin('users', 'users.id', '=', 'leads.created_by')
                ->where('leads.is_deleted', 0)
                ->get();

            
        } else {
            $leads = Lead::select('leads.*', 'users.name as createdBy', 'users.user_id as customer_id')
                ->addSelect(DB::raw('DATE_FORMAT(leads.created_at,"%d-%b-%Y %h:%i %p") as createdOn'))
                ->leftJoin('users', 'users.id', '=', 'leads.created_by')
                ->whereIn('lead_id', function ($empLeads) {
                    $empLeads->select('lead_id')->from('lead_employee')->where('employee_id', Auth::user()->user_id);
                })->where(['leads.is_active' => 1, 'leads.is_deleted' => 0])->get();
        }
        return view('admin.leads-list', compact('leads'))->with('no', 1);
    }

    public function createLead(Request $request, $leadId = null)
    {
        if ($request->hasfile('file_url')) {
            $leadFile = $request->file('file_url');
            $fileName = md5(microtime()) . '.' . $leadFile->getClientOriginalExtension();
            $leadFileName = $leadFile->getClientOriginalName();
            $leadFilePath = $request->file('file_url')->storeAs('uploads/' . date('Y') . '/' . date('m'), $fileName, 'public');
        } else {
            $leadFileName = $leadFilePath = '';
        }
        if ($leadId) {
            $lead = Lead::where('lead_id', $leadId)->first();
            $lead->title = $request->title;
            $lead->description = $request->description;
            $lead->file_name = $leadFileName;
            $lead->file_url = $leadFilePath;
            $lead->save();
            $status =  \Excel::import(new LeadExport($leadId),request()->file('file_url'));
    
            session()->flash('level', 'success');
            session()->flash('message', 'Lead Detail Updated Successfully');
        } else {
            $lead = new Lead();
            $lead->lead_id = md5(microtime());
            $lead->title = $request->title;
            $lead->description = $request->description;
            $lead->file_name = $leadFileName;
            $lead->file_url = $leadFilePath;
            $lead->created_by = Auth::id();
            $lead->save();
            $status =  \Excel::import(new LeadExport($lead->lead_id),request()->file('file_url'));
            session()->flash('level', 'success');
            session()->flash('message', 'Lead Created Successfully');
        }
        return redirect()->back();
    }

    public function mappingLead(Request $request, $leadId = null)
    {
        if (count($request->leadEmployees) > 0) {
            DB::table('lead_employee')->where('lead_id', $leadId)->delete();
            $leadEmployees = array();
            foreach ($request->leadEmployees as $employeeId) {
                $leadEmployees[] = array(
                    'employee_id' => $employeeId,
                    'lead_id' => $leadId,
                );
            }
            if (count($leadEmployees) > 0) {
                DB::table('lead_employee')->insert($leadEmployees);
            }
        }
        session()->flash('level', 'success');
        session()->flash('message', 'Lead Employee Mapped Successfully');
        return redirect()->back();
    }

    public function updateLead(Request $request, $leadId = null){
        $validator = \Validator::make($request->all(),[
            'status' => 'required',
            'reason' => 'required'
        ]);
        if($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $leadCustomer = LeadCustomer::find($leadId);
        if ($leadCustomer) {
            $leadCustomer->reason = $request->input('reason');
            $leadCustomer->status = $request->input('status');
            $leadCustomer->save();
        }
        session()->flash('level', 'success');
        session()->flash('message', 'Lead Employee Mapped Successfully');
        return redirect()->back();
    }

    public function view(Request $request, $leadId = null){
        $leadCustomer = LeadCustomer::where(['lead_id'=>$leadId])->get();
        
        return view('admin.leads-list-view', compact('leadCustomer'));
    }

    public function accept($leadId = null){
        $leadCustomer = LeadCustomer::find($leadId);
        if ($leadCustomer) {
            $leadCustomer->lead_call_id = Auth::user()->user_id;
            $leadCustomer->save();
            session()->flash('level', 'success');
            session()->flash('message', 'Lead Called Accepted Successfully');
        }
        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Documents;
use Illuminate\Http\Request;

class DocumentsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }


    public function index()
    {
        $documents = Documents::where('is_deleted', 0)->get();
        return view('admin.documents-list', compact('documents'))->with('no', 1);
    }

    public function createDocument(Request $request, $documentId = null)
    {
        if ($documentId) {
            $document = Documents::where('document_id', $documentId)->first();
            $document->label_en = $request->label_en;
            $document->label_de = $request->label_de;
            $document->label_fr = $request->label_fr;
            $document->label_it = $request->label_it;
            $document->save();
            session()->flash('level', 'success');
            session()->flash('message', 'Document Updated Successfully');
        } else {
            $document = new Documents();
            $document->document_id = md5(microtime());
            $document->label_en = $request->label_en;
            $document->label_de = $request->label_de;
            $document->label_fr = $request->label_fr;
            $document->label_it = $request->label_it;
            $document->save();
            session()->flash('level', 'success');
            session()->flash('message', 'Document Created Successfully');
        }
        return redirect()->back();
    }
}

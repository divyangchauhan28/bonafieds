<?php

namespace App\Http\Controllers;

use App\Models\Commission;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CommissionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }


    public function index()
    {
        $commissions = Commission::where('is_deleted', 0)->get();
        return view('admin.commissions-list', compact('commissions'))->with('no', 1);
    }

    public function createCommission(Request $request, $commissionId = null)
    {
        if ($commissionId) {
            $type = Commission::where('commission_id', $commissionId)->first();
            $type->title = $request->title;
            $type->target_from = $request->target_from;
            $type->target_to = $request->target_to;
            $type->approach = $request->approach;
            $type->save();
            session()->flash('level', 'success');
            session()->flash('message', 'Commission Slab Updated Successfully');
        } else {
            $type = new Commission();
            $type->commission_id = md5(microtime());
            $type->title = $request->title;
            $type->target_from = $request->target_from;
            $type->target_to = $request->target_to;
            $type->approach = $request->approach;
            $type->save();
            session()->flash('level', 'success');
            session()->flash('message', 'Commission Slab Created Successfully');
        }
        return redirect()->back();
    }

    public function employeeCommission(Request $request)
    {
        $customerId = $employeeId = 0;
        if (Auth::user()->is_admin) {
            if ($request->isMethod('post')) {
                $customerId = $request->customerId ?? 0;
                $employeeId = $request->employeeId ?? 0;
                $policies = DB::table('customer_commission')
                    ->select('customer_insurance.*', 'customers.*', 'employee.name as employeeName', 'approver.name as approverName', 'insurance_providers.label_en as insProvider', 'insurance_types.label_en as insType', 'customer_commission.cc_id', 'customer_commission.sales_point', 'customer_commission.employee_commission', 'customer_commission.commission_status', 'customer_commission.commission_type')
                    ->addSelect(DB::raw('DATE_FORMAT(customer_commission.updated_on,"%d-%b-%Y %h:%i %p") as updatedOn'))
                    ->leftJoin('customer_insurance', 'customer_insurance.ci_id', '=', 'customer_commission.policy_id')
                    ->leftJoin('customers', 'customers.customer_id', '=', 'customer_insurance.customer_id')
                    ->leftJoin('insurance_providers', 'insurance_providers.provider_id', '=', 'customer_insurance.provider_id')
                    ->leftJoin('insurance_types', 'insurance_types.type_id', '=', 'customer_insurance.type_id')
                    ->leftJoin('users as employee', 'employee.id', '=', 'customer_insurance.employee_id')
                    ->leftJoin('users as approver', 'approver.id', '=', 'customer_commission.updated_by')
                    ->where(['customer_insurance.is_dead' => 0, 'customers.is_deleted' => 0]);
                if ($request->customerId) {
                    $policies = $policies->where(['customer_insurance.customer_id' => $request->customerId]);
                }
                if ($request->employeeId) {
                    $policies = $policies->where(['customer_insurance.employee_id' => $request->employeeId]);
                }
                $policies = $policies->get();
            } else {
                $policies = DB::table('customer_commission')
                    ->select('customer_insurance.*', 'customers.*', 'employee.name as employeeName', 'approver.name as approverName', 'insurance_providers.label_en as insProvider', 'insurance_types.label_en as insType', 'customer_commission.cc_id', 'customer_commission.sales_point', 'customer_commission.employee_commission', 'customer_commission.commission_status', 'customer_commission.commission_type')
                    ->addSelect(DB::raw('DATE_FORMAT(customer_commission.updated_on,"%d-%b-%Y %h:%i %p") as updatedOn'))
                    ->leftJoin('customer_insurance', 'customer_insurance.ci_id', '=', 'customer_commission.policy_id')
                    ->leftJoin('customers', 'customers.customer_id', '=', 'customer_insurance.customer_id')
                    ->leftJoin('insurance_providers', 'insurance_providers.provider_id', '=', 'customer_insurance.provider_id')
                    ->leftJoin('insurance_types', 'insurance_types.type_id', '=', 'customer_insurance.type_id')
                    ->leftJoin('users as employee', 'employee.id', '=', 'customer_insurance.employee_id')
                    ->leftJoin('users as approver', 'approver.id', '=', 'customer_commission.updated_by')
                    ->where(['customer_insurance.is_dead' => 0, 'customers.is_deleted' => 0])
                    ->get();
            }
            $customers = DB::table('customer_insurance')
                ->select('customers.customer_id as customerId')
                ->addSelect(DB::raw("CONCAT(salutation,' ',firstname,' ',lastname) as customerName"))
                ->leftJoin('customers', 'customers.customer_id', '=', 'customer_insurance.customer_id')
                ->where(['customer_insurance.is_dead' => 0, 'customer_insurance.is_deleted' => 0])
                ->pluck('customerName', 'customerId')->toArray();
            $employees = User::select('id', 'name')->where(['is_employee' => 1, 'is_active' => 1, 'is_blocked' => 0, 'is_deleted' => 0])->pluck('name', 'id')->toArray();
        } else {
            if ($request->isMethod('post')) {
                $customerId = $request->customerId ?? 0;
                $employeeId = $request->employeeId ?? 0;
                $policies = DB::table('customer_insurance')
                    ->select('customer_insurance.*', 'customers.*', 'employee.name as employeeName', 'approver.name as approverName', 'insurance_providers.label_en as insProvider', 'insurance_types.label_en as insType', 'customer_commission.cc_id', 'customer_commission.sales_point', 'customer_commission.employee_commission', 'customer_commission.commission_status', 'customer_commission.commission_type')
                    ->addSelect(DB::raw('DATE_FORMAT(customer_commission.updated_on,"%d-%b-%Y %h:%i %p") as updatedOn'))
                    ->leftJoin('customer_commission', 'customer_commission.policy_id', '=', 'customer_insurance.ci_id')
                    ->leftJoin('customers', 'customers.customer_id', '=', 'customer_insurance.customer_id')
                    ->leftJoin('insurance_providers', 'insurance_providers.provider_id', '=', 'customer_insurance.provider_id')
                    ->leftJoin('insurance_types', 'insurance_types.type_id', '=', 'customer_insurance.type_id')
                    ->leftJoin('users as employee', 'employee.id', '=', 'customer_insurance.employee_id')
                    ->leftJoin('users as approver', 'approver.id', '=', 'customer_commission.updated_by')
                    ->where(['customer_insurance.employee_id' => Auth::id(), 'customer_insurance.is_approved' => 1, 'customer_insurance.is_dead' => 0, 'customers.is_deleted' => 0]);
                if ($request->customerId) {
                    $policies = $policies->where(['customer_insurance.customer_id' => $request->customerId]);
                }
                $policies = $policies->get();
            } else {
                $customerId = $request->customerId ?? 0;
                $employeeId = $request->employeeId ?? 0;
                $policies = DB::table('customer_insurance')
                    ->select('customer_insurance.*', 'customers.*', 'employee.name as employeeName', 'approver.name as approverName', 'insurance_providers.label_en as insProvider', 'insurance_types.label_en as insType', 'customer_commission.cc_id', 'customer_commission.sales_point', 'customer_commission.employee_commission', 'customer_commission.commission_status', 'customer_commission.commission_type')
                    ->addSelect(DB::raw('DATE_FORMAT(customer_commission.updated_on,"%d-%b-%Y %h:%i %p") as updatedOn'))
                    ->leftJoin('customer_commission', 'customer_commission.policy_id', '=', 'customer_insurance.ci_id')
                    ->leftJoin('customers', 'customers.customer_id', '=', 'customer_insurance.customer_id')
                    ->leftJoin('insurance_providers', 'insurance_providers.provider_id', '=', 'customer_insurance.provider_id')
                    ->leftJoin('insurance_types', 'insurance_types.type_id', '=', 'customer_insurance.type_id')
                    ->leftJoin('users as employee', 'employee.id', '=', 'customer_insurance.employee_id')
                    ->leftJoin('users as approver', 'approver.id', '=', 'customer_commission.updated_by')
                    ->where(['customer_insurance.employee_id' => Auth::id(), 'customer_insurance.is_approved' => 1, 'customer_insurance.is_dead' => 0, 'customers.is_deleted' => 0])
                    ->get();
            }
            $customers = DB::table('customer_insurance')
                ->select('customers.customer_id as customerId')
                ->addSelect(DB::raw("CONCAT(salutation,' ',firstname,' ',lastname) as customerName"))
                ->leftJoin('customers', 'customers.customer_id', '=', 'customer_insurance.customer_id')
                ->where(['customer_insurance.employee_id' => Auth::id(), 'customer_insurance.is_dead' => 0, 'customer_insurance.is_deleted' => 0])
                ->pluck('customerName', 'customerId')->toArray();
            $employees = array();
        }
        return view('admin.employee-commission', compact('customerId', 'employeeId', 'customers', 'employees', 'policies'))->with('no', 1);
    }

    public function employeePaySlip()
    {
        if (Auth::user()->is_admin) {
            $payslips = DB::table('employee_payslip')
                ->select('employee.name as employeeName', 'employee_payslip.*', 'admin.name as adminName')
                ->addSelect(DB::raw('DATE_FORMAT(uploaded_on,"%d-%b-%Y %h:%i %p") as uploadedOn'))
                ->leftJoin('users as employee', 'employee.id', '=', 'employee_payslip.employee_id')
                ->leftJoin('users as admin', 'admin.id', '=', 'employee_payslip.uploaded_by')
                ->where('employee_payslip.is_deleted', 0)
                ->get();
        } else {
            $payslips = DB::table('employee_payslip')
                ->select('employee.name as employeeName', 'employee_payslip.*', 'admin.name as adminName')
                ->addSelect(DB::raw('DATE_FORMAT(uploaded_on,"%d-%b-%Y %h:%i %p") as uploadedOn'))
                ->leftJoin('users as employee', 'employee.id', '=', 'employee_payslip.employee_id')
                ->leftJoin('users as admin', 'admin.id', '=', 'employee_payslip.uploaded_by')
                ->where(['employee_id' => Auth::id(), 'employee_payslip.is_deleted' => 0])
                ->get();
        }
        return view('admin.employee-payslip', compact('payslips'))->with('no', 1);
    }

}

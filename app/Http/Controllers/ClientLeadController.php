<?php

namespace App\Http\Controllers;

use App\Models\Lead;
use App\Models\ClientLead;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade\Pdf;

class ClientLeadController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function index()
    {
        $clientLeads = ClientLead::get();
        // echo "<pre>";print_r($clientLeads);exit;
        return view('admin.clientleads-list', compact('clientLeads'))->with('no', 1);
    }

    public function showLeadStep()
    {
        return view('admin.clientlead-create');
    }

    public function storeLeadStep(Request $request)
    {
        $clientLead = new ClientLead();
        $clientLead->step1 = $request->step1;
        $clientLead->step2 = $request->step2;
        $clientLead->step3 = $request->step3;
        $clientLead->step4 = $request->step4;
        $clientLead->step5 = $request->step5;
        $clientLead->step6 = $request->step6;
        $clientLead->step7 = $request->step7;
        $clientLead->step8 = $request->step8;
        $clientLead->step9 = $request->step9;
        $clientLead->save();

        return redirect()->back();
    }

    public function generatePDF($id)
    {
        $clientLead = ClientLead::find($id)->toArray();
        // echo "<pre>";print_r($clientLead);exit;

        // return view('admin.clientleads-pdf', compact('clientLead'));
        
        $pdf = PDF::loadView('admin/clientleads-pdf', $clientLead);
        return $pdf->download('itsolutionstuff.pdf');
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\InsuranceProviders;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InsuranceProvidersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }


    public function index()
    {
        $providers = InsuranceProviders::where('is_deleted', 0)->get();
        return view('admin.providers-list', compact('providers'))->with('no', 1);
    }

    public function createProvider(Request $request, $providerId = null)
    {
        if ($providerId) {
            $provider = InsuranceProviders::where('provider_id', $providerId)->first();
            $provider->label_en = $request->label_en;
            $provider->label_de = $request->label_de;
            $provider->label_fr = $request->label_fr;
            $provider->label_it = $request->label_it;
            $provider->save();
            session()->flash('level', 'success');
            session()->flash('message', 'Insurance Provider Updated Successfully');
        } else {
            $provider = new InsuranceProviders();
            $provider->provider_id = md5(microtime());
            $provider->label_en = $request->label_en;
            $provider->label_de = $request->label_de;
            $provider->label_fr = $request->label_fr;
            $provider->label_it = $request->label_it;
            $provider->save();
            session()->flash('level', 'success');
            session()->flash('message', 'Insurance Provider Created Successfully');
        }
        return redirect()->back();
    }

    public function mappingProvider(Request $request, $providerId = null)
    {
        if (count($request->providerTypes) > 0) {
            DB::table('provider_types')->where('provider_id', $providerId)->delete();
            $providerTypes = array();
            foreach ($request->providerTypes as $typeId) {
                $providerTypes[] = array(
                    'provider_id' => $providerId,
                    'type_id' => $typeId,
                );
            }
            if (count($providerTypes) > 0) {
                DB::table('provider_types')->insert($providerTypes);
            }
        }
        session()->flash('level', 'success');
        session()->flash('message', 'Provide Types Mapped Successfully');
        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Modules;
use Illuminate\Http\Request;

class ModulesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }


    public function index()
    {
        $modules = Modules::all();
        return view('admin.modules-list', compact('modules'))->with('no', 1);
    }

    public function createModule(Request $request, $moduleId = null)
    {
        if ($moduleId) {
            $module = Modules::where('module_id', $moduleId)->first();
            session()->flash('level', 'success');
            session()->flash('message', 'Module Updated Successfully');
        } else {
            $module = new Modules();
            $module->module_id = md5(microtime());
            $module->module_identifier = $request->module_identifier;
            session()->flash('level', 'success');
            session()->flash('message', 'Module Created Successfully');
        }
        $module->module_title = $request->module_title;
        $module->is_default = $request->is_default;
        $module->save();
        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MessageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }


    public function index()
    {
        $messages = Message::select('messages.*')
            ->addSelect(DB::raw("mt.message_subject as message_action"))
            ->leftJoin('messages as mt', 'mt.id', '=', 'messages.action_id')
            ->get();
        return view('admin.messages-list', compact('messages'))->with('no', 1);
    }

    public function createMessage(Request $request, $messageId = null)
    {
        if ($messageId) {
            $message = Message::where('message_id', $messageId)->first();
            $message->message_type = $request->message_type;
            $message->message_subject = $request->message_subject;
            $message->action_id = $request->action_id ?? 0;
            $message->save();
            session()->flash('level', 'success');
            session()->flash('message', 'Message Updated Successfully');
        } else {
            $message = new Message();
            $message->message_id = md5(microtime());
            $message->message_type = $request->message_type;
            $message->message_subject = $request->message_subject;
            $message->action_id = $request->action_id ?? 0;
            $message->save();
            session()->flash('level', 'success');
            session()->flash('message', 'Message Created Successfully');
        }
        return redirect()->back();
    }
}

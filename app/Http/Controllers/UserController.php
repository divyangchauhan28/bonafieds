<?php

namespace App\Http\Controllers;

use App\Mail\VerifyMail;
use App\Models\User;
use App\Models\VerifyUser;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DataTables;
use Mail;
use phpDocumentor\Reflection\DocBlock\Tags\Uses;


class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);//, 'verified'
    }

    public function index()
    {
        return view('user.index', [
            'user' => User::where('user_id', Auth::user()->user_id)->first()
        ]);
    }

    public function profile()
    {
        return view('user.profile', [
            'user' => User::where('user_id', Auth::user()->user_id)->first()
        ]);
    }

    public function createUser(Request $request, $userId = null)
    {
        if ($userId) {
            $user = User::where('user_id', $userId)->first();
            $user->name = $request->name;
            $user->contact_number = $request->contact_number;
            $user->is_admin = $request->role == "Admin" ? 1 : 0;
            $user->is_employee = $request->role == "Employee" ? 1 : 0;
            $user->commission_id = $request->role == "Employee" ? $request->commission_id : null;
            $user->save();
            session()->flash('level', 'success');
            session()->flash('message', 'User Detail Saved Successfully');
        }
//        else {
//            $user = new User();
//            $user->user_id = md5(microtime());
//            $user->name = $request->name;
//            $user->email = $request->email;
//            $user->password = Hash::make('1234ABCD');
//            $user->contact_number = $request->contact_number;
//            $user->is_admin = $request->role == "Admin" ? 1 : 0;
//            $user->is_employee = $request->role == "Employee" ? 1 : 0;
//            $user->commission_id = $request->commission_id;
//            $user->save();
//            VerifyUser::create(['user_id' => $user->id, 'token' => sha1(time())]);
//            Mail::to($user->email)->send(new VerifyMail($user));
//            session()->flash('level', 'success');
//            session()->flash('message', 'User Detail Updated Successfully');
//        }
        return redirect()->back();
    }

    public function changePassword(Request $request)
    {
        $this->validate($request, [
            'oldpassword' => 'required',
            'password' => ['required', 'string', 'min:8', 'confirmed']
        ]);
        $hashedPassword = Auth::user()->password;
        if (Hash::check($request->oldpassword, $hashedPassword)) {
            if (!Hash::check($request->password, $hashedPassword)) {
                $user = User::find(Auth::user()->id);
                $user->password = Hash::make($request->password);
                $user->save();
                session()->flash('level', 'success');
                session()->flash('message', 'Password Updated Successfully');
                return redirect()->back();
            } else {
                session()->flash('level', 'warning');
                session()->flash('message', 'New password can not be the old Password!');
                return redirect()->back();
            }
        } else {
            session()->flash('level', 'warning');
            session()->flash('message', 'Old password Doesnt Matched ');
            return redirect()->back();
        }
    }

    public function updateProfile(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'profile_image' => 'required|mimes:jpg,JPG,png,PNG,jpeg,JPEG',
            'contact_number' => 'required'
        ]);
        if ($request->hasfile('profile_image')) {
            $profileImage = $request->file('profile_image');
            $fileName = md5(microtime()) . '.' . $profileImage->getClientOriginalExtension();
            $profileImagePath = $request->file('profile_image')->storeAs('uploads/' . date('Y') . '/' . date('m'), $fileName, 'public');
        }
        $user = User::find(Auth::user()->id);
        $user->name = $request->name;
        $user->profile_image = $profileImagePath ?? $user->profile_image;
        $user->contact_number = $request->contact_number;
        $user->save();
        session()->flash('level', 'success');
        session()->flash('message', 'Profile Updated Successfully');
        return redirect()->back();
    }

    public function users()
    {
        $users = User::select('users.*', 'commissions.title as commissionSlabs')
            ->leftJoin('commissions', 'commissions.commission_id', '=', 'users.commission_id')
            ->where('users.is_deleted', 0)
            ->get();
        return view('admin.users-list', compact('users'))->with('no', 1);
    }

    public function getUsers(Request $request)
    {
        if ($request->ajax()) {
            $data = User::latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('role', function ($row) {
                    if ($row->is_employee) {
                        return "<span class='label bg-greensea'>Employee</span>";
                    } elseif ($row->is_admin) {
                        return "<span class='label bg-greensea'>Admin</span>";
                    } else {
                        return "<span class='label bg-warning'>Customer</span>";
                    }
                })
                ->addColumn('status', function ($row) {
                    if ($row->is_active) {
                        return "<span class='label bg-greensea'>Yes</span>";
                    } else {
                        return "<span class='label bg-warning'>No</span>";
                    }
                })
                ->addColumn('blocked', function ($row) {
                    if ($row->is_blocked) {
                        return "<span class='label bg-greensea'>Yes</span>";
                    } else {
                        return "<span class='label bg-warning'>No</span>";
                    }
                })
                ->addColumn('deleted', function ($row) {
                    if ($row->is_deleted) {
                        return "<span class='label bg-greensea'>Yes</span>";
                    } else {
                        return "<span class='label bg-warning'>No</span>";
                    }
                })
                ->addColumn('action', function ($row) {
                    $actionBtn = '<a href="javascript:void(0)" class="edit btn btn-success btn-xs">Edit</a> <a href="javascript:void(0)" class="delete btn btn-danger btn-xs">Delete</a>';
                    return $actionBtn;
                })->rawColumns(['role', 'status', 'blocked', 'deleted', 'action'])
                ->make(true);
        }
    }

    public function managePaySlip(Request $request, $payslipId = null)
    {
        if ($request->hasfile('payslip_file')) {
            $payslipFile = $request->file('payslip_file');
            $fileName = md5(microtime()) . '.' . $payslipFile->getClientOriginalExtension();
            $payslipFilePath = $request->file('payslip_file')->storeAs('uploads/' . date('Y') . '/' . date('m'), $fileName, 'public');
        } else {
            $payslipFilePath = '';
        }
        if ($payslipId) {
            $payslipDetail = DB::table('employee_payslip')->where('payslip_id', $payslipId)->first();
            DB::table('employee_payslip')->where('payslip_id', $payslipId)->update([
                'month' => $request->month,
                'year' => $request->year,
                'payslip_file' => $payslipFilePath ?? $payslipDetail->payslip_file,
                'payslip_amount' => $request->payslip_amount
            ]);
            session()->flash('level', 'success');
            session()->flash('message', 'Employee Payslip Updated Successfully');
        } else {
            DB::table('employee_payslip')->insert([
                'payslip_id' => md5(microtime()),
                'employee_id' => $request->employee_id,
                'month' => $request->month,
                'year' => $request->year,
                'payslip_file' => $payslipFilePath,
                'payslip_amount' => $request->payslip_amount,
                'uploaded_by' => Auth::id(),
                'uploaded_on' => Carbon::now()
            ]);
            session()->flash('level', 'success');
            session()->flash('message', 'Employee Payslip Uploaded Successfully');
        }
        return redirect()->back();
    }

    public function userPassword(Request $request, $userId = null)
    {
        if ($userId) {
            $user = User::where('user_id', $userId)->first();
            $user->password = Hash::make($request->password);
            $user->save();
            session()->flash('level', 'success');
            session()->flash('message', 'Employee / Admin Password Changed Successfully');
            return redirect()->back();
        }
    }

}

<?php
namespace App\Exports;

use App\Exports\AktuellerBestandSheetImport;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;


class LeadExport implements WithMultipleSheets
{
    public $leadId;
    public function __construct($leadId = '')
    {
        $this->leadId = $leadId;
    }
    public function sheets(): array
    {
        return [
            0 => null,
            1 => new AktuellerBestandSheetImport($this->leadId),
            
        ];
    }


}

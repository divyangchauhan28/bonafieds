<?php
namespace App\Exports;

use App\Models\LeadCustomer;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class AktuellerBestandSheetImport implements ToModel, WithHeadingRow
{
    use Importable;

    public $leadId;
    public function __construct($leadId = '')
    {
        $this->leadId = $leadId;
    }
    /**
     * @param array $row
     *
     * @return User|null
     */
    public function model(array $row)
    {
        if(!empty($row['nachname'])) {
            $leadCustomer = new LeadCustomer();
            $leadCustomer->lead_id = $this->leadId;
            $leadCustomer->nachname = isset($row['nachname']) ? $row['nachname'] : null;
            $leadCustomer->vorname = isset($row['vorname']) ? $row['vorname'] : null;
            $leadCustomer->plz = isset($row['plz']) ? $row['plz'] : null;
            $leadCustomer->land = isset($row['land']) ? $row['land'] : null;
            $leadCustomer->ortschaft = isset($row['ortschaft']) ? $row['ortschaft'] : null;
            $leadCustomer->strasse = isset($row['strasse']) ? $row['strasse'] : null;
            $leadCustomer->haus_nummer = isset($row['haus_nummer']) ? $row['haus_nummer'] : null;
            $leadCustomer->nummer = isset($row['nummer']) ? $row['nummer'] : null;
            $leadCustomer->bemerkungen = isset($row['bemerkungen']) ? $row['bemerkungen'] : null;
            $leadCustomer->empfehlungsgeber = isset($row['empfehlungsgeber']) ? $row['empfehlungsgeber'] : null;
            $leadCustomer->aktueller_bestand = isset($row['aktueller_bestand']) ? $row['aktueller_bestand'] : null;
            $leadCustomer->a_kunde = isset($row['a_kunde']) ? $row['a_kunde'] : null;
            $leadCustomer->b_kunde = isset($row['b_kunde']) ? $row['b_kunde'] : null;
            $leadCustomer->c_kunde = isset($row['c_kunde']) ? $row['c_kunde'] : null;
            $leadCustomer->erreicht = isset($row['erreicht']) ? $row['erreicht'] : null;
            $leadCustomer->termin = isset($row['termin']) ? $row['termin'] : null;
            $leadCustomer->abschluss = isset($row['abschluss']) ? $row['abschluss'] : null;
            $leadCustomer->save();
        }
    }

}

<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\AjaxController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\CommissionController;
use App\Http\Controllers\CustomersController;
use App\Http\Controllers\DocumentsController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\InsuranceProvidersController;
use App\Http\Controllers\InsuranceTypesController;
use App\Http\Controllers\LeadController;
use App\Http\Controllers\ClientLeadController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\ModulesController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes(['verify' => true]);
//Route::get('/email/verify/{userId}/{token}', [RegisterController::class, 'verifyUser'])->name('verify.user');
Route::get('change/lang', [HomeController::class, 'lang_change'])->name('LangChange');
Route::group(['middleware' => ['auth']], function () {
    // AJAX Routes
    Route::any('ajax/change-universal-enum', [AjaxController::class, 'ChangeUniversalEnum'])->name('change.universal.enum');
    Route::any('ajax/manage-user', [AjaxController::class, 'manageUser'])->name('ajax.manage.user');
    Route::any('ajax/set-password', [AjaxController::class, 'setPassword'])->name('ajax.set.password');
    Route::any('ajax/manage-provider', [AjaxController::class, 'manageProvider'])->name('ajax.manage.provider');
    Route::any('ajax/provider-mapping', [AjaxController::class, 'providerMapping'])->name('ajax.provider.mapping');
    Route::any('ajax/manage-type', [AjaxController::class, 'manageType'])->name('ajax.manage.type');
    Route::any('ajax/manage-document', [AjaxController::class, 'manageDocument'])->name('ajax.manage.document');
    Route::any('ajax/manage-commission', [AjaxController::class, 'manageCommission'])->name('ajax.manage.commission');
    Route::any('ajax/monthly-commission', [AjaxController::class, 'monthlyCommission'])->name('ajax.monthly.commission');
    Route::any('ajax/monthly-sales', [AjaxController::class, 'monthlySales'])->name('ajax.monthly.sales');
    Route::any('ajax/manage-message', [AjaxController::class, 'manageMessage'])->name('ajax.manage.message');
    Route::any('ajax/manage-lead', [AjaxController::class, 'manageLead'])->name('ajax.manage.lead');
    Route::any('ajax/lead-mapping', [AjaxController::class, 'leadMapping'])->name('ajax.lead.mapping');
    Route::any('ajax/lead-chart', [AjaxController::class, 'leadChart'])->name('ajax.lead.chart');
    Route::any('ajax/lead-call', [AjaxController::class, 'leadCall'])->name('ajax.lead.call');
    Route::any('ajax/manage-module', [AjaxController::class, 'manageModule'])->name('ajax.manage.module');
    Route::any('ajax/manage-customer', [AjaxController::class, 'manageCustomer'])->name('ajax.manage.customer');
    Route::any('ajax/manage-member', [AjaxController::class, 'manageMember'])->name('ajax.manage.member');
    Route::any('ajax/manage-policy', [AjaxController::class, 'managePolicy'])->name('ajax.manage.policy');
    Route::any('ajax/customer-policies', [AjaxController::class, 'customerPolicies'])->name('ajax.customer.policies');
    Route::any('ajax/policy-documents', [AjaxController::class, 'policyDocuments'])->name('ajax.policy.documents');
    Route::any('ajax/provider-types', [AjaxController::class, 'providerTypes'])->name('ajax.provider.types');
    Route::any('ajax/type-details', [AjaxController::class, 'typeDetails'])->name('ajax.type.details');
    Route::any('ajax/manage-payslip', [AjaxController::class, 'managePayslip'])->name('ajax.manage.payslip');
    Route::any('ajax/manage-meeting', [AjaxController::class, 'manageMeeting'])->name('ajax.manage.meeting');
    Route::any('ajax/manage-communication', [AjaxController::class, 'manageCommunication'])->name('ajax.manage.communication');
    Route::any('ajax/manage-conversation', [AjaxController::class, 'manageConversation'])->name('ajax.manage.conversation');
    Route::any('ajax/manage-dashboardchatconversation', [AjaxController::class, 'manageDashboardChatConversation'])->name('ajax.manage.dashboardchatconversation');
    Route::any('ajax/manage-chat-conversation', [AjaxController::class, 'manageChatConversation'])->name('ajax.managechat.conversation');
    Route::any('ajax/loadContacts', [AjaxController::class, 'loadContacts'])->name('ajax.loadContacts');
    Route::any('ajax/loadDashboardContacts', [AjaxController::class, 'loadDashboardContacts'])->name('ajax.loadDashboardContacts');
    Route::any('ajax/chatCount', [AjaxController::class, 'chatCount'])->name('ajax.chatCount');
    
    Route::any('ajax/chat-reply', [AjaxController::class, 'chatReply'])->name('ajax.chat.reply');
    Route::any('ajax/manage-news', [AjaxController::class, 'manageNews'])->name('ajax.manage.news');
    Route::any('ajax/manage-youtubes', [AjaxController::class, 'manageYoutubes'])->name('ajax.manage.youtubes');
    Route::any('ajax/manage-companys', [AjaxController::class, 'manageCompanys'])->name('ajax.manage.companys');

    Route::any('ajax/post-policy-document', [AjaxController::class, 'policyDocument'])->name('ajax.postpolicy.documents');
    Route::any('ajax/post-policy-document-comment', [AjaxController::class, 'policyDocumentAddComment'])->name('ajax.postpolicy.documentcomment');
    Route::any('ajax/show-policy-document-comment', [AjaxController::class, 'policyDocumentShowComment'])->name('ajax.postpolicy.showdocumentcomment');
    // Auth Routes
    Route::get('/user', [UserController::class, 'index'])->name('user-dashboard');
    Route::any('/user/create', [UserController::class, 'createUser'])->name('user.create');
    Route::get('/user/profile', [UserController::class, 'profile'])->name('user.profile');
    Route::any('/user/change-password', [UserController::class, 'changePassword'])->name('change-password');
    Route::any('/user/update-profile', [UserController::class, 'updateProfile'])->name('update-profile');

    // Customer Routes
    Route::any('/customers', [CustomersController::class, 'index'])->name('customers');
    Route::any('/create-customer/{customerId?}', [CustomersController::class, 'createCustomer'])->name('create.customer');
    Route::any('/customer/members/{customerId?}', [CustomersController::class, 'customerMembers'])->name('customer.members');
    Route::any('/create-member/{memberId?}', [CustomersController::class, 'createMember'])->name('create.member');
    Route::any('/create-policy/{policyId?}', [CustomersController::class, 'createPolicy'])->name('create.policy');
    Route::any('/policy-status', [CustomersController::class, 'policyStatus'])->name('policy.status');
    Route::any('/policy-rework', [CustomersController::class, 'policyRework'])->name('policy.rework');
    Route::any('/policy-document', [CustomersController::class, 'policyDocument'])->name('policy.documents');
    Route::any('/pending-document', [CustomersController::class, 'pendingDocument'])->name('pending.documents');

    Route::any('/customers-search', [CustomersController::class, 'searchCustomer'])->name('customers.search');

    Route::get('/admin', [AdminController::class, 'index'])->name('admin.dashboard');
    Route::get('/admin/profile', [AdminController::class, 'profile'])->name('admin.profile');
    // Admin Routes
    Route::group(['middleware' => ['admin']], function () {
        // User Routes
        Route::get('/admin/users', [UserController::class, 'users'])->name('users');
        Route::any('/admin/create-user/{userId?}', [UserController::class, 'createUser'])->name('create.user');
        Route::any('/admin/change-password/{userId?}', [UserController::class, 'userPassword'])->name('user.password');

        // Insurance Provider Routes
        Route::any('/admin/providers', [InsuranceProvidersController::class, 'index'])->name('providers');
        Route::any('/admin/create-provider/{providerId?}', [InsuranceProvidersController::class, 'createProvider'])->name('create.provider');
        Route::any('/admin/mapping-provider/{providerId?}', [InsuranceProvidersController::class, 'mappingProvider'])->name('mapping.provider');

        // Insurance Type Routes
        Route::any('/admin/types', [InsuranceTypesController::class, 'index'])->name('types');
        Route::any('/admin/create-type/{typeId?}', [InsuranceTypesController::class, 'createType'])->name('create.type');

        // Document Routes
        Route::any('/admin/documents', [DocumentsController::class, 'index'])->name('documents');
        Route::any('/admin/create-document/{documentId?}', [DocumentsController::class, 'createDocument'])->name('create.document');

        // Commission Routes
        Route::any('/admin/commissions', [CommissionController::class, 'index'])->name('commissions');
        Route::any('/admin/create-commission/{commissionId?}', [CommissionController::class, 'createCommission'])->name('create.commission');
        Route::any('/employee-commission', [CommissionController::class, 'employeeCommission'])->name('employee.commission');
        Route::any('/manage-payslip/{payslipId?}', [UserController::class, 'managePaySlip'])->name('manage.payslip');
        Route::any('/employee-payslip', [CommissionController::class, 'employeePaySlip'])->name('employee.payslip');

        // Message Routes
        Route::any('/admin/messages', [MessageController::class, 'index'])->name('messages');
        Route::any('/admin/create-message/{messageId?}', [MessageController::class, 'createMessage'])->name('create.message');

        // Communication Routes
        Route::any('/admin/communication', [AdminController::class, 'communication'])->name('communication');
        Route::any('/admin/create-communication/{comId?}', [AdminController::class, 'createCommunication'])->name('manage.communication');

        // Chat Routes
        Route::any('/admin/chat', [AdminController::class, 'chat'])->name('chat');

        // Meeting Routes
        Route::any('/admin/meeting', [AdminController::class, 'meetings'])->name('meetings');
        Route::any('/admin/manage-meeting/{meetingId?}', [AdminController::class, 'manageMeeting'])->name('manage.meeting');

        // News Routes
        Route::any('/admin/news', [AdminController::class, 'news'])->name('news');
        Route::any('/admin/create-news/{newsId?}', [AdminController::class, 'createNews'])->name('create.news');
        
        // Youtubes Routes
        Route::any('/admin/youtubes', [AdminController::class, 'youtubes'])->name('youtubes');
        Route::any('/employee-youtubes', [EmployeeController::class, 'youtubes'])->name('employee.youtubes');
        Route::any('/admin/create-youtubes/{youtubesId?}', [AdminController::class, 'createYoutubes'])->name('create.youtubes');
        
        // Companys Routes
        Route::any('/admin/companys', [AdminController::class, 'companys'])->name('companys');
        Route::any('/employee-companys', [EmployeeController::class, 'companys'])->name('employee.companys');
        Route::any('/admin/create-companys/{companysId?}', [AdminController::class, 'createCompanys'])->name('create.companys');

        // Lead Routes
        Route::any('/admin/leads', [LeadController::class, 'index'])->name('leads');
        Route::any('/admin/leads-view/{leadId?}', [LeadController::class, 'view'])->name('leads.view');
        Route::any('/admin/leads-accept/{leadId?}', [LeadController::class, 'accept'])->name('leads.accept');
        Route::any('/admin/create-lead/{leadId?}', [LeadController::class, 'createLead'])->name('create.lead');
        Route::any('/admin/mapping-lead/{leadId?}', [LeadController::class, 'mappingLead'])->name('mapping.lead');
        Route::any('/admin/update-lead/{leadId?}', [LeadController::class, 'updateLead'])->name('update.lead');

        // Module Routes
        Route::any('/admin/modules', [ModulesController::class, 'index'])->name('modules');
        Route::any('/admin/create-module/{moduleId?}', [ModulesController::class, 'createModule'])->name('create.module');

        // New Client Lead Routes
        Route::any('/admin/clientleads', [ClientLeadController::class, 'index'])->name('clientleads');
        Route::any('/admin/create-clientlead', [ClientLeadController::class, 'showLeadStep'])->name('create.clientleads');
        Route::any('/admin/store-clientlead', [ClientLeadController::class, 'storeLeadStep']);
        Route::get('/employee-ranking', [EmployeeController::class, 'ranking'])->name('employee.ranking');
        Route::get('/admin/generatepdf/{clientid}', [ClientLeadController::class, 'generatePDF']);
    });
});



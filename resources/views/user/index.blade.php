@extends('layouts.admin')
@section('content')
    <section id="content">
        <div class="page page-sidebar-sm-layout">
            <div class="pageheader">
                <h2>Dashboard <span></span></h2>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{url('/user')}}"><i class="fa fa-home"></i> Home</a>
                        </li>
                        <li>
                            <a href="javascript:;">Dashboard</a></li>
                        </li>
                    </ul>
                </div>
            </div>
            <p class="lead">{{__('auth.user_message')}}</p>
        </div>
    </section>

@endsection

@extends('layouts.admin')
@section('content')
    <section id="content">
        <div class="page page-sidebar-sm-layout">
            <div class="pageheader">
                <h2>Profile <span></span></h2>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{url('/user')}}"><i class="fa fa-home"></i> Home</a>
                        </li>
                        <li>
                            <a href="javascript:;">Profile</a></li>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                @if(Session::has('message'))
                    <div class="col-md-12">
                        <div class="alert alert-{{ Session::get('level')}}">
                            <strong>{!! (Session::get('level') == 'success')? 'Success!':'Error!' !!}</strong> {!! Session::get('message')!!}
                        </div>
                    </div>
                @endif
                <div class="col-md-6">
                    <section class="tile tile-simple">
                        <div class="tile-body">
                            <div class="profile-settings">
                                <div class="row">
                                    <div class="form-group col-md-12 legend">
                                        <h4><strong>Personal</strong> Settings</h4>
                                        <p>Your personal account settings</p>
                                    </div>
                                </div>
                                <form method="POST" action="{{ url('user/update-profile') }}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="form-group col-sm-6">
                                            <label for="fullname">Full Name</label>
                                            <input type="text" class="form-control" id="fullname" name="name" value="{{$user['name']}}">
                                            @error('name')
                                            <span class="text-danger pull-left">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label for="avatar">Profile Image <span class="text-thin">PNG, JPEG, JPG. Max 1Mb</span></label>
                                            <input type="file" id="avatar" name="profile_image" class="filestyle" data-buttonText="Find file" data-iconName="fa fa-inbox">
                                            @error('profile_image')
                                            <span class="text-danger pull-left">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-6">
                                            <label for="email">E-mail</label>
                                            <input type="email" class="form-control" id="email" value="{{$user['email']}}" readonly>
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label for="phone">Phone</label>
                                            <input type="text" class="form-control" id="phone" name="contact_number" value="{{$user['contact_number']}}">
                                            @error('contact_number')
                                            <span class="text-danger pull-left">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-sm-6">
                                            <button type="submit" class="btn btn-primary btn-sm">Update Profile</button>
                                            <a href="{{url('/user')}}" class="btn btn-warning btn-sm">Cancel</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-md-6">
                    <section class="tile tile-simple">
                        <div class="tile-body">
                            <div class="profile-settings">
                                <div class="row">
                                    <div class="form-group col-md-12 legend">
                                        <h4><strong>Security</strong> Settings</h4>
                                        <p>Secure your account</p>
                                    </div>
                                </div>
                                <form method="POST" action="{{ url('user/change-password') }}">
                                    @csrf
                                    <div class="row">
                                        <div class="form-group col-sm-6">
                                            <label for="email">Username</label>
                                            <input type="email" name="email" class="form-control" id="email" value="{{$user['email']}}" readonly>
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label for="oldpassword">Current Password</label>
                                            <input type="password" class="form-control" id="oldpassword" name="oldpassword" value="">
                                            @error('oldpassword')
                                            <span class="text-danger pull-left">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-6">
                                            <label for="password">New Password</label>
                                            <input type="password" class="form-control" id="password" name="password" value="">
                                            @error('password')
                                            <span class="text-danger pull-left">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label for="password-confirm">Confirm Password</label>
                                            <input type="password" class="form-control" id="password-confirm" name="password_confirmation" value="">
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-sm-6">
                                            <button type="submit" class="btn btn-primary btn-sm">Change Password</button>
                                            <a href="{{url('/user')}}" class="btn btn-warning btn-sm">Cancel</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>
@endsection

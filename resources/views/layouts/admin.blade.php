<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>{{ config('app.name', 'Customer Resource Management') }}</title>
    <link rel="icon" type="image/ico" href="{{ asset('images/favicon.ico') }}"/>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @include('layouts.partials.header')
    @yield('style')
</head>
<body id="minovate" class="appWrapper sidebar-offcanvas scheme-greensea greensea-scheme-color">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<div id="wrap" class="animsition">
    <section id="header">
        <header class="clearfix">
            <div class="branding">
                <a class="brand" href="{{ url('/admin') }}">
                    <span><strong>CRM</strong>
                         @if(Auth::user()->is_admin)
                            {{__('auth.common.admin')}}
                        @elseif(Auth::user()->is_employee)
                            {{__('auth.common.employee')}}
                        @else
                            {{__('auth.common.customer')}}
                        @endif
                    </span>
                </a>
                <a href="#" class="offcanvas-toggle visible-xs-inline"><i class="fa fa-bars"></i></a>
            </div>
            <ul class="nav-right pull-right list-inline">
                <li class="dropdown nav-profile divided-right">
                    <a href class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{Storage::url(Auth::user()->profile_image)}}" alt="" class="img-circle size-30x30">
                        <span>{{ Auth::user()->name }} <i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul class="dropdown-menu animated littleFadeInRight" role="menu">
                        <li>
                            <a href="{{route('user.profile')}}">
                                <i class="fa fa-user"></i>{{__('auth.common.profile')}}
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i>{{ __('auth.common.logout') }}</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a role="button" tabindex="0" class="dropdown-toggle settings divided-right" data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-language"></i>&nbsp;{{strtoupper(session()->get('locale') ?? 'en')  }}
                    </a>
                    <ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp">
                        <li>
                            <a role="button" href="javascript:ChangeLanguage('en')" tabindex="0" class="tile-refresh">
                                <img src="{{asset('images/usa.png')}}"/> English
                            </a>
                        </li>
                        <li>
                            <a role="button" href="javascript:ChangeLanguage('de')" tabindex="0" class="tile-refresh">
                                <img src="{{asset('images/germany.png')}}"/> German
                            </a>
                        </li>
                        {{--                        <li>--}}
                        {{--                            <a role="button" href="javascript:ChangeLanguage('fr')" tabindex="0" class="tile-refresh">--}}
                        {{--                                <img src="{{asset('images/france.png')}}"/> French--}}
                        {{--                            </a>--}}
                        {{--                        </li>--}}
                        {{--                        <li>--}}
                        {{--                            <a role="button" href="javascript:ChangeLanguage('it')" tabindex="0" class="tile-refresh">--}}
                        {{--                                <img src="{{asset('images/italy.png')}}"/> Italian--}}
                        {{--                            </a>--}}
                        {{--                        </li>--}}
                    </ul>
                </li>
            </ul>
        </header>
    </section>
    <div id="controls">
        @include('layouts.partials.admin-navigation')
    </div>
    @yield('content')
</div>
<div class="modal fade" id="modal_popup" role="dialog" aria-labelledby="modal_popup" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
        </div>
    </div>
</div>
<div id="loading" class="hide">
    <div id="loading-content">
        <img src="{{asset('images/loading.gif')}}" alt="Loading..." height="175" width="175"/>
    </div>
</div>
@include('layouts.partials.footer')
@yield('javascript')
</body>
</html>

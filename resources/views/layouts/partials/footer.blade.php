<!-- vendor javascripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="{{ asset('js/vendor/jquery/jquery-1.11.2.min.js') }}"><\/script>')</script>
<script src="{{ asset('js/vendor/bootstrap/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/vendor/jRespond/jRespond.min.js') }}"></script>
<script src="{{ asset('js/vendor/sparkline/jquery.sparkline.min.js') }}"></script>
<script src="{{ asset('js/vendor/slimscroll/jquery.slimscroll.min.') }}js"></script>
<script src="{{ asset('js/vendor/animsition/js/jquery.animsition.min.js') }}"></script>
<script src="{{ asset('js/vendor/screenfull/screenfull.min.js') }}"></script>
<!-- Datatable javascripts -->
<script src="{{ asset('js/vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/vendor/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js') }}"></script>
<script src="{{ asset('js/vendor/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('js/vendor/datatables/extensions/ColVis/js/dataTables.colVis.min.js') }}"></script>
<script src="{{ asset('js/vendor/datatables/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
<script src="{{ asset('js/vendor/datatables/extensions/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('js/vendor/daterangepicker/moment.min.js') }}"></script>
<script src="{{ asset('js/vendor/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('js/vendor/filestyle/bootstrap-filestyle.min.js') }}"></script>
<script src="{{ asset('js/vendor/slider/bootstrap-slider.min.js') }}"></script>
<!-- ============================================
============== Custom JavaScripts ===============
============================================= -->
<script src="{{ asset('js/main.js') }}"></script>
<!--/ custom javascripts -->
<!-- ===============================================
============== Page Specific Scripts ===============
================================================ -->
<script>
    $(window).load(function () {
        $("#loading").addClass('hide');
    });

    function allUniversalEnum(actionType, relationId) {
        $.ajax({
            url: "{{route('change.universal.enum')}}",
            method: "get",
            data: {actionType: actionType, relationId: relationId},
            dataType: "json",
            success: function (response) {
                location.reload();
            }
        });
    }
</script>
<!--/ Page Specific Scripts -->
<script type="text/javascript">
    var url = "{{ route('LangChange') }}";

    function ChangeLanguage(lang) {
        window.location.href = url + "?lang=" + lang;
    }
</script>
<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
    (function (b, o, i, l, e, r) {
        b.GoogleAnalyticsObject = l;
        b[l] || (b[l] =
            function () {
                (b[l].q = b[l].q || []).push(arguments)
            });
        b[l].l = +new Date;
        e = o.createElement(i);
        r = o.getElementsByTagName(i)[0];
        e.src = 'https://www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e, r)
    }(window, document, 'script', 'ga'));
    ga('create', 'UA-XXXXX-X', 'auto');
    ga('send', 'pageview');
</script>


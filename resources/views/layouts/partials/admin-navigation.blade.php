<aside id="sidebar">
    <div id="sidebar-wrap">
        <div class="panel-group slim-scroll" role="tablist">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" href="#sidebarNav">
                            Navigation <i class="fa fa-angle-up"></i>
                        </a>
                    </h4>
                </div>
                <div id="sidebarNav" class="panel-collapse collapse in" role="tabpanel">
                    <div class="panel-body">
                        <ul id="navigation">
                            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> <span>{{__('auth.common.dashboard')}}</span></a></li>
                            @if(Auth::user()->is_admin)
                                <li><a href="{{route('users')}}"><i class="fa fa-users"></i> <span>{{__('auth.common.users')}}</span></a></li>
                                <li><a href="{{route('providers')}}"><i class="fa fa-institution"></i> <span>{{__('auth.common.insuranceProviders')}}</span></a></li>
                                <li><a href="{{route('types')}}"><i class="fa fa-fax"></i> <span>{{__('auth.common.insuranceTypes')}}</span></a></li>
                                <li><a href="{{route('documents')}}"><i class="fa fa-briefcase"></i> <span>{{__('auth.common.documents')}}</span></a></li>
                                <li><a href="{{route('commissions')}}"><i class="fa fa-money"></i> <span>{{__('auth.common.commission')}}</span></a></li>
                                <li><a href="{{route('chat')}}"><i class="fa fa-envelope-o"></i> <span>{{__('auth.common.chat')}}</span><span class="badge bg-lightred">{{$chatCount}}</span></a></li>
                                <li><a href="{{route('communication')}}"><i class="fa fa-envelope-o"></i> <span>{{__('auth.common.communication')}}</span></a></li>
                                <li><a href="{{route('leads')}}"><i class="fa fa-sitemap"></i> <span>Leads</span></a></li>
                                <li><a href="{{route('clientleads')}}"><i class="fa fa-sitemap"></i> <span>Client Leads</span></a></li>
                                <li><a href="{{route('customers')}}"><i class="fa fa-user"></i> <span>{{__('auth.common.myCustomers')}}</span></a></li>
                                <li><a href="{{route('policy.status')}}"><i class="fa fa-ticket"></i> <span>{{__('auth.customer.policy')}}</span></a></li>
                                <li><a href="{{route('employee.commission')}}"><i class="fa fa-money"></i> <span>{{__('auth.common.employeeCommission')}}</span></a></li>
                                <li><a href="{{route('employee.payslip')}}"><i class="fa fa-money"></i> <span>{{__('auth.common.employeePayslip')}}</span></a></li>
                                <li><a href="{{route('meetings')}}"><i class="fa fa-calendar"></i> <span>{{__('auth.common.meetings')}}</span></a></li>
                                <li><a href="{{route('news')}}"><i class="fa fa-bullhorn"></i> <span>{{__('auth.common.news')}}</span></a></li>
                                {{--<li><a href="{{route('modules')}}"><i class="fa fa-cubes"></i> <span>{{__('auth.common.modules')}}</span></a></li>--}}
                                {{--<li><a href="javascript:;"><i class="fa fa-sliders"></i> <span>{{__('auth.common.userPermissions')}}</span></a></li>--}}
                            @elseif(Auth::user()->is_employee)
                                <li><a href="{{route('leads')}}"><i class="fa fa-sitemap"></i> <span>Leads</span></a></li>
                                <li><a href="{{route('clientleads')}}"><i class="fa fa-sitemap"></i> <span>Client Leads</span></a></li>
                                <li><a href="{{route('customers')}}"><i class="fa fa-user"></i> <span>{{__('auth.common.myCustomers')}}</span></a></li>
                                <li><a href="{{route('policy.status')}}"><i class="fa fa-ticket"></i> <span>{{__('auth.common.statusOfRequests')}}</span></a></li>
                                <li><a href="{{route('meetings')}}"><i class="fa fa-calendar"></i> <span>{{__('auth.common.meetings')}}</span></a></li>
                                <li><a href="{{route('chat')}}"><i class="fa fa-envelope-o"></i> <span>{{__('auth.common.chat')}}</span><span class="badge bg-lightred">{{$chatCount}}</span></a></li>
                                <li><a href="{{route('communication')}}"><i class="fa fa-envelope-o"></i> <span>{{__('auth.common.communication')}}</span><span class="badge bg-lightred">{{$communicationCount}}</span></a></li>
                                <li><a href="{{route('employee.commission')}}"><i class="fa fa-money"></i> <span>{{__('auth.common.commissions')}}</span></a></li>
                                <li><a href="{{route('employee.payslip')}}"><i class="fa fa-money"></i> <span>{{__('auth.common.payslip')}}</span></a></li>
                            @else
                                {{--<li><a href="javascript:;"><i class="fa fa-user"></i> <span>{{__('auth.common.customers')}}</span></a></li>--}}
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</aside>

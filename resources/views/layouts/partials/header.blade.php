<!-- Vendor css files -->
<link rel="stylesheet" href="{{ asset('css/vendor/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/vendor/animate.css') }}">
<link rel="stylesheet" href="{{ asset('css/vendor/font-awesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('js/vendor/animsition/css/animsition.min.css') }}">
<!-- Datatable css files -->
<link rel="stylesheet" href="{{ asset('js/vendor/datatables/css/jquery.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('js/vendor/datatables/datatables.bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('js/vendor/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css') }}">
<link rel="stylesheet" href="{{ asset('js/vendor/datatables/extensions/Responsive/css/dataTables.responsive.css') }}">
<link rel="stylesheet" href="{{ asset('js/vendor/datatables/extensions/ColVis/css/dataTables.colVis.min.css') }}">
<link rel="stylesheet" href="{{ asset('js/vendor/datatables/extensions/TableTools/css/dataTables.tableTools.min.css') }}">
<link rel="stylesheet" href="{{ asset('js/vendor/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
<!-- project main css files -->
<link rel="stylesheet" href="{{ asset('css/main.css') }}">
<link rel="stylesheet" href="{{ asset('css/custom.css') }}">
<!--/ stylesheets -->
<!-- ==========================================
================= Modernizr ===================
=========================================== -->
<script src="{{ asset('js/vendor/modernizr/modernizr-2.8.3-respond-1.4.2.min.js') }}"></script>
<!--/ modernizr -->

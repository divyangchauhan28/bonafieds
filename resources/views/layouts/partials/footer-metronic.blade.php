<!--end::Main-->
<script>var hostUrl = "assets/";</script>
<!--begin::Javascript-->
<!--begin::Global Javascript Bundle(used by all pages)-->
<script src="{{ asset('metronic/plugins/global/plugins.bundle.js') }}"></script>
<script src="{{ asset('metronic/js/scripts.bundle.js') }}"></script>
<!--end::Global Javascript Bundle-->
<!--begin::Page Vendors Javascript(used by this page)-->
<script src="{{ asset('metronic/plugins/custom/fullcalendar/fullcalendar.bundle.js') }}"></script>
<!--end::Page Vendors Javascript-->
<!--begin::Page Custom Javascript(used by this page)-->
<script src="{{ asset('metronic/js/custom/widgets.js') }}"></script>
<script src="{{ asset('metronic/js/custom/apps/chat/chat.js') }}"></script>
<script src="{{ asset('metronic/js/custom/modals/create-app.js') }}"></script>
<script src="{{ asset('metronic/js/custom/modals/upgrade-plan.js') }}"></script>
<script src="{{ asset('metronic/plugins/custom/datatables/datatables.bundle.js') }}"></script>
<!--end::Page Custom Javascript-->
<!--end::Javascript-->
<!--/ custom javascripts -->
<!-- ===============================================
============== Page Specific Scripts ===============
================================================ -->
<script>
    $(document).ajaxStart(function () {
        $("#loading").removeClass('hide');
    }).ajaxStop(function () {
        $("#loading").addClass('hide');
    });

    $(window).load(function () {
        $("#loading").addClass('hide');
    });

    function allUniversalEnum(actionType, relationId) {
        $.ajax({
            url: "{{route('change.universal.enum')}}",
            method: "get",
            data: {actionType: actionType, relationId: relationId},
            dataType: "json",
            success: function (response) {
                location.reload();
            }
        });
    }
</script>
<!--/ Page Specific Scripts -->
<script type="text/javascript">
    var url = "{{ route('LangChange') }}";

    function ChangeLanguage(lang) {
        window.location.href = url + "?lang=" + lang;
    }
</script>

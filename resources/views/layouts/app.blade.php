<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<!--<![endif]-->
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <title>{{ config('app.name', 'Customer Resource Management') }}</title>
    <link rel="icon" type="image/ico" href="{{ asset('images/favicon.ico') }}"/>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- ============================================
          ================= Stylesheets ===================
          ============================================= -->
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset('css/vendor/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/vendor/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('css/vendor/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/vendor/animsition/css/animsition.min.css') }}">
    <!-- project main css files -->
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <!--/ stylesheets -->
    <!-- ==========================================
    ================= Modernizr ===================
    =========================================== -->
    <script src="{{ asset('js/vendor/modernizr/modernizr-2.8.3-respond-1.4.2.min.js') }}"></script>
    <!--/ modernizr -->
</head>

<body id="minovate" class="appWrapper scheme-greensea greensea-scheme-color">


<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->


<!-- ====================================================
================= Application Content ===================
===================================================== -->
<div id="wrap" class="animsition">
    <div class="page page-core page-login">
        <div class="text-center"><h3 class="text-light text-white"><span class="text-lightred"><img src="{{ asset('images/crm-logo.png') }}" alt="" class="img-circle size-30x30"></span> Customer Resource Management</h3></div>
        @yield('content')
    </div>
</div>
<!--/ Application Content -->
<!-- ============================================
        ============== Vendor JavaScripts ===============
        ============================================= -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="{{ asset('js/vendor/jquery/jquery-1.11.2.min.js') }}"><\/script>')</script>
<script src="{{ asset('js/vendor/bootstrap/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/vendor/jRespond/jRespond.min.js') }}"></script>
<script src="{{ asset('js/vendor/sparkline/jquery.sparkline.min.js') }}"></script>
<script src="{{ asset('js/vendor/slimscroll/jquery.slimscroll.min.') }}js"></script>
<script src="{{ asset('js/vendor/animsition/js/jquery.animsition.min.js') }}"></script>
<script src="{{ asset('js/vendor/screenfull/screenfull.min.js') }}"></script>
<!--/ vendor javascripts -->
<!-- ============================================
============== Custom JavaScripts ===============
============================================= -->
<script src="{{ asset('js/main.js') }}"></script>
<!--/ custom javascripts -->
<!-- ===============================================
============== Page Specific Scripts ===============
================================================ -->
<script>
    $(window).load(function () {
    });
</script>
<!--/ Page Specific Scripts -->
<script type="text/javascript">
    var url = "{{ route('LangChange') }}";
    $(".Langchange").change(function () {
        window.location.href = url + "?lang=" + $(this).val();
    });
</script>
<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
    (function (b, o, i, l, e, r) {
        b.GoogleAnalyticsObject = l;
        b[l] || (b[l] =
            function () {
                (b[l].q = b[l].q || []).push(arguments)
            });
        b[l].l = +new Date;
        e = o.createElement(i);
        r = o.getElementsByTagName(i)[0];
        e.src = 'https://www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e, r)
    }(window, document, 'script', 'ga'));
    ga('create', 'UA-XXXXX-X', 'auto');
    ga('send', 'pageview');
</script>
</body>
</html>

<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<!--<![endif]-->
<head>
    <head>
        <base href="">
        <title>{{ config('app.name', 'Property Customer Resource Management') }}</title>
        <meta charset="utf-8"/>
        <meta name="description" content="{{ config('app.name', 'Property Customer Resource Management') }}"/>
        <meta name="keywords" content="{{ config('app.name', 'Property Customer Resource Management') }}"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta property="og:locale" content="en_US"/>
        <meta property="og:type" content="article"/>
        <meta property="og:title" content="{{ config('app.name', 'Property Customer Resource Management') }}"/>
        <meta property="og:url" content="https://keenthemes.com/metronic"/>
        <meta property="og:site_name" content="Property Customer Resource Management"/>
        <link rel="canonical" href="https://preview.keenthemes.com/metronic8"/>
        <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}"/>
        @include('layouts.partials.header-metronic')
        @yield('style')
    </head>

<body id="kt_body" class="header-tablet-and-mobile-fixed aside-enabled">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<!--begin::Root-->
<div class="d-flex flex-column flex-root">
    <!--begin::Page-->
    <div class="page d-flex flex-row flex-column-fluid">
        <!--begin::Aside-->
    @include('layouts.partials.admin-navigation-metronic')
    <!--end::Aside-->
        <!--begin::Wrapper-->
        <div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
        @include('layouts.partials.topbar-metronic')
        <!--begin::Content-->
            <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
                @yield('content')
            </div>
            <!--end::Content-->
            <!--begin::Footer-->
            <div class="footer py-4 d-flex flex-lg-column" id="kt_footer">
                <!--begin::Container-->
                <div class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-between">
                    <!--begin::Copyright-->
                    <div class="text-dark order-2 order-md-1">
                        <span class="text-muted fw-bold me-1">2021&copy; Bonafied - CRM</span>
                        <a href="https://keenthemes.com" target="_blank" class="text-gray-800 text-hover-primary"></a>
                    </div>
                    <!--end::Copyright-->
                    <!--begin::Credits-->
                    <div class="text-dark order-2 order-1">
                        <span class="text-muted fw-bold me-1">Design and Developed By :</span>
                        <a href="http://poojacreators.com" target="_blank" class="text-gray-800 text-hover-primary">Pooja Creators</a>
                    </div>
                    <!--end::Credits-->
                </div>
                <!--end::Container-->
            </div>
            <!--end::Footer-->
        </div>
        <!--end::Wrapper-->
    </div>
    <!--end::Page-->
</div>
<!--end::Root-->
<div class="modal fade" id="modal_popup" role="dialog" aria-labelledby="modal_popup" aria-hidden="true" data-bs-focus="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
        </div>
    </div>
</div>
<div id="loading" class="hide">
    <div id="loading-content">
        <img src="{{asset('images/loading.gif')}}" alt="Loading..." height="175" width="175"/>
    </div>
</div>
@include('layouts.partials.footer-metronic')
@yield('javascript')

<script>
    $(document).ready(function(){
        $('#loading').hide();
        setInterval(function(){
            chatCount();
        },5000);
    });

    function chatCount() {
        $.ajax({
            url: "{{ route('ajax.chatCount') }}",
            method: "get",
            dataType: "json",
            success: function (response) {
                $('.chatCount').html(response);
            }
        });
    }
</script>
</body>
</html>

<!DOCTYPE html>
<html>
<head>
    <title>Meeting Invitation</title>
</head>
<body>
<h2>Welcome,</h2>
<br/>
Meeting Detail : {{$meeting_detail}}
<br/>
Meeting on : {{$meeting_on}} @ {{$start_time}} to {{$end_time}}
<br/>
<br/>
</body>
</html>

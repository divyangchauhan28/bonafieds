@extends('layouts.admin-metronic')
@section('breadcrums')
    <div class="page-title d-flex flex-column me-5">
        <!--begin::Title-->
        <h1 class="d-flex flex-column text-dark fw-bolder fs-3 mb-0">{{__('auth.customer.customer')}} {{__('auth.common.list')}}</h1>
        <!--end::Title-->
        <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 pt-1">
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('admin.dashboard')}}" class="text-muted text-hover-primary">{{__('auth.common.home')}}</a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-dark">{{__('auth.customer.customer')}} {{__('auth.common.list')}}</li>
            <!--end::Item-->
        </ul>
    </div>
@endsection
@section('content')
    @if(Session::has('message'))
        <div class="col-md-12">
            <div class="alert alert-{{ Session::get('level')}}">
                <strong>{!! (Session::get('level') == 'success')? 'Success!':'Error!' !!}</strong> {!! Session::get('message')!!}
            </div>
        </div>
    @endif
    <div id="kt_content_container" class="container-xxl">
        <div class="row">
            <div class="col-md-12">
                @if(Auth::user()->is_admin)
                    <!-- <div id="commission-chart" style="height:400px;" class="mb-10"></div> -->
                    <div class="card card-bordered">
                        <div class="card-body">
                            <div id="kt_apexcharts_1" style="height: 350px;"></div>
                        </div>
                    </div>
                @endif
                <div class="clearfix"></div>
                <form class="ajax-form row" id="FilterPolicy" data-reload-form="false" action="{{route('customers.search')}}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-md-3 mb-10">
                            <label for="" class="form-label">From Date</label>
                            <input class="form-control" placeholder="Select from date" id="from_date" name="from_date" value="{{ $start }}" />
                        </div>
                        <div class="col-md-3 mb-10">
                            <label for="" class="form-label">To Date</label>
                            <input class="form-control" placeholder="Select to date" id="to_date" name="to_date" value="{{ $end }}"/>
                        </div>
                        <div class="col-md-3 mb-10">
                            <label class="form-label">Status</label>
                            <select class="form-control" name="is_active">
                                <option value="1" @if(@$status == 1) selected="selected" @endif>Active</option>
                                <option value="0" @if(@$status == 0) selected="selected" @endif>InActive</option>
                            </select>
                        </div>
                        <div class="col-md-1 mb-10">
                            <label for="" class="form-label mt-5"></label>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-filter"></i></button>
                        </div>
                    </div>
                </form>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header border-0 pt-6">
                        <div class="card-title">
                            <h2>{{__('auth.customer.customer')}} {{__('auth.common.list')}}</h2>
                        </div>
                        @if(Auth::user()->is_admin)
                        <a href="javascript:ManageCustomers();" class="btn btn-primary align-self-center">{{__('auth.customer.customer')}}</a>
                        @endif
                    </div>
                    <div class="card-body pt-0">
                        <div class="table-responsive">
                            <table class="table table-row-dashed table-row-gray-300 gy-5 gs-7 rounded" id="all-customers">
                                <thead>
                                <tr>
                                    <th>{{__('auth.common.number')}}</th>
                                    <th>{{__('auth.customer.customerName')}}</th>
                                    <th class="text-center">{{__('auth.customer.mobile')}}</th>
                                    <th class="text-center">{{__('auth.customer.email')}}</th>
                                    <th class="text-center">{{__('category')}}</th>
                                    <th class="text-center">{{__('auth.customer.birthdate')}}</th>
                                    <th class="text-center">{{__('auth.customer.household')}}</th>
                                    <th class="text-center">{{__('auth.customer.gender')}}</th>
                                    <th class="text-center">{{__('auth.customer.maritalStatus')}}</th>
                                    <th class="text-center">{{__('auth.customer.nationality')}}</th>
                                    @if(Auth::user()->is_admin)
                                        <th class="text-center">{{__('auth.common.status')}}
                                        </th>
                                    @endif
                                        <th class="text-center">{{__('auth.common.action')}}
                                        </th>
                                    
                                </tr>
                                </thead>
                                @if(count($customers) > 0)
                                    <tbody>
                                    @foreach($customers as $record)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$record->salutation." ".$record->firstname." ".$record->lastname}}</td>
                                            <td class="text-center">{{$record->mobile}}</td>
                                            <td class="text-center">{{$record->email}}</td>
                                            <td class="text-center">{{$record->category}}</td>
                                            <td class="text-center">{{$record->birth_date}}</td>
                                            <td class="text-center">{{$record->family_member?? '0'  }}</td>
                                            <td class="text-center">{{$record->gender}}</td>
                                            <td class="text-center">{{$record->marital_status}}</td>
                                            <td class="text-center">{{$record->nationality}}</td>
                                            @if(Auth::user()->is_admin)
                                                <td class="text-center">
                                                    @if ($record->is_active)
                                                        <a href="javascript:allUniversalEnum('CustomerInactive', '{{$record->customer_id}}');" class='text-success'>{{__('auth.common.active')}}</a>
                                                    @else
                                                        <a href="javascript:allUniversalEnum('CustomerActive', '{{$record->customer_id}}');" class='text-warning'>{{__('auth.common.inactive')}}</a>
                                                    @endif
                                                </td>
                                            @endif
                                            <td class="text-center">
                                                @if(Auth::user()->is_admin)
                                                    <a href="#" class="btn btn-sm btn-light btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">{{__('auth.common.action')}}</a>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-160px py-4" data-kt-menu="true">
                                                        <div class="menu-item px-3">
                                                            <a href="{{route('customer.members',[$record->customer_id])}}" class="menu-link px-3">Add Member</a>
                                                        </div>
                                                        <div class="menu-item px-3">
                                                            <a href="javascript:ManageCustomers('{{$record->customer_id}}');" class="menu-link px-3">{{__('auth.common.edit')}}</a>
                                                        </div>
                                                        <div class="menu-item px-3">
                                                            <a href="javascript:allUniversalEnum('CustomerDelete', '{{$record->customer_id}}');" class="menu-link px-3">{{__('auth.common.delete')}}</a>
                                                        </div>
                                                    </div>
                                                @else
                                                    <a href="{{route('customer.members',[$record->customer_id])}}" class="btn btn-xs btn-warning btn-rounded">View</a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#all-customers').DataTable();
            $('#from_date').flatpickr();
            $('#to_date').flatpickr();
            
        });

        function ManageCustomers(cId = null) {
            $.ajax({
                url: "{{ route('ajax.manage.customer') }}",
                method: "get",
                data: {cId: cId},
                dataType: "json",
                success: function (response) {
                    $("#modal_popup .modal-dialog").addClass('modal-lg');
                    $("#modal_popup .modal-content").html(response.body);
                    $('#modal_popup').modal('show');
                }
            });
        }
        
    var element = document.getElementById('kt_apexcharts_1');

    var height = parseInt(KTUtil.css(element, 'height'));
    var labelColor = KTUtil.getCssVariableValue('--bs-gray-500');
    var borderColor = KTUtil.getCssVariableValue('--bs-gray-200');
    var baseColor = KTUtil.getCssVariableValue('--bs-primary');
    var secondaryColor = KTUtil.getCssVariableValue('--bs-gray-300');

    var options = {
        series: [{
            name: 'Total',
            data: [{{ $count_data }}]
        }],
        chart: {
            fontFamily: 'inherit',
            type: 'bar',
            height: height,
            toolbar: {
                show: false
            }
        },
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: ['30%'],
                endingShape: 'rounded'
            },
        },
        legend: {
            show: false
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            show: true,
            width: 2,
            colors: ['transparent']
        },
        xaxis: {
            categories: [{!! $date_data !!}],
            axisBorder: {
                show: false,
            },
            axisTicks: {
                show: false
            },
            labels: {
                style: {
                    colors: labelColor,
                    fontSize: '12px'
                }
            }
        },
        yaxis: {
            labels: {
                style: {
                    colors: labelColor,
                    fontSize: '12px'
                }
            }
        },
        fill: {
            opacity: 1
        },
        states: {
            normal: {
                filter: {
                    type: 'none',
                    value: 0
                }
            },
            hover: {
                filter: {
                    type: 'none',
                    value: 0
                }
            },
            active: {
                allowMultipleDataPointsSelection: false,
                filter: {
                    type: 'none',
                    value: 0
                }
            }
        },
        tooltip: {
            style: {
                fontSize: '12px'
            },
            y: {
                formatter: function (val) {
                    return val + ' customers'
                }
            }
        },
        colors: [baseColor, secondaryColor],
        grid: {
            borderColor: borderColor,
            strokeDashArray: 4,
            yaxis: {
                lines: {
                    show: true
                }
            }
        }
    };
    var chart = new ApexCharts(element, options);
    chart.render();
    </script>
@endsection

@extends('layouts.admin-metronic')
@section('breadcrums')
    <div class="page-title d-flex flex-column me-5">
        <!--begin::Title-->
        <h1 class="d-flex flex-column text-dark fw-bolder fs-3 mb-0">{{__('auth.customer.memberList')}}</h1>
        <!--end::Title-->
        <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 pt-1">
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('admin.dashboard')}}" class="text-muted text-hover-primary">{{__('auth.common.home')}}</a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('customers')}}" class="text-muted text-hover-primary">{{__('auth.common.customers')}}</a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-dark">{{__('auth.common.insurance')}} {{__('auth.customer.memberList')}}</li>
            <!--end::Item-->
        </ul>
    </div>
@endsection
@section('content')
    @if(Session::has('message'))
        <div class="col-md-12">
            <div class="alert alert-{{ Session::get('level')}}">
                <strong>{!! (Session::get('level') == 'success')? 'Success!':'Error!' !!}</strong> {!! Session::get('message')!!}
            </div>
        </div>
    @endif
    <div id="kt_content_container" class="container-xxl">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header border-0 pt-6">
                        <div class="card-title">
                            <h2>{{__('auth.customer.memberList')}}</h2>
                        </div>
                        @if(Auth::user()->is_admin)
                            <a href="javascript:ManageMembers('{{$customer->customer_id}}');" class="btn btn-primary align-self-center">{{__('Member')}}</a>
                        @endif
                    </div>
                    <div class="card-body pt-0">
                        <div class="table-responsive">
                            <table class="table table-row-dashed table-row-gray-300 gy-5 gs-7 rounded" id="all-customers">
                                <thead>
                                <tr>
                                    <th>{{__('auth.common.number')}}</th>
                                    <th>{{__('auth.customer.customerName')}}</th>
                                    <th class="text-center">{{__('auth.customer.mobile')}}</th>
                                    <th class="text-center">{{__('auth.customer.email')}}</th>
                                    <th class="text-center">{{__('auth.customer.birthdate')}}</th>
                                    <th class="text-center">{{__('auth.customer.gender')}}</th>
                                    <th class="text-center">{{__('auth.customer.maritalStatus')}}</th>
                                    <th class="text-center">{{__('auth.customer.nationality')}}</th>
                                    @if(Auth::user()->is_admin)
                                        <th class="text-center">{{__('auth.common.status')}}</th>
                                    @endif
                                    <th class="text-center">{{__('auth.common.action')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td>{{$customer->salutation." ".$customer->firstname." ".$customer->lastname}}</td>
                                    <td class="text-center">{{$customer->mobile}}</td>
                                    <td class="text-center">{{$customer->email}}</td>
                                    <td class="text-center">{{$customer->birth_date}}</td>
                                    <td class="text-center">{{$customer->gender}}</td>
                                    <td class="text-center">{{$customer->marital_status}}</td>
                                    <td class="text-center">{{$customer->nationality}}</td>
                                    @if(Auth::user()->is_admin)
                                        <td class="text-center"></td>
                                    @endif
                                    <td class="text-center">
                                        <a href="javascript:CustomerPolicies('{{$customer->customer_id}}');" class="text-warning">View Policy</a>
                                    </td>
                                </tr>
                                @if(count($members) > 0)
                                    @foreach($members as $record)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$record->salutation." ".$record->firstname." ".$record->lastname}}</td>
                                            <td class="text-center">{{$record->mobile}}</td>
                                            <td class="text-center">{{$record->email}}</td>
                                            <td class="text-center">{{$record->birth_date}}</td>
                                            <td class="text-center">{{$record->gender}}</td>
                                            <td class="text-center">{{$record->marital_status}}</td>
                                            <td class="text-center">{{$record->nationality}}</td>
                                            @if(Auth::user()->is_admin)
                                                <td class="text-center">
                                                    @if ($record->is_active)
                                                        <a href="javascript:allUniversalEnum('CustomerInactive', '{{$record->customer_id}}');" class='text-success'>{{__('auth.common.active')}}</a>
                                                    @else
                                                        <a href="javascript:allUniversalEnum('CustomerActive', '{{$record->customer_id}}');" class='text-warning'>{{__('auth.common.inactive')}}</a>
                                                    @endif
                                                </td>
                                            @endif
                                            <td class="text-center">
                                                <a href="#" class="btn btn-sm btn-light btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">{{__('auth.common.action')}}</a>
                                                <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-160px py-4" data-kt-menu="true">
                                                    <div class="menu-item px-3">
                                                        <a href="javascript:CustomerPolicies('{{$record->customer_id}}');" class="menu-link px-3">View Policy</a>
                                                    </div>
                                                    @if(Auth::user()->is_admin)
                                                        <div class="menu-item px-3">
                                                            <a href="javascript:ManageMembers('{{$customer->customer_id}}','{{$record->customer_id}}');" class="menu-link px-3">{{__('auth.common.edit')}}</a>
                                                        </div>
                                                        <div class="menu-item px-3">
                                                            <a href="javascript:allUniversalEnum('CustomerDelete', '{{$record->customer_id}}');" class="menu-link px-3">{{__('auth.common.delete')}}</a>
                                                        </div>
                                                    @endif
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12" id="customerPolicies"></div>
        </div>
    </div>
@endsection
@section('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#all-customers').DataTable();
        });

        function ManageMembers(cId, mId = null) {
            $.ajax({
                url: "{{ route('ajax.manage.member') }}",
                method: "get",
                data: {cId: cId, mId: mId},
                dataType: "json",
                success: function (response) {
                    $("#modal_popup .modal-dialog").addClass('modal-lg');
                    $("#modal_popup .modal-content").html(response.body);
                    $('#modal_popup').modal('show');
                }
            });
        }

        function CustomerPolicies(cId) {
            $.ajax({
                url: "{{ route('ajax.customer.policies') }}",
                method: "get",
                data: {cId: cId},
                dataType: "json",
                success: function (response) {
                    $("#customerPolicies").html(response.body);
                    $('#all-policies').DataTable();
                }
            });
        }

        function CreatePolicy(cId, pId = null) {
            $.ajax({
                url: "{{ route('ajax.manage.policy') }}",
                method: "get",
                data: {cId: cId, pId: pId},
                dataType: "json",
                success: function (response) {
                    $("#modal_popup .modal-dialog").addClass('modal-lg');
                    $("#modal_popup .modal-content").html(response.body);
                    $('#modal_popup').modal('show');
                }
            });
        }

        function policyDocuments(pId) {
            $.ajax({
                url: "{{ route('ajax.policy.documents') }}",
                method: "get",
                data: {pId: pId},
                dataType: "json",
                success: function (response) {
                    $("#modal_popup .modal-dialog").addClass('modal-lg');
                    $("#modal_popup .modal-content").html(response.body);
                    $('#modal_popup').modal('show');
                }
            });
        }


    </script>
@endsection

@extends('layouts.app')
@section('content')
    <div class="container w-420 p-15 bg-white mt-40 text-center">
        <h2 class="text-light text-greensea">{{ __('Register') }}</h2>
        <form name="form" class="form-validation mt-20" method="POST" action="{{ route('register') }}">
            @csrf
            <p class="help-block text-left">{{__('auth.signup')}}</p>
            <div class="form-group @error('name') has-error @enderror">
                <input id="name" type="text" class="form-control underline-input @error('name') parsley-error @enderror" name="name" value="{{ old('name') }}" placeholder="{{ __('auth.name') }}" required autocomplete="name" autofocus>
                @error('name')
                <span class="text-danger pull-left">{{ $message }}</span>
                @enderror
            </div>
            <div class="form-group @error('email') has-error @enderror">
                <input id="email" type="email" class="form-control underline-input @error('email') parsley-error @enderror" name="email" value="{{ old('email') }}" placeholder="{{ __('auth.email') }}" required autocomplete="email">
                @error('email')
                <span class="text-danger pull-left">{{ $message }}</span>
                @enderror
            </div>
            <div class="form-group @error('password') has-error @enderror">
                <input id="password" type="password" placeholder="{{ __('auth.password') }}" class="form-control underline-input @error('password') parsley-error @enderror" name="password" required autocomplete="new-password">
                @error('password')
                <span class="text-danger pull-left">{{ $message }}</span>
                @enderror
            </div>
            <div class="form-group">
                <input id="password-confirm" type="password" placeholder="{{ __('auth.confirm') }}" class="form-control underline-input @error('password') parsley-error @enderror" name="password_confirmation" required autocomplete="new-password">
            </div>
            <div class="bg-slategray lt wrap-reset mt-20 text-left">
                <p class="m-0">
                    <button type="submit" class="btn btn-greensea b-0 text-uppercase pull-right">{{ __('auth.register') }}</button>
                    <a href="{{ route('login') }}" class="btn btn-lightred b-0 text-uppercase">{{__('auth.back')}}</a>
                </p>
            </div>
        </form>
    </div>
@endsection

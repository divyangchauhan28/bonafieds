@extends('layouts.app')
@section('content')
    <div class="container w-420 p-15 bg-white mt-40 text-center">
        <h2 class="text-light text-greensea">{{ __('auth.reset') }}</h2>
        <form name="form" class="form-validation mt-20" method="POST" action="{{ route('password.email') }}">
            @csrf
            <p class="help-block text-left">{{__('auth.reset_message')}}</p>
            <div class="form-group @error('email') has-error @enderror">
                <input id="email" type="email" class="form-control underline-input @error('email') parsley-error @enderror" placeholder="{{ __('auth.email') }}" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                @error('email')
                <span class="text-danger pull-left">{{ $message }}</span>
                @enderror
            </div>
            <div class="bg-slategray lt wrap-reset mt-40 text-left">
                <p class="m-0">
                    <button type="submit" class="btn btn-greensea b-0 text-uppercase pull-right">{{ __('auth.sent_link') }}</button>
                    <a href="{{ route('login') }}" class="btn btn-lightred b-0 text-uppercase">{{__('auth.back')}}</a>
                </p>
            </div>
        </form>
    </div>
@endsection

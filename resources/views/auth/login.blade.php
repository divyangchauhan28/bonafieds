@extends('layouts.app')
@section('content')
    <div class="container w-420 p-15 bg-white mt-40 text-center">
        <h2 class="text-light text-greensea">{{ __('auth.login') }}</h2>
        @if (session('status'))
            <div class="alert alert-success">{{ session('status') }}</div>
        @endif
        @if (session('warning'))
            <div class="alert alert-warning">{{ session('warning') }}</div>
        @endif
        <form name="form" class="form-validation mt-20" method="POST" action="{{ route('login') }}">
            @csrf
            <div class="form-group @error('email') has-error @enderror">
                <input id="email" type="email" placeholder="{{ __('auth.email') }}" class="form-control underline-input @error('email') parsley-error @enderror" value="{{ old('email') }}" name="email" required autocomplete="email" autofocus>
                @error('email')
                <span class="text-danger pull-left">{{ $message }}</span>
                @enderror
            </div>
            <div class="form-group @error('password') has-error @enderror">
                <input id="password" type="password" placeholder="{{ __('auth.password') }}" class="form-control underline-input @error('password') parsley-error @enderror" name="password" required autocomplete="current-password">
                @error('password')
                <span class="text-danger pull-left">{{ $message }}</span>
                @enderror
            </div>
            <div class="form-group text-left mt-20">
                <button type="submit" class="btn btn-greensea b-0 br-2 mr-5">{{ __('auth.login') }}</button>
                <label class="checkbox checkbox-custom-alt checkbox-custom-sm inline-block">
                    <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}><i></i> {{ __('auth.remember') }}
                </label>
                @if (Route::has('password.request'))
                    <a class="pull-right mt-10" href="{{ route('password.request') }}">{{ __('auth.forgot') }}</a>
                @endif
            </div>
        </form>
        <div class="bg-slategray lt wrap-reset mt-40">
            <p class="m-0">
                <a href="{{ route('register') }}" class="text-uppercase">{{ __('auth.register') }}</a>
            </p>
        </div>
    </div>
@endsection

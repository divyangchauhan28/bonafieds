@extends('layouts.app')
@section('content')
    <div class="container w-420 p-15 bg-white mt-40 text-center">
        <h2 class="text-light text-greensea">{{ __('auth.verify_header') }}</h2>
        @if (session('resent'))
            <div class="alert alert-success" role="alert">
                {{ __('auth.verify_title') }}
            </div>
        @endif
        <h4 class="mb-0 mt-40">{{ __('auth.verify_body') }}</h4>
        <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
            @csrf
            <div class="bg-slategray lt wrap-reset mt-20 text-center">
                <p class="m-0">
                    <button type="submit" class="btn btn-greensea b-0 text-uppercase">{{ __('auth.verify_button') }}</button>
                </p>
            </div>
        </form>
    </div>
@endsection


{{--@extends('layouts.app')--}}

{{--@section('content')--}}
{{--<div class="container">--}}
{{--    <div class="row justify-content-center">--}}
{{--        <div class="col-md-8">--}}
{{--            <div class="card">--}}
{{--                <div class="card-header">{{ __('Verify Your Email Address') }}</div>--}}

{{--                <div class="card-body">--}}
{{--                    @if (session('resent'))--}}
{{--                        <div class="alert alert-success" role="alert">--}}
{{--                            {{ __('A fresh verification link has been sent to your email address.') }}--}}
{{--                        </div>--}}
{{--                    @endif--}}

{{--                    {{ __('Before proceeding, please check your email for a verification link.') }}--}}
{{--                    {{ __('If you did not receive the email') }},--}}
{{--                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">--}}
{{--                        @csrf--}}
{{--                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('click here to request another') }}</button>.--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
{{--@endsection--}}

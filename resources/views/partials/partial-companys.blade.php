<div class="modal-header tile-header dvd dvd-btm">
    <h1 class="custom-font m-0"><strong>{{$companys ? __('auth.common.edit') : __('auth.common.add')}}</strong> {{__('auth.common.companys')}}</h1>
</div>
<form class="ajax-form" id="addEditLead" data-reload-form="false" action="{{route('create.companys', $companys->companys_id ?? null)}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="modal-body pb-0">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group mb-5">
                    <label for="title">{{__('auth.common.companys_detail')}}</label>
                    <input type="text" class="form-control" name="companys_detail" id="companys_detail" value="{{$companys->companys_detail ?? ''}}"/>
                </div>
                <div class="form-group mb-5">
                    <label for="avatar">{{__('auth.common.image_doc')}} {{__('auth.leads.file')}} <span class="text-thin">Max 20Mb</span></label>
                    <input type="file" id="image_docs" name="image_docs" class="filestyle" {{ (optional($companys)->companys_id ? null : 'required') }}>
                </div>
                <div class="form-group mb-5">
                    <label for="link">{{__('auth.common.companys_link')}}</label>
                    <input type="url" class="form-control" name="companys_link" id="companys_link" value="{{$companys->companys_link ?? ''}}" required/>
                </div>
                <div class="form-group mb-5">
                    <label for="link">{{__('auth.common.companys_description')}}</label>
                    <textarea class="form-control" name="companys_description" id="companys_description">{{ $companys->companys_description ?? '' }}</textarea>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">{{$companys ? __('auth.common.update') : __('auth.common.submit')}}</button>
        <button type="button" class="btn btn-warning close-modal" data-bs-dismiss="modal">{{__('auth.common.close')}}</button>
    </div>
</form>

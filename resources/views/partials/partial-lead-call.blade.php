<div class="modal-header tile-header dvd dvd-btm">
    <h1 class="custom-font m-0"><strong>Lead</strong> Update</h1>
</div>
<form class="ajax-form" id="mappingLead" data-reload-form="false" action="{{route('update.lead', $lead->id ?? null)}}" method="POST">
    @csrf
    <div class="modal-body pb-0">
        <div class="row">
            <h4 class="col-md-12 custom-font"><strong>Reason : </strong></h4>
            <div class="col-md-12 mb-5">
            <textarea type="checkbox" class="form-control"  
            name="reason"
            value="{{ $lead->reason ?? '' }}">{{ $lead->reason ?? '' }}</textarea>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="row">
            <h4 class="col-md-12 custom-font"><strong>Status : </strong></h4>
            <div class="col-md-12 mb-5">
            <select class="form-select"  name="status">
            <option >Select Status</option>            
            <option value="pending" {{ $lead->status==='pending' ? 'selected' : '' }}>Pending</option>
                        <option value="Accept" {{ $lead->status==='Accept' ? 'selected' : '' }}>Accept</option>
                        <option value="Decline" {{ $lead->status==='Decline' ? 'selected' : '' }}>Decline</option>
                    </select>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-warning close-modal" data-bs-dismiss="modal">Close</button>
    </div>
</form>

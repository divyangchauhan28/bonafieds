<div class="modal-header tile-header dvd dvd-btm">
    <h1 class="custom-font m-0"><strong>Lead</strong> Mapping</h1>
</div>
<form class="ajax-form" id="mappingLead" data-reload-form="false" action="{{route('mapping.lead', $lead->lead_id ?? null)}}" method="POST">
    @csrf
    <div class="modal-body pb-0">
        <div class="row">
            <h4 class="col-md-12 custom-font"><strong>Lead Title : </strong>{{$lead->title}}</h4>
            @if(count($employees) > 0)
                @foreach($employees as $employee)
                    <div class="col-md-3 mb-5">
                        <label for="employee-{{$employee->user_id}}" class="checkbox checkbox-custom-alt">
                            <input type="checkbox" class="lead-class" id="employee-{{$employee->user_id}}" name="leadEmployees[]"
                                   @if (in_array($employee->user_id,$mappedEmployees))
                                   checked="checked"
                                   @endif
                                   value="{{$employee->user_id}}"><i></i>{{$employee->employeeName}}
                        </label>
                    </div>
                @endforeach
            @endif
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-warning close-modal" data-bs-dismiss="modal">Close</button>
    </div>
</form>

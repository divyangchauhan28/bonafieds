<!--begin::Messenger-->
<div class="card w-100 rounded-0 border-0" id="kt_drawer_chat_messenger">
    <!--begin::Card header-->
    <div class="card-header pe-5" id="kt_drawer_chat_messenger_header">
        <!--begin::Title-->
        <div class="card-title">
            <!--begin::User-->
            <div class="d-flex justify-content-center flex-column me-3">
                <a href="#" class="fs-4 fw-bolder text-gray-900 text-hover-primary me-1 mb-2 lh-1">{{$empData->name}}</a>
                <!--begin::Info-->
                <div class="mb-0 lh-1">
                    <span class="badge badge-success badge-circle w-10px h-10px me-1"></span>
                    <span class="fs-7 fw-bold text-muted">Active</span>
                </div>
                <!--end::Info-->
            </div>
            <!--end::User-->
        </div>
        <!--end::Title-->
        <!--begin::Card toolbar-->
        <div class="card-toolbar">
            <!--begin::Menu-->
            <div class="me-2">
                <!-- <button class="btn btn-sm btn-icon btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                    <i class="bi bi-three-dots fs-3"></i>
                </button> -->
                <!--begin::Menu 3-->
                <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-bold w-200px py-3" data-kt-menu="true">
                    <!--begin::Heading-->
                    <div class="menu-item px-3">
                        <div class="menu-content text-muted pb-2 px-3 fs-7 text-uppercase">Contacts</div>
                    </div>
                    <!--end::Heading-->
                    <!--begin::Menu item-->
                    <div class="menu-item px-3">
                        <a href="#" class="menu-link px-3" data-bs-toggle="modal" data-bs-target="#kt_modal_users_search">Add Contact</a>
                    </div>
                    <!--end::Menu item-->
                    <!--begin::Menu item-->
                    <div class="menu-item px-3">
                        <a href="#" class="menu-link flex-stack px-3" data-bs-toggle="modal" data-bs-target="#kt_modal_invite_friends">Invite Contacts
                            <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="" data-bs-original-title="Specify a contact email to send an invitation" aria-label="Specify a contact email to send an invitation"></i></a>
                    </div>
                    <!--end::Menu item-->
                    <!--begin::Menu item-->
                    <div class="menu-item px-3" data-kt-menu-trigger="hover" data-kt-menu-placement="right-start">
                        <a href="#" class="menu-link px-3">
                            <span class="menu-title">Groups</span>
                            <span class="menu-arrow"></span>
                        </a>
                        <!--begin::Menu sub-->
                        <div class="menu-sub menu-sub-dropdown w-175px py-4">
                            <!--begin::Menu item-->
                            <div class="menu-item px-3">
                                <a href="#" class="menu-link px-3" data-bs-toggle="tooltip" title="" data-bs-original-title="Coming soon">Create Group</a>
                            </div>
                            <!--end::Menu item-->
                            <!--begin::Menu item-->
                            <div class="menu-item px-3">
                                <a href="#" class="menu-link px-3" data-bs-toggle="tooltip" title="" data-bs-original-title="Coming soon">Invite Members</a>
                            </div>
                            <!--end::Menu item-->
                            <!--begin::Menu item-->
                            <div class="menu-item px-3">
                                <a href="#" class="menu-link px-3" data-bs-toggle="tooltip" title="" data-bs-original-title="Coming soon">Settings</a>
                            </div>
                            <!--end::Menu item-->
                        </div>
                        <!--end::Menu sub-->
                    </div>
                    <!--end::Menu item-->
                    <!--begin::Menu item-->
                    <div class="menu-item px-3 my-1">
                        <a href="#" class="menu-link px-3" data-bs-toggle="tooltip" title="" data-bs-original-title="Coming soon">Settings</a>
                    </div>
                    <!--end::Menu item-->
                </div>
                <!--end::Menu 3-->
            </div>
            <!--end::Menu-->
            <!--begin::Close-->
            <div class="btn btn-sm btn-icon btn-active-light-primary" id="kt_drawer_chat_close">
                <!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
                <span class="svg-icon svg-icon-2">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                        <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor"></rect>
                        <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="currentColor"></rect>
                    </svg>
                </span>
                <!--end::Svg Icon-->
            </div>
            <!--end::Close-->
        </div>
        <!--end::Card toolbar-->
    </div>
    <!--end::Card header-->
    <!--begin::Card body-->
    <div class="card-body" id="kt_drawer_chat_messenger_body">
        <!--begin::Messages-->
        <div class="scroll-y me-n5 pe-5" data-kt-element="messages" data-kt-scroll="true" data-kt-scroll-activate="true" data-kt-scroll-height="auto" data-kt-scroll-dependencies="#kt_drawer_chat_messenger_header, #kt_drawer_chat_messenger_footer" data-kt-scroll-wrappers="#kt_drawer_chat_messenger_body" data-kt-scroll-offset="0px">
        @if(count($conversation) > 0)
            @foreach($conversation as $record)
            <!--begin::Message(in)-->
            <div class="d-flex {{($record->askedBy == Auth::id()) ? 'justify-content-end' : 'justify-content-start'}} mb-10">
                <!--begin::Wrapper-->
                <div class="d-flex flex-column align-items-start">
                    <!--begin::User-->
                    <div class="d-flex align-items-center mb-2">
                    @if($record->askedBy == Auth::id())
                            <!--begin::Details-->
                                <div class="ms-3">
                                    <span class="text-muted fs-7 mb-1">{{\Carbon\Carbon::parse($record->communicationDate)->diffForHumans()}}</span>
                                    <a href="javascript:;" class="fs-5 fw-bolder text-gray-900 text-hover-primary me-1">You</a>
                                </div>
                                <!--end::Details-->
                                <!--begin::Avatar-->
                                <div class="symbol symbol-35px symbol-circle">
                                    @if(Auth::user()->profile_image)
                                        <img alt="{{ Auth::user()->name}} 1" src="{{Storage::url('app/public/'.Auth::user()->profile_image) }}"/>
                                    @else
                                        <img alt="{{ Auth::user()->name}}" src="{{asset('images/profile-photo.jpg')}}"/>
                                    @endif
                                </div>
                                <!--end::Avatar-->
                        @else
                            <!--begin::Avatar-->
                            <div class="symbol symbol-35px symbol-circle">
                                @if(Auth::user()->profile_image)
                                    <img alt="{{$empData->name }} 2" src="{{Storage::url('app/public/'.$empData->profile_image)}}"/>
                                @else
                                    <img alt="{{ Auth::user()->name}}" src="{{asset('images/profile-photo.jpg')}}"/>
                                @endif
                            </div>
                            <!--end::Avatar-->
                            <!--begin::Details-->
                            <div class="ms-3">
                                <a href="javascript:;" class="fs-5 fw-bolder text-gray-900 text-hover-primary me-1">{{$empData->name }}</a>
                                <span class="text-muted fs-7 mb-1">{{\Carbon\Carbon::parse($record->communicationDate)->diffForHumans()}}</span>
                            </div>
                            <!--end::Details-->
                        @endif
                    </div>
                    <!--end::User-->
                    <!--begin::Text-->
                    <div class="p-5 rounded bg-light-info text-dark fw-bold mw-lg-400px text-start" data-kt-element="message-text">{{ $record->communication}}</div>
                    <!--end::Text-->
                </div>
                <!--end::Wrapper-->
            </div>
            <!--end::Message(in)-->
            <!--end::Message(out)-->
            <!--begin::Message(template for out)-->
            <div class="d-flex justify-content-end mb-10 d-none" data-kt-element="template-out">
                <!--begin::Wrapper-->
                <div class="d-flex flex-column align-items-end">
                    <!--begin::User-->
                    <div class="d-flex align-items-center mb-2">
                        <!--begin::Details-->
                        <div class="me-3">
                            <span class="text-muted fs-7 mb-1">Just now</span>
                            <a href="#" class="fs-5 fw-bolder text-gray-900 text-hover-primary ms-1">You</a>
                        </div>
                        <!--end::Details-->
                        <!--begin::Avatar-->
                        <div class="symbol symbol-35px symbol-circle">
                            <img alt="Pic" src="/metronic8/demo1/assets/media/avatars/300-1.jpg">
                        </div>
                        <!--end::Avatar-->
                    </div>
                    <!--end::User-->
                    <!--begin::Text-->
                    <div class="p-5 rounded bg-light-primary text-dark fw-bold mw-lg-400px text-end" data-kt-element="message-text"></div>
                    <!--end::Text-->
                </div>
                <!--end::Wrapper-->
            </div>
            <!--end::Message(template for out)-->
            <!--begin::Message(template for in)-->
            <div class="d-flex justify-content-start mb-10 d-none" data-kt-element="template-in">
                <!--begin::Wrapper-->
                <div class="d-flex flex-column align-items-start">
                    <!--begin::User-->
                    <div class="d-flex align-items-center mb-2">
                        <!--begin::Avatar-->
                        <div class="symbol symbol-35px symbol-circle">
                            <img alt="Pic" src="/metronic8/demo1/assets/media/avatars/300-25.jpg">
                        </div>
                        <!--end::Avatar-->
                        <!--begin::Details-->
                        <div class="ms-3">
                            <a href="#" class="fs-5 fw-bolder text-gray-900 text-hover-primary me-1">Brian Cox</a>
                            <span class="text-muted fs-7 mb-1">Just now</span>
                        </div>
                        <!--end::Details-->
                    </div>
                    <!--end::User-->
                    <!--begin::Text-->
                    <div class="p-5 rounded bg-light-info text-dark fw-bold mw-lg-400px text-start" data-kt-element="message-text">Right before vacation season we have the next Big Deal for you.</div>
                    <!--end::Text-->
                </div>
                <!--end::Wrapper-->
            </div>
            <!--end::Message(template for in)-->
            @endforeach
        @endif
        </div>
        <!--end::Messages-->
    </div>
    <!--end::Card body-->
    <!--begin::Card footer-->
    <div class="card-footer pt-4" id="kt_drawer_chat_messenger_footer">
        <!--begin::Input-->
        <textarea class="form-control form-control-flush mb-3" rows="1" data-kt-element="input" placeholder="Type a message" id="communication" name="communication"></textarea>
        <!--end::Input-->
        <!--begin:Toolbar-->
        <div class="d-flex flex-stack">
            <!--begin::Actions-->
            <div class="d-flex align-items-center me-2">
                <!-- <button class="btn btn-sm btn-icon btn-active-light-primary me-1" type="button" data-bs-toggle="tooltip" title="" data-bs-original-title="Coming soon">
                    <i class="bi bi-paperclip fs-3"></i>
                </button>
                <button class="btn btn-sm btn-icon btn-active-light-primary me-1" type="button" data-bs-toggle="tooltip" title="" data-bs-original-title="Coming soon">
                    <i class="bi bi-upload fs-3"></i>
                </button> -->
            </div>
            <!--end::Actions-->
            <!--begin::Send-->
            <input type="hidden" name="parentId" id="parentId" value="{{count($conversation) > 0 ? $conversation[0]->parent_id : 0}}"/>
            <input type="hidden" name="userId" id="userId" value="{{$empData->user_id}}"/>
            <a href="javascript:SubmitQueryReply();" class="btn btn-primary" type="button" data-kt-element="send">Send</a>
            <!--end::Send-->
        </div>
        <!--end::Toolbar-->
    </div>
    <!--end::Card footer-->
</div>
<!--end::Messenger-->
<div class="modal-header tile-header dvd dvd-btm">
    <h1 class="custom-font m-0"><strong>{{$message ? __('auth.common.edit') : __('auth.common.add')}}</strong> {{__('auth.common.message')}}</h1>
</div>
<form class="ajax-form" id="addEditMessage" data-reload-form="false" action="{{route('create.message', $message->message_id ?? null)}}" method="POST">
    @csrf
    <div class="modal-body pb-0">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group mb-5">
                    <label for="title">{{__('auth.messages.selectType')}}</label>
                    {!! Form::select('message_type', $types,old('message_type'),['class' => 'form-control chosen-select','id'=>'message_type', 'placeholder'=>'Select Message Type']) !!}
                </div>
                <div class="form-group mb-5">
                    <label for="title">{{__('auth.messages.selectAction')}}</label>
                    {!! Form::select('action_id', $actions,old('action_id'),['class' => 'form-control chosen-select','id'=>'action_id', 'placeholder'=>'Create Action']) !!}
                </div>
                <div class="form-group mb-5">
                    <label for="title" id="messageAction">{{__('auth.messages.messageAction')}}</label>
                    <label for="title" id="messageSubject" style="display: none">{{__('auth.messages.messageSubject')}}</label>
                    <input type="text" class="form-control" name="message_subject" id="message_subject" value="{{$message->message_subject ?? ''}}"/>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">{{$message ? __('auth.common.update') : __('auth.common.submit')}}</button>
        <button type="button" class="btn btn-warning close-modal" data-bs-dismiss="modal">{{__('auth.common.close')}}</button>
    </div>
</form>
<script>
    $(function () {
        $("#action_id").change(function () {
            if ($(this).val() > 0) {
                $('#messageAction').hide();
                $('#messageSubject').show();
            } else {
                $('#messageAction').show();
                $('#messageSubject').hide();
            }
        });
    });
</script>

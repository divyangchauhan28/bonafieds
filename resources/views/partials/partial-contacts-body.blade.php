<!--begin::List-->
<div class="scroll-y me-n5 pe-5 h-200px h-lg-auto" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_header, #kt_toolbar, #kt_footer, #kt_chat_contacts_header" data-kt-scroll-wrappers="#kt_content, #kt_chat_contacts_body" data-kt-scroll-offset="5px">
@if(count($employees) > 0)
    @foreach($employees as $record)
        <!--begin::User-->
            <div class="d-flex flex-stack py-4">
                <!--begin::Details-->
                <div class="d-flex align-items-center">
                    <!--begin::Avatar-->
                    <div class="symbol symbol-45px symbol-circle">
                        <?php 
                            $ext = \File::extension($record->profile_image);
                            $ext_array = array(
                                'jpeg','jpg','png','gif'
                            );
                        ?>
                        @if(in_array($ext,$ext_array))
                            <img alt="{{$record->name}}" src="{{Storage::url('app/public/'.$record->profile_image)}}"/>
                        @else
                            <img alt="{{$record->name}}" src="{{asset('images/profile-photo.jpg')}}"/>
                        @endif
                    </div>
                    <!--end::Avatar-->
                    <!--begin::Details-->
                    <div class="ms-5">
                        <a href="javascript:LoadUserConversation('{{$record->user_id}}');" class="fs-5 fw-bolder text-gray-900 text-hover-primary mb-2">{{$record->name}}</a>
                        <div class="fw-bold text-muted">{{$record->email}}</div>
                    </div>
                    <!--end::Details-->
                </div>
                <!--end::Details-->
                <!--begin::Lat seen-->
                <div class="d-flex flex-column align-items-end ms-2">
                    {{--                                            <span class="text-muted fs-7 mb-1">5 hrs</span>--}}
                    <span class="badge badge-sm badge-circle badge-light-danger">{{$record->unreadCount}}</span>
                </div>
                <!--end::Lat seen-->
            </div>
            <!--end::User-->
            <!--begin::Separator-->
            <div class="separator separator-dashed d-none"></div>
            <!--end::Separator-->
        @endforeach
    @endif

</div>
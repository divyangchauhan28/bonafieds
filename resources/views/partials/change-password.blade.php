<div class="modal-header tile-header dvd dvd-btm">
    <h1 class="custom-font m-0"><strong>{{ __('Change Password')}}</strong></h1>
</div>
<form class="ajax-form" id="addEditLead" data-reload-form="false" action="{{route('user.password', $user->user_id ?? null)}}" method="POST">
    @csrf
    <div class="modal-body pb-0">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group mb-5">
                    <label for="title">{{__('auth.password')}}</label>
                    <input type="password" class="form-control" name="password" id="password" required value=""/>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">{{__('auth.common.update')}}</button>
        <button type="button" class="btn btn-warning close-modal" data-dismiss="modal">{{__('auth.common.close')}}</button>
    </div>
</form>

<div class="modal-header tile-header dvd dvd-btm">
    <h1 class="custom-font m-0"><strong>{{__('auth.common.meetings')}}</strong></h1>
</div>
<form class="ajax-form" id="addEditCommunication" data-reload-form="false" action="{{route('manage.communication', $communication->com_id ?? null)}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="modal-body pb-0">
        <div class="row">
            <div class="col-md-12 mb-5">
                <div class="form-group">
                    <label for="title">{{__('auth.common.communication')}}</label>
                    <textarea class="form-control" name="message" id="message" rows="6" required>{{$communication->message ?? ''}}</textarea>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12 mb-5">
                <div class="form-group">
                    <label for="avatar">{{__('auth.common.attachment')}}<span class="text-thin"> (Max 20Mb)</span></label>
                    <input type="file" id="attachment" name="attachment" class="filestyle">
                </div>
            </div>
            <div class="clearfix"></div>
            <h4 class="col-md-12 custom-font"><strong>Employees</strong></h4>
            @if(count($employees) > 0)
                @foreach($employees as $employee)
                    <div class="col-md-4 mb-5">
                        <label for="employee-{{$employee->employee_id}}" class="checkbox checkbox-custom-alt m-0">
                            <input type="checkbox" class="communication-employees" id="employee-{{$employee->employee_id}}" name="communicationEmployees[]"
                                   @if (in_array($employee->employee_id,$comEmployees))
                                   checked="checked"
                                   @endif
                                   value="{{$employee->employee_id}}"><i></i>{{$employee->employeeName}}
                        </label>
                    </div>
                @endforeach
            @endif
            <div class="clearfix mb-10">&nbsp;</div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">{{$communication ? __('auth.common.update') : __('auth.common.submit')}}</button>
        <button type="button" class="btn btn-warning close-modal" data-bs-dismiss="modal">{{__('auth.common.close')}}</button>
    </div>
</form>
<script>
    $(function () {
        $(":file").filestyle({buttonText: "&nbsp;Select Attachment"});
    });
</script>

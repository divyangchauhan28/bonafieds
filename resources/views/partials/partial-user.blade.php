<div class="modal-header tile-header dvd dvd-btm">
    <h1 class="custom-font m-0"><strong>{{$user ? __('auth.common.edit') : __('auth.common.add')}}</strong> {{__('auth.common.user')}}</h1>
</div>
<form class="ajax-form" id="addEditType" data-reload-form="false" action="{{route('create.user', $user->user_id ?? null)}}" method="POST">
    @csrf
    <div class="modal-body pb-0">
        <div class="row">
            <div class="col-md-12 mb-10">
                <div class="form-group">
                    <label for="title">{{__('auth.fullname')}}<b>*</b></label>
                    <input type="text" class="form-control" name="name" id="name" required="" placeholder="Full Name" value="{{$user->name ?? ''}}">
                </div>
            </div>
            @if ($user)
                <div class="col-md-12 mb-10">
                    <div class="form-group">
                        <label for="title">{{__('auth.emailAddress')}}<b>*</b></label>
                        <input type="text" class="form-control" name="email" id="email" readonly="" placeholder="Email Address" value="{{$user->email ?? ''}}">
                    </div>
                </div>
            @else
                <div class="col-md-12 mb-10">
                    <div class="form-group">
                        <label for="title">{{__('auth.emailAddress')}}<b>*</b></label>
                        <input type="text" class="form-control" name="email" id="email" required="" placeholder="Email Address" value="{{$user->email ?? ''}}">
                    </div>
                </div>
            @endif
            <div class="col-md-12 mb-10">
                <div class="form-group">
                    <label for="title">{{__('auth.contactNumber')}}<b>*</b></label>
                    <input type="text" class="form-control" name="contact_number" id="contact_number" required="" placeholder="Contact Number" value="{{$user->contact_number ?? ''}}">
                </div>
            </div>
            <div class="col-md-6 mb-10">
                <div class="form-group">
                    <label for="title">{{__('auth.role')}}<b>*</b></label>
                    {!! Form::select('role', array('Admin'=>'Admin','Employee'=>'Employee','Customer'=>'Customer'),$user->role ?? '',['class' => 'form-control','id'=>'role']) !!}
                </div>
            </div>
            <div class="col-md-6 mb-10" style="display: none;" id="commissionSlab">
                <div class="form-group">
                    <label for="title">{{__('auth.commission.commissionSlabs')}}<b>*</b></label>
                    {!! Form::select('commission_id', $slabs,$user->commission_id ?? '',['class' => 'form-control','id'=>'commission_id']) !!}
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">{{$user ? __('auth.common.update') : __('auth.common.submit')}}</button>
        <button type="button" class="btn btn-warning close-modal" data-bs-dismiss="modal">{{__('auth.common.close')}}</button>
    </div>
</form>
<script>
    $(function () {
        $(document).ready(function () {
            validateRole();
        })

        function validateRole() {
            if ($("#role").val() == "Employee") {
                $('#commissionSlab').show();
            } else {
                $('#commissionSlab').hide();
            }
        }

        $("#role").change(function () {
            validateRole();
        });

    });
</script>

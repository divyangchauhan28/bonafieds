<div class="modal-header tile-header dvd dvd-btm">
    <h1 class="custom-font m-0"><strong>Provider</strong> Mapping</h1>
</div>
<form class="ajax-form" id="addEditProvider" data-reload-form="false" action="{{route('mapping.provider', $provider->provider_id ?? null)}}" method="POST">
    @csrf
    <div class="modal-body pb-0">
        <div class="row">
            <h4 class="col-md-12 custom-font"><strong>{{$provider->label_en}}</strong></h4>
            @if(count($types) > 0)
                @foreach($types as $type)
                    <div class="col-md-4 mb-5">
                        <label for="type-{{$type->type_id}}" class="checkbox checkbox-custom-alt">
                            <input type="checkbox" class="provider-class" id="type-{{$type->type_id}}" name="providerTypes[]"
                                   @if (in_array($type->type_id,$mappedProviders))
                                   checked="checked"
                                   @endif
                                   value="{{$type->type_id}}"><i></i>{{$type->typeTitle}}
                        </label>
                    </div>
                @endforeach
            @endif
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-warning close-modal" data-bs-dismiss="modal">Close</button>
    </div>
</form>

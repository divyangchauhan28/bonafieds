<div class="modal-header tile-header dvd dvd-btm">
    <h1 class="custom-font m-0"><strong>{{$lead ? __('auth.common.edit') : __('auth.common.add')}}</strong> Lead</h1>
</div>
<form class="ajax-form" id="addEditLead" data-reload-form="false" action="{{route('create.lead', $lead->lead_id ?? null)}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="modal-body pb-0">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group mb-5">
                    <label for="title">{{__('auth.leads.lead')}} {{__('auth.leads.title')}}</label>
                    <input type="text" class="form-control" name="title" id="message_subject" value="{{$lead->title ?? ''}}"/>
                </div>
                <div class="form-group mb-5">
                    <label for="title">{{__('auth.leads.lead')}} {{__('auth.leads.description')}}</label>
                    <input type="text" class="form-control" name="description" id="description" value="{{$lead->description ?? ''}}"/>
                </div>
                <div class="form-group mb-5">
                    <label for="avatar">{{__('auth.leads.lead')}} {{__('auth.leads.file')}} <span class="text-thin">xlsx. Max 10Mb</span></label>
                    <input type="file" id="file_url" name="file_url" class="filestyle">
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">{{$lead ? __('auth.common.update') : __('auth.common.submit')}}</button>
        <button type="button" class="btn btn-warning close-modal" data-bs-dismiss="modal">{{__('auth.common.close')}}</button>
    </div>
</form>
<script>
    $(function () {
        $(":file").filestyle({buttonText: "&nbsp;Select Excel"});
    });
</script>

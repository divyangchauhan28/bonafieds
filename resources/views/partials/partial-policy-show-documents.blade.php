<div class="modal-header tile-header dvd dvd-btm">
    <h1 class="custom-font m-0"><strong>{{ __('auth.common.manage')}}</strong> {{__('auth.common.comment')}}</h1>
</div>
<form class="ajax-form" id="addPolicyDocement" data-reload-form="false" action="{{route('policy.documents')}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="modal-body pb-0">
        <div class="row">
            <div class="clearfix"></div>
            <div class="col-md-12">
                <table class="table table-bordered" id="policy-documents">
                    <thead>
                    <tr>
                        <th>{{__('auth.common.number')}}</th>
                        <th>{{__('auth.common.comment')}}</th>
                        <th>{{__('auth.leads.file')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($policyComments) > 0)
                        @foreach($policyComments as $record)
                            <tr>
                                <td>{{$no++}}</td>
                                <td>{{$record->name}} : {{$record->comment}}</td>
                                <td>{{ date('d/m/Y h:i a',strtotime($record->created_at))}}</td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                <div class="clearfix">&nbsp;</div>
            </div>
        </div>
    </div>
</form>

<div class="modal-header tile-header dvd dvd-btm">
    <h1 class="custom-font m-0"><strong>{{$provider ? __('auth.common.edit') : __('auth.common.add')}}</strong> {{__('auth.providers.provider')}}</h1>
</div>
<form class="ajax-form" id="addEditProvider" data-reload-form="false" action="{{route('create.provider', $provider->provider_id ?? null)}}" method="POST">
    @csrf
    <div class="modal-body pb-0">
        <div class="row">
            <div class="col-md-6 mb-5">
                <div class="form-group">
                    <label for="title">{{__('auth.providers.providerEnglish')}}</label>
                    <input type="text" class="form-control" name="label_en" id="label_en" required value="{{$provider->label_en ?? ''}}"/>
                </div>
            </div>
            <div class="col-md-6 mb-5">
                <div class="form-group">
                    <label for="title">{{__('auth.providers.providerGerman')}}</label>
                    <input type="text" class="form-control" name="label_de" id="label_de" required value="{{$provider->label_de ?? ''}}"/>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">{{$provider ? __('auth.common.update') : __('auth.common.submit')}}</button>
        <button type="button" class="btn btn-warning close-modal" data-bs-dismiss="modal">{{__('auth.common.close')}}</button>
    </div>
</form>

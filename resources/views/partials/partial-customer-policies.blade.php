<div class="card">
    <div class="card-header border-0 pt-6">
        <div class="card-title">
            <h2>{{__('auth.customer.member')}} {{__('auth.customer.policies')}}</h2>
        </div>
        @if(Auth::user()->is_admin)
            <a href="javascript:CreatePolicy('{{$customer->customer_id}}');" class="btn btn-primary align-self-center">{{__('auth.common.insurance')}}</a>
        @endif
    </div>
    <div class="card-body pt-0">
        <dv class="table-responsive">
            <table class="table table-row-dashed table-row-gray-300 gy-5 gs-7 rounded" id="all-policies">
                <thead>
                <tr>
                    <th>{{__('auth.common.number')}}</th>
                    <th>{{__('auth.common.insuranceProviders')}}</th>
                    <th>{{__('auth.common.insuranceTypes')}}</th>
                    <th class="text-center">{{__('auth.types.amount')}}</th>
                    <th class="text-center">{{__('auth.types.duration')}}</th>
                    <th class="text-center">{{__('auth.customer.paymentMode')}}</th>
                    <th class="text-center">{{__('auth.customer.policyStatus')}}</th>
                    <th class="text-center">{{__('auth.common.status')}}</th>
                    <th class="text-center">{{__('auth.common.dead')}}</th>
                    <th class="text-center">{{__('auth.common.delete')}}</th>
                    @if(Auth::user()->is_admin)
                        <th class="text-center">{{__('auth.common.action')}}</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @if(count($policies) > 0)
                    @foreach($policies as $record)
                        <tr>
                            <td>{{$no++}}</td>
                            <td>{{$record->insProvider}}</td>
                            <td>{{$record->insType}}</td>
                            <td class="text-center">{{$record->insurance_amount." CHF"}}</td>
                            <td class="text-center">{{$record->insurance_duration." Years"}}</td>
                            <td class="text-center">{{$record->installment_mode}}</td>
                            <td class="text-center">{{$record->insurance_status}}</td>
                            <td class="text-center">
                                @if ($record->is_active)
                                    <a href="javascript:allUniversalEnum('PolicyInactive', '{{$record->ci_id}}');" class='text-success'>{{__('auth.common.active')}}</a>
                                @else
                                    <a href="javascript:allUniversalEnum('PolicyActive', '{{$record->ci_id}}');" class='text-warning'>{{__('auth.common.inactive')}}</a>
                                @endif
                            </td>
                            <td class="text-center">
                                @if ($record->is_dead)
                                    <a href="javascript:allUniversalEnum('PolicyUndead', '{{$record->ci_id}}');" class='text-danger'>{{__('auth.common.yes')}}</a>
                                @else
                                    <a href="javascript:allUniversalEnum('PolicyDead', '{{$record->ci_id}}');" class='text-warning'>{{__('auth.common.no')}}</a>
                                @endif
                            </td>
                            <td class="text-center">
                                @if ($record->is_deleted)
                                    <a href="javascript:allUniversalEnum('PolicyUndelete', '{{$record->ci_id}}');" class='text-danger'>{{__('auth.common.yes')}}</a>
                                @else
                                    <span class='text-success'>{{__('auth.common.no')}}</span>
                                @endif
                            </td>
                            @if(Auth::user()->is_admin)
                                <td class="text-center">
                                    <a href="javascript:policyDocuments('{{$record->ci_id}}');"><i class="fas fa-upload text-warning"></i></a>
                                    <a href="javascript:allUniversalEnum('PolicyDelete', '{{$record->ci_id}}');"><i class="fas fa-trash text-danger"></i></a>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </dv>
    </div>
</div>
</div>

<!--begin::Card header-->
<div class="card-header" id="kt_chat_messenger_header">
    <!--begin::Title-->
    <div class="card-title">
        <!--begin::User-->
        <div class="d-flex justify-content-center flex-column me-3">
            <a href="#" class="fs-4 fw-bolder text-gray-900 text-hover-primary me-1 mb-2 lh-1">{{$empData->name}}</a>
        </div>
        <!--end::User-->
    </div>
    <!--end::Title-->
</div>
<!--end::Card header-->
<!--begin::Card body-->
<div class="card-body" id="kt_chat_messenger_body">
    <!--begin::Messages-->
    <div class="scroll-y me-n5 pe-5" data-kt-element="messages" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_header, #kt_toolbar, #kt_footer, #kt_chat_messenger_header, #kt_chat_messenger_footer" data-kt-scroll-wrappers="#kt_content, #kt_chat_messenger_body" data-kt-scroll-offset="5px" style="height:550px;">
    @if(count($conversation) > 0)
        @foreach($conversation as $record)
            <!--begin::Message(in)-->
                <div class="d-flex {{($record->askedBy == Auth::id()) ? 'justify-content-end' : 'justify-content-start'}} mb-10">
                    <!--begin::Wrapper-->
                    <div class="d-flex flex-column {{($record->askedBy == Auth::id()) ? 'align-items-end' : 'align-items-start'}}">
                        <!--begin::User-->
                        <div class="d-flex align-items-center mb-2">
                        @if($record->askedBy == Auth::id())
                            <!--begin::Details-->
                                <div class="ms-3">
                                    <a href="javascript:;" class="fs-5 fw-bolder text-gray-900 text-hover-primary me-1">{{ Auth::user()->name}}</a>
                                    <span class="text-muted fs-7 mb-1">{{\Carbon\Carbon::parse($record->communicationDate)->diffForHumans()}}</span>
                                </div>
                                <!--end::Details-->
                                <!--begin::Avatar-->
                                <div class="symbol symbol-35px symbol-circle">
                                    @if(Auth::user()->profile_image)
                                        <img alt="{{ Auth::user()->name}}" src="{{Storage::url(Auth::user()->profile_image) }}"/>
                                    @else
                                        <img alt="{{ Auth::user()->name}}" src="{{asset('images/profile-photo.jpg')}}"/>
                                    @endif
                                </div>
                                <!--end::Avatar-->
                        @else
                            <!--begin::Avatar-->
                                <div class="symbol symbol-35px symbol-circle">
                                    @if(Auth::user()->profile_image)
                                        <img alt="{{$empData->name }}" src="{{Storage::url($empData->profile_image)}}"/>
                                    @else
                                        <img alt="{{ Auth::user()->name}}" src="{{asset('images/profile-photo.jpg')}}"/>
                                    @endif
                                </div>
                                <!--end::Avatar-->
                                <!--begin::Details-->
                                <div class="ms-3">
                                    <span class="text-muted fs-7 mb-1">{{\Carbon\Carbon::parse($record->communicationDate)->diffForHumans()}}</span>
                                    <a href="javascript:;" class="fs-5 fw-bolder text-gray-900 text-hover-primary me-1">{{$empData->name }}</a>
                                </div>
                                <!--end::Details-->
                            @endif
                        </div>
                        <!--end::User-->
                        <!--begin::Text-->
                        <div class="p-5 rounded bg-light-info text-dark fw-bold mw-lg-400px text-start" data-kt-element="message-text">{{ $record->communication}}</div>
                        <!--end::Text-->
                    </div>
                    <!--end::Wrapper-->
                </div>
                <!--end::Message(in)-->
            @endforeach
        @endif
    </div>
    <!--end::Messages-->
</div>
<!--end::Card body-->
<!--begin::Card footer-->
<div class="card-footer pt-4" id="kt_chat_messenger_footer">
    <!--begin::Input-->
    <input type="hidden" name="parentId" id="parentId" value="{{count($conversation) > 0 ? $conversation[0]->parent_id : 0}}"/>
    <input type="hidden" name="userId" id="userId" value="{{$empData->user_id}}"/>
    <textarea class="form-control form-control-flush mb-3" rows="1" name="communication" id="communication" data-kt-element="input" placeholder="Type a message"></textarea>
    <!--end::Input-->
    <!--begin:Toolbar-->
    <div class="d-flex flex-stack">
        <!--begin::Send-->
        <a class="btn btn-primary pull-right" href="javascript:SubmitQueryReply();"><i class="fas fa-reply"></i> Reply</a>
        <!--end::Send-->
    </div>
    <!--end::Toolbar-->
</div>
<!--end::Card footer-->


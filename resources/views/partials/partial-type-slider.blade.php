@if($type)
    <div class="col-sm-6 mb-5">
        <div class="form-group">
            <label>Insurance Amount</label><br/>
            <input id="insuranceAmount" name="insuranceAmount" data-slider-id='insuranceAmountSlider' type="number" data-slider-min="{{$type->min_amount}}" data-slider-max="{{$type->max_amount}}" data-slider-step="50" data-slider-value="{{$type->min_amount}}"/>
            <span id="insuranceAmountCurrentSliderValLabel"><span id="insuranceAmountSliderVal">{{$type->min_amount}} CHF</span></span>
        </div>
    </div>
    <div class="col-sm-6 mb-5">
        <div class="form-group">
            <label>Insurance Duration</label><br/>
            <input id="insuranceDuration" name="insuranceDuration" data-slider-id='insuranceDurationSlider' type="number" data-slider-min="{{$type->min_duration}}" data-slider-max="{{$type->max_duration}}" data-slider-step="1" data-slider-value="{{$type->min_duration}}"/>
            <span id="insuranceDurationCurrentSliderValLabel"><span id="insuranceDurationSliderVal">{{$type->min_duration}} Years</span></span>
        </div>
    </div>
@endif

<div class="modal-header tile-header dvd dvd-btm">
    <h1 class="custom-font m-0"><strong>{{__('auth.common.meetings')}}</strong></h1>
</div>
<form class="ajax-form" id="addEditMeeting" data-reload-form="false" action="{{route('manage.meeting', $meeting->meeting_id ?? null)}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="modal-body pb-0">
        <div class="row">
            <div class="col-md-12 mb-5">
                <div class="form-group">
                    <label for="title">{{__('auth.common.meeting_details')}}</label>
                    <input type="text" class="form-control" name="meeting_detail" id="meeting_detail" required value="{{$meeting->meeting_detail ?? ''}}"/>
                </div>
            </div>
            <div class="col-sm-4 mb-5">
                <label for="title">{{__('auth.common.meeting_on')}}<b>*</b></label>
                <div class="input-group mb-5">
                    {!! Form::text('meeting_on',$meeting ? date('d/m/Y',strtotime($meeting->meeting_on)):"",['class'=>'form-control','id'=>'meeting_on', 'placeholder'=>'Meeting Date', 'required' => 'required'])!!}
                    <span class="input-group-text"><i class="fas fa-calendar"></i></span>
                </div>
            </div>
            <div class="col-sm-4 mb-5">
                <label for="title">{{__('auth.common.start_time')}}<b>*</b></label>
                <div class="input-group mb-5">
                    {!! Form::text('start_time',$meeting ? date('d/m/Y',strtotime($meeting->start_time)):"",['class'=>'form-control','id'=>'start_time', 'placeholder'=>'Start Time', 'required' => 'required'])!!}
                    <span class="input-group-text"><i class="fas fa-clock"></i></span>
                </div>
            </div>
            <div class="col-sm-4 mb-5">
                <label for="title">{{__('auth.common.end_time')}}<b>*</b></label>
                <div class="input-group mb-5">
                    {!! Form::text('end_time',$meeting ? date('d/m/Y',strtotime($meeting->end_time)):"",['class'=>'form-control','id'=>'end_time', 'placeholder'=>'End Time', 'required' => 'required'])!!}
                    <span class="input-group-text"><i class="fas fa-clock"></i></span>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12 mb-5">
                <div class="form-group">
                    <label for="title">{{__('auth.common.attachment_link')}}</label>
                    <input type="text" class="form-control" name="attachment_link" id="attachment_link" value="{{$meeting->attachment_link ?? ''}}"/>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6 mb-5">
                <div class="form-group">
                    <label for="avatar">{{__('auth.common.agenda_doc')}}<span class="text-thin"> (Max 20Mb)</span></label>
                    <input type="file" id="agenda_doc" name="agenda_doc" class="filestyle">
                </div>
            </div>
            <div class="col-md-6 mb-5">
                <div class="form-group">
                    <label for="avatar">{{__('auth.common.mom_doc')}}<span class="text-thin"> (Max 20Mb)</span></label>
                    <input type="file" id="mom_doc" name="mom_doc" class="filestyle">
                </div>
            </div>
            <div class="clearfix"></div>
            <h4 class="col-md-12 custom-font"><strong>Employees</strong></h4>
            @if(count($employees) > 0)
                @foreach($employees as $employee)
                    <div class="col-md-4 mb-5">
                        <label for="employee-{{$employee->employee_id}}" class="checkbox checkbox-custom-alt">
                            <input type="checkbox" class="meeting-employees" id="employee-{{$employee->employee_id}}" name="meetingEmployees[]"
                                   @if (in_array($employee->employee_id,$mappedEmployees))
                                   checked="checked"
                                   @endif
                                   value="{{$employee->employee_id}}"><i></i>{{$employee->employeeName}}
                        </label>
                    </div>
                @endforeach
            @endif
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">{{$meeting ? __('auth.common.update') : __('auth.common.submit')}}</button>
        <button type="button" class="btn btn-warning close-modal" data-bs-dismiss="modal">{{__('auth.common.close')}}</button>
    </div>
</form>
<script>
    $(function () {
        $("#meeting_on").daterangepicker({
                singleDatePicker: true,
                showDropdowns: false,
                locale: {
                    format: "DD/MM/YYYY"
                }
            }
        );
        $("#start_time").flatpickr({
            enableTime: true,
            noCalendar: true,
            dateFormat: "h:i K"
        });
        $("#end_time").flatpickr({
            enableTime: true,
            noCalendar: true,
            dateFormat: "h:i K"
        });
    });
</script>

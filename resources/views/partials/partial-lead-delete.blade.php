<div class="modal-header tile-header dvd dvd-btm">
    <h1 class="custom-font m-0"><strong>{{ __('auth.common.delete')}}</strong> Lead {{ $lead?'#'.$lead->id:'' }}</h1>
</div>
    <div class="modal-body pb-0">
        Are Sure Want to delete this lead ?        
    </div>
    <div class="modal-footer">
        <button type="button" onclick="javascript:allUniversalEnum('LeadDelete','{{$lead->lead_id}}');" class="btn btn-danger">{{__('auth.common.delete')}}</button>
        <button type="button" class="btn btn-warning close-modal" data-bs-dismiss="modal">{{__('auth.common.close')}}</button>
    </div>
<script>
    $(function () {
        $(":file").filestyle({buttonText: "&nbsp;Select Excel"});
    });
</script>

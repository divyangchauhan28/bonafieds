<div class="modal-header tile-header dvd dvd-btm">
    <h1 class="custom-font m-0"><strong>{{$document ? __('auth.common.edit') : __('auth.common.add')}}</strong> {{__('auth.documents.document')}}</h1>
</div>
<form class="ajax-form" id="addEditDocument" data-reload-form="false" action="{{route('create.document', $document->document_id ?? null)}}" method="POST">
    @csrf
    <div class="modal-body pb-0">
        <div class="row">
            <div class="col-md-6 mb-5">
                <div class="form-group">
                    <label for="title">{{__('auth.documents.documentEnglish')}}</label>
                    <input type="text" class="form-control" name="label_en" id="label_en" required value="{{$document->label_en ?? ''}}"/>
                </div>
            </div>
            <div class="col-md-6 mb-5">
                <div class="form-group">
                    <label for="title">{{__('auth.documents.documentGerman')}}</label>
                    <input type="text" class="form-control" name="label_de" id="label_de" required value="{{$document->label_de ?? ''}}"/>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">{{$document ? __('auth.common.update') : __('auth.common.submit')}}</button>
        <button type="button" class="btn btn-warning close-modal" data-bs-dismiss="modal">{{__('auth.common.close')}}</button>
    </div>
</form>

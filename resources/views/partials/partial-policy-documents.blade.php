<div class="modal-header tile-header dvd dvd-btm">
    <h1 class="custom-font m-0"><strong>{{ __('auth.common.manage')}}</strong> {{__('auth.common.documents')}}</h1>
</div>
<form class="ajax-form" id="addPolicyDocement" data-reload-form="false" action="{{route('policy.documents')}}" method="POST" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="policy_id" value="{{$policy->ci_id}}"/>
    <div class="modal-body pb-0">
        <div class="row">
            {{--@if(Auth::user()->is_employee)--}}
            <div class="col-md-6 mb-5">
                <div class="form-group">
                    <label for="title">{{__('auth.documents.documentTitle')}}</label>
                    {!! Form::select('document_id', $documents, old('document_id'), ['class' => 'form-control','id'=>'document_id','required'=>'required','placeholder'=>'Select Document']) !!}
                </div>
            </div>
            <div class="col-md-6 mb-5">
                <div class="form-group">
                    <label for="avatar">{{__('auth.customer.policyDocuments')}}<span class="text-thin"> (Max 20Mb)</span></label>
                    <input type="file" id="policy_document" name="policy_document" class="filestyle">
                </div>
            </div>
            <div class="clearfix"></div>
            {{--            @endif--}}
            <div class="col-md-12">
                <table class="table table-bordered" id="policy-documents">
                    <thead>
                    <tr>
                        <th>{{__('auth.common.number')}}</th>
                        <th>{{__('auth.documents.documentTitle')}}</th>
                        <th>{{__('auth.leads.file')}}</th>
                        <th class="text-center">{{__('auth.documents.documentStatus')}}</th>
                        <th class="text-center">{{__('auth.common.comment')}}</th>
                        <th class="text-center">{{__('auth.common.delete')}}</th>
                        <th class="text-center">{{__('auth.common.action')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($policyDocuments) > 0)
                        @foreach($policyDocuments as $record)
                            <tr>
                                <td>{{$no++}}</td>
                                <td>{{$record->docTitle}}</td>
                                <td><a href="{{Storage::url($record->document_path)}}" download="{{$record->document_path}}" target="_blank">{{$record->document_name}}</a></td>
                                <td class="text-center">
                                    @if(Auth::user()->is_employee)
                                        @if ($record->is_approved == 1)
                                            <span class='text-success'>{{__('auth.documents.approved')}}</span>
                                        @elseif($record->is_approved == 0)
                                            <span class='text-danger'>{{__('auth.documents.rejected')}}</span>
                                        @else
                                            <span class='text-warning'>{{__('auth.documents.pending')}}</span>
                                        @endif
                                    @else
                                        @if($record->is_approved == 2)
                                            <a href="javascript:allUniversalEnum('DocumentApprove', '{{$record->pd_id}}');"><i class="fa fa-thumbs-up text-success"></i></a>
                                            <a href="javascript:documentReject('{{$record->pd_id}}');"><i class="fa fa-thumbs-down text-danger"></i></a>
                                        @elseif($record->is_approved == 1)
                                            <span class='text-success'>{{ __('auth.documents.approved')}}</span>
                                        @else
                                            <span class='text-danger' {{ $record->is_approved }}>{{  __('auth.documents.rejected')}}</span>
                                        @endif
                                    @endif
                                </td>
                                <td>
                                @if ($record->is_approved == 0 || $record->comment_count > 0)
                                    <a href="javascript:policyDocumentComment('{{ $record->pd_id }}');">Add Comment</a><br />
                                    <a href="javascript:policyDocumentShowComment('{{ $record->pd_id }}');">Show Comment</a>
                                @else
                                -
                                @endif
                                </td>
                                <td class="text-center">
                                    @if ($record->is_deleted)
                                        <a href="javascript:allUniversalEnum('DocumentUndelete', '{{$record->document_id}}');" class='text-danger'>{{__('auth.common.yes')}}</a>
                                    @else
                                        <span class='text-success'>{{__('auth.common.no')}}</span>
                                    @endif
                                </td>
                                <td class="text-center">
                                    @if($record->is_approved !== 1)
                                        <a href="javascript:allUniversalEnum('PolicyDocumentDelete', '{{$record->pd_id}}');"><i class="fa fa-trash text-danger"></i></a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                <div class="clearfix">&nbsp;</div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        {{--        @if(Auth::user()->is_employee)--}}
        <button type="submit" class="btn btn-primary">{{__('auth.common.submit')}}</button>
        {{--        @endif--}}
        <button type="button" class="btn btn-warning close-modal" data-bs-dismiss="modal">{{__('auth.common.close')}}</button>
    </div>
</form>
<script>
    $(document).ready(function () {
        // $(":file").filestyle({buttonText: "&nbsp;Select Document"});
        $('#policy-documents').DataTable();

        $("#addPolicyDocement").submit(function(event) {
            event.preventDefault();
            var pid = document.getElementsByName('policy_id')[0].value;
            var formData = new FormData(this);
            $.ajax({
                url: "{{ route('ajax.postpolicy.documents') }}",
                method: "POST",
                data:formData,
                dataType: "json",
                cache:false,
                contentType: false,
                processData: false,
                success: function (response) {
                    // $('#modal_popup').modal('hide'); 
                    policyDocuments(pid);
                }
            });
        });
    });

    async function documentReject(relationId){
        const { value: text } = await swal.fire({
            input: 'textarea',
            inputLabel: 'Comment',
            inputPlaceholder: 'Type your Reject Comment here...',
            inputAttributes: {
                'aria-label': 'Type your reject comment here'
            },
            showCancelButton: true,
            inputValidator: (value) => {
                if (!value) {
                    return 'You need to write comment!'
                }
            }
        });
        if (text) {
            var actionType = 'DocumentReject';
            var comment = text;
            $.ajax({
                url: "{{route('change.universal.enum')}}",
                method: "get",
                data: {actionType: actionType, relationId: relationId, comment: comment},
                dataType: "json",
                success: function (response) {
                    location.reload();
                    //$('#modal_popup').modal('hide'); 
                }
            });
        }
    }

    async function policyDocumentComment(pdId) {
        const { value: text } = await swal.fire({
            input: 'textarea',
            inputLabel: 'Comment',
            inputPlaceholder: 'Type your Comment here...',
            inputAttributes: {
                'aria-label': 'Type your comment here'
            },
            showCancelButton: true,
            inputValidator: (value) => {
                if (!value) {
                return 'You need to write comment!'
                }
            }
        });
        if (text) {
            var comment = text;
            $.ajax({
                url: "{{ route('ajax.postpolicy.documentcomment') }}",
                method: "POST",
                data: {"_token": "{{ csrf_token() }}", policy_document_id: pdId, comment: comment},
                dataType: "json",
                success: function (response) {
                    // $('#modal_popup').modal('hide'); 
                }
            });
        }
    }

    function policyDocumentShowComment(pdId) {
        $.ajax({
            url: "{{ route('ajax.postpolicy.showdocumentcomment') }}",
            method: "get",
            data: {pId: pdId},
            dataType: "json",
            success: function (response) {
                $("#modal_popup .modal-content").html(response.body);
                $('#modal_popup').modal('show');
            }
        });
    }
</script>

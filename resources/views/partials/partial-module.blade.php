<div class="modal-header tile-header dvd dvd-btm">
    <h1 class="custom-font m-0"><strong>{{$module ? __('auth.common.edit') : __('auth.common.add')}}</strong> {{__('auth.common.module')}}</h1>
</div>
<form class="ajax-form" id="addEditModuler" data-reload-form="false" action="{{route('create.module', $module->module_id ?? null)}}" method="POST">
    @csrf
    <input type="hidden" name="module_identifier" id="Identifier">
    <div class="modal-body pb-0">
        <div class="row">
            <div class="col-md-12 mb-5">
                <div class="form-group">
                    <label for="title">{{__('auth.commission.title')}}</label>
                    <input type="text" class="form-control" name="module_title" id="Title" required value="{{$module->module_title ?? ''}}"/>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">{{$module ? __('auth.common.update') : __('auth.common.submit')}}</button>
        <button type="button" class="btn btn-warning close-modal" data-bs-dismiss="modal">{{__('auth.common.close')}}</button>
    </div>
</form>
<script>
    $("#Title").keyup(function () {
        var Text = jQuery(this).val();
        title = Text.replace(/\s+/g, '_').toUpperCase();
        $("#Identifier").val(title);
    });
</script>

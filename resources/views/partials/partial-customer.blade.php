<div class="modal-header tile-header dvd dvd-btm">
    <h1 class="custom-font m-0"><strong>{{$customer ? __('auth.common.edit') : __('auth.common.add')}}</strong> {{__('auth.customer.customer')}}</h1>
</div>
<form class="ajax-form" id="addEditCustomer" data-reload-form="false" action="{{route('create.customer', $customer->customer_id ?? null)}}" method="POST">
    @csrf
    <div class="modal-body pb-0">
        <div class="row">
            <div class="clearfix"></div>
            <div class="col-md-6 mb-5">
                <div class="form-group">
                    <label for="title">{{__('auth.common.employeeName')}}</label>
                    {!! Form::select('employee_id', $employee,$customer->employee_id ?? old('employee_id'),['class' => 'form-control chosen-select','id'=>'employee_id', 'placeholder'=>'Select Employee']) !!}
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-2 mb-5">
                <div class="form-group">
                    <label for="title">{{__('auth.customer.title')}}<b>*</b></label>
                    {!! Form::select('salutation', array('Herr'=>'Herr','Frau'=>'Frau'),old('salutation'),['class' => 'form-control','id'=>'salutation']) !!}
                </div>
            </div>
            <div class="col-sm-10 mb-5">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="title">{{__('auth.customer.firstName')}}<b>*</b></label>
                            <input type="text" class="form-control" name="firstname" id="firstname" required value="{{$customer->firstname ?? ''}}"/>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="title">{{__('auth.customer.lastName')}}<b>*</b></label>
                            <input type="text" class="form-control" name="lastname" id="lastname" required value="{{$customer->lastname ?? ''}}"/>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="title">{{__('auth.customer.gender')}}<b>*</b></label>
                            {!! Form::select('gender', array('Männlich'=>'Männlich','Weiblich'=>'Weiblich'),old('gender'),['class' => 'form-control','id'=>'gender']) !!}
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="title">{{__('auth.customer.maritalStatus')}}</label>
                            <input type="text" class="form-control" name="marital_status" id="marital_status" value="{{$customer->marital_status ?? ''}}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-3 mb-5">
                <label for="title">{{__('auth.customer.birthdate')}}<b>*</b></label>
                <div class="input-group mb-5">
                    {!! Form::text('birth_date',$customer ? date('d/m/Y',strtotime($customer->birth_date)):"",['class'=>'form-control','id'=>'birth_date', 'placeholder'=>'Date of Birth'])!!}
                    <span class="input-group-text"><i class="fas fa-calendar"></i></span>
                </div>
            </div>
            <div class="col-md-3 mb-5">
                <div class="form-group">
                    <label for="title">{{__('auth.customer.telephone')}}</label>
                    <input type="number" class="form-control" name="telephone" id="telephone" value="{{$customer->telephone ?? ''}}"/>
                </div>
            </div>
            <div class="col-md-3 mb-5">
                <div class="form-group">
                    <label for="title">{{__('auth.customer.mobile')}}<b>*</b></label>
                    <input type="number" class="form-control" name="mobile" id="mobile" required value="{{$customer->mobile ?? ''}}"/>
                </div>
            </div>
            <div class="col-md-3 mb-5">
                <div class="form-group">
                    <label for="title">{{__('auth.customer.email')}}<b>*</b></label>
                    <input type="email" class="form-control" name="email" id="email" required value="{{$customer->email ?? ''}}"/>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-5 mb-5">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="title">{{__('auth.customer.street')}}</label>
                            <input type="text" class="form-control" name="street" id="street" value="{{$customer->street ?? ''}}"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="title">{{__('auth.customer.city')}}<b>*</b></label>
                            <input type="text" class="form-control" name="city" id="city" required value="{{$customer->city ?? ''}}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-7 mb-5">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="title">{{__('auth.customer.postalCode')}}</label>
                            <input type="number" class="form-control" name="postal_code" id="postal_code" value="{{$customer->postal_code ?? ''}}"/>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="title">{{__('auth.customer.country')}}<b>*</b></label>
                            <input type="text" class="form-control" name="country" id="country" required value="{{$customer->country ?? ''}}"/>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="title">{{__('auth.customer.nationality')}}<b>*</b></label>
                            <input type="text" class="form-control" name="nationality" id="nationality" required value="{{$customer->nationality ?? ''}}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">{{$customer ? __('auth.common.update') : __('auth.common.submit')}}</button>
        <button type="button" class="btn btn-warning close-modal" data-bs-dismiss="modal">{{__('auth.common.close')}}</button>
    </div>
</form>
<script>
    $(function () {
        $("#birth_date").daterangepicker({
                singleDatePicker: true,
                showDropdowns: false,
                locale: {
                    format: "DD/MM/YYYY"
                }
            }
        );
    });
</script>


<div class="modal-header tile-header dvd dvd-btm">
    <h1 class="custom-font m-0"><strong>{{$youtubes ? __('auth.common.edit') : __('auth.common.add')}}</strong> {{__('auth.common.youtubes')}}</h1>
</div>
<form class="ajax-form" id="addEditLead" data-reload-form="false" action="{{route('create.youtubes', $youtubes->youtubes_id ?? null)}}" method="POST">
    @csrf
    <div class="modal-body pb-0">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group mb-5">
                    <label for="title">{{__('auth.common.youtubes_detail')}}</label>
                    <input placeholder="Paste embedded youtube link" type="url" class="form-control" name="youtubes_detail" id="youtubes_detail" value="{{$youtubes->youtubes_detail ?? ''}}" required/>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">{{$youtubes ? __('auth.common.update') : __('auth.common.submit')}}</button>
        <button type="button" class="btn btn-warning close-modal" data-bs-dismiss="modal">{{__('auth.common.close')}}</button>
    </div>
</form>

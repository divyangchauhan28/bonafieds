<div class="modal-header tile-header dvd dvd-btm">
    <h1 class="custom-font m-0"><strong>{{$paySlip ? __('auth.common.edit') : __('auth.common.add')}}</strong> {{__('auth.common.payslip')}}</h1>
</div>
<form class="ajax-form" id="addPayslip" data-reload-form="false" action="{{route('manage.payslip',$paySlip->payslip_id ?? null)}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="modal-body pb-0">
        <div class="row">
            <div class="col-md-6 mb-5">
                <div class="form-group">
                    <label for="title">{{__('auth.common.employeeName')}}</label>
                    {!! Form::select('employee_id', $employee,$paySlip->employee_id ?? old('employee_id'),['class' => 'form-control chosen-select','id'=>'employee_id', 'placeholder'=>'Select Employee']) !!}
                </div>
            </div>
            <div class="col-md-6 mb-5">
                <div class="form-group">
                    <label for="title">Amount</label>
                    <input type="text" class="form-control" name="payslip_amount" id="payslip_amount" value="{{$paySlip->payslip_amount ?? null}}"/>
                </div>
            </div>
            <div class="col-md-6 mb-5">
                <div class="form-group">
                    <label for="title">Month</label>
                    {!! Form::selectMonth('month',$paySlip->month ?? null,['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-6 mb-5">
                <div class="form-group">
                    <label for="title">Year</label>
                    <input type="text" class="form-control" name="year" id="year" value="{{$paySlip->year ?? null}}"/>
                </div>
            </div>
            <div class="col-md-12 mb-5">
                <div class="form-group">
                    <label for="avatar">{{__('auth.common.payslip')}} {{__('auth.leads.file')}} <span class="text-thin">Payslip File. Max 10Mb</span></label>
                    <input type="file" id="payslip_file" name="payslip_file" class="filestyle" value="{{$paySlip->payslip_file ?? null}}">
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">{{$paySlip ? __('auth.common.update') : __('auth.common.submit')}}</button>
        <button type="button" class="btn btn-warning close-modal" data-bs-dismiss="modal">{{__('auth.common.close')}}</button>
    </div>
</form>
<script>
    $(function () {
        $(".chosen-select").trigger("chosen:updated");
        $(":file").filestyle({buttonText: "&nbsp;Select Payslip File"});
    });
</script>

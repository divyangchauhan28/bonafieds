<div class="modal-header tile-header dvd dvd-btm">
    <h1 class="custom-font m-0"><strong>{{$policy ? __('auth.common.edit') : __('auth.common.add')}}</strong> {{__('auth.customer.policy')}}</h1>
</div>
<form class="ajax-form" id="addEditPolicy" data-reload-form="false" action="{{route('create.policy', $policy->ci_id ?? null)}}" method="POST">
    @csrf
    <div class="modal-body pb-0">
        <div class="row">
            <div class="clearfix"></div>
            <input type="hidden" name="customer_id" value="{{$customer->customer_id}}">
            @if(count($employee) > 0)
                <div class="col-sm-4 mb-5">
                    <div class="form-group">
                        <label for="title">{{__('auth.common.employee')}}<b>*</b></label>
                        {!! Form::select('provider_id', $employee,old('employee_id'),['class' => 'form-control','id'=>'employee_id','placeholder'=>'Select Employee']) !!}
                    </div>
                </div>
                <div class="clearfix"></div>
            @endif
            <div class="col-sm-4 mb-5">
                <div class="form-group">
                    <label for="title">{{__('auth.common.insuranceProviders')}}<b>*</b></label>
                    {!! Form::select('provider_id', $providers,old('provider_id'),['class' => 'form-control','id'=>'provider_id','placeholder'=>'Select Insurance Provider']) !!}
                </div>
            </div>
            <div class="col-sm-4 mb-5">
                <div class="form-group">
                    <label for="title">{{__('auth.common.insuranceTypes')}}<b>*</b></label>
                    {!! Form::select('type_id', $types,old('type_id'),['class' => 'form-control','id'=>'type_id']) !!}
                </div>
            </div>
            <div class="col-sm-4 mb-5">
                <div class="form-group">
                    <label for="title">{{__('auth.customer.paymentMode')}}<b>*</b></label>
                    {!! Form::select('installment_mode', array('Daily'=>'Daily','Fortnightly'=>'Fortnightly','Monthly'=>'Monthly','Quarterly'=>'Quarterly','Half Yearly'=>'Half Yearly','Yearly'=>'Yearly','One Time'=>'One Time'),old('installment_mode'),['class' => 'form-control','id'=>'installment_mode']) !!}
                </div>
            </div>
            <div id="type-slider"></div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">{{$policy ? __('auth.common.update') : __('auth.common.submit')}}</button>
        <button type="button" class="btn btn-warning close-modal" data-bs-dismiss="modal">{{__('auth.common.close')}}</button>
    </div>
</form>
<script>
    $(function () {
        $('#provider_id').change(function () {
            var providerId = $(this).val();
            $.ajax({
                url: "{{ route('ajax.provider.types') }}",
                method: "get",
                data: {providerId: providerId},
                dataType: "json",
                success: function (response) {
                    var ddlType = $("#type_id");
                    $("#type_id").empty();
                    var option = $("<option/>");
                    option.html('Select Insurance Type');
                    option.val('0');
                    ddlType.append(option);
                    $(response).each(function () {
                        var option = $("<option/>");
                        option.html(this.typeTitle);
                        option.val(this.typeId);
                        ddlType.append(option);
                    });
                }
            });
        })

        $('#type_id').change(function () {
            var typeId = $(this).val();
            $.ajax({
                url: "{{ route('ajax.type.details') }}",
                method: "get",
                data: {typeId: typeId},
                dataType: "json",
                success: function (response) {
                    $("#type-slider").html(response.body);
                    // $("#insuranceAmount").slider({
                    //     formatter: function (value) {
                    //         return 'Insurance Amount : ' + value + 'CHF';
                    //     }
                    // });
                    // $("#insuranceDuration").slider({
                    //     formatter: function (value) {
                    //         return 'Insurance Duration : ' + value + 'Years';
                    //     }
                    // });
                    //
                    // $("#insuranceAmount").on("slide", function (slideEvt) {
                    //     $("#insuranceAmountSliderVal").text(slideEvt.value + ' CHF');
                    // });
                    //
                    // $("#insuranceDuration").on("slide", function (slideEvt) {
                    //     $("#insuranceDurationSliderVal").text(slideEvt.value + ' Years');
                    // });
                    //
                    // $("#insuranceAmount, #insuranceDuration").slider();
                }
            });
        })
    });
</script>


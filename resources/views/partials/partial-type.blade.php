<div class="modal-header tile-header dvd dvd-btm">
    <h1 class="custom-font m-0"><strong>{{$type ? __('auth.common.edit') : __('auth.common.add')}}</strong> {{__('auth.common.type')}}</h1>
</div>
<form class="ajax-form" id="addEditType" data-reload-form="false" action="{{route('create.type', $type->type_id ?? null)}}" method="POST">
    @csrf
    <div class="modal-body pb-0">
        <div class="row">
            <div class="col-md-6 mb-5">
                <div class="form-group">
                    <label for="title">{{__('auth.types.typeEnglish')}}</label>
                    <input type="text" class="form-control" name="label_en" id="label_en" required value="{{$type->label_en ?? ''}}"/>
                </div>
            </div>
            <div class="col-md-6 mb-5">
                <div class="form-group">
                    <label for="title">{{__('auth.types.typeGerman')}}</label>
                    <input type="text" class="form-control" name="label_de" id="label_de" required value="{{$type->label_de ?? ''}}"/>
                </div>
            </div>
            <div class="col-md-6 mb-5">
                <div class="form-group">
                    <label for="title">{{__('auth.types.minAmount')}}</label>
                    <input type="text" class="form-control" name="min_amount" id="min_amount" required value="{{$type->min_amount ?? ''}}"/>
                </div>
            </div>
            <div class="col-md-6 mb-5">
                <div class="form-group">
                    <label for="title">{{__('auth.types.maxAmount')}}</label>
                    <input type="text" class="form-control" name="max_amount" id="max_amount" required value="{{$type->max_amount ?? ''}}"/>
                </div>
            </div>
            <div class="col-md-6 mb-5">
                <div class="form-group">
                    <label for="title">{{__('auth.types.minDuration')}}</label>
                    <input type="text" class="form-control" name="min_duration" id="min_duration" required value="{{$type->min_duration ?? ''}}"/>
                </div>
            </div>
            <div class="col-md-6 mb-5">
                <div class="form-group">
                    <label for="title">{{__('auth.types.maxDuration')}}</label>
                    <input type="text" class="form-control" name="max_duration" id="max_duration" required value="{{$type->max_duration ?? ''}}"/>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">{{$type ? __('auth.common.update') : __('auth.common.submit')}}</button>
        <button type="button" class="btn btn-warning close-modal" data-bs-dismiss="modal">{{__('auth.common.close')}}</button>
    </div>
</form>

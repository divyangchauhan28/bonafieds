<div class="modal-header tile-header dvd dvd-btm">
    <h1 class="custom-font m-0"><strong>{{$commission ? __('auth.common.edit') : __('auth.common.add')}}</strong> Commission</h1>
</div>
<form class="ajax-form" id="addEditType" data-reload-form="false" action="{{route('create.commission', $commission->commission_id ?? null)}}" method="POST">
    @csrf
    <div class="modal-body pb-0">
        <div class="row">
            <div class="col-md-6 mb-5">
                <div class="form-group">
                    <label for="title">{{__('auth.commission.commission')}}</label>
                    <input type="text" class="form-control" name="title" id="title" required value="{{$commission->title ?? ''}}"/>
                </div>
            </div>
            <div class="col-md-6 mb-5">
                <div class="form-group">
                    <label for="title">{{__('auth.commission.commissionPercent')}}</label>
                    <input type="text" class="form-control" name="approach" id="approach" required value="{{$commission->approach ?? ''}}"/>
                </div>
            </div>
            <div class="col-md-6 mb-5">
                <div class="form-group">
                    <label for="title">{{__('auth.commission.targetSalesFrom')}}</label>
                    <input type="text" class="form-control" name="target_from" id="target_from" required value="{{$commission->target_from ?? ''}}"/>
                </div>
            </div>
            <div class="col-md-6 mb-5">
                <div class="form-group">
                    <label for="title">{{__('auth.commission.targetSalesTo')}}</label>
                    <input type="text" class="form-control" name="target_to" id="target_to" required value="{{$commission->target_to ?? ''}}"/>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">{{$commission ? __('auth.common.update') : __('auth.common.submit')}}</button>
        <button type="button" class="btn btn-warning close-modal" data-bs-dismiss="modal">{{__('auth.common.close')}}</button>
    </div>
</form>

@extends('layouts.admin-metronic')
@section('breadcrums')
<!-- breadcrums -->
<div class="page-title d-flex flex-column me-5">
    <!--begin::Title-->
    <h1 class="d-flex flex-column text-dark fw-bolder fs-3 mb-0">{{__('auth.common.dashboard')}}</h1>
    <!--end::Title-->
    <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 pt-1">
        <!--begin::Item-->
        <li class="breadcrumb-item text-muted">
            <a href="{{route('admin.dashboard')}}" class="text-muted text-hover-primary">{{__('auth.common.home')}}</a>
        </li>
        <!--end::Item-->
        <!--begin::Item-->
        <li class="breadcrumb-item">
            <span class="bullet bg-gray-200 w-5px h-2px"></span>
        </li>
        <!--end::Item-->
        <!--begin::Item-->
        <li class="breadcrumb-item text-dark">{{__('auth.common.dashboard')}}</li>
        <!--end::Item-->
    </ul>
</div>
@endsection
@section('content')
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <div id="kt_content_container" class="container-xxl">
            @if(Auth::user()->is_admin || Auth::user()->is_employee)
            <!-- user admin or emp -->
                @if(count($birthdays) > 0)
                <!-- birthdays -->
                    <div class="alert alert-cyan">
                        Today is the birthday of
                        <strong>
                            @foreach($birthdays as $row)
                                @if($no++ > 0)
                                    {{", ".$row->salutation." ".$row->firstname." ".$row->lastname}}
                                @else
                                    {{$row->salutation." ".$row->firstname." ".$row->lastname}}
                                @endif
                            @endforeach
                        </strong>
                        @if(count($birthdays) == 1)
                            {{($row->gender == 'Männlich') ? 'wish him!' : 'wish her!'}}
                        @else
                            wish them!
                        @endif
                    </div>
                @endif
                @if($pendingDoc > 0)
                <!-- pendingDoc -->
                    <div class="alert alert-warning">
                        <strong>Document Upload Pending : {{$pendingDoc}}!</strong> Kindly upload it from <a href="{{route('pending.documents')}}" class="alert-link">Here</a>.
                    </div>
                @endif
                @if(Auth::user()->is_employee)
                <!-- emp -->
                    <div class="row g-5 g-xl-8">
                        <!-- chart -->
                        <div class="col-xl-7">
                            <div class="card card-xl-stretch mb-xl-8">
                                <div class="card-header border-0 pt-6">
                                    <div class="card-title">
                                        <h2>Monthly Sales</h2>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div id="commission-chart" style="height: 400px;" class="mb-10"></div>
                                </div>
                            </div>
                        </div>
                        <!-- ranking -->
                        <div class="col-xl-5">
                            <div class="card card-xl-stretch mb-xl-8">
                                <div class="card-header border-0 pt-6">
                                    <div class="card-title align-items-start flex-column">
                                        <h2>Top 5 {{__('auth.common.ranking')}}</h2>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="scroll-y me-n5 pe-5 h-lg-auto" data-kt-element="messages" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_header, #kt_toolbar, #kt_footer, #kt_chat_messenger_header, #kt_chat_messenger_footer" data-kt-scroll-wrappers="#kt_content, #kt_chat_messenger_body" data-kt-scroll-offset="5px">
                                    @if(count($ranking) > 0)
                                        @foreach($ranking as $record)
                                            <!--begin::Message(in)-->
                                                <div class="d-flex justify-content-start mb-10">
                                                    <!--begin::Wrapper-->
                                                    <div class="d-flex flex-column align-items-start">
                                                        <!--begin::User-->
                                                        <div class="d-flex align-items-center mb-2">
                                                            <!--begin::Avatar-->
                                                            <div class="symbol symbol-35px symbol-circle">
                                                                <img alt="{{$record->name}}" src="{{ asset('images/profile-photo.png') }}"/>
                                                            </div>
                                                            <!--end::Avatar-->
                                                            <!--begin::Details-->
                                                            <div class="ms-3">
                                                                <span class="fs-5 fw-bolder text-gray-900 text-hover-primary me-1">{{$record->name}}</span>
                                                                <span class="text-muted fs-7 mb-1">({{$record->salesPointCount}})</span>
                                                            </div>
                                                            <!--end::Details-->
                                                        </div>
                                                        <!--end::User-->
                                                    </div>
                                                    <!--end::Wrapper-->
                                                </div>
                                                <!--end::Message(in)-->
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                @endif
                <div class="row g-5 g-xl-8">
                    <!-- meeting -->
                    <div class="col-xl-7">
                        <div class="card card-xl-stretch mb-xl-8">
                            <div class="card-header border-0 pt-6">
                                <div class="card-title">
                                    <h2>{{__('auth.common.meetings')}}</h2>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-row-dashed gy-5 gs-7 rounded" id="all-meetings">
                                        <thead>
                                        <tr>
                                            <th>{{__('auth.common.number')}}</th>
                                            <th class="text-left">{{__('auth.common.meeting_details')}}</th>
                                            <th class="text-center">{{__('auth.common.documents')}}</th>
                                        </tr>
                                        </thead>
                                        @if(count($upcommingMeetings) > 0)
                                            <tbody>
                                            @foreach($upcommingMeetings as $record)
                                                <tr>
                                                    <td>{{$no++}}</td>
                                                    <td class="text-left">{!! $record->meeting_detail."<br/>on ".$record->meetingOn." @ ".$record->meetingTime !!}</td>
                                                    <td class="text-center">
                                                        @if($record->agenda_doc)
                                                            <a href="{{Storage::url($record->agenda_doc)}}" download="{{$record->agenda_title}}" target="_blank">
                                                                <i class="fa fa-download"></i> Agenda</a>&nbsp;&nbsp;
                                                        @endif
                                                        @if($record->mom_doc)
                                                            <a href="{{Storage::url($record->mom_doc)}}" download="{{$record->mom_title}}" target="_blank">
                                                                <i class="fa fa-download"></i> MOM</a>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        @endif
                                    </table>
                                </div>
                            </div>
                            <div class="card-header border-0 pt-6">
                                <div class="card-title">
                                    <h2>{{__('auth.common.chat')}}</h2>
                                </div>
                            </div>
                            <div class="card-body" id="kt_chat_contacts_body">
                                <input type="hidden" id="user_id" value="" name="user_id">
                                @include('partials.partial-contacts-body')
                            </div>
                        </div>
                    </div>
                    <!-- news -->
                    <div class="col-xl-5">
                        <div class="card card-xl-stretch mb-xl-8">
                            <div class="card-header border-0 pt-6">
                                <div class="card-title">
                                    <h2>{{__('auth.common.news')}}</h2>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="scroll-y me-n5 pe-5 h-lg-auto" data-kt-element="messages" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_header, #kt_toolbar, #kt_footer, #kt_chat_messenger_header, #kt_chat_messenger_footer" data-kt-scroll-wrappers="#kt_content, #kt_chat_messenger_body" data-kt-scroll-offset="5px">
                                    @if(count($news) > 0)
                                        @foreach($news as $record)
                                            <!--begin::Message(in)-->
                                            <div class="d-flex justify-content-start mb-10">
                                                <!--begin::Wrapper-->
                                                <div class="d-flex flex-column align-items-start">
                                                    <!--begin::User-->
                                                    <div class="d-flex align-items-center mb-2">
                                                        <!--begin::Avatar-->
                                                        <div class="symbol symbol-35px symbol-circle">
                                                            <img alt="{{$record->createdBy}}" src="{{ asset('images/profile-photo.png') }}"/>
                                                        </div>
                                                        <!--end::Avatar-->
                                                        <!--begin::Details-->
                                                        <div class="ms-3">
                                                            <span class="fs-5 fw-bolder text-gray-900 text-hover-primary me-1">{{$record->createdBy}}</span>
                                                            <span class="text-muted fs-7 mb-1">{{\Carbon\Carbon::createFromTimeStamp(strtotime($record->created_on))->diffForHumans()}}</span>
                                                        </div>
                                                        <!--end::Details-->
                                                    </div>
                                                    <!--end::User-->
                                                    <!--begin::Text-->
                                                    <div class="p-5 rounded bg-light-info text-dark fw-bold mw-lg-400px text-start" data-kt-element="message-text">{{$record->news_detail}}
                                                        @if(!is_null($record->image_docs) && !empty($record->image_docs))
                                                            <a href="{{Storage::url($record->image_docs)}}" target="_blank"><i class="fa fa-download"></i> Attachment</a>
                                                        @endif
                                                    </div>
                                                    <!--end::Text-->
                                                </div>
                                                <!--end::Wrapper-->
                                            </div>
                                            <!--end::Message(in)-->
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            <!--end::Wrapper-->
                        </div>
                        <!--end::Message(in)-->
                    </div>
                </div>
            @endif
        </div>
        <div id="kt_drawer_chat" class="bg-body" data-kt-drawer="true" data-kt-drawer-name="chat" data-kt-drawer-activate="true" data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'300px', 'md': '500px'}" data-kt-drawer-direction="end" data-kt-drawer-toggle="#kt_drawer_chat_toggle" data-kt-drawer-close="#kt_drawer_chat_close">
        </div>
    </div>
<!-- </div> -->
@endsection
@section('javascript')
<style>
    #kt_drawer_chat_messenger_body{
        height: 500px;overflow: scroll;
    }
</style>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#all-meetings').DataTable({"pageLength": 5});
            google.charts.load('current', {packages: ['corechart']});
            google.charts.setOnLoadCallback(LoadMonthlyCommission);
            @if(Auth::user()->is_employee)
                function LoadMonthlyCommission() {
                    $.ajax({
                        url: "{{ route('ajax.monthly.sales') }}",
                        method: "get",
                        dataType: "json",
                        success: function (response) {
                            var jsonData = response;
                            var data = new google.visualization.DataTable();
                            data.addColumn('string', 'Month');
                            data.addColumn('number', 'Sale');
                            $.each(jsonData, function (i, jsonData) {
                                var month = jsonData.month;
                                var sale = parseInt($.trim(jsonData.sale));
                                data.addRows([[month, sale]]);
                            });
                            var options = {
                                vAxis: {title: 'Sales'},
                                hAxis: {title: 'Months'},
                                seriesType: 'bars',
                                series: {1: {type: 'line'}}
                            };
                            var chart = new google.visualization.ComboChart(document.getElementById('commission-chart'));
                            chart.draw(data, options);
                        }
                    });
                }
            @endif
        });
        $('#loading').hide();
        setInterval(function(){
            LoadContacts();
        },10000);

        setInterval(function(){
            var uId = $('#user_id').val();
            if(uId!=''){
                LoadChatConversation(uId);
            }
        },10000);
    function LoadChatConversation(uId) {
        $('#user_id').val(uId);
        // $('#loading').show();
        $.ajax({
            url: "{{ route('ajax.managechat.conversation') }}",
            method: "get",
            data: {uId: uId},
            dataType: "json",
            success: function (response) {
                // $('#loading').hide();
                $('#kt_drawer_chat_messenger_body').html(response.body);
                var kcm    = $('#kt_drawer_chat_messenger_body .scroll-y');
                var height = kcm[0].scrollHeight;
                kcm.scrollTop(height);
            }
        });
    }
    function LoadContacts() {
        $.ajax({
            url: "{{ route('ajax.loadDashboardContacts') }}",
            method: "get",
            // data: {uId: uId},
            dataType: "json",
            success: function (response) {
                $('#kt_chat_contacts_body').html(response.body);
            }
        });
    }
    function LoadUserConversation(uId) {
        $('#user_id').val(uId);
        $('#loading').show();
        $.ajax({
            url: "{{ route('ajax.manage.dashboardchatconversation') }}",
            method: "get",
            data: {uId: uId},
            dataType: "json",
            success: function (response) {
                $('#kt_drawer_chat').addClass('drawer-on');
                $('#kt_drawer_chat').html(response.body);
                $('#loading').hide();
                // $('ul.chats').animate({scrollTop: $('ul.chats').height()}, 2000);
                var kcm    = $('#kt_drawer_chat_messenger .scroll-y');
                var height = kcm[0].scrollHeight;
                kcm.scrollTop(height);
            }
        });
    }
    function SubmitQueryReply() {
        var userId = $('#userId').val();
        var parentId = $('#parentId').val();
        var communication = $('textarea#communication').val();
        $.ajax({
            url: "{{ route('ajax.chat.reply') }}",
            method: "get",
            data: {userId: userId, parentId: parentId, communication: communication},
            dataType: "json",
            success: function (response) {
                LoadUserConversation(userId);
                var kcm    = $('#kt_drawer_chat_messenger_body .scroll-y');
                var height = kcm[0].scrollHeight;
                kcm.scrollTop(height);
                // $('ul.chats').animate({scrollTop: $('ul.chats').height()}, 2000);
            }
        });
    }
</script>
@endsection
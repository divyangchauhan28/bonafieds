@extends('layouts.admin-metronic')
@section('breadcrums')
    <div class="page-title d-flex flex-column me-5">
        <!--begin::Title-->
        <h1 class="d-flex flex-column text-dark fw-bolder fs-3 mb-0">{{__('auth.common.communication')}}</h1>
        <!--end::Title-->
        <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 pt-1">
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('admin.dashboard')}}" class="text-muted text-hover-primary">{{__('auth.common.home')}}</a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-dark">{{__('auth.common.communication')}}</li>
            <!--end::Item-->
        </ul>
    </div>
@endsection
@section('content')
    @if(Session::has('message'))
        <div class="col-md-12">
            <div class="alert alert-{{ Session::get('level')}}">
                <strong>{!! (Session::get('level') == 'success')? 'Success!':'Error!' !!}</strong> {!! Session::get('message')!!}
            </div>
        </div>
    @endif
    <div id="kt_content_container" class="container-xxl">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header border-0 pt-6">
                        <div class="card-title">
                            <h2>{{__('auth.common.communication')}} {{__('auth.common.list')}}</h2>
                        </div>
                        @if(Auth::user()->is_admin)
                            <a href="javascript:ManageCommunication();" class="btn btn-primary align-self-center">{{__('auth.common.communication')}}</a>
                        @endif
                    </div>
                    <div class="card-body pt-0">
                        <div class="table-responsive">
                            <table class="table table-row-dashed table-row-gray-300 gy-5 gs-7 rounded" id="all-communication">
                                <thead>
                                <tr>
                                    <th>{{__('auth.common.number')}}</th>
                                    <th>{{__('auth.common.communication')}}</th>
                                    <th class="text-center">{{__('auth.common.attachment')}}</th>
                                    <th class="text-center">{{__('auth.common.createdOn')}}</th>
                                    <th class="text-center">{{__('auth.common.createdBy')}}</th>
                                    @if(Auth::user()->is_admin)
                                        <th class="text-center">{{__('auth.common.action')}}</th>
                                    @else
                                        <th class="text-center">{{__('auth.common.viewedOn')}}</th>
                                    @endif
                                </tr>
                                </thead>
                                @if(count($communications) > 0)
                                    <tbody>
                                    @foreach($communications as $record)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$record->message}}</td>
                                            <td class="text-center"><a href="{{Storage::url($record->attachment)}}" download="{{$record->attachment}}" target="_blank"><i class="fa fa-download"></i> {{__('auth.common.attachment')}}</a></td>
                                            <td class="text-center">{{$record->createdOn}}</td>
                                            <td class="text-center">{{$record->createdBy}}</td>
                                            @if(Auth::user()->is_admin)
                                                <td class="text-center">
                                                    <a href="#" class="btn btn-sm btn-light btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">{{__('auth.common.action')}}</a>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-160px py-4" data-kt-menu="true">
                                                        <div class="menu-item px-3">
                                                            <a href="javascript:ManageCommunication('{{$record->com_id}}');" class="menu-link px-3">{{__('auth.common.edit')}}</a>
                                                        </div>
                                                        <div class="menu-item px-3">
                                                            <a href="javascript:allUniversalEnum('CommunicationDelete', '{{$record->com_id}}');" class="menu-link px-3">{{__('auth.common.delete')}}</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            @else
                                                <td class="text-center">
                                                    @if($record->is_viewed)
                                                        <span class='label bg-greensea'>{{$record->viewedOn}}</span>
                                                    @else
                                                        <a href="javascript:allUniversalEnum('CommunicationViewed', '{{$record->com_id}}');" class="btn btn-xs btn-warning btn-rounded">{{__('View')}}</a>
                                                    @endif
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                    </tbody>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#all-communication').DataTable();
        });

        function ManageCommunication(cId = null) {
            $.ajax({
                url: "{{ route('ajax.manage.communication') }}",
                method: "get",
                data: {cId: cId},
                dataType: "json",
                success: function (response) {
                    $("#modal_popup .modal-dialog").addClass('modal-lg');
                    $("#modal_popup .modal-content").html(response.body);
                    $('#modal_popup').modal('show');
                }
            });
        }
    </script>
@endsection

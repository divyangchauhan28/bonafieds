@extends('layouts.admin-metronic')
@section('breadcrums')
    <div class="page-title d-flex flex-column me-5">
        <!--begin::Title-->
        <h1 class="d-flex flex-column text-dark fw-bolder fs-3 mb-0">{{__('auth.common.youtubes')}}</h1>
        <!--end::Title-->
        <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 pt-1">
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('admin.dashboard')}}" class="text-muted text-hover-primary">{{__('auth.common.home')}}</a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-dark">{{__('auth.common.youtubes')}}</li>
            <!--end::Item-->
        </ul>
    </div>
@endsection
@section('content')
    @if(Session::has('message'))
        <div class="col-md-12">
            <div class="alert alert-{{ Session::get('level')}}">
                <strong>{!! (Session::get('level') == 'success')? 'Success!':'Error!' !!}</strong> {!! Session::get('message')!!}
            </div>
        </div>
    @endif
    <div id="kt_content_container" class="container-xxl">
        <div class="card">
            <div class="card-header border-0 pt-6">
                <div class="card-title">
                    <h2>{{__('auth.common.youtubes')}} {{__('auth.common.list')}}</h2>
                </div>
            </div>
            <div class="card-body pt-0">
                <div class="row">
                        @if(count($youtubes) > 0)
                            @foreach($youtubes as $i => $record)
                                <div class="col-md-6 pb-3">
                                    <!--begin::Video-->
                                    <div class="card-rounded ratio ratio-16x9">
                                        <iframe src="{{$record->youtubes_detail}}?enablejsapi=1&origin=https%3A%2F%2Fpreview.keenthemes.com" title="YouTube video {{$i}}" allowfullscreen="allowfullscreen" class="rounded" data-gtm-yt-inspected-1_19="true" id="197746483" data-gtm-yt-inspected-31871541_91="true"></iframe>
                                    </div>
                                    <!--end::Video-->
                                </div>
                            @endforeach
                        @endif
                </div>
            </div>
        </div>
    </div>
@endsection
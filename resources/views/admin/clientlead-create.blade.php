@extends('layouts.admin-metronic')
@section('breadcrums')
    <div class="page-title d-flex flex-column me-5">
        <!--begin::Title-->
        <h1 class="d-flex flex-column text-dark fw-bolder fs-3 mb-0">Client Leads</h1>
        <!--end::Title-->
        <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 pt-1">
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('admin.dashboard')}}" class="text-muted text-hover-primary">{{__('auth.common.home')}}</a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-dark">Client Leads</li>
            <!--end::Item-->
        </ul>
    </div>
@endsection
@section('content')
    @if(Session::has('message'))
        <div class="col-md-12">
            <div class="alert alert-{{ Session::get('level')}}">
                <strong>{!! (Session::get('level') == 'success')? 'Success!':'Error!' !!}</strong> {!! Session::get('message')!!}
            </div>
        </div>
    @endif
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-xxl">
            <!--begin::Card-->
            <div class="card">
                <!--begin::Card body-->
                <div class="card-body">
                    <!--begin::Stepper-->
                    <div class="stepper stepper-links d-flex flex-column pt-15" id="kt_create_account_stepper" data-kt-stepper="true" action="/action_page.php" method="get">
                        <!--begin::Nav-->
                        <div class="stepper-nav mb-5">
                            <!--begin::Step 1-->
                            <div class="stepper-item current" data-kt-stepper-element="nav">
                                <h3 class="stepper-title">Step 1</h3>
                            </div>
                            <!--end::Step 1-->
                            <!--begin::Step 2-->
                            <div class="stepper-item" data-kt-stepper-element="nav">
                                <h3 class="stepper-title">Step 2</h3>
                            </div>
                            <!--end::Step 2-->
                            <!--begin::Step 3-->
                            <div class="stepper-item" data-kt-stepper-element="nav">
                                <h3 class="stepper-title">Step 3</h3>
                            </div>
                            <!--end::Step 3-->
                            <!--begin::Step 4-->
                            <div class="stepper-item" data-kt-stepper-element="nav">
                                <h3 class="stepper-title">Step 4</h3>
                            </div>
                            <!--end::Step 4-->
                            <!--begin::Step 5-->
                            <div class="stepper-item" data-kt-stepper-element="nav">
                                <h3 class="stepper-title">Step 5</h3>
                            </div>
                            <!--end::Step 5-->
                            <!--begin::Step 6-->
                            <div class="stepper-item" data-kt-stepper-element="nav">
                                <h3 class="stepper-title">Step 6</h3>
                            </div>
                            <!--end::Step 6-->
                            <!--begin::Step 7-->
                            <div class="stepper-item" data-kt-stepper-element="nav">
                                <h3 class="stepper-title">Step 7</h3>
                            </div>
                            <!--end::Step 7-->

                            <!--begin::Step 8-->
                            <div class="stepper-item" data-kt-stepper-element="nav">
                                <h3 class="stepper-title">Step 8</h3>
                            </div>
                            <!--end::Step 8-->
                            <!--begin::Step 9-->
                            <div class="stepper-item" data-kt-stepper-element="nav">
                                <h3 class="stepper-title">Step 9</h3>
                            </div>
                            <!--end::Step 9-->
                        </div>
                        <!--end::Nav-->
                        <!--begin::Form-->
                        <form class="mx-auto mw-600px w-100 pt-15 pb-10 fv-plugins-bootstrap5 fv-plugins-framework" novalidate="novalidate" id="kt_create_account_form">
                            <!--begin::Step 1-->
                            <div class="current" data-kt-stepper-element="content">
                                <div class="w-100">
                                    <div class="pb-10 pb-lg-15">
                                        <h2 class="fw-bolder d-flex align-items-center text-dark">Step 1</h2>
                                    </div>
                                    <div class="fv-row fv-plugins-icon-container">
                                        {!! Form::token() !!}
                                        <div class="row">
                                            <div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2">
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Client Name</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step1[clientname]" value="">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="fv-row mb-7">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Date</span></label>
                                                        <input type="date" class="form-control form-control-solid" name="step1[date]" value="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Step 1-->
                            <!--begin::Step 2-->
                            <div data-kt-stepper-element="content">
                                <!--begin::Wrapper-->
                                <div class="w-100">
                                    <div class="pb-10 pb-lg-15">
                                        <h2 class="fw-bolder d-flex align-items-center text-dark">Step 2</h2>
                                    </div>
                                    <div class="fv-row fv-plugins-icon-container">
                                        <div class="row">
                                            <div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2">
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Surname</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step2[1][surname]" value="">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="fv-row mb-7">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Name</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step2[1][name]" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2">
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Realtionship</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step2[1][realtionship]" value="">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="fv-row mb-7">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Phonenumber</span></label>
                                                        <input type="number" class="form-control form-control-solid" name="step2[1][phonenumber]" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2">
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Age</span></label>
                                                        <input type="number" class="form-control form-control-solid" name="step2[1][age]" value="">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="fv-row mb-7">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Profession</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step2[1][profession]" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2">
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>City</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step2[1][city]" value="">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span></span></label>
                                                        <button type="button" class="form-control addmore" data-step="2" data-value="2">Add More</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="step2"></div>
                                    </div>
                                </div>
                                <!--end::Wrapper-->
                            </div>
                            <!--end::Step 2-->
                            <!--begin::Step 3-->
                            <div data-kt-stepper-element="content">
                                <!--begin::Wrapper-->
                                <div class="w-100">
                                    <div class="pb-10 pb-lg-15">
                                        <h2 class="fw-bolder d-flex align-items-center text-dark">Step 3</h2>
                                    </div>
                                    <div class="fv-row fv-plugins-icon-container">
                                        <div class="row">
                                            <div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2">
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Surname</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step3[1][surname]" value="">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="fv-row mb-7">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Name</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step3[1][name]" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2">
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Realtionship</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step3[1][realtionship]" value="">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="fv-row mb-7">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Phonenumber</span></label>
                                                        <input type="number" class="form-control form-control-solid" name="step3[1][phonenumber]" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2">
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Age</span></label>
                                                        <input type="number" class="form-control form-control-solid" name="step3[1][age]" value="">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="fv-row mb-7">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Profession</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step3[1][profession]" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2">
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>City</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step3[1][city]" value="">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span></span></label>
                                                        <button type="button" class="form-control addmore" data-step="3" data-value="2">Add More</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="step3"></div>
                                    </div>
                                </div>
                                <!--end::Wrapper-->
                            </div>
                            <!--end::Step 3-->
                            <!--begin::Step 4-->
                            <div data-kt-stepper-element="content">
                                <!--begin::Wrapper-->
                                <div class="w-100">
                                    <div class="pb-10 pb-lg-15">
                                        <h2 class="fw-bolder d-flex align-items-center text-dark">Step 4</h2>
                                    </div>
                                    <div class="fv-row fv-plugins-icon-container">
                                        <div class="row">
                                            <div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2">
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Surname</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step4[1][surname]" value="">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="fv-row mb-7">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Name</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step4[1][name]" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2">
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Realtionship</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step4[1][realtionship]" value="">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="fv-row mb-7">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Phonenumber</span></label>
                                                        <input type="number" class="form-control form-control-solid" name="step4[1][phonenumber]" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2">
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Age</span></label>
                                                        <input type="number" class="form-control form-control-solid" name="step4[1][age]" value="">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="fv-row mb-7">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Profession</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step4[1][profession]" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2">
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>City</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step4[1][city]" value="">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span></span></label>
                                                        <button type="button" class="form-control addmore" data-step="4" data-value="2">Add More</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="step4"></div>
                                    </div>
                                </div>
                                <!--end::Wrapper-->
                            </div>
                            <!--end::Step 4-->
                            <!--begin::Step 5-->
                            <div data-kt-stepper-element="content">
                                <!--begin::Wrapper-->
                                <div class="w-100">
                                    <div class="pb-10 pb-lg-15">
                                        <h2 class="fw-bolder d-flex align-items-center text-dark">Step 5</h2>
                                    </div>
                                    <div class="fv-row fv-plugins-icon-container">
                                        <div class="row">
                                            <div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2">
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Surname</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step5[surname]" value="">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="fv-row mb-7">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Name</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step5[name]" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Address/Number</span></label>
                                                        <textarea type="text" class="form-control form-control-solid" name="step5[address]"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2">
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Postal Code / City</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step5[postalcode]" value="">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="fv-row mb-7">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Phone-Mobile Number</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step5[phone]" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2">
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Email</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step5[email]" value="">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="fv-row mb-7">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Profession</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step5[profession]" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2">
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Employer name</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step5[employername]" value="">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="fv-row mb-7">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Birthday</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step5[birthday]" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2">
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>ID Number</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step5[idnumber]" value="">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="fv-row mb-7">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Marital status</span></label>
                                                        <select name="step5[maritalstatus]" class="form-control form-control-solid">
                                                            <option value="single">Single</option>
                                                            <option value="concubinated">Concubinated</option>
                                                            <option value="divorced">Divorced</option>
                                                            <option value="widowed">Widowed</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2">
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Nationaltity</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step5[nationaltity]" value="">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="fv-row mb-7">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Citizenship</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step5[citizenship]" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2">
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Approval</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step5[approval]" value="">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="fv-row mb-7">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>How long in switzerland</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step5[switzerland]" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2">
                                                <div class="col">
                                                    <div class="fv-row mb-7">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>IBAN number</span></label>
                                                        <select name="step5[ibannumber]" class="form-control form-control-solid">
                                                            <option value="bankaccount">Bankaccount</option>
                                                            <option value="postaccount">Postaccount</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Wrapper-->
                            </div>
                            <!--end::Step 5-->
                            <!--begin::Step 6-->
                            <div data-kt-stepper-element="content">
                                <!--begin::Wrapper-->
                                <div class="w-100">
                                    <div class="pb-10 pb-lg-15">
                                        <h2 class="fw-bolder d-flex align-items-center text-dark">Step 6</h2>
                                    </div>
                                    <div class="fv-row fv-plugins-icon-container">
                                        <div class="row">
                                            <div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2">
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Surname</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step6[1][surname]" value="">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="fv-row mb-7">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Name</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step6[1][name]" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2">
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Realtionship</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step6[1][realtionship]" value="">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="fv-row mb-7">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Phonenumber</span></label>
                                                        <input type="number" class="form-control form-control-solid" name="step6[1][phonenumber]" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2">
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Age</span></label>
                                                        <input type="number" class="form-control form-control-solid" name="step6[1][age]" value="">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="fv-row mb-7">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Profession</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step6[1][profession]" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2">
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>City</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step6[1][city]" value="">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span></span></label>
                                                        <button type="button" class="form-control addmore" data-step="6" data-value="2">Add More</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="step6"></div>
                                    </div>
                                </div>
                                <!--end::Wrapper-->
                            </div>
                            <!--end::Step 6-->
                            <!--begin::Step 7-->
                            <div data-kt-stepper-element="content">
                                <!--begin::Wrapper-->
                                <div class="w-100">
                                    <div class="pb-10 pb-lg-15">
                                        <h2 class="fw-bolder d-flex align-items-center text-dark">Step 7</h2>
                                    </div>
                                    <div class="fv-row fv-plugins-icon-container">
                                        <div class="row">
                                            <div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2">
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Surname</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step7[1][surname]" value="">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="fv-row mb-7">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Name</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step7[1][name]" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2">
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Realtionship</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step7[1][realtionship]" value="">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="fv-row mb-7">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Phonenumber</span></label>
                                                        <input type="number" class="form-control form-control-solid" name="step7[1][phonenumber]" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2">
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Age</span></label>
                                                        <input type="number" class="form-control form-control-solid" name="step7[1][age]" value="">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="fv-row mb-7">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Profession</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step7[1][profession]" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2">
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>City</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step7[1][city]" value="">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span></span></label>
                                                        <button type="button" class="form-control addmore" data-step="7" data-value="2">Add More</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="step7"></div>
                                    </div>
                                </div>
                                <!--end::Wrapper-->
                            </div>
                            <!--end::Step 7-->
                            <!--begin::Step 8-->
                            <div data-kt-stepper-element="content">
                                <div class="w-100">
                                    <div class="pb-10 pb-lg-15">
                                        <h2 class="fw-bolder d-flex align-items-center text-dark">Step 8</h2>
                                    </div>
                                    <div class="fv-row fv-plugins-icon-container">
                                        <div class="row">
                                            <div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2">
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>When do we sit together next time</span></label>
                                                        <input type="date" class="form-control form-control-solid" name="step8[appointment]" value="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Step 8-->
                            <!--begin::Step 9-->
                            <div data-kt-stepper-element="content">
                                <!--begin::Wrapper-->
                                <div class="w-100">
                                    <div class="pb-10 pb-lg-15">
                                        <h2 class="fw-bolder d-flex align-items-center text-dark">Step 9</h2>
                                    </div>
                                    <div class="fv-row fv-plugins-icon-container">
                                        <div class="row">
                                            <div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2">
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Surname</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step9[1][surname]" value="">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="fv-row mb-7">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Name</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step9[1][name]" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2">
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Realtionship</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step9[1][realtionship]" value="">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="fv-row mb-7">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Phonenumber</span></label>
                                                        <input type="number" class="form-control form-control-solid" name="step9[1][phonenumber]" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2">
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Age</span></label>
                                                        <input type="number" class="form-control form-control-solid" name="step9[1][age]" value="">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="fv-row mb-7">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>Profession</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step9[1][profession]" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2">
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span>City</span></label>
                                                        <input type="text" class="form-control form-control-solid" name="step9[1][city]" value="">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="fs-6 fw-bold form-label mt-3"><span></span></label>
                                                        <button type="button" class="form-control addmore" data-step="9" data-value="2">Add More</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="step9"></div>
                                    </div>
                                </div>
                                <!--end::Wrapper-->
                            </div>
                            <!--end::Step 9-->
                            <!--begin::Actions-->
                            <div class="d-flex flex-stack pt-15">
                                <!--begin::Wrapper-->
                                <div class="mr-2">
                                    <button type="button" class="btn btn-lg btn-light-primary me-3" data-kt-stepper-action="previous">
                                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr063.svg-->
                                    <span class="svg-icon svg-icon-4 me-1">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <rect opacity="0.5" x="6" y="11" width="13" height="2" rx="1" fill="currentColor"></rect>
                                            <path d="M8.56569 11.4343L12.75 7.25C13.1642 6.83579 13.1642 6.16421 12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75L5.70711 11.2929C5.31658 11.6834 5.31658 12.3166 5.70711 12.7071L11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25C13.1642 17.8358 13.1642 17.1642 12.75 16.75L8.56569 12.5657C8.25327 12.2533 8.25327 11.7467 8.56569 11.4343Z" fill="currentColor"></path>
                                        </svg>
                                    </span>
                                    <!--end::Svg Icon-->Back</button>
                                </div>
                                <!--end::Wrapper-->
                                <!--begin::Wrapper-->
                                <div>
                                    <button type="button" class="btn btn-lg btn-primary me-3" data-kt-stepper-action="submit">
                                        <span class="indicator-label">Submit 
                                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr064.svg-->
                                        <span class="svg-icon svg-icon-3 ms-2 me-0">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                <rect opacity="0.5" x="18" y="13" width="13" height="2" rx="1" transform="rotate(-180 18 13)" fill="currentColor"></rect>
                                                <path d="M15.4343 12.5657L11.25 16.75C10.8358 17.1642 10.8358 17.8358 11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25L18.2929 12.7071C18.6834 12.3166 18.6834 11.6834 18.2929 11.2929L12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75C10.8358 6.16421 10.8358 6.83579 11.25 7.25L15.4343 11.4343C15.7467 11.7467 15.7467 12.2533 15.4343 12.5657Z" fill="currentColor"></path>
                                            </svg>
                                        </span>
                                        <!--end::Svg Icon--></span>
                                        <span class="indicator-progress">Please wait... 
                                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                    </button>
                                    <button type="button" class="btn btn-lg btn-primary" data-kt-stepper-action="next">Continue 
                                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr064.svg-->
                                    <span class="svg-icon svg-icon-4 ms-1 me-0">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <rect opacity="0.5" x="18" y="13" width="13" height="2" rx="1" transform="rotate(-180 18 13)" fill="currentColor"></rect>
                                            <path d="M15.4343 12.5657L11.25 16.75C10.8358 17.1642 10.8358 17.8358 11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25L18.2929 12.7071C18.6834 12.3166 18.6834 11.6834 18.2929 11.2929L12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75C10.8358 6.16421 10.8358 6.83579 11.25 7.25L15.4343 11.4343C15.7467 11.7467 15.7467 12.2533 15.4343 12.5657Z" fill="currentColor"></path>
                                        </svg>
                                    </span>
                                    <!--end::Svg Icon--></button>
                                </div>
                                <!--end::Wrapper-->
                            </div>
                            <!--end::Actions-->
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Stepper-->
                </div>
                <!--end::Card body-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
@endsection
@section('javascript')
    <script src="{{ asset('metronic/js/custom/modals/create-account.js') }}"></script>
    <script>
        var elements = document.getElementsByClassName("addmore");
        
        var myFunction = function() {
            var step = this.getAttribute("data-step");
            var value = this.getAttribute("data-value");
            var dataString = '<div class="row"><div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2"><div class="col"><div class="fv-row mb-7 fv-plugins-icon-container"><label class="fs-6 fw-bold form-label mt-3"><span>Surname</span></label><input type="text" class="form-control form-control-solid" name="step'+step+'['+value+'][surname]" value=""></div></div><div class="col"><div class="fv-row mb-7"><label class="fs-6 fw-bold form-label mt-3"><span>Name</span></label><input type="text" class="form-control form-control-solid" name="step'+step+'['+value+'][name]" value=""></div></div></div><div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2"><div class="col"><div class="fv-row mb-7 fv-plugins-icon-container"><label class="fs-6 fw-bold form-label mt-3"><span>Realtionship</span></label><input type="text" class="form-control form-control-solid" name="step'+step+'['+value+'][realtionship]" value=""></div></div><div class="col"><div class="fv-row mb-7"><label class="fs-6 fw-bold form-label mt-3"><span>Phonenumber</span></label><input type="number" class="form-control form-control-solid" name="step'+step+'['+value+'][phonenumber]" value=""></div></div></div><div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2"><div class="col"><div class="fv-row mb-7 fv-plugins-icon-container"><label class="fs-6 fw-bold form-label mt-3"><span>Age</span></label><input type="number" class="form-control form-control-solid" name="step'+step+'['+value+'][age]" value=""></div></div><div class="col"><div class="fv-row mb-7"><label class="fs-6 fw-bold form-label mt-3"><span>Profession</span></label><input type="text" class="form-control form-control-solid" name="step'+step+'['+value+'][profession]" value=""></div></div></div><div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2"><div class="col"><div class="fv-row mb-7 fv-plugins-icon-container"><label class="fs-6 fw-bold form-label mt-3"><span>City</span></label><input type="text" class="form-control form-control-solid" name="step'+step+'['+value+'][city]" value=""></div></div></div></div>';
            document.getElementById("step"+step).innerHTML += dataString;
            this.setAttribute("data-value", parseInt(value)+1);
        };

        for (var i = 0; i < elements.length; i++) {
            elements[i].addEventListener('click', myFunction, false);
        }

    </script> 
@endsection
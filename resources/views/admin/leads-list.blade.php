@extends('layouts.admin-metronic')
@section('breadcrums')
    <div class="page-title d-flex flex-column me-5">
        <!--begin::Title-->
        <h1 class="d-flex flex-column text-dark fw-bolder fs-3 mb-0">{{__('auth.common.insurance')}} Leads</h1>
        <!--end::Title-->
        <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 pt-1">
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('admin.dashboard')}}" class="text-muted text-hover-primary">{{__('auth.common.home')}}</a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-dark">{{__('auth.common.insurance')}} Leads</li>
            <!--end::Item-->
        </ul>
    </div>
@endsection
@section('content')
    @if(Session::has('message'))
        <div class="col-md-12">
            <div class="alert alert-{{ Session::get('level')}}">
                <strong>{!! (Session::get('level') == 'success')? 'Success!':'Error!' !!}</strong> {!! Session::get('message')!!}
            </div>
        </div>
    @endif
    <div id="kt_content_container" class="container-xxl">
        <div class="row">
        @if(Auth::user()->is_admin)
                    <!-- <div id="commission-chart" style="height:400px;" class="mb-10"></div> -->
                    <div class="card card-bordered">
                        <div class="card-body">
                            <div id="kt_apexcharts_lead_1" style="height: 350px;"></div>
                        </div>
                    </div>
                @endif
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header border-0 pt-6">
                        <div class="card-title">
                            <h2>{{__('auth.common.insurance')}} Leads</h2>
                        </div>
                        @if(Auth::user()->is_admin)
                            <a href="javascript:ManageLead();" class="btn btn-primary align-self-center">Lead</a>
                        @endif
                    </div>
                    <div class="card-body pt-0">
                        <div class="table-responsive">
                            <table class="table table-row-dashed table-row-gray-300 gy-5 gs-7 rounded" id="all-leads">
                                <thead>
                                <tr>
                                    <th>{{__('auth.common.number')}}</th>
                                    <th>{{__('auth.leads.title')}}</th>
                                    <th>{{__('auth.leads.description')}}</th>
                                    <th>{{__('auth.leads.file')}}</th>
                                    <th class="text-center">{{__('auth.common.createdOn')}}</th>
                                    <th class="text-center">{{__('auth.common.createdBy')}}</th>
                                    @if(Auth::user()->is_admin)
                                        <th class="text-center">{{__('auth.common.status')}}</th>
                                        <th class="text-center">{{__('auth.common.action')}}</th>
                                    @else
                                        <th class="text-center">{{__('auth.common.action')}}</th>
                                    @endif
                                </tr>
                                </thead>
                                @if(count($leads) > 0)
                                    <tbody>
                                    @foreach($leads as $record)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$record->title}}</td>
                                            <td>{{$record->description}}</td>
                                            <td>{{$record->file_name}}</td>
                                            <td class="text-center">{{$record->createdOn}}</td>
                                            <td class="text-center">{{$record->createdBy}}</td>
                                            @if(Auth::user()->is_admin)
                                                <td class="text-center">
                                                    @if ($record->is_active)
                                                        <a href="javascript:allUniversalEnum('LeadInactive', '{{$record->lead_id}}');" class='text-success'>{{__('auth.common.active')}}</a>
                                                    @else
                                                        <a href="javascript:allUniversalEnum('LeadActive', '{{$record->lead_id}}');" class='text-warning'>{{__('auth.common.inactive')}}</a>
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    <a href="#" class="btn btn-sm btn-light btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">{{__('auth.common.action')}}</a>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-160px py-4" data-kt-menu="true">
                                                        <div class="menu-item px-3">
                                                            <a href="javascript:ManageLead('{{$record->lead_id}}');" class="menu-link px-3">{{__('auth.common.edit')}}</a>
                                                        </div>
                                                        <div class="menu-item px-3">
                                                            <a href="javascript:LeadMapping('{{$record->lead_id}}');" class="menu-link px-3">Lead Mapping</a>
                                                        </div>
                                                        <div class="menu-item px-3">
                                                            <a href="javascript:LeadDelete('{{$record->lead_id}}');" class="menu-link px-3">{{__('auth.common.delete')}}</a>
                                                        </div>
                                                        <div class="menu-item px-3">
                                                            <a href="{{route('leads.view',[$record->lead_id])}}" class="menu-link px-3">{{__('auth.common.view')}}</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            @endif
                                            @if(!Auth::user()->is_admin)
                                            <td class="text-center">
                                            <a href="{{route('leads.view',[$record->lead_id])}}" class="btn btn-xs btn-warning btn-rounded">{{__('auth.common.view')}}</a>
                                            </td>
                                            @endif
                                            
                                        </tr>
                                    @endforeach
                                    </tbody>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#all-leads').DataTable();
        });

        function ManageLead(lId = null) {
            $.ajax({
                url: "{{ route('ajax.manage.lead') }}",
                method: "get",
                data: {lId: lId},
                dataType: "json",
                success: function (response) {
                    $("#modal_popup .modal-content").html(response.body);
                    $('#modal_popup').modal('show');
                }
            });
        }

        function LeadDelete(lId = null) {
            $.ajax({
                url: "{{ route('ajax.manage.lead') }}",
                method: "get",
                data: {lId: lId,isDelete: true},
                dataType: "json",
                success: function (response) {
                    $("#modal_popup .modal-content").html(response.body);
                    $('#modal_popup').modal('show');
                }
            });
        }

        function LeadMapping(lId = null) {
            $.ajax({
                url: "{{ route('ajax.lead.mapping') }}",
                method: "get",
                data: {lId: lId},
                dataType: "json",
                success: function (response) {
                    $("#modal_popup .modal-dialog").addClass('modal-lg');
                    $("#modal_popup .modal-content").html(response.body);
                    $('#modal_popup').modal('show');
                }
            });
        }

    
    
    
    function renderChart(data,month,year){
        var monthNames = month.map(function(value,index){

            return value+' - '+year[index];
        })
        var element = document.getElementById('kt_apexcharts_lead_1');
        var height = parseInt(KTUtil.css(element, 'height'));
        var labelColor = KTUtil.getCssVariableValue('--bs-gray-500');
        var borderColor = KTUtil.getCssVariableValue('--bs-gray-200');
        var baseColor = KTUtil.getCssVariableValue('--bs-primary');
        var secondaryColor = KTUtil.getCssVariableValue('--bs-gray-300');
        var options = {
                series: [{
                    name: 'Total Lead : ',
                    data: data
                }],
                chart: {
                    fontFamily: 'inherit',
                    type: 'bar',
                    height: height,
                    toolbar: {
                        show: false
                    }
                },
                plotOptions: {
                    bar: {
                        horizontal: false,
                        columnWidth: ['30%'],
                        endingShape: 'rounded'
                    },
                },
                legend: {
                    show: false
                },
                dataLabels: {
                    enabled: false
                },
                stroke: {
                    show: true,
                    width: 2,
                    colors: ['transparent']
                },
                xaxis: {
                    categories: monthNames,
                    axisBorder: {
                        show: false,
                    },
                    axisTicks: {
                        show: false
                    },
                    labels: {
                        style: {
                            colors: labelColor,
                            fontSize: '12px'
                        }
                    }
                },
                yaxis: {
                    labels: {
                        style: {
                            colors: labelColor,
                            fontSize: '12px'
                        }
                    }
                },
                fill: {
                    opacity: 1
                },
                states: {
                    normal: {
                        filter: {
                            type: 'none',
                            value: 0
                        }
                    },
                    hover: {
                        filter: {
                            type: 'none',
                            value: 0
                        }
                    },
                    active: {
                        allowMultipleDataPointsSelection: false,
                        filter: {
                            type: 'none',
                            value: 0
                        }
                    }
                },
                tooltip: {
                    style: {
                        fontSize: '12px'
                    },
                    y: {
                        formatter: function (val) {
                            return val;
                        }
                    }
                },
                colors: [baseColor, secondaryColor],
                grid: {
                    borderColor: borderColor,
                    strokeDashArray: 4,
                    yaxis: {
                        lines: {
                            show: true
                        }
                    }
                }
            };
        var chart = new ApexCharts(element, options);
        chart.render();
    }

    $.ajax({
            url: "{{ route('ajax.lead.chart') }}",
            method: "get",
            dataType: "json",
            success: function (response) {
                renderChart(response.data,response.month,response.year)
            }
        });

        
    </script>
@endsection

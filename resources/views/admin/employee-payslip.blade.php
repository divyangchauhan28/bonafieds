@extends('layouts.admin-metronic')
@section('breadcrums')
    <div class="page-title d-flex flex-column me-5">
        <!--begin::Title-->
        <h1 class="d-flex flex-column text-dark fw-bolder fs-3 mb-0">{{__('auth.common.employeePayslip')}}</h1>
        <!--end::Title-->
        <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 pt-1">
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('admin.dashboard')}}" class="text-muted text-hover-primary">{{__('auth.common.home')}}</a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-dark">{{__('auth.common.employeePayslip')}}</li>
            <!--end::Item-->
        </ul>
    </div>
@endsection
@section('content')
    @if(Session::has('message'))
        <div class="col-md-12">
            <div class="alert alert-{{ Session::get('level')}}">
                <strong>{!! (Session::get('level') == 'success')? 'Success!':'Error!' !!}</strong> {!! Session::get('message')!!}
            </div>
        </div>
    @endif
    <div id="kt_content_container" class="container-xxl">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header border-0 pt-6">
                        <div class="card-title">
                            <h2>{{__('auth.common.employeePayslip')}}</h2>
                        </div>
                        @if(Auth::user()->is_admin)
                            <a href="javascript:ManagePayslip();" class="btn btn-primary align-self-center"><i class="fas fa-plus"></i> {{__('auth.common.payslip')}}</a>
                        @endif
                    </div>
                    <div class="card-body pt-0">
                        <div class="table-responsive">
                            <table class="table table-row-dashed table-row-gray-300 gy-5 gs-7 rounded" id="all-payslips">
                                <thead>
                                <tr>
                                    <th>{{__('auth.common.number')}}</th>
                                    <th>{{__('auth.common.employeeName')}}</th>
                                    <th class="text-center">Month Year</th>
                                    <th class="text-center">Amount</th>
                                    <th class="text-center">{{__('auth.leads.file')}}</th>
                                    <th class="text-center">{{__('auth.commission.updatedBy')}}</th>
                                    <th class="text-center">{{__('auth.commission.updatedOn')}}</th>
                                    <th class="text-center">{{__('auth.common.action')}}</th>
                                </tr>
                                </thead>
                                @if(count($payslips) > 0)
                                    <tbody>
                                    @foreach($payslips as $record)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$record->employeeName}}</td>
                                            <td class="text-center">{{Carbon\Carbon::create()->month($record->month)->startOfMonth()->format('F')."-".$record->year}}</td>
                                            <td class="text-center">{{$record->payslip_amount." CHF"}}</td>
                                            <td class="text-center">
                                                @if($record->payslip_file)
                                                    <a href="{{Storage::url($record->payslip_file)}}" class=""><i class="fas fa-download"></i> Payslip</a>
                                                @endif
                                            </td>
                                            <td class="text-center">{{$record->adminName}}</td>
                                            <td class="text-center">{{$record->uploadedOn}}</td>
                                            <td class="text-center">
                                                <a href="#" class="btn btn-sm btn-light btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">{{__('auth.common.action')}}</a>
                                                <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-160px py-4" data-kt-menu="true">
                                                    <div class="menu-item px-3">
                                                        <a href="javascript:ManagePayslip('{{$record->payslip_id}}');" class="menu-link px-3">{{__('auth.common.edit')}}</a>
                                                    </div>
                                                    <div class="menu-item px-3">
                                                        <a href="javascript:allUniversalEnum('PayslipDelete', '{{$record->payslip_id}}');" class="menu-link px-3">{{__('auth.common.delete')}}</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#all-payslips').DataTable();
        });
        function ManagePayslip(pId = null) {
            $.ajax({
                url: "{{ route('ajax.manage.payslip') }}",
                method: "get",
                data: {pId: pId},
                dataType: "json",
                success: function (response) {
                    $("#modal_popup .modal-content").html(response.body);
                    $('#modal_popup').modal('show');
                }
            });
        }
    </script>
@endsection

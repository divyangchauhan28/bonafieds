@extends('layouts.admin-metronic')
@section('breadcrums')
    <div class="page-title d-flex flex-column me-5">
        <!--begin::Title-->
        <h1 class="d-flex flex-column text-dark fw-bolder fs-3 mb-0">{{__('auth.common.user')}} {{__('auth.common.list')}}</h1>
        <!--end::Title-->
        <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 pt-1">
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('admin.dashboard')}}" class="text-muted text-hover-primary">{{__('auth.common.home')}}</a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-dark">{{__('auth.common.user')}} {{__('auth.common.list')}}</li>
            <!--end::Item-->
        </ul>
    </div>
@endsection
@section('content')
    @if(Session::has('message'))
        <div class="col-md-12">
            <div class="alert alert-{{ Session::get('level')}}">
                <strong>{!! (Session::get('level') == 'success')? 'Success!':'Error!' !!}</strong> {!! Session::get('message')!!}
            </div>
        </div>
    @endif
    <div id="kt_content_container" class="container-xxl">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header border-0 pt-6">
                        <div class="card-title">
                            <h2>{{__('auth.common.user')}} {{__('auth.common.list')}}</h2>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <div class="table-responsive">
                            <table class="table table-row-dashed table-row-gray-300 gy-5 gs-7 rounded" id="all-users">
                                <thead>
                                <tr>
                                    <th>{{__('auth.common.number')}}</th>
                                    <th>{{__('auth.name')}}</th>
                                    <th class="text-center">{{__('auth.email')}}</th>
                                    <th class="text-center">{{__('auth.phone')}}</th>
                                    <th class="text-center">{{__('auth.role')}}</th>
                                    <th class="text-center">{{__('auth.commission.commissionSlabs')}}</th>
                                    <th class="text-center">{{__('auth.common.status')}}</th>
                                    <th class="text-center">{{__('auth.common.blocked')}}</th>
                                    <th class="text-center">{{__('auth.common.action')}}</th>
                                </tr>
                                </thead>
                                @if(count($users) > 0)
                                    <tbody>
                                    @foreach($users as $record)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$record->name}}</td>
                                            <td class="text-center">{{$record->email}}</td>
                                            <td class="text-center">{{$record->contact_number}}</td>
                                            <td class="text-center">
                                                @if ($record->is_employee)
                                                    <span class='label bg-greensea'>{{__('auth.common.employee')}}</span>
                                                @elseif($record->is_admin)
                                                    <span class='label bg-greensea'>{{__('auth.common.admin')}}</span>
                                                @else
                                                    <span class='label bg-warning'>{{__('auth.common.customer')}}</span>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                <span class='text-primary'>{{$record->commissionSlabs ?? 'NA'}}</span>
                                            </td>
                                            <td class="text-center">
                                                @if ($record->is_active)
                                                    <a href="javascript:allUniversalEnum('UserInactive', '{{$record->user_id}}');" class='text-success'>{{__('auth.common.active')}}</a>
                                                @else
                                                    <a href="javascript:allUniversalEnum('UserActive', '{{$record->user_id}}');" class='text-warning'>{{__('auth.common.inactive')}}</a>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if ($record->is_blocked)
                                                    <a href="javascript:allUniversalEnum('UserUnblock', '{{$record->user_id}}');" class='text-danger'>{{__('auth.common.yes')}}</a>
                                                @else
                                                    <a href="javascript:allUniversalEnum('UserBlock', '{{$record->user_id}}');" class='text-success'>{{__('auth.common.no')}}</a>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                <a href="#" class="btn btn-sm btn-light btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">{{__('auth.common.action')}}</a>
                                                <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-160px py-4" data-kt-menu="true">
                                                    <div class="menu-item px-3">
                                                        <a href="javascript:ManageUser('{{$record->user_id}}');" class="menu-link px-3">{{__('auth.common.edit')}}</a>
                                                    </div>
                                                    @if($record->user_id !== Auth::user()->user_id)
                                                        <div class="menu-item px-3">
                                                            <a href="javascript:SetPassword('{{$record->user_id}}');" class="menu-link px-3">{{__('auth.password')}}</a>
                                                        </div>
                                                    @endif
                                                    <div class="menu-item px-3">
                                                        <a href="javascript:allUniversalEnum('UserDelete', '{{$record->user_id}}');" class="menu-link px-3">{{__('auth.common.delete')}}</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#all-users').DataTable();
        });

        function ManageUser(uId = null) {
            $.ajax({
                url: "{{ route('ajax.manage.user') }}",
                method: "get",
                data: {uId: uId},
                dataType: "json",
                success: function (response) {
                    $("#modal_popup .modal-content").html(response.body);
                    $('#modal_popup').modal('show');
                }
            });
        }

        function SetPassword(uId = null) {
            $.ajax({
                url: "{{ route('ajax.set.password') }}",
                method: "get",
                data: {uId: uId},
                dataType: "json",
                success: function (response) {
                    $("#modal_popup .modal-content").html(response.body);
                    $('#modal_popup').modal('show');
                }
            });
        }

    </script>
@endsection

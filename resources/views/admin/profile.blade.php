@extends('layouts.admin-metronic')
@section('breadcrums')
    <div class="page-title d-flex flex-column me-5">
        <!--begin::Title-->
        <h1 class="d-flex flex-column text-dark fw-bolder fs-3 mb-0">Profile</h1>
        <!--end::Title-->
        <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 pt-1">
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('admin.dashboard')}}" class="text-muted text-hover-primary">{{__('auth.common.home')}}</a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-dark">Profile</li>
            <!--end::Item-->
        </ul>
    </div>
@endsection
@section('content')
    <div id="kt_content_container" class="container-xxl">
        <div class="row">
            @if(Session::has('message'))
                <div class="col-md-12">
                    <div class="alert alert-{{ Session::get('level')}}">
                        <strong>{!! (Session::get('level') == 'success')? 'Success!':'Error!' !!}</strong> {!! Session::get('message')!!}
                    </div>
                </div>
            @endif
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header border-0 pt-6">
                        <div class="card-title">
                            <h3 class="card-title align-items-start flex-column">
                                <span class="card-label fw-bolder text-dark">Personal Setting</span>
                                <span class="text-muted mt-1 fw-bold fs-7">Your personal account settings</span>
                            </h3>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <form method="POST" action="{{ url('user/update-profile') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="form-group col-sm-6 mb-5">
                                    <label for="fullname">Full Name</label>
                                    <input type="text" class="form-control" id="fullname" name="name" value="{{$user['name']}}">
                                    @error('name')
                                    <span class="text-danger pull-left">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-sm-6 mb-5">
                                    <label for="avatar">Profile Image <span class="text-thin">PNG, JPEG, JPG. Max 1Mb</span></label>
                                    <input type="file" id="avatar" name="profile_image" class="filestyle" data-buttonText="Find file" data-iconName="fa fa-inbox">
                                    @error('profile_image')
                                    <span class="text-danger pull-left">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-sm-6 mb-5">
                                    <label for="email">E-mail</label>
                                    <input type="email" class="form-control" id="email" value="{{$user['email']}}" readonly>
                                </div>
                                <div class="form-group col-sm-6 mb-5">
                                    <label for="phone">Phone</label>
                                    <input type="number" class="form-control" id="phone" name="contact_number" value="{{$user['contact_number']}}">
                                    @error('contact_number')
                                    <span class="text-danger pull-left">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-sm-6">
                                    <button type="submit" class="btn btn-primary btn-sm">Update Profile</button>
                                    <a href="{{route('admin.dashboard')}}" class="btn btn-warning btn-sm">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header border-0 pt-6">
                        <div class="card-title">
                            <h3 class="card-title align-items-start flex-column">
                                <span class="card-label fw-bolder text-dark">Security Setting</span>
                                <span class="text-muted mt-1 fw-bold fs-7">Secure your account</span>
                            </h3>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <form method="POST" action="{{ url('user/change-password') }}">
                            @csrf
                            <div class="row">
                                <div class="form-group col-sm-6 mb-5">
                                    <label for="email">Username</label>
                                    <input type="email" name="email" class="form-control" id="email" value="{{$user['email']}}" readonly>
                                </div>
                                <div class="form-group col-sm-6 mb-5">
                                    <label for="oldpassword">Current Password</label>
                                    <input type="password" class="form-control" id="oldpassword" name="oldpassword" value="">
                                    @error('oldpassword')
                                    <span class="text-danger pull-left">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-sm-6 mb-5">
                                    <label for="password">New Password</label>
                                    <input type="password" class="form-control" id="password" name="password" value="">
                                    @error('password')
                                    <span class="text-danger pull-left">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-sm-6 mb-5">
                                    <label for="password-confirm">Confirm Password</label>
                                    <input type="password" class="form-control" id="password-confirm" name="password_confirmation" value="">
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-sm-6">
                                    <button type="submit" class="btn btn-primary btn-sm">Change Password</button>
                                    <a href="{{route('admin.dashboard')}}" class="btn btn-warning btn-sm">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.admin-metronic')
@section('breadcrums')
    <div class="page-title d-flex flex-column me-5">
        <!--begin::Title-->
        <h1 class="d-flex flex-column text-dark fw-bolder fs-3 mb-0">{{__('auth.common.dashboard')}}</h1>
        <!--end::Title-->
        <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 pt-1">
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('admin.dashboard')}}" class="text-muted text-hover-primary">{{__('auth.common.home')}}</a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-dark">{{__('auth.common.dashboard')}}</li>
            <!--end::Item-->
        </ul>
    </div>
@endsection
@section('content')
    <div id="kt_content_container" class="container-xxl">
        <!--begin::Layout-->
        <input type="hidden" name="user_id" id="user_id" value="">
        <div class="d-flex flex-column flex-lg-row">
            <!--begin::Sidebar-->
            <div class="flex-column flex-lg-row-auto w-100 w-lg-300px w-xl-400px mb-10 mb-lg-0 userListing">
                <!--begin::Contacts-->
                <div class="card card-flush">
                    <!--begin::Card body-->
                    <div class="card-body pt-5" id="kt_chat_contacts_body">
                        <!--begin::List-->
                        <div class="scroll-y me-n5 pe-5 h-200px h-lg-auto" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_header, #kt_toolbar, #kt_footer, #kt_chat_contacts_header" data-kt-scroll-wrappers="#kt_content, #kt_chat_contacts_body" data-kt-scroll-offset="5px">
                        @if(count($employees) > 0)
                            @foreach($employees as $record)
                                <!--begin::User-->
                                    <div class="d-flex flex-stack py-4">
                                        <!--begin::Details-->
                                        <div class="d-flex align-items-center">
                                            <!--begin::Avatar-->
                                            <div class="symbol symbol-45px symbol-circle">
                                                <?php 
                                                    $ext = \File::extension($record->profile_image);
                                                    $ext_array = array(
                                                        'jpeg','jpg','png','gif'
                                                    );
                                                ?>
                                                @if(in_array($ext,$ext_array))
                                                    <img alt="{{$record->name}}" src="{{Storage::url($record->profile_image)}}"/>
                                                @else
                                                    <img alt="{{$record->name}}" src="{{asset('images/profile-photo.jpg')}}"/>
                                                @endif
                                            </div>
                                            <!--end::Avatar-->
                                            <!--begin::Details-->
                                            <div class="ms-5">
                                                <a href="javascript:LoadUserConversation('{{$record->user_id}}');" class="fs-5 fw-bolder text-gray-900 text-hover-primary mb-2">{{$record->name}}</a>
                                                <div class="fw-bold text-muted">{{$record->email}}</div>
                                            </div>
                                            <!--end::Details-->
                                        </div>
                                        <!--end::Details-->
                                        <!--begin::Lat seen-->
                                        <div class="d-flex flex-column align-items-end ms-2">
                                            {{--                                            <span class="text-muted fs-7 mb-1">5 hrs</span>--}}
                                            <span class="badge badge-sm badge-circle badge-light-danger">{{$record->unreadCount}}</span>
                                        </div>
                                        <!--end::Lat seen-->
                                    </div>
                                    <!--end::User-->
                                    <!--begin::Separator-->
                                    <div class="separator separator-dashed d-none"></div>
                                    <!--end::Separator-->
                                @endforeach
                            @endif

                        </div>
                        <!--end::List-->
                    </div>
                    <!--end::Card body-->
                </div>
                <!--end::Contacts-->
            </div>
            <!--end::Sidebar-->
            <!--begin::Content-->
            <div class="flex-lg-row-fluid ms-lg-7 ms-xl-10">
                <!--begin::Messenger-->
                <div class="card" id="load-conversation"></div>
                <!--end::Messenger-->
            </div>
            <!--end::Content-->
        </div>
        <!--end::Layout-->
    </div>
@endsection
@section('javascript')
    <script>

        $(document).ready(function(){
            $('#loading').remove();
            
            setInterval(function(){
                LoadContacts();
            },5000);

            setInterval(function(){
                var uId = $('#user_id').val();
                if(uId!=''){
                    LoadChatConversation(uId);
                }
            },10000);
        });

        function LoadContacts() {
            $.ajax({
                url: "{{ route('ajax.loadContacts') }}",
                method: "get",
                // data: {uId: uId},
                dataType: "json",
                success: function (response) {
                    $('#kt_chat_contacts_body').html(response.body);
                    // var kcm    = $('#kt_chat_messenger_body .scroll-y');
                    // var height = kcm[0].scrollHeight;
                    // kcm.scrollTop(height);
                }
            });
        }

        function LoadChatConversation(uId) {
            $('#user_id').val(uId);
            $.ajax({
                url: "{{ route('ajax.managechat.conversation') }}",
                method: "get",
                data: {uId: uId},
                dataType: "json",
                success: function (response) {
                    $('#kt_chat_messenger_body').html(response.body);
                    var kcm    = $('#kt_chat_messenger_body .scroll-y');
                    var height = kcm[0].scrollHeight;
                    kcm.scrollTop(height);
                }
            });
        }

        function LoadUserConversation(uId) {
            $('#user_id').val(uId);
            $.ajax({
                url: "{{ route('ajax.manage.conversation') }}",
                method: "get",
                data: {uId: uId},
                dataType: "json",
                success: function (response) {
                    $('#load-conversation').html(response.body);
                    $('ul.chats').animate({scrollTop: $('ul.chats').height()}, 2000);
                    var kcm    = $('#kt_chat_messenger_body .scroll-y');
                    var height = kcm[0].scrollHeight;
                    kcm.scrollTop(height);
                }
            });
        }

        function SubmitQueryReply() {
            var userId = $('#userId').val();
            var parentId = $('#parentId').val();
            var communication = $('textarea#communication').val();
            $.ajax({
                url: "{{ route('ajax.chat.reply') }}",
                method: "get",
                data: {userId: userId, parentId: parentId, communication: communication},
                dataType: "json",
                success: function (response) {
                    LoadUserConversation(userId);
                    var kcm    = $('#kt_chat_messenger_body .scroll-y');
                    var height = kcm[0].scrollHeight;
                    kcm.scrollTop(height);
                    $('ul.chats').animate({scrollTop: $('ul.chats').height()}, 2000);
                }
            });
        }
    </script>
@endsection

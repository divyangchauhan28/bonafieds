@extends('layouts.admin-metronic')
@section('breadcrums')
    <div class="page-title d-flex flex-column me-5">
        <!--begin::Title-->
        <h1 class="d-flex flex-column text-dark fw-bolder fs-3 mb-0">{{__('auth.common.meetings')}}</h1>
        <!--end::Title-->
        <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 pt-1">
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('admin.dashboard')}}" class="text-muted text-hover-primary">{{__('auth.common.home')}}</a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-dark">{{__('auth.common.meetings')}}</li>
            <!--end::Item-->
        </ul>
    </div>
@endsection
@section('content')
    @if(Session::has('message'))
        <div class="col-md-12">
            <div class="alert alert-{{ Session::get('level')}}">
                <strong>{!! (Session::get('level') == 'success')? 'Success!':'Error!' !!}</strong> {!! Session::get('message')!!}
            </div>
        </div>
    @endif
    <div id="kt_content_container" class="container-xxl">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header border-0 pt-6">
                        <div class="card-title">
                            <h2>{{__('auth.common.meetings')}}</h2>
                        </div>
                        @if(Auth::user()->is_admin)
                            <a href="javascript:ManageMeeting();" class="btn btn-primary align-self-center">{{__('auth.common.meeting')}}</a>
                        @endif
                    </div>
                    <div class="card-body pt-0">
                        <div class="table-responsive">
                            <table class="table table-row-dashed table-row-gray-300 gy-5 gs-7 rounded" id="all-meetings">
                                <thead>
                                <tr>
                                    <th>{{__('auth.common.number')}}</th>
                                    <th>{{__('auth.common.meeting_details')}}</th>
                                    <th class="text-center">{{__('auth.common.meeting_on')}}</th>
                                    <th class="text-center">{{__('auth.common.attachment_link')}}</th>
                                    <th class="text-center">{{__('auth.common.documents')}}</th>
                                    <th class="text-center">{{__('auth.common.scheduled_by')}}</th>
                                    <th class="text-center">{{__('auth.common.schedule_on')}}</th>
                                    @if(Auth::user()->is_admin)
                                        <th class="text-center">{{__('auth.common.status')}}</th>
                                        <th class="text-center">{{__('auth.common.action')}}</th>
                                    @endif
                                </tr>
                                </thead>
                                @if(count($meetings) > 0)
                                    <tbody>
                                    @foreach($meetings as $record)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$record->meeting_detail}}</td>
                                            <td class="text-center">{{$record->meetingOn." @ ".$record->meetingTime}}</td>
                                            <td class="text-center">
                                                @if(!is_null($record->attachment_link))
                                                    <a href="{{$record->attachment_link}}" target="_blank"><i class="fas fa-link text-primary"></i></a>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if($record->agenda_doc && $record->agenda_title)
                                                    <a href="{{Storage::url($record->agenda_doc)}}" download="{{$record->agenda_title}}" target="_blank"><i class="fas fa-download"></i> Agenda</a>
                                                @endif
                                                @if($record->mom_doc && $record->mom_title)
                                                    <a href="{{Storage::url($record->mom_doc)}}" download="{{$record->mom_title}}" target="_blank"><i class="fas fa-download"></i> MOAM</a>
                                                @endif
                                            </td>
                                            <td class="text-center">{{$record->scheduledBy}}</td>
                                            <td class="text-center">{{$record->scheduleOn}}</td>
                                            @if(Auth::user()->is_admin)
                                                <td class="text-center">
                                                    @if ($record->is_active)
                                                        <a href="javascript:allUniversalEnum('MeetingInactive', '{{$record->meeting_id}}');" class='text-success'>{{__('auth.common.active')}}</a>
                                                    @else
                                                        <a href="javascript:allUniversalEnum('MeetingActive', '{{$record->meeting_id}}');" class='text-warning'>{{__('auth.common.inactive')}}</a>
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    <a href="#" class="btn btn-sm btn-light btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">{{__('auth.common.action')}}</a>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-160px py-4" data-kt-menu="true">
                                                        <div class="menu-item px-3">
                                                            <a href="javascript:ManageMeeting('{{$record->meeting_id}}');" class="menu-link px-3">{{__('auth.common.edit')}}</a>
                                                        </div>
                                                        <div class="menu-item px-3">
                                                            <a href="javascript:allUniversalEnum('MeetingDelete', '{{$record->meeting_id}}');" class="menu-link px-3">{{__('auth.common.delete')}}</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                    </tbody>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#all-meetings').DataTable();
        });

        function ManageMeeting(mId = null) {
            $.ajax({
                url: "{{ route('ajax.manage.meeting') }}",
                method: "get",
                data: {mId: mId},
                dataType: "json",
                success: function (response) {
                    $("#modal_popup .modal-dialog").addClass('modal-lg');
                    $("#modal_popup .modal-content").html(response.body);
                    $('#modal_popup').modal('show');
                }
            });
        }
    </script>
@endsection

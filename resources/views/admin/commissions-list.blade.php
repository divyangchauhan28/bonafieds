@extends('layouts.admin-metronic')
@section('breadcrums')
    <div class="page-title d-flex flex-column me-5">
        <!--begin::Title-->
        <h1 class="d-flex flex-column text-dark fw-bolder fs-3 mb-0">{{__('auth.commission.commission')}} {{__('auth.commission.slabs')}}</h1>
        <!--end::Title-->
        <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 pt-1">
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('admin.dashboard')}}" class="text-muted text-hover-primary">{{__('auth.common.home')}}</a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-dark">{{__('auth.commission.commission')}} {{__('auth.commission.slabs')}}</li>
            <!--end::Item-->
        </ul>
    </div>
@endsection
@section('content')
    @if(Session::has('message'))
        <div class="col-md-12">
            <div class="alert alert-{{ Session::get('level')}}">
                <strong>{!! (Session::get('level') == 'success')? 'Success!':'Error!' !!}</strong> {!! Session::get('message')!!}
            </div>
        </div>
    @endif
    <div id="kt_content_container" class="container-xxl">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header border-0 pt-6">
                        <div class="card-title">
                            <h2>{{__('auth.commission.commission')}} {{__('auth.commission.slabs')}}</h2>
                        </div>
                        <a href="javascript:ManageCommissions();" class="btn btn-primary align-self-center">{{__('auth.commission.commission')}}</a>
                    </div>
                    <div class="card-body pt-0">
                        <div class="table-responsive">
                            <table class="table table-row-dashed table-row-gray-300 gy-5 gs-7 rounded" id="all-commissions">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>{{__('auth.commission.title')}}</th>
                                    <th class="text-center">{{__('auth.commission.commission')}}</th>
                                    <th class="text-center">{{__('auth.commission.targetSales')}}</th>
                                    <th class="text-center">{{__('auth.common.status')}}</th>
                                    <th class="text-center">{{__('auth.common.action')}}</th>
                                </tr>
                                </thead>
                                @if(count($commissions) > 0)
                                    <tbody>
                                    @foreach($commissions as $record)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$record->title}}</td>
                                            <td class="text-center">{{$record->approach}}</td>
                                            <td class="text-center">{{$record->target_from." - ".$record->target_to}}</td>
                                            <td class="text-center">
                                                @if ($record->is_active)
                                                    <a href="javascript:allUniversalEnum('CommissionInactive', '{{$record->commission_id}}');" class='text-success'>{{__('auth.common.active')}}</a>
                                                @else
                                                    <a href="javascript:allUniversalEnum('CommissionActive', '{{$record->commission_id}}');" class='text-warning'>{{__('auth.common.inactive')}}</a>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                <a href="#" class="btn btn-sm btn-light btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">{{__('auth.common.action')}}</a>
                                                <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-160px py-4" data-kt-menu="true">
                                                    <div class="menu-item px-3">
                                                        <a href="javascript:ManageCommissions('{{$record->commission_id}}');" class="menu-link px-3">{{__('auth.common.edit')}}</a>
                                                    </div>
                                                    <div class="menu-item px-3">
                                                        <a href="javascript:allUniversalEnum('CommissionDelete', '{{$record->commission_id}}');" class="menu-link px-3">{{__('auth.common.delete')}}</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#all-commissions').DataTable();
        });

        function ManageCommissions(cId = null) {
            $.ajax({
                url: "{{ route('ajax.manage.commission') }}",
                method: "get",
                data: {cId: cId},
                dataType: "json",
                success: function (response) {
                    $("#modal_popup .modal-content").html(response.body);
                    $('#modal_popup').modal('show');
                }
            });
        }
    </script>
@endsection

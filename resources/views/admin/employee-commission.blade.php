@extends('layouts.admin-metronic')
@section('breadcrums')
    <div class="page-title d-flex flex-column me-5">
        <!--begin::Title-->
        <h1 class="d-flex flex-column text-dark fw-bolder fs-3 mb-0">{{__('auth.common.employeeCommission')}}</h1>
        <!--end::Title-->
        <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 pt-1">
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('admin.dashboard')}}" class="text-muted text-hover-primary">{{__('auth.common.home')}}</a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-dark">{{__('auth.common.employeeCommission')}}</li>
            <!--end::Item-->
        </ul>
    </div>
@endsection
@section('content')
    @if(Session::has('message'))
        <div class="col-md-12">
            <div class="alert alert-{{ Session::get('level')}}">
                <strong>{!! (Session::get('level') == 'success')? 'Success!':'Error!' !!}</strong> {!! Session::get('message')!!}
            </div>
        </div>
    @endif
    <div id="kt_content_container" class="container-xxl">
        <div class="row">
            <div class="col-md-12">
                @if(Auth::user()->is_admin)
                    <div id="commission-chart" style="height: 400px;" class="mb-10"></div>
                @endif
                <div class="clearfix"></div>
                <form class="ajax-form row" id="FilterPolicy" data-reload-form="false" action="{{route('employee.commission')}}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-md-4 mb-10">
                            <div class="form-group">
                                {!! Form::select('customerId', $customers,$customerId,['class' => 'form-control','id'=>'customerId','placeholder'=>'Select Customer']) !!}
                            </div>
                        </div>
                        @if(Auth::user()->is_admin)
                            <div class="col-sm-4 mb-10">
                                <div class="form-group">
                                    {!! Form::select('employeeId', $employees,$employeeId,['class' => 'form-control','id'=>'employeeId','placeholder'=>'Select Employee']) !!}
                                </div>
                            </div>
                        @endif
                        <div class="col-md-1">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-filter"></i></button>
                        </div>
                    </div>
                </form>
                <div class="clearfix"></div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header border-0 pt-6">
                            <div class="card-title">
                                <h2>{{__('auth.common.statusOfRequests')}}</h2>
                            </div>
                        </div>
                        <div class="card-body pt-0">
                            <div class="table-responsive">
                                <table class="table table-row-dashed table-row-gray-300 gy-5 gs-7 rounded" id="all-commission">
                                    <thead>
                                    <tr>
                                        <th>{{__('auth.common.number')}}</th>
                                        @if (Auth::user()->is_admin)
                                            <th>{{__('auth.common.employeeName')}}</th>
                                        @endif
                                        <th>{{__('auth.customer.customerName')}}</th>
                                        <th>{{__('auth.common.insurance')}}</th>
                                        <th class="text-center">{{__('auth.types.amount')}}</th>
                                        <th class="text-center">{{__('auth.customer.policyStatus')}}</th>
                                        <th class="text-center">{{__('auth.commission.salesPoint')}}</th>
                                        <th class="text-center">{{__('auth.common.commission')}}</th>
                                        <th class="text-center">{{__('auth.commission.commissionStatus')}}</th>
                                        <th class="text-center">{{__('auth.commission.updatedBy')}}</th>
                                        <th class="text-center">{{__('auth.commission.updatedOn')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($policies) > 0)
                                        @foreach($policies as $record)
                                            <tr>
                                                <td>{{$no++}}</td>
                                                @if (Auth::user()->is_admin)
                                                    <td>{{$record->employeeName}}</td>
                                                @endif
                                                <td>{{$record->salutation." ".$record->firstname." ".$record->lastname}}</td>
                                                <td>{{$record->insProvider." - ".$record->insType}}</td>
                                                <td class="text-center">
                                                    @if($record->commission_type == 'Credit')
                                                        <span class="text-success">{{$record->insurance_amount." CHF"}}</span>
                                                    @else
                                                        <span class="text-danger">{{$record->insurance_amount." CHF"}}</span>
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    @if($record->insurance_status == 'Approve')
                                                        <span class="text-success">{{__('auth.documents.approved')}}</span>
                                                    @elseif($record->insurance_status == 'Reject')
                                                        <span class="text-danger">{{__('auth.documents.rejected')}}</span>
                                                    @else
                                                        <span class="text-warning">{{__('auth.common.inprogress')}}</span>
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    @if($record->commission_type == 'Credit')
                                                        <span class="text-success">{{$record->sales_point}}</span>
                                                    @else
                                                        <span class="text-danger">{{$record->sales_point}}</span>
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    @if($record->commission_type == 'Credit')
                                                        <span class="text-success">{{$record->employee_commission." CHF"}}</span>
                                                    @else
                                                        <span class="text-danger">{{$record->employee_commission." CHF"}}</span>
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    @if ($record->commission_status == 2)
                                                        @if(Auth::user()->is_employee)
                                                            <span class='label bg-warning'>{{__('auth.documents.pending')}}</span>
                                                        @else
                                                            <a href="javascript:allUniversalEnum('CommissionApprove', '{{$record->cc_id}}');" class='text-success'><i class="fa fa-thumbs-up"></i></a>
                                                            <a href="javascript:allUniversalEnum('CommissionReject', '{{$record->cc_id}}');" class='text-warning'><i class="fa fa-thumbs-down"></i></a>
                                                        @endif
                                                    @elseif($record->commission_status == 1)
                                                        <span class='text-success'>{{__('auth.documents.approved')}}</span>
                                                    @else
                                                        <span class='text-danger'>{{__('auth.documents.rejected')}}</span>
                                                    @endif
                                                </td>
                                                <td class="text-center">{{$record->approverName}}</td>
                                                <td class="text-center">{{$record->updatedOn}}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#all-commission').DataTable();
            google.charts.load('current', {packages: ['corechart']});
            google.charts.setOnLoadCallback(LoadMonthlyCommission);
        });
        @if(Auth::user()->is_admin)
        function LoadMonthlyCommission(comMonth = 0, comYear = 0) {
            $.ajax({
                url: "{{ route('ajax.monthly.commission') }}",
                method: "get",
                data: {comMonth: comMonth, comYear: comYear},
                dataType: "json",
                success: function (response) {
                    var jsonData = response;
                    var data = new google.visualization.DataTable();
                    data.addColumn('string', 'Employee');
                    data.addColumn('number', 'Minimum');
                    data.addColumn('number', 'Maximum');
                    data.addColumn('number', 'Actual');
                    $.each(jsonData, function (i, jsonData) {
                        var employeeName = jsonData.employeeName;
                        var minimum = parseInt($.trim(jsonData.minimum));
                        var maximum = parseInt($.trim(jsonData.maximum));
                        var actual = parseInt($.trim(jsonData.actual));
                        data.addRows([[employeeName, minimum, maximum, actual]]);
                    });
                    var options = {
                        title: 'Monthly Sales',
                        vAxis: {title: 'Sales Target'},
                        hAxis: {title: 'Employees'},
                        isStacked: true,
                        seriesType: 'bars',
                        series: {2: {type: 'line'}}
                    };
                    var chart = new google.visualization.ComboChart(document.getElementById('commission-chart'));
                    chart.draw(data, options);
                }
            });
        }
        @endif
    </script>
@endsection

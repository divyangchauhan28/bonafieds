@extends('layouts.admin')
@section('content')
    <section id="content">
        <div class="page page-sidebar-sm-layout">
            <div class="pageheader">
                <h2>{{__('auth.common.message')}}<span> {{__('auth.common.types')}}</span></h2>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{url('/admin')}}"><i class="fa fa-home"></i> {{__('auth.common.home')}}</a>
                        </li>
                        <li>
                            <a href="javascript:;">{{__('auth.common.messageTypes')}}</a></li>
                        </li>
                    </ul>
                    <div class="page-toolbar">
                        <a href="javascript:ManageMessage();" class="btn no-border btn-primary"><i class="fa fa-plus"></i> {{__('auth.common.message')}}</a></a>
                    </div>
                </div>
            </div>
            <div class="row">
                @if(Session::has('message'))
                    <div class="col-md-12">
                        <div class="alert alert-{{ Session::get('level')}}">
                            <strong>{!! (Session::get('level') == 'success')? 'Success!':'Error!' !!}</strong> {!! Session::get('message')!!}
                        </div>
                    </div>
                @endif
                <div class="col-md-12">
                    <section class="tile tile-simple">
                        <div class="tile-header dvd dvd-btm">
                            <h1 class="custom-font"><strong>{{__('auth.common.message')}}</strong> {{__('auth.common.types')}}</h1>
                        </div>
                        <div class="tile-body">
                            <table class="table table-bordered" id="all-messages">
                                <thead>
                                <tr>
                                    <th>{{__('auth.common.number')}}</th>
                                    <th>{{__('auth.common.type')}}</th>
                                    <th>{{__('auth.common.action')}}</th>
                                    <th>{{__('auth.messages.subject')}}</th>
                                    <th class="text-center">{{__('auth.common.status')}}</th>
                                    <th class="text-center">{{__('auth.common.delete')}}</th>
                                    <th class="text-center">{{__('auth.common.action')}}</th>
                                </tr>
                                </thead>
                                @if(count($messages) > 0)
                                    <tbody>
                                    @foreach($messages as $record)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$record->message_type}}</td>
                                            <td>{{$record->action_id == 0 ? $record->message_subject : $record->message_action}}</td>
                                            <td>{{$record->action_id == 0 ? '' : $record->message_subject}}</td>
                                            <td class="text-center">
                                                @if ($record->is_active)
                                                    <a href="javascript:allUniversalEnum('MessageInactive', '{{$record->message_id}}');" class='label bg-greensea'>{{__('auth.common.active')}}</a>
                                                @else
                                                    <a href="javascript:allUniversalEnum('MessageActive', '{{$record->message_id}}');" class='label bg-warning'>{{__('auth.common.inactive')}}</a>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if ($record->is_deleted)
                                                    <a href="javascript:allUniversalEnum('MessageUndelete', '{{$record->message_id}}');" class='label bg-danger'>{{__('auth.common.yes')}}</a>
                                                @else
                                                    <span class='label bg-greensea'>{{__('auth.common.no')}}</span>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                <a href="javascript:ManageMessage('{{$record->message_id}}');" class="btn btn-xs btn-greensea btn-rounded"><i class="fa fa-pencil"></i></a>
                                                <a href="javascript:allUniversalEnum('MessageDelete', '{{$record->message_id}}');" class="btn btn-xs btn-danger btn-rounded"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                @endif
                            </table>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#all-messages').DataTable();
        });

        function ManageMessage(mId = null) {
            $.ajax({
                url: "{{ route('ajax.manage.message') }}",
                method: "get",
                data: {mId: mId},
                dataType: "json",
                success: function (response) {
                    $("#modal_popup .modal-content").html(response.body);
                    $('#modal_popup').modal('show');
                }
            });
        }
    </script>
@endsection

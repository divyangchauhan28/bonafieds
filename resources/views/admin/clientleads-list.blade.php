<?php
// echo "<pre>";
// print_r($clientLeads[0]->step1['clientname']);
// exit;
?>
@extends('layouts.admin-metronic')
@section('breadcrums')
    <div class="page-title d-flex flex-column me-5">
        <!--begin::Title-->
        <h1 class="d-flex flex-column text-dark fw-bolder fs-3 mb-0">{{__('auth.common.insurance')}} {{__('auth.leads.leads')}}</h1>
        <!--end::Title-->
        <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 pt-1">
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('admin.dashboard')}}" class="text-muted text-hover-primary">{{__('auth.common.home')}}</a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-dark">{{__('auth.common.insurance')}} {{__('auth.leads.leads')}}</li>
            <!--end::Item-->
        </ul>
    </div>
@endsection
@section('content')
    @if(Session::has('message'))
        <div class="col-md-12">
            <div class="alert alert-{{ Session::get('level')}}">
                <strong>{!! (Session::get('level') == 'success')? 'Success!':'Error!' !!}</strong> {!! Session::get('message')!!}
            </div>
        </div>
    @endif
    <div id="kt_content_container" class="container-xxl">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header border-0 pt-6">
                        <div class="card-title">
                            <h2>{{__('auth.common.insurance')}} {{__('auth.leads.leads')}}</h2>
                        </div>
                        <a href="{{ route('create.clientleads') }}" class="btn btn-primary align-self-center">{{__('auth.leads.create')}} {{__('auth.leads.lead')}}</a>
                    </div>
                    <div class="card-body pt-0">
                        <div class="table-responsive">
                            <table class="table table-row-dashed table-row-gray-300 gy-5 gs-7 rounded" id="all-clientleads">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Client Name</th>
                                    <th class="text-center">Date</th>
                                    <th class="text-center">{{__('auth.common.action')}}</th>
                                </tr>
                                </thead>
                                @if(count($clientLeads) > 0)
                                    <tbody>
                                    @foreach($clientLeads as $record)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$record->step1['clientname']}}</td>
                                            <td class="text-center">{{$record->step1['date']}}</td>
                                            <td class="text-center">
                                                <a href="{{ url('admin/generatepdf/'.$record->id) }}" class='text-success'>Genrate PDF</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#all-clientleads').DataTable();
        });

        function ManageLead(lId = null) {
            $.ajax({
                url: "{{ route('ajax.manage.lead') }}",
                method: "get",
                data: {lId: lId},
                dataType: "json",
                success: function (response) {
                    $("#modal_popup .modal-content").html(response.body);
                    $('#modal_popup').modal('show');
                }
            });
        }

        function LeadMapping(lId = null) {
            $.ajax({
                url: "{{ route('ajax.lead.mapping') }}",
                method: "get",
                data: {lId: lId},
                dataType: "json",
                success: function (response) {
                    $("#modal_popup .modal-dialog").addClass('modal-lg');
                    $("#modal_popup .modal-content").html(response.body);
                    $('#modal_popup').modal('show');
                }
            });
        }
    </script>
@endsection

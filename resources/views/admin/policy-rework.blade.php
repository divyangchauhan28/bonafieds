@extends('layouts.admin-metronic')
@section('breadcrums')
    <div class="page-title d-flex flex-column me-5">
        <!--begin::Title-->
        <h1 class="d-flex flex-column text-dark fw-bolder fs-3 mb-0">{{__('auth.common.statusOfRequests')}}</h1>
        <!--end::Title-->
        <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 pt-1">
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('admin.dashboard')}}" class="text-muted text-hover-primary">{{__('auth.common.home')}}</a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-dark">{{__('auth.common.statusOfRequests')}}</li>
            <!--end::Item-->
        </ul>
    </div>
@endsection
@section('content')
    @if(Session::has('message'))
        <div class="col-md-12">
            <div class="alert alert-{{ Session::get('level')}}">
                <strong>{!! (Session::get('level') == 'success')? 'Success!':'Error!' !!}</strong> {!! Session::get('message')!!}
            </div>
        </div>
    @endif
    <div id="kt_content_container" class="container-xxl">
        <div class="row">
            <form class="ajax-form" id="FilterPolicy" data-reload-form="false" action="{{route('policy.rework')}}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-4 mb-10">
                        <div class="form-group">
                            {!! Form::select('customerId', $customers,$customerId,['class' => 'form-control','id'=>'customerId','placeholder'=>'Select Customer']) !!}
                        </div>
                    </div>
                    @if(Auth::user()->is_admin)
                        <div class="col-md-4 mb-10">
                            <div class="form-group">
                                {!! Form::select('employeeId', $employees,$employeeId,['class' => 'form-control','id'=>'employeeId','placeholder'=>'Select Employee']) !!}
                            </div>
                        </div>
                    @endif
                    <div class="col-md-1">
                        <button type="submit" class="btn btn-primary small"><i class="fa fa-filter"></i></button>
                    </div>
                </div>
            </form>
            <div class="clearfix"></div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header border-0 pt-6">
                        <div class="card-title">
                            <h2>{{__('auth.common.statusOfRequests')}}</h2>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <div class="table-responsive">
                            <table class="table table-row-dashed table-row-gray-300 gy-5 gs-7 rounded" id="all-policies">
                                <thead>
                                <tr>
                                    <th>{{__('auth.common.number')}}</th>
                                    <th>{{__('auth.customer.customerName')}}</th>
                                    <th>{{__('auth.common.insuranceProviders')}}</th>
                                    <th>{{__('auth.common.insuranceTypes')}}</th>
                                    <th class="text-center">{{__('auth.types.amount')}}</th>
                                    <th class="text-center">{{__('auth.types.duration')}}</th>
                                    <th class="text-center">{{__('auth.customer.paymentMode')}}</th>
                                    <th class="text-center">{{__('auth.customer.policyStatus')}}</th>
                                    <th class="text-center">{{__('auth.common.status')}}</th>
                                    <th class="text-center">{{__('auth.common.action')}}</th>
                                    <!-- @if(Auth::user()->is_admin)
                                        <th class="text-center">{{__('auth.common.action')}}</th>
                                    @endif -->
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($policies) > 0)
                                    @foreach($policies as $record)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$record->salutation." ".$record->firstname." ".$record->lastname}}</td>
                                            <td>{{$record->insProvider}}</td>
                                            <td>{{$record->insType}}</td>
                                            <td class="text-center">{{$record->insurance_amount." CHF"}}</td>
                                            <td class="text-center">{{$record->insurance_duration." ".__('auth.common.years')}}</td>
                                            <td class="text-center">
                                                @if($record->installment_mode == 'Daily')
                                                    <span class='text-success'>{{ __('auth.common.daily')}}</span>
                                                @elseif($record->installment_mode == 'Fortnightly')
                                                    <span class='text-danger'>{{__('auth.common.fortnightly')}}</span>
                                                @elseif($record->installment_mode == 'Monthly')
                                                    <span class='text-danger'>{{__('auth.common.monthly')}}</span>
                                                @elseif($record->installment_mode == 'Quarterly')
                                                    <span class='text-danger'>{{__('auth.common.quarterly')}}</span>
                                                @elseif($record->installment_mode == 'Half Yearly')
                                                    <span class='text-danger'>{{__('auth.common.halfYearly')}}</span>
                                                @elseif($record->installment_mode == 'Yearly')
                                                    <span class='text-danger'>{{__('auth.common.yearly')}}</span>
                                                @else
                                                    <span class='text-warning'>{{__('auth.common.oneTime')}}</span>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if(Auth::user()->is_employee)
                                                    @if($record->insurance_status == 'Approve')
                                                        <span class='text-success'>{{ __('auth.documents.approved')}}</span>
                                                    @elseif($record->insurance_status == 'Reject')
                                                        <span class='text-danger'>{{__('auth.documents.rejected')}}</span>
                                                    @else
                                                        <span class='text-warning'>{{__('auth.common.inprogress')}}</span>
                                                    @endif
                                                @else
                                                    @if($record->insurance_status == 'In Progress')
                                                        <a href="javascript:allUniversalEnum('PolicyApprove', '{{$record->ci_id}}');" class='text-success'>{{__('auth.common.approve')}}</a>
                                                        <a href="javascript:allUniversalEnum('PolicyReject', '{{$record->ci_id}}');" class='text-danger'>{{__('auth.common.reject')}}</a>
                                                    @elseif($record->insurance_status == 'Approve')
                                                        <span class='text-success'>{{ __('auth.documents.approved')}}</span>
                                                    @else
                                                        <span class='text-danger'>{{  __('auth.documents.rejected')}}</span>
                                                    @endif
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if(Auth::user()->is_employee)
                                                    @if ($record->is_active)
                                                        <span class='text-success'>{{__('auth.common.active')}}</span>
                                                    @else
                                                        <span class='text-warning'>{{__('auth.common.inactive')}}</span>
                                                    @endif
                                                @else
                                                    @if ($record->is_active)
                                                        <a href="javascript:allUniversalEnum('PolicyInactive', '{{$record->ci_id}}');" class='text-success'>{{__('auth.common.active')}}</a>
                                                    @else
                                                        <a href="javascript:allUniversalEnum('PolicyActive', '{{$record->ci_id}}');" class='text-warning'>{{__('auth.common.inactive')}}</a>
                                                    @endif
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                <a href="javascript:policyDocuments('{{$record->ci_id}}');" class="btn-icon-warning"><i class="fas fa-upload"></i></a>
                                                @if(Auth::user()->is_admin)
                                                    <a href="javascript:allUniversalEnum('PolicyDelete', '{{$record->ci_id}}');" class="text-danger"><i class="fas fa-trash"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#all-policies').DataTable();
        });

        function policyDocuments(pId) {
            $.ajax({
                url: "{{ route('ajax.policy.documents') }}",
                method: "get",
                data: {pId: pId},
                dataType: "json",
                success: function (response) {
                    $("#modal_popup .modal-dialog").addClass('modal-lg');
                    $("#modal_popup .modal-content").html(response.body);
                    $('#modal_popup').modal('show');
                }
            });
        }
    </script>
@endsection

@extends('layouts.admin')
@section('content')
    <section id="content">
        <div class="page page-sidebar-sm-layout">
            <div class="pageheader">
                <h2>CRM<span> {{__('auth.common.modules')}}</span></h2>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{url('/admin')}}"><i class="fa fa-home"></i> {{__('auth.common.home')}}</a>
                        </li>
                        <li>
                            <a href="javascript:;">CRM {{__('auth.common.modules')}}</a></li>
                        </li>
                    </ul>
                    <div class="page-toolbar">
                        <a href="javascript:ManageModules();" class="btn no-border btn-primary"><i class="fa fa-plus"></i> {{__('auth.common.module')}}</a></a>
                    </div>
                </div>
            </div>
            <div class="row">
                @if(Session::has('message'))
                    <div class="col-md-12">
                        <div class="alert alert-{{ Session::get('level')}}">
                            <strong>{!! (Session::get('level') == 'success')? 'Success!':'Error!' !!}</strong> {!! Session::get('message')!!}
                        </div>
                    </div>
                @endif
                <div class="col-md-12">
                    <section class="tile tile-simple">
                        <div class="tile-header dvd dvd-btm">
                            <h1 class="custom-font"><strong>{{__('auth.common.module')}}</strong> {{__('auth.common.list')}}</h1>
                        </div>
                        <div class="tile-body">
                            <table class="table table-bordered" id="all-modules">
                                <thead>
                                <tr>
                                    <th>{{__('auth.common.number')}}</th>
                                    <th>{{__('auth.commission.title')}}</th>
                                    <th>{{__('auth.modules.dentifier')}}</th>
                                    <th class="text-center">{{__('auth.common.action')}}</th>
                                </tr>
                                </thead>
                                @if(count($modules) > 0)
                                    <tbody>
                                    @foreach($modules as $record)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$record->module_title}}</td>
                                            <td>{{$record->module_identifier}}</td>
                                            <td class="text-center">
                                                <a href="javascript:ManageModules('{{$record->module_id}}');" class="btn btn-xs btn-greensea btn-rounded"><i class="fa fa-pencil"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                @endif
                            </table>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#all-modules').DataTable();
        });

        function ManageModules(mId = null) {
            $.ajax({
                url: "{{ route('ajax.manage.module') }}",
                method: "get",
                data: {mId: mId},
                dataType: "json",
                success: function (response) {
                    $("#modal_popup .modal-content").html(response.body);
                    $('#modal_popup').modal('show');
                }
            });
        }

    </script>
@endsection

@extends('layouts.admin-metronic')
@section('breadcrums')
<div class="page-title d-flex flex-column me-5">
    <!--begin::Title-->
    <h1 class="d-flex flex-column text-dark fw-bolder fs-3 mb-0">{{__('auth.common.pendingDocument')}} {{__('auth.common.list')}}</h1>
    <!--end::Title-->
    <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 pt-1">
        <!--begin::Item-->
        <li class="breadcrumb-item text-muted">
            <a href="{{route('admin.dashboard')}}" class="text-muted text-hover-primary">{{__('auth.common.home')}}</a>
        </li>
        <!--end::Item-->
        <!--begin::Item-->
        <li class="breadcrumb-item">
            <span class="bullet bg-gray-200 w-5px h-2px"></span>
        </li>
        <!--end::Item-->
        <!--begin::Item-->
        <li class="breadcrumb-item text-dark">{{__('auth.common.pendingDocument')}} {{__('auth.common.list')}}</li>
        <!--end::Item-->
    </ul>
</div>
@endsection
@section('content')
@if(Session::has('message'))
<div class="col-md-12">
    <div class="alert alert-{{ Session::get('level')}}">
        <strong>{!! (Session::get('level') == 'success')? 'Success!':'Error!' !!}</strong> {!! Session::get('message')!!}
    </div>
</div>
@endif
<div id="kt_content_container" class="container-xxl">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header border-0 pt-6">
                    <div class="card-title">
                        <h2>{{__('auth.common.pendingDocument')}} {{__('auth.common.list')}}</h2>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <div class="table-responsive">
                        <table class="table table-row-dashed table-row-gray-300 gy-5 gs-7 rounded" id="all-policies">
                            <thead>
                                <tr>
                                    <th>{{__('auth.common.number')}}</th>
                                    <th>{{__('auth.customer.customerName')}}</th>
                                    <th>{{__('auth.common.insuranceProviders')}}</th>
                                    <th>{{__('auth.common.insuranceTypes')}}</th>
                                    <th class="text-center">{{__('auth.types.amount')}}</th>
                                    <th class="text-center">{{__('auth.types.duration')}}</th>
                                    <th class="text-center">{{__('auth.customer.paymentMode')}}</th>
                                    <th class="text-center">{{__('auth.common.documents')}}</th>
                                    <th class="text-center">{{__('auth.customer.policyStatus')}}</th>
                                    <th class="text-center">{{__('auth.common.action')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($policies) > 0)
                                @foreach($policies as $record)
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td>{{$record->salutation." ".$record->firstname." ".$record->lastname}}</td>
                                    <td>{{$record->insProvider}}</td>
                                    <td>{{$record->insType}}</td>
                                    <td class="text-center">{{$record->insurance_amount." CHF"}}</td>
                                    <td class="text-center">{{$record->insurance_duration." Years"}}</td>
                                    <td class="text-center">{{$record->installment_mode}}</td>
                                    <td class="text-center">
                                        @if($record->totalDocs == 0)
                                        <span class="text-warning">{{$record->completeDocs." / ".$record->totalDocs}}</span>
                                        @else
                                        @if($record->totalDocs > $record->completeDocs)
                                        <span class="text-danger">{{$record->completeDocs." / ".$record->totalDocs}}</span>
                                        @else
                                        <span class="text-greensea">{{$record->completeDocs." / ".$record->totalDocs}}</span>
                                        @endif
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        @if ($record->is_active)
                                        <a href="javascript:allUniversalEnum('PolicyInactive', '{{$record->ci_id}}');" class='label bg-greensea'>{{__('auth.common.active')}}</a>
                                        @else
                                        <a href="javascript:allUniversalEnum('PolicyActive', '{{$record->ci_id}}');" class='label bg-warning'>{{__('auth.common.inactive')}}</a>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        <a href="javascript:policyDocuments('{{$record->ci_id}}');" class="btn-icon-warning"><i class="fas fa-upload"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript">
    $(document).ready(function() {
        $('#all-policies').DataTable();
    });

    function policyDocuments(pId) {
        $.ajax({
            url: "{{ route('ajax.policy.documents') }}",
            method: "get",
            data: {
                pId: pId
            },
            dataType: "json",
            success: function(response) {
                $("#modal_popup .modal-dialog").addClass('modal-lg');
                $("#modal_popup .modal-content").html(response.body);
                $('#modal_popup').modal('show');
            }
        });
    }
</script>
@endsection
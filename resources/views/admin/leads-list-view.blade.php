@extends('layouts.admin-metronic')
@section('breadcrums')
    <div class="page-title d-flex flex-column me-5">
        <!--begin::Title-->
        <h1 class="d-flex flex-column text-dark fw-bolder fs-3 mb-0">{{__('auth.common.insurance')}} Leads</h1>
        <!--end::Title-->
        <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 pt-1">
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('admin.dashboard')}}" class="text-muted text-hover-primary">{{__('auth.common.home')}}</a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-dark">{{__('auth.common.insurance')}} Leads</li>
            <!--end::Item-->
        </ul>
    </div>
@endsection
@section('content')
    @if(Session::has('message'))
        <div class="col-md-12">
            <div class="alert alert-{{ Session::get('level')}}">
                <strong>{!! (Session::get('level') == 'success')? 'Success!':'Error!' !!}</strong> {!! Session::get('message')!!}
            </div>
        </div>
    @endif
    @if($errors->any())
    <div class="col-md-12">
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
     
    <ul class="p-0 m-0" style="list-style: none;">
        @foreach($errors->all() as $error)
        <li>{{$error}}</li>
        @endforeach
    </ul>    
</div>
</div>
@endif
    <div id="kt_content_container" class="container-xxl">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header border-0 pt-6">
                        <div class="card-title">
                            <h2>{{__('auth.common.insurance')}} Leads</h2>
                        </div>
                        @if(Auth::user()->is_admin)
                            <a href="javascript:ManageLead();" class="btn btn-primary align-self-center">Lead</a>
                        @endif
                    </div>
                    <div class="card-body pt-0">
                        <div class="table-responsive">
                            <table class="table table-row-dashed table-row-gray-300 gy-5 gs-7 rounded" id="all-leads">
                                <thead>
                                <tr>
                                    <th>{{__('auth.common.number')}}</th>
                                    <th>{{__('auth.common.nachname')}}</th>
                                    <th>{{__('auth.common.vorname')}}</th>
                                    <th>{{__('auth.common.plz')}}</th>
                                    <th class="text-center">{{__('auth.common.strasse')}}</th>
                                    <th class="text-center">{{__('auth.common.land')}}</th>
                                    <th class="text-center">{{__('auth.common.ortschaft')}}</th>
                                    <th class="text-center">{{__('auth.common.haus_nummer')}}</th>
                                    <th class="text-center">{{__('auth.common.bemerkungen')}}</th>
                                    <th class="text-center">{{__('auth.common.empfehlungsgeber')}}</th>
                                    <th class="text-center">{{__('auth.common.a_kunde')}}</th>
                                    <th class="text-center">{{__('auth.common.b_kunde')}}</th>
                                    <th class="text-center">{{__('auth.common.c_kunde')}}</th>
                                    <th class="text-center">{{__('auth.common.erreicht')}}</th>
                                    <th class="text-center">{{__('auth.common.termin')}}</th>
                                    <th class="text-center">{{__('auth.common.abschluss')}}</th>
                                    <th class="text-center">{{__('auth.common.nummer')}}</th>
                                    <th class="text-center">{{__('auth.common.status')}}</th>
                                    <th class="text-center">{{__('auth.common.reason')}}</th>
                                    <th class="text-center">{{__('auth.common.action')}}</th>
                                </tr>
                                </thead>
                                @if(count($leadCustomer) > 0)
                                    <tbody>
                                    @foreach($leadCustomer as $record)
                                        <tr>
                                            <td>{{$record->id}}</td>
                                            <td>{{$record->nachname}}</td>
                                            <td>{{$record->vorname}}</td>
                                            <td>{{$record->plz}}</td>
                                            <td class="text-center">{{$record->strasse}}</td>
                                            <td class="text-center">{{$record->land}}</td>
                                            <td class="text-center">{{$record->ortschaft}}</td>
                                            <td class="text-center">{{$record->haus_nummer}}</td>
                                            <td class="text-center">{{$record->bemerkungen}}</td>
                                            <td class="text-center">{{$record->empfehlungsgeber}}</td>
                                            <td class="text-center">{{$record->a_kunde}}</td>
                                            <td class="text-center">{{$record->b_kunde}}</td>
                                            <td class="text-center">{{$record->c_kunde}}</td>
                                            <td class="text-center">{{$record->erreicht}}</td>
                                            <td class="text-center">{{$record->termin}}</td>
                                            <td class="text-center">{{$record->abschluss}}</td>
                                            <td class="text-center">{{$record->nummer}}</td>
                                            <td class="text-center">{{$record->status}}</td>
                                            <td class="text-center">{{$record->reason}}</td>
                                            <td class="text-center">
                                                @if($record->lead_call_id && $record->lead_call_id===Auth::user()->user_id && $record->reason)
                                                <a href="javascript:LeadCall('{{$record->id}}');" class="menu-link px-3">Update Comment</a>
                                                @elseif($record->lead_call_id && $record->lead_call_id===Auth::user()->user_id)
                                                <a href="javascript:LeadCall('{{$record->id}}');" class="menu-link px-3">Call</a>
                                                @elseif($record->lead_call_id)
                                                <a href="#" class="menu-link px-3">Already Accepted</a>
                                                @else
                                                <a href="{{route('leads.accept',[$record->id])}}" class="btn btn-xs btn-warning btn-rounded">Accept</a>
                                                @endif
                                            </td>
                                            
                                        </tr>
                                    @endforeach
                                    </tbody>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#all-leads').DataTable();
        });
        function LeadCall(lId = null) {
            $.ajax({
                url: "{{ route('ajax.lead.call') }}",
                method: "get",
                data: {lId: lId},
                dataType: "json",
                success: function (response) {
                    $("#modal_popup .modal-dialog").addClass('modal-lg');
                    $("#modal_popup .modal-content").html(response.body);
                    $('#modal_popup').modal('show');
                }
            });
        }
    </script>
@endsection
